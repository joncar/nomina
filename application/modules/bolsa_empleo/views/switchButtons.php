<div class="row">
	<div class="col-12 col-md-6">
		<a href="<?= base_url('bolsa_empleo/bolsaEmpleo/convocatorias') ?>">
			<div class="card">
				<div class="card-body">
					Ver Convocatorias
				</div>
			</div>
		</a>
	</div>
	<div class="col-12 col-md-6">
		<a href="<?= base_url('bolsa_empleo/bolsaEmpleo/solicitudes_empleo') ?>">
			<div class="card">
				<div class="card-body">
					Registrarse en la bolsa
				</div>
			</div>
		</a>
	</div>
</div>