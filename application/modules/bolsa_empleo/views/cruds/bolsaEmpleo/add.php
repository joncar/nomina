<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';
?>
<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $insert_url, 'method="post" onsubmit="save(this); return false;" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Añadir <?php echo $subject?>
                </h3>
                <p>
					Se esta registrando en la bolsa de empleo de la FUNDACION CLINCA NELSON MANDELA, al estar registrado tendras la posibilidad de ser contactado por la  FCNM para participar en los diversos procesos de selección que la institucion requiera segun sus necesidades.
				</p>
				<p>&nbsp;</p>
            </div>
        </div>

        <!--begin::Form-->        
        <div class="kt-portlet__body">
            

            

            <nav>
			  <div class="nav nav-tabs" id="nav-tab" role="tablist">
			    <a class="nav-link active" id="nav-datos-tab" data-toggle="tab" href="#nav-datos" role="tab" aria-controls="nav-datos" aria-selected="true">Información Personal</a>
			    <a class="nav-link" id="nav-direcciones-tab" data-toggle="tab" href="#nav-direcciones" role="tab" aria-controls="nav-direcciones" aria-selected="false">Direcciones</a>			    
			    <a class="nav-link" id="nav-contacto-tab" data-toggle="tab" href="#nav-contacto" role="tab" aria-controls="nav-contacto" aria-selected="false">Datos de Contacto</a>
			    <a class="nav-link" id="nav-academicos-tab" data-toggle="tab" href="#nav-academicos" role="tab" aria-controls="nav-academicos" aria-selected="false">Datos Académicos</a>
			    <a class="nav-link" id="nav-generales-tab" data-toggle="tab" href="#nav-generales" role="tab" aria-controls="nav-generales" aria-selected="false">Datos Generales</a>
			    <!--<a class="nav-link" href="<?= base_url() ?>bolsa_empleo/bolsaEmpleo/convocatorias">Ver Todas las Convocatorias</a>-->
			  </div>
			</nav>
			<div class="tab-content" id="nav-tabContent">
			  
			  <div class="tab-pane fade show active" id="nav-datos" role="tabpanel" aria-labelledby="nav-datos-tab">
			  	<div class="kt-section kt-section--first">
	            	<div class="row">
	            		<?php foreach($fields as $field): if(strpos($input_fields[$field->field_name]->input,'departamentos_id'))break; ?>
		                	<div class="col-12 col-md-4">
			                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
			                        <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
			                        <?php echo $input_fields[$field->field_name]->input ?>                            
			                    </div>
		                    </div>
		                <?php endforeach ?>
	                </div>
	                <div class="kt-portlet__foot">
	                	<div class="row">
	                		<div class="col-12 col-md-6">
	                			<a href="<?= base_url('empleos') ?>" class="btn btn-info">Volver</a>
	                		</div>
	                		<div class="col-12 col-md-6">
	                			<div class="kt-form__actions text-right mr-20">
						            <button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-direcciones-tab')[0].click()" class="btn btn-success">Siguiente</button>
						        </div>
	                		</div>
	                	</div>
				        
				    </div>
	            </div>
			  </div>
			  <div class="tab-pane fade" id="nav-direcciones" role="tabpanel" aria-labelledby="nav-caracterizacion-tab">
			  	<div class="kt-section kt-section--first">
			  		<div class="row">
		            	<?php 
					  		$n = 0;
					  		foreach($fields as $field): 
					  			if(strpos($input_fields[$field->field_name]->input,'celular1'))break;
					  			if(strpos($input_fields[$field->field_name]->input,'departamentos_id'))$n = 1;
					  		?>
		                	<?php if($n==1): ?>
			                	<div class="col-12 col-md-4">
				                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
				                        <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>				                        
				                        <?php echo $input_fields[$field->field_name]->input ?>                            
				                    </div>
			                    </div>
		                	<?php endif ?>
		                <?php endforeach ?> 
	            	</div>
		        </div>
		        <div class="kt-portlet__foot">
			        <div class="kt-form__actions text-right mr-20">
			        	<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-datos-tab')[0].click()" class="btn btn-success">Atras</button>
			            <button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-contacto-tab')[0].click()" class="btn btn-success">Siguiente</button>
			        </div>
			    </div>
			  </div>

			  <div class="tab-pane fade" id="nav-contacto" role="tabpanel" aria-labelledby="nav-caracterizacion-tab">
			  	<div class="kt-section kt-section--first">
			  		<div class="row">
		            	<?php 
					  		$n = 0;
					  		foreach($fields as $field):		
					  			if(strpos($input_fields[$field->field_name]->input,'categorias_trabajos_id'))break;			  			
					  			if(strpos($input_fields[$field->field_name]->input,'celular1'))$n = 1;
					  		?>
		                	<?php if($n==1): ?>
			                	<div class="col-12 col-md-4">
				                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
				                        <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>				                        
				                        <?php echo $input_fields[$field->field_name]->input ?>                            
				                    </div>
			                    </div>
		                	<?php endif ?>
		                <?php endforeach ?> 
	            	</div>
		        </div>
		        <div class="kt-portlet__foot">
			        <div class="kt-form__actions text-right mr-20">
			        	<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-direcciones-tab')[0].click()" class="btn btn-success">Atras</button>
			            <button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-academicos-tab')[0].click()" class="btn btn-success">Siguiente</button>
			        </div>
			    </div>
			  </div>

			  <div class="tab-pane fade" id="nav-academicos" role="tabpanel" aria-labelledby="nav-caracterizacion-tab">
			  	<div class="kt-section kt-section--first">
			  		<div class="row">
			  			<div class="col-12">
			            	<?php 
						       $crud = get_instance()->solicitudes_empleo_estudios(2);
						       get_instance()->load->view('predesign/body_table',['crud'=>$crud]); 
						    ?> 
					    </div>
	            	</div>
		        </div>
		        <div class="kt-portlet__foot">
			        <div class="kt-form__actions text-right mr-20">
			        	<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-contacto-tab')[0].click()" class="btn btn-success">Atras</button>
			            <button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-generales-tab')[0].click()" class="btn btn-success">Siguiente</button>
			        </div>
			    </div>
			  </div>

			  <div class="tab-pane fade" id="nav-generales" role="tabpanel" aria-labelledby="nav-caracterizacion-tab">
			  	<div class="kt-section kt-section--first">
			  		<div class="row">
		            	<?php 
					  		$n = 0;
					  		foreach($fields as $field):					  			
					  			if(strpos($input_fields[$field->field_name]->input,'categorias_trabajos_id'))$n = 1;
					  		?>
		                	<?php if($n==1): ?>
			                	<div class="col-12 col-md-<?= strpos($input_fields[$field->field_name]->input,'checkbox') || strpos($input_fields[$field->field_name]->input,'radio')?12:4 ?>">
				                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
				                        <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>				                        
				                        <?php echo $input_fields[$field->field_name]->input ?>                            
				                    </div>
			                    </div>
		                	<?php endif ?>
		                <?php endforeach ?> 
	            	</div>
		        </div>
		        <div class="kt-portlet__foot">
			        <div class="kt-form__actions text-right mr-20">
			        	<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-academicos-tab')[0].click()" class="btn btn-success">Atras</button>
			            <button type='submit' class="btn btn-success">Guardar</button>                    
			        </div>
			    </div>
			  </div>
			  


			

            




            <!-- Start of hidden inputs -->
            <?php
                foreach($hidden_fields as $hidden_field){
                    echo $hidden_field->input;
                }
            ?>
            <?php if ($is_ajax) { ?>
                <input type="hidden" name="is_ajax" value="true" />
            <?php }?>
            <!-- End of hidden inputs -->
        </div>    

        <!--end::Form-->
    </div>    
    <div class="report"></div>     
    <!--end::Form-->
    <?php echo form_close(); ?>
</div>

<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "Almacenado correctamente";
	var message_insert_error = "Error al insertar";
</script>

<?php get_instance()->hcss[] = "
	<style>
		.kt-section{
			margin:20px 0;
		}
		.chzn-drop,.chzn-search,.chzn-results,.chzn-search input{
			min-width:100% !important;
		}
		[type='radio']:not(:checked), [type='radio']:checked {
		    position: initial;
		    left: initial;
		    opacity: 1;
		    margin-right:5px;
		}
		@media screen and (max-width:480px){
			.nav {
			    flex-wrap: wrap;
			}
		}
	</style>
"; ?>