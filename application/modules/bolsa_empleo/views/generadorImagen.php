<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Generar imagen de ofertas de empleo
            </h3>
			<p>&nbsp;</p>
        </div>
    </div>
    <form action="bolsa_empleo/admin/generarImagen/1" method="post" onsubmit=" generarImagen(this); return false;">
	    <div class="kt-portlet__body">
	        <div class="row">
	        	<div class="col-12 col-md-6">
	        		<div class="row">
	        			<div class="col-12 col-md-6">
			        		<div class="form-group" id="cargo_field_box">
		                        <div>
		                        	<label for='field-cargo' id="cargo_display_as_box">
		                        		Cargo:
		                        	</label>
		                        </div>
		                        <input type="text" name="cargo" class="form-control" id="field-cargo">
		                    </div>
	                    </div>
	                    <div class="col-12 col-md-6">
		                    <div class="form-group" id="tipo_contrato_field_box">
		                        <div>
		                        	<label for='field-tipo_contrato' id="tipo_contrato_display_as_box">
		                        		Tipo de Contrato:
		                        	</label>
		                        </div>
		                        <input type="text" name="tipo_contrato" class="form-control" id="field-tipo_contrato">
		                    </div>
	                	</div>
	                	<div class="col-12 col-md-6">
		                    <div class="form-group" id="horario_field_box">
		                        <div>
		                        	<label for='field-horario' id="horario_display_as_box">
		                        		Horario:
		                        	</label>
		                        </div>
		                        <input type="text" name="horario" class="form-control" id="field-horario">
		                    </div>
	                    </div>
	                    <div class="col-12 col-md-6">
		                    <div class="form-group" id="ciudad_field_box">
		                        <div>
		                        	<label for='field-ciudad' id="ciudad_display_as_box">
		                        		Ciudad:
		                        	</label>
		                        </div>
		                        <input type="text" name="ciudad" class="form-control" id="field-ciudad">
		                    </div>
	                    </div>
	                    <div class="col-12 col-md-6">
		                    <div class="form-group" id="funciones_field_box">
		                        <div>
		                        	<label for='field-funciones' id="funciones_display_as_box">
		                        		Funciones:
		                        	</label>
		                        </div>
		                        <input type="text" name="funciones" class="form-control" id="field-funciones">
		                    </div>
	                    </div>
	                    <div class="col-12 col-md-6">
		                    <div class="form-group" id="salario_field_box">
		                        <div>
		                        	<label for='field-salario' id="salario_display_as_box">
		                        		Salario:
		                        	</label>
		                        </div>
		                        <input type="text" name="salario" class="form-control" id="field-salario">
		                    </div>
	                    </div>
	                    <div class="col-12 col-md-6">
		                    <div class="form-group" id="postulacion_field_box">
		                        <div>
		                        	<label for='field-postulacion' id="postulacion_display_as_box">
		                        		Postulación:
		                        	</label>
		                        </div>
		                        <input type="text" name="postulacion" class="form-control" id="field-postulacion">
		                    </div>    
	                    </div>
	                    <div class="col-12">
		                    <div class="form-group" id="postulacion_field_box">
		                        <button class="btn btn-info" type="submit">
		                        	Enviar
		                        </button>
		                    </div>
	                    </div>  
                    </div>
	        	</div>
	        	<div class="col-12 col-md-6">
	        		<div class="response"></div>
	        		<a download="concurso.png" href="<?= base_url() ?>img/empleo-blank.jpeg" title="Click Descargar Imagen">
	        			<img src="<?= base_url() ?>img/empleo-blank.jpeg" alt="" style="width:100%" id="imagen">
	        		</a>
	        	</div>
	        </div>
	    </div>
    </form>
</div>

<script>
	function generarImagen(f){
		info('.response','Generando imagen');
		$("#imagen").attr('src','');
		sendForm(f,'',function(data){
			$('.response').html('').removeClass('alert alert-info');
			$("#imagen").attr('src',data);
			console.log(data);
		});
	}
</script>