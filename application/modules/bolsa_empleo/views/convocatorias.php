<?= $output ?>
<div class="row">
	<div class="col-12">
		<a href="<?= base_url('empleos') ?>" class="btn btn-info">Volver</a>
	</div>
</div>
<div class="modal" tabindex="-1" id="pedirCedula">
   <form action="bolsa_empleo/bolsaEmpleo/consultarCedula" onsubmit="conectarDoc(this); return false;">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Indica tu documento</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
        	<div class="row">
        		<div class="col-12 col-md-6">
        			<div class="form-group">
        				<label for="">Tipo de Documento</label>
        				<?= form_dropdown_from_query('tipos_documento_id','tipos_documento','id','iniciales','') ?>
        			</div>
        		</div>
        		<div class="col-12 col-md-6">
        			<div class="form-group">
        				<label for="">#Documento</label>
        				<input type="text" name="documento" value="" class="form-control">
        			</div>
        		</div>
        	</div>
        	<div class="response">
        		
        	</div>
	      </div>
	      <div class="modal-footer text-right">        
	        <button type="submit" class="btn btn-primary">Guardar</button>
	        <button type="submit" class="btn btn-danger">Cancelar</button>
	      </div>
	    </div>
	  </div>
  </form>
</div>
<script>
	var tipos_documento = '<?= !empty($_SESSION['tipos_documento_id']) && 1!=1?$_SESSION['tipos_documento_id']:'' ?>';
	var documento = '<?= !empty($_SESSION['documento']) && 1!=1?$_SESSION['documento']:'' ?>';
	var idConvocatoria = '';
	var tdSeleccionado = '';
	function postularse(id){
		idConvocatoria = id;
		if(tipos_documento=='' || documento == ''){
			$("#pedirCedula").modal('toggle');
		}else{
			for(var i=0; i<$(".flexigrid table tr").length;i++){
			  if($($($(".flexigrid table tr")[i]).find('td')[0]).text().trim()==idConvocatoria){
			    var tr = $($(".flexigrid table tr")[i]).find('td');
					tdSeleccionado = $(tr[tr.length-1]);
					tdSeleccionado.html('Postulandose');
					enviarPostulacion(idConvocatoria);
			  }
			}
		}
	}

	function enviarPostulacion(id){
		$.post('<?= base_url('bolsa_empleo/bolsaEmpleo/postularse') ?>/'+id,{},function(data){
			data = JSON.parse(data);
			if(data.success){
				tdSeleccionado.html('<span class="badge badge-success">'+data.msj+'</span>');
			}else{
				tdSeleccionado.html('<span class="badge badge-danger">'+data.msj+'</span>');
			}
		});
	}

	function conectarDoc(f){
		info('.response','Consultando su documento, por favor espere...');
		sendForm(f,'',function(data){
			data = JSON.parse(data);
			if(data.success){
				$('.response').html('').removeClass('alert alert-info alert-danger');
				tipos_documento = data.tipos_documento;
				documento = data.documento;
				postularse(idConvocatoria);
				$("#pedirCedula").modal('toggle');
			}else{
				if(typeof(data.msj)!='undefined'){
					error('.response',data.msj);
				}else{
					document.location.href = "<?= base_url('bolsa_empleo/bolsaEmpleo/solicitudes_empleo/') ?>/"+idConvocatoria;
				}
			}
		});
	}
</script>	