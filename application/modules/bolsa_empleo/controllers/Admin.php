<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        } 

        function solicitudes_empleo_estudios($act = 2){
            $crud = new ajax_grocery_crud();            
            $crud->set_table('solicitudes_empleo_estudios')
                 ->set_theme('bootstrap')
                 ->set_subject('Estudios')
                 ->field_type('solicitudes_empleos_id','hidden','')
                 ->field_type('anulado','hidden',0)
                 ->replace_form_add('predesign/body_table')
                 ->replace_form_edit('predesign/body_table')
                 ->set_rel('solicitudes_empleos_id');
            if(!is_numeric($act)){
              $act = '';
            }
            $crud = $crud->render($act);
            return $crud;  
        }

        /* Funcion para vincular empresa con aportes en linea*/
        function solicitudes_empleo($x = ''){
            $this->as['solicitudes_empleo'] = 'view_postulaciones';
            $crud = $this->crud_function('','');
            $crud->set_primary_key('id');
            $crud->unset_delete()
                 ->unset_edit()
                 ->unset_add()
                 ->unset_read()
                 ->display_as('convocatorias_id','Convocatoria')
                 ->set_relation('convocatorias_id','convocatorias','id')
                 ->unset_columns('view_postulaciones.id','nro_convocatoria')
                 ->set_field_upload('hoja_vida','hojas_vida')
                 ->callback_column('s13e915d9',function($val,$row){
                    return $row->convocatorias_id;
                 });
            $crud = $crud->render();
            $crud->title = 'Bolsa de Empleo, Clínica Nelson Mandela';
            $crud->crud = 'user';
            $crud->view = 'panel';
            $this->loadView($crud);
        }
        /*function solicitudes_empleo($x = ''){
            $crud = new ajax_grocery_crud();
            $crud->set_table('solicitudes_empleo')
                 ->set_subject('Postulación')
                 ->set_theme('bootstrap')
                 ->set_relation('ciudades_id','ciudades','ciudad')
                 ->set_relation('departamentos_id','departamentos','departamento')
                 ->set_relation('ciudad_nacimiento','ciudades','ciudad')
                 ->set_relation('departamento_nacimiento','departamentos','departamento')                 
                 ->display_as('tipos_documento_id','Tipo de Documento')
                 ->display_as('ciudades_id','Municipio de Residencia')
                 ->display_as('departamentos_id','Departamento de Residencia')
                 ->display_as('ciudades_nacimiento','Municipio de Nacimiento')
                 ->display_as('departamentos_nacimiento','Departamento de Nacimiento')
                 ->display_as('pais_nacimiento','Pais de Nacimiento')
                 ->display_as('anho_finalizacion','Año de Finalización de estudios')
                 ->display_as('categorias_trabajos_id','Categoría del trabajo al que aspira tener')
                 //->display_as('aspiracion_cargo','Cargo del trabajo al que aspira tener')
                 //->display_as('aspiracion_salario','Salario que aspira tener')
                 //->display_as('acepta_registro','Se esta registrando en la bolsa de empleos de la FUNDACION CLINICA NELSON MANDELA, NO quiere decir que este iniciando un proceso de selección. ')
                 //->display_as('acepta_manejo_datos','Autorizacion de manejo de datos personales. Sus datos personales seran tratados según nuestras politicas de tratamiento de datos')
                 //->display_as('acepta_envio_email','Notificacion de oferta de empleo. Enviaremos por correo electronico ofertas de empleos a las cuales puedes aplicar.')
                 ->display_as('hoja_vida','Adjunta tu Hoja de Vida [PDF]')
                 ->required_fields_array()
                 ->set_field_upload('hoja_vida','hojas_vida')
                 ->replace_form_add('cruds/bolsaEmpleo/add.php','bolsa_empleo')
                 ->replace_form_edit('cruds/bolsaEmpleo/edit.php','bolsa_empleo')                 
                 ->field_type('genero','dropdown',['M'=>'Masculino','F'=>'Femenino'])
                 ->field_type('estado_civil','dropdown',['S'=>'Soltero','C'=>'Casado','V'=>'Viudo','X'=>'Concubino','D'=>'Divorciado'])                 
                 //->field_type('acepta_registro','checkbox')
                 //->field_type('acepta_manejo_datos','checkbox')
                 //->field_type('acepta_envio_email','checkbox')
                 ->callback_field('fecha_nacimiento',function($val){return '<input type="date" name="fecha_nacimiento" id="field-fecha_nacimiento" class="form-control" value="'.$val.'">';})
                 ->callback_before_insert(function($post){
                    $date1 = new DateTime(date("Y-m-d H:i:s",strtotime($post['fecha_nacimiento'])));
                    $date2 = new DateTime(date("Y-m-d H:i:s"));
                    $edad = $date2->diff($date1)->format('%Y');
                    $post['edad'] = $edad;
                    return $post;
                 })
                 ->callback_after_insert(function($post,$primary){
                    Ajax_grocery_crud::table_body_after_insert($post,$primary,'solicitudes_empleos_id','solicitudes_empleo_estudios','nivel_estudio');
                    get_instance()->enviarcorreo((object)$post,21,$post['email']);                    
                 })
                 ->callback_after_update(function($post,$primary){
                    Ajax_grocery_crud::table_body_after_update($post,$primary,'solicitudes_empleos_id','solicitudes_empleo_estudios','nivel_estudio');                    
                 });
            $crud = $crud->render();
            $crud->title = 'Bolsa de Empleo, Clínica Nelson Mandela';
            $crud->crud = 'user';
            $crud->view = 'panel';
            $this->loadView($crud);
        }*/

        function generarImagen(){
            $this->as['generarImagen'] = 'convocatorias';            
            $crud = $this->crud_function('','');
            $crud->set_subject('Convocatorias');  
            $crud->columns('id','cargo','tipo_contrato','horario','ciudad','funciones','salario','postulacion','fecha_registro','fecha_culminacion','imagen');
            $crud->callback_after_insert(function($post,$primary){
                $imagen = get_instance()->echoImagen($primary);
                get_instance()->db->update('convocatorias',['imagen'=>$imagen],['id'=>$primary]);
            })
            ->callback_after_update(function($post,$primary){
                $imagen = get_instance()->echoImagen($primary);
                get_instance()->db->update('convocatorias',['imagen'=>$imagen],['id'=>$primary]);
            });            
            if($crud->getParameters()=='list'){
                $crud->set_field_upload('imagen','img/convocatorias');
            }else{
                $crud->field_type('imagen','hidden');
            }
            $crud = $crud->render();
            $crud->title = 'Convocatorias';
            $this->loadView($crud);        
        }

        /*function generarImagen($action = ''){
            if(empty($action)){
                $this->loadView([
                    'view'=>'panel',
                    'crud'=>'user',
                    'output'=>$this->load->view('generadorImagen',[],TRUE)
                ]);
            }else{
                $this->echoImagen();              
            }
        }*/

        function echoImagen($primary){
            $path = 'img/convocatorias/';
            $name = 'concurso-'.$primary.'.png';
            ob_end_clean();
            $font = "fonts/Calibri Regular.ttf";
            $img = imagecreatefromjpeg('img/empleo-blank.jpeg');
            $cargo = isset($_POST['cargo'])?$_POST['cargo']:'';
            $contrato = isset($_POST['tipo_contrato'])?$_POST['tipo_contrato']:'';
            $horario = isset($_POST['horario'])?$_POST['horario']:'';
            $ciudad = isset($_POST['ciudad'])?$_POST['ciudad']:'';
            $funciones = isset($_POST['funciones'])?$_POST['funciones']:'';
            $salario = isset($_POST['salario'])?$_POST['salario']:'';
            $postulacion = isset($_POST['postulacion'])?$_POST['postulacion']:'';
            $desde = isset($_POST['fecha_registro'])?$_POST['fecha_registro']:'';
            $hasta = isset($_POST['fecha_culminacion'])?$_POST['fecha_culminacion']:'';
            $desde = 'Fecha Desde '.$desde.' hasta '.$hasta;
            $fontsize = 18;
            Imagettftext($img,$fontsize,0,12,208,ImageColorAllocate($img,255,255,255),$font,$ciudad);
            Imagettftext($img,$fontsize,0,12,353,ImageColorAllocate($img,255,255,255),$font,$cargo);
            Imagettftext($img,$fontsize,0,12,519,ImageColorAllocate($img,255,255,255),$font,$contrato);
            //Imagettftext($img,$fontsize,0,461,183,ImageColorAllocate($img,34,62,101),$font,$horario);            
            Imagettftext($img,$fontsize,0,380,228,ImageColorAllocate($img,0,0,0),$font,wordwrap($funciones,60,"\n",true));
            Imagettftext($img,$fontsize,0,380,523,ImageColorAllocate($img,0,0,0),$font,$salario);
            //Imagettftext($img,$fontsize,0,505,296,ImageColorAllocate($img,34,62,101),$font,$postulacion);

            //Imagettftext($img,46,0,70,62,ImageColorAllocate($img,34,62,101),$font,'Nro. '.$primary);

            //Imagettftext($img,12,0,27,701,ImageColorAllocate($img,34,62,101),$font,$desde);
            //Imagettftext($img,12,0,(27+(strlen($desde)*7.5)),701,ImageColorAllocate($img,255,0,0),$font,' NRO: '.$primary);
            ob_start();
            imagepng($img);
            $content = ob_get_contents();
            ob_end_clean();
            file_put_contents($path.$name,$content);
            return $name;
        }
    }
?>
