<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class BolsaEmpleo extends Main{
        function __construct() {
            parent::__construct();
        } 

        function solicitudes_empleo_estudios($act = 2){
            $crud = new ajax_grocery_crud();            
            $crud->set_table('solicitudes_empleo_estudios')
                 ->set_theme('bootstrap')
                 ->set_subject('Estudios')
                 ->field_type('solicitudes_empleos_id','hidden','')
                 ->field_type('anulado','hidden',0)
                 ->replace_form_add('predesign/body_table')
                 ->replace_form_edit('predesign/body_table')
                 ->set_rel('solicitudes_empleos_id');
            if(!is_numeric($act)){
              $act = '';
            }
            $crud = $crud->render($act);
            return $crud;  
        }

        /* Funcion para vincular empresa con aportes en linea*/
        function solicitudes_empleo($x = ''){
            $crud = new ajax_grocery_crud();
            $crud->set_table('solicitudes_empleo')
                 ->set_subject('Postulación')
                 ->set_theme('bootstrap')
                 ->unset_list()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_edit()
                 ->set_relation('ciudades_id','ciudades','ciudad')
                 ->set_relation('departamentos_id','departamentos','departamento')
                 ->set_relation('ciudad_nacimiento','ciudades','ciudad')
                 ->set_relation('departamento_nacimiento','departamentos','departamento')
                 ->set_relation_dependency('ciudades_id','departamentos_id','departamentos_id')
                 ->set_relation_dependency('ciudad_nacimiento','departamento_nacimiento','departamentos_id')
                 ->display_as('tipos_documento_id','Tipo de Documento')
                 ->display_as('ciudades_id','Municipio de Residencia')
                 ->display_as('departamentos_id','Departamento de Residencia')
                 ->display_as('ciudades_nacimiento','Municipio de Nacimiento')
                 ->display_as('departamentos_nacimiento','Departamento de Nacimiento')
                 ->display_as('pais_nacimiento','Pais de Nacimiento')
                 ->display_as('anho_finalizacion','Año de Finalización de estudios')
                 ->display_as('categorias_trabajos_id','Categoría del trabajo al que aspira tener')
                 ->display_as('aspiracion_cargo','Cargo del trabajo al que aspira tener')
                 ->display_as('aspiracion_salario','Salario que aspira tener')
                 ->display_as('acepta_registro','Se esta registrando en la bolsa de empleos de la FUNDACION CLINICA NELSON MANDELA, NO quiere decir que este iniciando un proceso de selección. ')
                 ->display_as('acepta_manejo_datos','Autorizacion de manejo de datos personales. Sus datos personales seran tratados según nuestras politicas de tratamiento de datos')
                 ->display_as('acepta_envio_email','Notificacion de oferta de empleo. Enviaremos por correo electronico ofertas de empleos a las cuales puedes aplicar.')
                 ->display_as('hoja_vida','Adjunta tu Hoja de Vida [PDF]')
                 ->required_fields_array()
                 ->set_field_upload('hoja_vida','hojas_vida')
                 ->replace_form_add('cruds/bolsaEmpleo/add.php','bolsa_empleo')
                 ->field_type('edad','hidden')
                 ->field_type('genero','dropdown',['M'=>'Masculino','F'=>'Femenino'])
                 ->field_type('estado_civil','dropdown',['S'=>'Soltero','C'=>'Casado','V'=>'Viudo','X'=>'Concubino','D'=>'Divorciado'])                 
                 ->field_type('acepta_registro','checkbox')
                 ->field_type('acepta_manejo_datos','checkbox')
                 ->field_type('acepta_envio_email','checkbox')
                 ->callback_field('fecha_nacimiento',function($val){return '<input type="date" name="fecha_nacimiento" id="field-fecha_nacimiento" class="form-control">';})
                 ->callback_before_insert(function($post){
                    $date1 = new DateTime(date("Y-m-d H:i:s",strtotime($post['fecha_nacimiento'])));
                    $date2 = new DateTime(date("Y-m-d H:i:s"));
                    $edad = $date2->diff($date1)->format('%Y');
                    $post['edad'] = $edad;
                    return $post;
                 })
                 ->callback_after_insert(function($post,$primary){
                    Ajax_grocery_crud::table_body_after_insert($post,$primary,'solicitudes_empleos_id','solicitudes_empleo_estudios','nivel_estudio');
                    get_instance()->enviarcorreo((object)$post,21,$post['email']);
                    $_SESSION['tipos_documento_id'] = $post['tipos_documento_id'];
                    $_SESSION['documento'] = $post['documento'];
                    get_instance()->postularse($post['nro_convocatoria']);
                 })
                 ->callback_after_update(function($post,$primary){
                    Ajax_grocery_crud::table_body_after_update($post,$primary,'solicitudes_empleos_id','solicitudes_empleo_estudios','nivel_estudio');                    
                 })
                 ->set_rules('tipos_documento_id','Tipo de documento','required|callback_validarDocumento');
            if(is_numeric($x)){
                $crud->field_type('nro_convocatoria','hidden',$x);
            }
            if(empty($x) || is_numeric($x)){
                $crud = $crud->render(2);
            }else{
                $crud = $crud->render();
            }
            $crud->title = 'Bolsa de Empleo, Clínica Nelson Mandela';
            $crud->crud = 'user';
            $crud->view = 'panel';
            $this->loadView($crud);
        }

        function convocatorias($x = ''){
            $crud = new ajax_grocery_crud();
            $crud->set_table('convocatorias')
                 ->set_subject('Convocatorias')
                 ->set_theme('bootstrap')
                 ->unset_add()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_edit()
                 ->set_field_upload('imagen','img/convocatorias')
                 ->where('fecha_registro <=',date("Y-m-d"))
                 ->where('fecha_culminacion >=',date("Y-m-d"))
                 ->display_as('id','#Convocatoria')
                 ->columns('id','cargo','tipo_contrato','horario','ciudad','funciones','salario','postulacion','fecha_registro','fecha_culminacion','imagen','accion')
                 ->callback_column('accion',function($val,$row){                    
                    return '<a href="javascript:postularse('.$row->id.')">Postularse</a>';
                 });
            $crud = $crud->render();                 
            $crud->title = 'Bolsa de Empleo, Clínica Nelson Mandela';
            $crud->crud = 'user';
            $crud->view = 'panel';
            $crud->output = $this->load->view('convocatorias',['output'=>$crud->output],TRUE);
            $this->loadView($crud);
        }

        function consultarCedula(){
            if(!empty($_POST['tipos_documento_id']) && !empty($_POST['documento'])){
                $documento = $this->db->get_where('solicitudes_empleo',['tipos_documento_id'=>$_POST['tipos_documento_id'],'documento'=>$_POST['documento']]);
                if($documento->num_rows()>0){
                    $_SESSION['tipos_documento_id'] = $_POST['tipos_documento_id'];
                    $_SESSION['documento'] = $_POST['documento'];
                    echo json_encode(['success'=>true,'tipos_documento_id'=>$_POST['tipos_documento_id'],'documento'=>$_POST['documento']]);
                }else{
                    echo json_encode(['success'=>false,'tipos_documento_id'=>$_POST['tipos_documento_id'],'documento'=>$_POST['documento']]);
                }
            }else{
                echo json_encode(['success'=>false,'msj'=>'Por favor indica un documento']);
            }
        }

        function postularse($id){
            if(is_numeric($id) && !empty($_SESSION['tipos_documento_id']) && !empty($_SESSION['documento'])){
                $documento = $this->db->get_where('solicitudes_empleo',['tipos_documento_id'=>$_SESSION['tipos_documento_id'],'documento'=>$_SESSION['documento']]);                
                if($documento->num_rows()>0){
                    $postulacion = $this->db->get_where('postulaciones',['solicitudes_empleo_id'=>$documento->row()->id,'convocatorias_id'=>$id]);
                    if($postulacion->num_rows()==0){
                        $this->db->insert('postulaciones',['solicitudes_empleo_id'=>$documento->row()->id,'convocatorias_id'=>$id]);
                        echo json_encode(['success'=>true,'msj'=>'Postulación registrada con éxito']);
                    }else{
                        echo json_encode(['success'=>false,'msj'=>'ya se ha registrado tu postulación']);
                    }                    
                }else{
                    echo json_encode(['success'=>false,'msj'=>'Documento no encontrado en la bolsa']);
                }
            }else{
                echo json_encode(['success'=>false,'msj'=>'Ocurrió un error intente de nuevo']);
            }
        }

        function validarDocumento(){
            $documento = $this->db->get_where('solicitudes_empleo',['tipos_documento_id'=>$_POST['tipos_documento_id'],'documento'=>$_POST['documento']]);
            if($documento->num_rows()>0){
                $this->form_validation->set_message('validarDocumento','Su postulación ya ha sido recibida, debe esperar al llamado a proceso de selección');
                return false;
            }
            return true;
        }

        function switchButtons(){
            $this->loadView(['view'=>'panel','crud'=>'user','output'=>$this->load->view('switchButtons',[],TRUE),'title'=>'Bolsa de Empleo']);
        }
    }
?>
