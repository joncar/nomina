<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $insert_url, 'method="post" onsubmit="save(this); return false;" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Añadir <?php echo $subject?>
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        <div class="kt-portlet__body">
            

            
			<div class="kt-section kt-section--first">
            	<div class="row">
            		<?php foreach($fields as $field): ?>
            				<div class="col-12 col-md-4">
			                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
			                        <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
			                        <?php echo $input_fields[$field->field_name]->input ?>                            
			                    </div>
		                    </div>			                
	                <?php endforeach ?>		                
                </div>
                <div class="kt-portlet__foot">
			        <div class="kt-form__actions text-right mr-20">
			        	<!--<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-servicios-tab').click()" class="btn btn-success">Atras</button>-->
			            <button type='submit' class="btn btn-success">Guardar</button>                    
			        </div>
			    </div>
            </div>

            <!-- Start of hidden inputs -->
            <?php
                foreach($hidden_fields as $hidden_field){
                    echo $hidden_field->input;
                }
            ?>
            <?php if ($is_ajax) { ?>
                <input type="hidden" name="is_ajax" value="true" />
            <?php }?>
            <!-- End of hidden inputs -->
        </div>    

        <!--end::Form-->
    </div>    
    <div class="report"></div>     
    <!--end::Form-->
    <?php echo form_close(); ?>
</div>

<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "Almacenado correctamente";
	var message_insert_error = "Error al insertar";
</script>

<?php get_instance()->hcss[] = "
	<style>
		.kt-section{
			margin:20px 0;
		}
		.chzn-drop,.chzn-search,.chzn-results,.chzn-search input{
			min-width:120px !important;
		}
		[type='radio']:not(:checked), [type='radio']:checked {
		    position: initial;
		    left: initial;
		    opacity: 1;
		    margin-right:5px;
		}
		@media screen and (max-width:480px){
			.nav {
			    flex-wrap: wrap;
			}
		}
	</style>
"; ?>