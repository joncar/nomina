<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Empresas extends Panel{
        function __construct() {
            parent::__construct();
        } 
        

        function companias(){
            $crud = $this->crud_function('',''); 
            $crud->columns('razon_social','nit','telefono','aportes_linea');           
            $crud->add_action('Sedes','',base_url('empresas/sedes').'/');
            $crud->add_action('Áreas','',base_url('empresas/areas').'/');
            $crud->add_action('Cargos','',base_url('empresas/cargos').'/');
            $crud->add_action('Centros de costos','',base_url('empresas/centro_costos').'/');
            $crud->add_action('Vincular en aportes en linea','',base_url('nomina/aportesEnLineaController/vincular_empresa_alinea').'/');
            $crud->display_as('tipos_arl_id','Tipo de ARL')
                 ->display_as('caja_compensacion_familiar_id','Caja de Compensación Familiar')
                 ->display_as('ley_1429','Aplica para Ley 1429 de 2010')
                 ->display_as('ley_590','Aplica para Ley 590 de 2000')
                 ->display_as('ley_1607','Aportante exonerado de pago parafiscales. <br/>Ley 1607 de 2012')
                 ->display_as('decreto_558','Aplica para Decreto 558 de 2020')
                 ->display_as('salario_base_id','Salario Base')
                 ->display_as('bancos_id','Banco')
                 ->display_as('tipos_cuenta_id','Tipo de cuenta')
                 ->display_as('operador_pago_id','Operador de pago')
                 ->display_as('riesgo_arl_id','Tipo de riesgo ARL')
                 ->display_as('anio_constitucion','Año de constitución')
                 ->display_as('departamentos_id','Departamento')
                 ->display_as('ciudades_id','Ciudad')
                 ->display_as('departamentos_otro','Otro (Opcional - en caso que el mismo no se encuentre en el listado)')
                 ->display_as('ciudades_otro','Otra (Opcional - en caso que el mismo no se encuentre en el listado)')
                 ->display_as('representante_legal','Nombre del Representante Legal')
                 ->display_as('cargo_legal','Cargo del Representante Legal')
                 ->display_as('tipo_documento_legal','Tipo de Documento del Representante Legal')
                 ->display_as('ciudad_legal','Ciudad de Residencia del Representante Legal')
                 ->display_as('ciudad_expedicion_legal','Ciudad de Expedición de Documento de Representante Legal')
                 ->display_as('documento_legal','NROº Documento del Representante Legal')
                 ->display_as('ciudades_otro','Otra (Opcional - en caso que el mismo no se encuentre en el listado)')
                 ->display_as('aportes_linea','Vinculación Aportes en Linea')
                 ->field_type('ingresos_salariales_afectan_subsidio_transporte','true_false',['0'=>'No, solo salario','1'=>'Si, salario e ingresos salariales (sin incluir horas extras o recargos).'])
                 ->field_type('pagar_pension_completa_licencias_no_remuneradas','true_false',['0'=>'No, pagar solamente la parte de pensión correspondiente a la empresa en licencias no remuneradas.','1'=>'Si, pagar pensión completa en licencias no remuneradas, tanto el porcentaje de la empresa como del empleado.'])
                 ->set_field_upload('firma','img/firmas')                                  
                 ->field_type('periodo_actual_desde','hidden','')
                 ->field_type('periodo_actual_hasta','hidden','')
                 ->field_type('logo','image',['path'=>'img/companias_logos','width'=>'160','height'=>'80'])
                 ->field_type('aportes_linea','invisible')
                 ->field_type('tipo_documento','dropdown',sqltoarray('tipos_documento','id,iniciales',NULL,'iniciales'))
                 ->set_relation('salario_base_id','salario_base','nombre')
                 ->set_relation('departamentos_id','departamentos','{departamento_id}-{departamento}')
                 ->set_relation('ciudades_id','ciudades','{ciudad_id}-{ciudad}')
                 ->set_relation('tipo_documento_legal','tipos_documento','iniciales')
                 ->set_relation('ciudad_legal','ciudades','{ciudad_id}-{ciudad}')
                 ->set_relation('ciudad_expedicion_legal','ciudades','{ciudad_id}-{ciudad}')
                 ->set_relation_dependency('ciudades_id','departamentos_id','departamentos_id')
                 ->set_relation('ciudad_ocupacional','ciudades','{ciudad_id}-{ciudad}')
                 ->unset_columns('user_id')
                 ->callback_column('aportes_linea',function($val,$row){return $val==0?'<span class="badge badge-danger">NO</span>':'<span class="badge badge-success">SI</span>';});
            if($this->user->admin!=1){
                $crud->where('user_id',$this->user->id);
                $crud->field_type('user_id','hidden',$this->user->id);
            }else{
                $crud->set_relation('user_id','user','nombre');
            }
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function get_header($x){        	
        	$crud = new ajax_grocery_crud();             
        	$crud->set_subject('Compañia')
        		 ->set_table('companias')
        		 ->set_theme('header_data')
        		 ->where('id',$x)
            	 ->columns('razon_social','nit','telefono')
            	 ->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read()
            	 ->set_url('empresas/companias/');
            $crud = $crud->render(1);            
            return $crud->output;
        }

        function sedes($x = ''){
            $crud = $this->crud_function('','');  
            $cmp = $this->elements->companias($x);           
            $crud->where('companias_id',$x)
            	 ->field_type('companias_id','hidden',$x)
            	 ->unset_columns('companias_id');
            $crud = $crud->render();            
            $crud->header = $this->get_header($x);
            $this->loadView($crud);
        }

        function areas($x = ''){
            $crud = $this->crud_function('',''); 
            $cmp = $this->elements->companias($x);                
            $crud->where('companias_id',$x)
            	 ->field_type('companias_id','hidden',$x)
            	 ->unset_columns('companias_id');
            $crud = $crud->render();            
            $crud->header = $this->get_header($x);
            $this->loadView($crud);
        }

        function cargos($x = ''){
            $crud = $this->crud_function('',''); 
            $cmp = $this->elements->companias($x);                
            $crud->where('companias_id',$x)
            	 ->field_type('companias_id','hidden',$x)
            	 ->unset_columns('companias_id');
            $crud = $crud->render();            
            $crud->header = $this->get_header($x);
            $this->loadView($crud);
        }

        function centro_costos($x = ''){
            $crud = $this->crud_function('',''); 
            $cmp = $this->elements->companias($x);                
            $crud->where('companias_id',$x)
            	 ->field_type('companias_id','hidden',$x)
            	 ->unset_columns('companias_id');
            $crud = $crud->render();            
            $crud->header = $this->get_header($x);
            $this->loadView($crud);
        }
    }
?>
