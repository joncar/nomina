<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Definiciones extends Panel{
        function __construct() {
            parent::__construct();
        } 

        function tipos_documento(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipo de documento');
            $crud = $crud->render();
            $crud->title = 'Tipo de documento';
            $this->loadView($crud);
        }

        function dotaciones(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Dotaciones');
            $crud = $crud->render();
            $crud->title = 'Dotaciones';
            $this->loadView($crud);
        }

        function adjuntos_conceptos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Conceptos para adjuntos empleados');
            $crud = $crud->render();
            $crud->title = 'Conceptos para adjuntos empleados';
            $this->loadView($crud);
        }

        function caja_compensacion_familiar(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Cajas de Compensación Familiar');
            $crud = $crud->render();
            $crud->title = 'Cajas de Compensación Familiar';
            $this->loadView($crud);
        }
        function tipos_arl(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipos ARL');
            $crud = $crud->render();
            $crud->title = 'Tipos ARL';
            $this->loadView($crud);
        }

        function tipos_contrato(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipo de contrato');
            $crud = $crud->render();
            $crud->title = 'Tipo de contrato';
            $this->loadView($crud);
        }
        function centro_costos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Centro de costos');
            $crud = $crud->render();
            $crud->title = 'Centro de costos';
            $this->loadView($crud);
        }

        function termino_contrato(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Términos de contrato');
            $crud = $crud->render();
            $crud->title = 'Términos de contrato';
            $this->loadView($crud);
        }


        

        function salario_base(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipo de salario');
            $crud = $crud->render();
            $crud->title = 'Tipo de salario';
            $this->loadView($crud);
        }
        function riesgo_arl(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipo de riesgo ARL');
            $crud = $crud->render();
            $crud->title = 'Tipo de riesgo ARL';
            $this->loadView($crud);
        }
        function medios_pago(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Medios de pago');
            $crud = $crud->render();
            $crud->title = 'Medios de pago';
            $this->loadView($crud);
        }
        function bancos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Bancos');
            $crud = $crud->render();
            $crud->title = 'Bancos';
            $this->loadView($crud);
        }
        function tipos_cuenta(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipos de cuenta');
            $crud = $crud->render();
            $crud->title = 'Tipos de cuenta';
            $this->loadView($crud);
        }
        function tipos_eps(){
        	$this->as['tipos_eps'] = 'eps';
            $crud = $this->crud_function('','');
            $crud->set_subject('EPS');
            $crud = $crud->render();
            $crud->title = 'EPS';
            $this->loadView($crud);
        }
        function fondo_pensiones(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Fondos de pensión');
            $crud = $crud->render();
            $crud->title = 'Fondos de pensión';
            $this->loadView($crud);
        }
        function fondo_cesantias(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Fondos de cesantía');
            $crud = $crud->render();
            $crud->title = 'Fondos de cesantía';
            $this->loadView($crud);
        }
        function operador_pago(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Operadores de pago');
            $crud = $crud->render();
            $crud->title = 'Operadores de pago';
            $this->loadView($crud);
        }

        function parametros(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Parámetros de cálculos');
            $crud = $crud->render();
            $crud->title = 'Parámetros de cálculos';
            $this->loadView($crud);
        }

        function sistemas(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Sistemas');
            $crud = $crud->render();
            $crud->title = 'Sistemas';
            $this->loadView($crud);
        }

        function fondo_solidaridad_pensional_rangos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Rangos de sueldo');
            $crud = $crud->render();
            $crud->title = 'Rangos de sueldo';
            $this->loadView($crud);
        }

        function departamentos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Departamento');
            $crud = $crud->render();
            $crud->title = 'Departamento';
            $this->loadView($crud);
        }

        function ciudades(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Ciudad');
            $crud->set_relation('departamentos_id','departamentos','{departamento_id} {departamento}');
            $crud = $crud->render();
            $crud->title = 'Ciudad';
            $this->loadView($crud);
        }

        function certificados(){
            $crud = $this->crud_function('','');
            $crud->field_type('url','string');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        
    }
?>
