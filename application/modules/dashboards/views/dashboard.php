<div class="row">
	<?php foreach($this->elements->companias()->result() as $c): ?>
		<div class="col-12 col-md-4">
			<div class="card">
				<div class="card-header">
					<?= $c->razon_social ?>
				</div>
				<div class="card-body">
					<h5 class="card-title">Cumpleañeros del mes <?= meses(date("m")) ?></h5>
					<table class="table table-bordered">
						<tbody>
							<?php 
								$this->db->where('companias_id',$c->id);
								$this->db->where('MONTH(fecha_nacimiento)',date("m"));
								$this->db->order_by('fecha_nacimiento','DESC');
								foreach($this->elements->empleados()->result() as $e):
							?>
								<tr><td><?= dateToPrint($e->fecha_nacimiento,'d') ?></td><td><?= $e->nombre ?> <?= $e->apellidos ?></td></tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	<?php endforeach ?>
</div>	