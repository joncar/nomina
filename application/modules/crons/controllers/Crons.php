<?php

require_once APPPATH.'/controllers/Panel.php';    

class Crons extends Main {

    function __construct() {
        parent::__construct();
    }

    function notificar_fin_contrato(){
    	$this->db->select("empleados_fin_contrato.*,CONCAT(nombre,' ',apellidos) as empleado,companias_id,empleados.documento",FALSE);
    	$this->db->join('empleados','empleados.id = empleados_fin_contrato.empleados_id');
    	$contratos = $this->db->get_where('empleados_fin_contrato',['notificar <='=>date("Y-m-d"),'fecha_fin >'=>date("Y-m-d")]);
    	foreach($contratos->result() as $c){
    		$this->db->select('user.*,companias.razon_social');
    		$this->db->join('user','user.id = companias.user_id');
    		$compania = $this->db->get_where('companias',['companias.id'=>$c->companias_id])->row();
    		//$this->db->update('empleados_fin_contrato',['notificar'=>date("Y-m-d",strtotime(date('Y-m-d').' +5 days'))],['id'=>$c->id]);    		
    		$date = new DateTime(date("Y-m-d",strtotime($c->fecha_fin)));
    		$date2 = new DateTime(date("Y-m-d"));
    		$c->dias = $date->diff($date2)->format('%a');
    		$c->empresa = $compania->razon_social;
    		$c->fecha_fin = dateToShow($c->fecha_fin);    		
    		$this->enviarcorreo($c,20,$compania->email);
    	}
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */