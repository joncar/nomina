<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class AportesEnLineaController extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
        } 

        /* Funcion para vincular empresa con aportes en linea*/
        function vincular_empresa_alinea($empresa,$action = ''){
            if(is_numeric($empresa)){
                $comp = $this->elements->companias($empresa);
                $vinc = $this->db->get_where('companias_aportesenlinea',['companias_id'=>$empresa]);
                if($vinc->num_rows()==0){                    
                    $crud = new ajax_grocery_crud();
                    $crud->set_table('companias_aportesenlinea')
                         ->set_subject('Aportante')
                         ->set_theme('bootstrap')
                         ->unset_edit()
                         ->unset_delete()
                         ->unset_print()
                         ->unset_export()
                         ->unset_list()
                         ->required_fields_array()
                         ->field_type('companias_id','hidden',$empresa)                         
                         ->field_type('tipo_documento_tesorero','dropdown',sqltoarray('tipos_documento','iniciales',NULL,'iniciales'))
                         ->field_type('tipo_documento_nomina','dropdown',sqltoarray('tipos_documento','iniciales',NULL,'iniciales'))
                         ->field_type('tipo_documento_legal','string',$comp->tipo_documento_legal->iniciales)
                         ->field_type('documento_legal','string',$comp->documento_legal)
                         ->field_type('codigo_ccf','dropdown',sqltoarray('administradoras','codigo,administradora',['tipo'=>'CCF'],'codigo','administradora',['administradora','ASC']))
                         ->field_type('codigo_ciiu','string','8621')
                         ->field_type('usuario_aportesnelinea','hidden','')
                         ->field_type('clave_aportesenlinea','hidden','')
                         ->set_rules('email_legal','Email de Responsable Legal','required|valid_email')
                         ->set_rules('email_tesorero','Email de Tesorero','required|valid_email')
                         ->set_lang_string('insert_success_message','Registro realizado con éxito <script>document.location.reload();</script>')
                         ->callback_before_insert(function($post){
                            $post['usuario_aportesnelinea'] = explode('@',$post['email_nomina'])[0].'_aportesenlinea';
                            return $post;
                         });
                    if($crud->getParameters(FALSE)=='insert_validation'){
                         $crud->set_rules('email_nomina','Email de Usuario Nomina','required|valid_email|callback_registrarAportante');
                    }
                    $crud->replace_form_add('cruds/add','empresas');
                    $crud = $crud->render((empty($action)?2:''));
                    $crud->title = 'Vinculación de empresa en Aportes en Linea';
                    $this->loadView($crud);
                }else{
                    $crud = new ajax_grocery_crud();
                    $crud->set_table('companias_aportesenlinea')
                         ->set_subject('Aportante')
                         ->set_theme('bootstrap')                         
                         ->unset_delete()
                         ->unset_print()
                         ->unset_export()
                         ->unset_list()
                         ->unset_add()
                         ->set_primary_key_value($vinc->row()->id)
                         ->required_fields_array()
                         ->fields('usuario_aportesnelinea','clave_aportesenlinea')
                         ->display_as('usuario_aportesnelinea','Usuario para conectarse en Aportes en linea')
                         ->display_as('clave_aportesenlinea','Clave de Usuario para conectarse en Aportes en linea');
                    $crud->replace_form_edit('cruds/edit','empresas');
                    $crud = $crud->render((empty($action)?3:''));
                    $crud->output = $this->success('Su compañia ya ha sido registrada en aportes en linea, Recuerda que debe crear su clave en el correo electrónico que le enviaran a su correo y luego colocarla en la planilla de registro de empresa, para poder liquidar a sus empleados').$crud->output;
                    $crud->title = 'Vinculación de empresa en Aportes en Linea';
                    $this->loadView($crud);
                }
            }
        }

        function registrarAportante(){
            if(is_numeric($_POST['companias_id'])){
                $empresa = $_POST['companias_id'];
                $this->load->library('AportesEnLinea');
                $registro = $this->aportesenlinea->registrarEmpresa($empresa);
                if($registro->success){
                    return true;                    
                }
                $this->form_validation->set_message('registrarAportante',$registro->msj);                
            }else{
                $this->form_validation->set_message('registrarAportante','ID de Compania no válido');
            }
            return false;
        }
        

        function liquidarAportesEnLinea($nomina){
        	if(is_numeric($nomina)){
        		 $nominaid = $nomina;
        		 $nomina = $this->db->get_where('historico_nomina',['id'=>$nomina]);
        		 if($nomina->num_rows()>0){
        		 	 $nomina = $nomina->row();	        		 
		             $vinc = $this->db->get_where('companias_aportesenlinea',['companias_id'=>$nomina->companias_id]);
		             if($vinc->num_rows()>0){
		             	if(!empty($vinc->row()->usuario_aportesnelinea) && !empty($vinc->row()->clave_aportesenlinea)){
			             	$this->load->library('AportesEnLinea');
			             	$token = $this->aportesenlinea->login($vinc->row()->usuario_aportesnelinea,$vinc->row()->clave_aportesenlinea);
		                	if($token->success){                                
		                		$vinculado = $this->aportesenlinea->liquidar($vinc->row(),$nomina);
		                		if($vinculado->success){
		                			$this->db->update('historico_nomina',['liquidado_aportesenlinea'=>1,'id_transaccion_aportesenlinea'=>$vinculado->id],['id'=>$nominaid]);
		                			redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj2='.urlencode('Liquidación realizada con éxito'));
		                			die();
		                		}else{
		                			redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Error al liquidar ['.$token->msj.']'));
		                			die();
		                		}
		                	}else{
		                		redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Error al conectarse en aportes en linea, no es posible encontrar el token ['.$token->msj.']'));
		                		die();
		                	}
	                	}else{
			             	redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Error al conectarse en aportes en linea, la cuenta ya ha sido enlazada, pero no se ha cargado su contraseña, <a href="'.base_url('nomina/aportesEnLineaController/vincular_empresa_alinea/'.$vinc->row()->companias_id).'">Cargar Contraseña</a>'));
		                	die();
			            }   
		             }else{
		             	redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Debe enlazar su empresa en aportes en linea para poder registrar liquidaciones <a href="'.base_url('nomina/aportesEnLineaController/vincular_empresa_alinea/'.$nomina->companias_id).'">Registrar</a>'));
	                	die();
		             }              
	         	 }else{
	         	 	redirect('nomina/finiquito/historico_nomina');
                	die();
	         	 }
            }else{
                redirect('nomina/finiquito/historico_nomina');
                die();
            }
        }

        function consultarAportesEnLinea($nomina){
			if(is_numeric($nomina)){
        		 $nominaid = $nomina;
        		 $nomina = $this->db->get_where('historico_nomina',['id'=>$nomina]);
        		 if($nomina->num_rows()>0){
        		 	 $nomina = $nomina->row();	        		 
		             $vinc = $this->db->get_where('companias_aportesenlinea',['companias_id'=>$nomina->companias_id]);
		             if($vinc->num_rows()>0){
		             	if(!empty($vinc->row()->usuario_aportesnelinea) && !empty($vinc->row()->clave_aportesenlinea)){
			             	if(!empty($nomina->id_transaccion_aportesenlinea)){
				             	$this->load->library('AportesEnLinea');
				             	$token = $this->aportesenlinea->login($vinc->row()->usuario_aportesnelinea,$vinc->row()->clave_aportesenlinea);
		                		if($token->success){
					             	$vinculado = $this->aportesenlinea->consultarEstadoLiquidacion($nomina->id_transaccion_aportesenlinea);                                    
					             	if($vinculado->success){
                                        ob_get_clean();
                                        $response = base64_decode($vinculado->data->informeDeValidacion);
                                        $headers = explode('|',$response);
                                        if(isset($headers[28]) && $headers[28]=='Error'){
                                            if(isset($headers[34])){                                                
                                                $msj = $headers[34];
                                            }else{
                                                $msj = $response;
                                            }
                                            $this->db->update('historico_nomina',['liquidado_aportesenlinea'=>0],['id'=>$nominaid]);
                                            redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Error al consultar ['.$msj.']'));
                                        }else{
                                            ob_get_clean();
                                            header('Content-type: application/vnd.ms-excel;charset=UTF-16LE');
                                            header('Content-Disposition: attachment; filename=Aporte.csv');
                                            header("Cache-Control: no-cache");                                        
                                            echo base64_decode($vinculado->data->informeDeValidacion);                                            
                                        }                                        
					             		
										die();				             		
			                		}else{
			                			$this->db->update('historico_nomina',['liquidado_aportesenlinea'=>0],['id'=>$nominaid]);
			                			redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Error al consultar ['.$vinculado->msj.']'));
			                			die();
			                		}
			                	}else{
			                		redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Error al conectarse en aportes en linea, no es posible encontrar el token ['.$token->msj.']'));
		                			die();
			                	}
			             	}else{
			             		redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Esta nomina aún no ha sido enviada, intente su envio nuevamente.'));
		                		die();
			             	}
	                	}else{
			             	redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Error al conectarse en aportes en linea, la cuenta ya ha sido enlazada, pero no se ha cargado su contraseña, <a href="'.base_url('nomina/aportesEnLineaController/vincular_empresa_alinea/'.$vinc->row()->companias_id).'">Cargar Contraseña</a>'));
		                	die();
			            }   
		             }else{
		             	redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Debe enlazar su empresa en aportes en linea para poder registrar liquidaciones <a href="'.base_url('nomina/aportesEnLineaController/vincular_empresa_alinea/'.$nomina->companias_id).'">Registrar</a>'));
	                	die();
		             }              
	         	 }else{
	         	 	redirect('nomina/finiquito/historico_nomina');
                	die();
	         	 }
            }else{
                redirect('nomina/finiquito/historico_nomina');
                die();
            }
        }
        
    }
?>

