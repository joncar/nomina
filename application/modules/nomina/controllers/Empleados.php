<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Empleados extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
        } 

        function showView($vista,$data,$title = '',$css = [],$js = []){
            $this->loadView([
                'view'=>'panel',
                'crud'=>'user',
                'output'=>$this->load->view($vista,$data,TRUE),
                'title'=>$title,
                'js_files'=>$js,
                'css_files'=>$css
            ]);
        }

        function empresa(){
            $this->as['empleados'] = 'companias';
            $crud = $this->crud_function('',''); 
            $crud->columns('razon_social','nit','telefono');                       
            if($this->user->admin!=1){
                $crud->where('user_id',$this->user->id);
            }
            $crud->field_type('user_id','hidden',$this->user->id)
                 ->unset_columns('user_id')
                 ->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read()
                 ->callback_column('razon_social',function($val,$row){
                    return '<a href="'.base_url('nomina/empleados/empleados/empresa/'.$row->id).'"><u>'.$val.'</u></a>';
                 });
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function empleados($act = '',$y = '',$z = ''){
            switch($act){
                case 'detalle':
                    $this->detalles($y);
                break;
                case 'edit':
                    redirect('nomina/empleados/empleados/detalle/'.$y);
                break;
                case 'recurrencias':
                    $this->recurrencias($y);
                break;
                case 'recurrente_ingresos':
                    $this->recurrente_ingresos($y,0);
                break;
                case 'recurrente_deducciones':
                    $this->recurrente_deducciones($y,0);
                break;
                case 'adjuntos':
                    $this->empleados_adjuntos($y);
                break;
                case 'dotaciones':
                    $this->empleados_dotaciones($y);
                break;
                case 'licencias':
                    $this->nomina_vac_inc_lic($y);
                break; 
                case 'sistemas':
                    $this->empleados_sistemas($y);
                break;  
                case 'empleados_notificacion_permisos':
                    $this->empleados_notificacion_permisos($y);
                break;
                case 'empleados_notificacion_horas':
                    $this->empleados_notificacion_horas($y);
                break;                              
                case 'empresa':
                    $compania = $this->elements->companias($y);
                    $crud = $this->crud_function('','');
                    $crud->columns('nombre','apellidos','tipos_documento_id','documento');            
                    $crud->add_fields('foto','nombre','apellidos','tipos_documento_id','documento','tipos_contrato_id','termino_contrato_id','fecha_contrato','fecha_terminacion','salario_base_id','salario','companias_id');                    
                    $crud->display_as('nombre','Nombre')
                         ->display_as('apellidos','Apellidos')
                         ->display_as('tipos_documento_id','Tipo de documento')
                         ->display_as('documento','Número de documento')
                         ->display_as('tipos_contrato_id','Tipo de contrato')
                         ->display_as('fecha_contrato','Fecha de contratación')
                         ->display_as('fecha_terminacion','Fecha de finalización')
                         ->display_as('fecha_nacimiento','Fecha de nacimiento')
                         ->display_as('fecha_expedicion_documento','Fecha de expedición de documento')
                         ->display_as('lugar_nacimiento','Lugar de nacimiento')
                         ->display_as('lugar_expedicion_documento','Lugar de expedición de documento')                         
                         ->display_as('direccion_hogar','Dirección de hogar')
                         ->display_as('caja_compensacion_familiar_id','Caja de Compensación Familiar')                         
                         ->display_as('salario_base_id','Salario base')
                         ->display_as('salario','Valor')
                         ->display_as('riesgo_arl_id','Clase de riesgo ARL')
                         ->display_as('subsidio_transporte','¿Subsidio de transporte?')
                         ->display_as('companias_id','Compañia')
                         ->display_as('sedes_id','Sede de Trabajo')
                         ->display_as('areas_id','Área')
                         ->display_as('cargos_id','Cargo')
                         ->display_as('centro_costos_id','Centro de Costos')
                         ->display_as('medios_pago_id','Medio de Pago')
                         ->display_as('bancos_id','Banco')
                         ->display_as('tipos_cuenta_id','Tipo de cuenta')
                         ->display_as('cuenta','Número de cuenta')
                         ->display_as('retencion_fuente_automatica','Retención en la Fuente Automática')
                         ->display_as('retencion_fuente_vivienda','<b>Valor mensualizado</b> de los intereses en préstamos para adquisición de vivienda')
                         ->display_as('retencion_fuente_medicina','<b>Valor mensualizado</b> de los pagos a medicina prepagada durante el año anterior')
                         ->display_as('retencion_fuente_dependientes','Deducción por dependientes o personas a cargo')
                         ->display_as('eps_id','EPS')
                         ->display_as('vacaciones','Días de vacaciones pendientes por disfrutar')
                         ->display_as('fondo_pensiones_id','Fondo de pensiones')
                         ->display_as('fondo_cesantias_id','Fondo de cesantías')
                         ->display_as('hogar_departamento','Departamento')
                         ->display_as('hogar_ciudad','Ciudad')
                         ->display_as('nacimiento_departamento','Departamento natal')
                         ->display_as('nacimiento_ciudad','Ciudad natal')
                         ->display_as('expedicion_departamento','Expedición Departamento')
                         ->display_as('expedicion_ciudad','Expedición Ciudad')
                         ->display_as('expedicion_departamento_otro','Otro (Opcional - en caso que el mismo no se encuentre en el listado)')
                         ->display_as('expedicion_ciudad_otra','Otra (Opcional - en caso que el mismo no se encuentre en el listado)')
                         ->display_as('hogar_departamento_otro','Otro (Opcional - en caso que el mismo no se encuentre en el listado)')
                         ->display_as('hogar_ciudad_otra','Otra (Opcional - en caso que el mismo no se encuentre en el listado)')
                         ->field_type('foto','image',['path'=>'img/empleados','width'=>'400','height'=>'511'])
                         ->field_type('dias_descanso','set',['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'])
                         ->disable_ftp()
                         ->display_as('termino_contrato_id','Término del contrato')
                         ->set_relation('cargos_id','cargos','nombre',['companias_id'=>$y])
                         ->set_relation('areas_id','areas','nombre',['companias_id'=>$y])
                         ->set_relation('sedes_id','sedes','nombre',['companias_id'=>$y])
                         ->set_relation('centro_costos_id','centro_costos','nombre',['companias_id'=>$y])
                         ->set_relation('tipos_contrato_id','tipos_contrato','nombre')
                         ->set_relation('termino_contrato_id','termino_contrato','nombre')
                         ->set_relation('tipos_documento_id','tipos_documento','nombre')
                         ->set_relation('salario_base_id','salario_base','nombre');
                         if($this->user->admin==1){
                            $crud->set_relation('companias_id','companias','razon_social');
                         }
                         else{
                            $crud->set_relation('companias_id','companias','razon_social',['user_id'=>$this->user->id]);
                         }                         
                    $crud->set_relation('hogar_departamento','departamentos','{departamento_id}-{departamento}')
                         ->set_relation('hogar_ciudad','ciudades','{ciudad_id}-{ciudad}')
                         ->set_relation_dependency('hogar_ciudad','hogar_departamento','departamentos_id')
                         ->set_relation('nacimiento_departamento','departamentos','{departamento_id}-{departamento}')
                         ->set_relation('nacimiento_ciudad','ciudades','{ciudad_id}-{ciudad}')
                         ->set_relation_dependency('nacimiento_ciudad','nacimiento_departamento','departamentos_id')
                         ->set_relation('expedicion_departamento','departamentos','{departamento_id}-{departamento}')
                         ->set_relation('expedicion_ciudad','ciudades','{ciudad_id}-{ciudad}')
                         ->set_relation_dependency('expedicion_ciudad','expedicion_departamento','departamentos_id')
                         ->set_relation_dependency('termino_contrato_id','tipos_contrato_id','tipos_contrato_id')
                         ->set_relation_dependency('salario_base_id','tipos_contrato_id','tipos_contrato_id')
                         ->callback_column('empleados.nombre',function($val,$row){
                            return '<a href="'.base_url('nomina/empleados/empleados/detalle/'.$row->id).'" target="_blank">'.$val.'</a>';
                         });
                    //Edit Fields
                    if(!empty($act) && is_numeric($act) && empty($z)){
                        redirect('nomina/empleados/empleados');
                    }
                    switch($act){
                        /*case 'datos':                            
                            //$crud->edit_fields('foto','nombre','apellidos','tipos_documento_id','documento','fecha_nacimiento','email','direccion_hogar','celular','tipos_contrato_id','termino_contrato_id','fecha_contrato','fecha_terminacion','riesgo_arl_id','subsidio_transporte','salario_base_id','salario','companias_id','eps_id','fondo_pensiones_id','fondo_cesantias_id','retencion_fuente_automatica','retencion_fuente_vivienda','retencion_fuente_medicina','retencion_fuente_dependientes');
                        break;
                        case 'trabajo':                            
                            //$crud->edit_fields('sedes_id','areas_id','cargos_id','centro_costos_id','dias_descanso');
                        break;
                        case 'pago':
                            //$crud->edit_fields('medios_pago_id','bancos_id','tipos_cuenta_id','cuenta');
                        break;*/
                        default:
                            $crud->field_type('descanso','hidden');
                        break;
                    }
                    $crud->where('empleados.companias_id',$y);
                    $crud = $crud->render();            
                    $crud->output = $this->load->view('empleados/datos-principales',['output'=>$crud->output],TRUE);
                    $this->loadView($crud);
                break;
                default:
                    $this->empresa();
                break;
            }            
        }

        function detalles($empleado){
            $empleado = $this->CalculosModel->get($empleado);
            $this->empresa = $this->elements->companias($empleado->companias_id);
            if($empleado){
                $output = $this->elements->crudColillas();
                $output->where('empleados_id',$empleado->id);
                $output->set_url('empleados/empleados_colillas/historico/'.$empleado->id.'/');
                $output = $output->render();
                $output->output = $this->load->view('empleados/detalle',['empresa'=>$this->empresa,'empleado'=>$empleado,'output'=>$output->output],TRUE,'nomina');
                $this->loadView($output);
            }else{
                redirect('nomina/empleados/empleados');
            }
        }

        function recurrencias($empleado){
            $empleado = $this->CalculosModel->get($empleado);
            if($empleado){
                $this->showView('empleados/recurrencias',['empleado'=>$empleado],'Datos de recurrencias');
            }else{
                redirect('nomina/empleados/empleados');
            }
        }

        function recurrente_ingresos($x = '',$return = 1){
            $this->as['empleados'] = 'recurrente_ingresos';
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Ingresos');            
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id')
                 ->field_type('tipo','dropdown',['1'=>'Salarial','2'=>'No Salarial'])
                 ->set_relation('concepto','recurrente_ingresos_conceptos','nombre')
                 ->set_relation_dependency('concepto','tipo','tipo');
            $crud->set_url('nomina/empleados/empleados/recurrente_ingresos/'.$x.'/');
            $crud = $crud->render(); 
            if($return == 1){                       
                return $crud;
            }else{
                $crud->title = 'Ingresos';
                $this->loadView($crud);
            }
        }

        function recurrente_deducciones($x = '',$return = 1){
            $this->as['empleados'] = 'recurrente_deducciones';
            $crud = $this->crud_function('','');      
            $crud->set_subject('Deducciones');       
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id')
                 ->field_type('tipo','dropdown',['1'=>'Deducción','2'=>'Préstamo'])
                 ->field_type('concepto','enum',sqltoarray('recurrente_deducciones_conceptos','nombre',NULL,'nombre'));
            $crud->set_url('nomina/empleados/empleados/recurrente_deducciones/'.$x.'/');
            $crud = $crud->render(); 
            if($return == 1){                       
                return $crud;
            }else{
                $crud->title = 'Deducciones';
                $crud->output = $this->load->view('empleados/recurrente_deducciones',['output'=>$crud->output],TRUE);
                $this->loadView($crud);
            }
        }

        function empleados_adjuntos($x = '',$y = ''){
            $this->as['empleados'] = 'empleados_adjuntos';
            $crud = $this->crud_function('','');      
            $crud->set_subject('Adjuntos');       
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id')
                 ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
                 ->set_field_upload('fichero','adjuntos')
                 ->field_type('nombre','enum',sqltoarray('adjuntos_conceptos','nombre',NULL,'nombre'));
            $crud->set_url('nomina/empleados/empleados_adjuntos/'.$x.'/');
            $crud = $crud->render(); 
            $crud->title = 'Adjuntos';
            $this->loadView($crud);
        }

        function empleados_permisos($x = '',$y = ''){ 
            //$this->as['empleados_permisos'] = 'nomina_vac_inc_lic';
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Permisos');                 
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');
                 //->columns('nombre','desde','hasta','dias','adjunto');   
            if($this->user->admin!=1 && !in_array(3,$this->user->getGroupsArray())){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete();
            }         
            $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/4/pdf/permiso/').'/');
            $crud = $crud->render(); 
            $crud->title = 'Permisos';
            $this->loadView($crud);
        }

        function empleados_dotaciones($x = '',$y = ''){    
            $this->as['empleados'] = 'empleados_dotaciones';        
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Dotaciones');                 
            $crud->where('empleados_id',$x)
                 ->display_as('dotaciones_id','Descripción')
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');            
            $crud = $crud->render(); 
            $crud->title = 'Dotaciones';
            $this->loadView($crud);
        }

        function nomina_vac_inc_lic($x = '',$y = ''){    
            $this->as['empleados'] = 'nomina_vac_inc_lic';        
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Vacaciones/Incapacidad/Licencias');                 
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id')
                 ->columns('nombre','desde','hasta','adjuntos')
                 ->callback_column('adjuntos',function($val,$row){
                    if($val!=''){
                        $val = explode(',',$val);
                        $str = [];
                        foreach($val as $f){
                            $str[] = '<a target="_blank" href="'.base_url('adjuntos/'.$f).'">'.$f.'</a>';
                        }
                        $val = implode(', ',$str);              
                    }
                    return $val;
                 });            
            $crud = $crud->render(); 
            $crud->title = 'Vacaciones/Incapacidad/Licencias';
            $this->loadView($crud);
        }

        function empleados_sistemas($x = '',$y = ''){    
            $this->as['empleados'] = 'empleados_sistemas';        
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Acceso a sistemas');                 
            $crud->where('empleados_id',$x)
                 ->display_as('sistemas_id','Sistema')
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');            
            $crud = $crud->render(); 
            $crud->title = 'Acceso a sistemas';
            $this->loadView($crud);
        }

        function empleados_notificacion_permisos($x = '',$y = ''){    
            $this->as['empleados'] = 'empleados_notificacion_permisos';        
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Notificación de Permisos');
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');            
            $crud = $crud->render(); 
            $crud->title = 'Notificación de Permisos';
            $this->loadView($crud);
        }

        function empleados_notificacion_horas($x = '',$y = ''){    
            $this->as['empleados'] = 'empleados_notificacion_horas';        
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Notificación de Horas Extras');
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');            
            $crud = $crud->render(); 
            $crud->title = 'Notificación de Horas Extras';
            $this->loadView($crud);
        }


        
        
    }
?>
