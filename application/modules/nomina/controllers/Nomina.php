<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Nomina extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
        } 

        function showView($vista,$data = [],$title = ''){
            $this->loadView([
                'view'=>'panel',
                'crud'=>'user',
                'output'=>$this->load->view($vista,$data,TRUE,'nomina'),
                'title'=>$title
            ]);
        }

        function elegir_periodo($empresa = ''){
            if(!is_numeric($empresa)){
                redirect('nomina/liquidar_nomina');
                die();
            }
            $empresa = $this->elements->companias($empresa);
            $this->empresa = $empresa;
            $msj = '';
            $defaultMsj = 'Debes elegir un periodo correcto';
            if(!empty($_POST)){
                if((empty($_POST['desde']) || empty($_POST['hasta']))){
                    $msj = $this->error($defaultMsj);
                }else{
                    if(!$this->CalculosModel->validateSimulation(false,$_POST['desde'],$_POST['hasta'])){
                        $msj = $this->error('Este periodo ya ha sido contabilizado, si deseas realizar una modificación debes eliminarlo desde el historial de nomina y volver a generar la nomina');
                    }else{
                        /*$desde = date("m/Y",strtotime($_POST['desde']));
                        $hasta = date("m/Y",strtotime($_POST['hasta']));
                        if($desde!=$hasta){
                            $msj = $this->error($defaultMsj);
                        }else{*/
                            $this->db->update('companias',['periodo_actual_desde'=>$_POST['desde'],'periodo_actual_hasta'=>$_POST['hasta']],['id'=>$empresa->id]);
                            redirect('nomina/liquidar_nomina/'.$empresa->id);
                        //}
                    }
                }
            }            
            $this->showView('nomina/elegir_periodo',['empresa'=>$empresa,'msj'=>$msj]);
        }

        function liquidar_nomina($empresa = '',$table = ''){
        	if(!is_numeric($empresa)){
    			$this->empresa();
        	}else{
        		$empresa = $this->elements->companias($empresa);
                if(empty($empresa->periodo_actual_desde)){
                    redirect('nomina/elegir_periodo/'.$empresa->id);
                }else{
                    if($empresa){
                        $this->empresa = $empresa;
                        if(empty($table)){
                            $this->CalculosModel->validateSimulation();
                            $empleados = $this->empleados(1);
                            $empleados->title = 'Liquidar Nomina';
                            $this->loadView($empleados);                        
                        }elseif($table == 'modals'){
                            $this->modals();
                        }elseif($table == 'refreshContadores'){
                            $this->CalculosModel->getTotalNomina($empresa,true);
                            redirect('nomina/liquidar_nomina/'.$empresa->id);
                        }else{
                            $this->$table(func_get_args());
                        }
                    }else{
                        redirect('nomina/liquidar_nomina');
                    }
                }
        	}
        }

        function empresa(){
        	$this->as['liquidar_nomina'] = 'companias';
        	$crud = $this->crud_function('',''); 
            $crud->columns('razon_social','nit','telefono');                       
            if($this->user->admin!=1){
                $crud->where('user_id',$this->user->id);
            }
            $crud->field_type('user_id','hidden',$this->user->id)
                 ->unset_columns('user_id')
                 ->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read()
                 ->callback_column('razon_social',function($val,$row){
                 	return '<a href="'.base_url('nomina/liquidar_nomina/'.$row->id).'"><u>'.$val.'</u></a>';
                 });
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        function empleados($return = 0){
        	if($this->empresa){
	        	$this->as['liquidar_nomina'] = 'view_nomina_empleados';
	        	$crud = $this->crud_function('','');
	        	$crud->set_primary_key('id');
	        	$crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
	        	$crud->where('companias_id',$this->empresa->id);
	        	$crud->where('view_nomina_empleados.reporte',1);
                $crud->where('fecha_contrato <= ',date("Y-m-d",$this->empresa->periodo[1]));
                $crud->where('(fecha_terminacion IS NULL OR \''.date("Y-m-d",$this->empresa->periodo[0]).'\' BETWEEN \''.date("Y-m-d",$this->empresa->periodo[0]).'\' AND fecha_terminacion OR \''.date("Y-m",$this->empresa->periodo[0]).'\' = DATE_FORMAT(fecha_terminacion,"%Y-%m"))','ESCAPE',TRUE);
	        	$crud->set_url('nomina/liquidar_nomina/'.$this->empresa->id.'/empleados/');
	        	$crud->display_as('horas_extras_recargos','Horas extras y recargos');
	        	$crud->display_as('vacaciones_incapacidad_licencia','Vac. Incap. y Lic.');
	        	$crud->display_as('deducciones_prestamos','Deducciones y Prestamos');
                $crud->display_as('permisos_notificados','Notif. Permisos/Horas');
	        	$crud->columns('empleado','salario','permisos_notificados','horas_extras_recargos','vacaciones_incapacidad_licencia','ingresos_adicionales','deducciones_prestamos','pago_empleado');
	        	$crud->callback_column('empleado',[$this,'empleados_column']);
                $crud->callback_column('horas_extras_recargos',[$this,'horas_extras_column']);
                $crud->callback_column('vacaciones_incapacidad_licencia',[$this,'vac_inc_lic_column']);
                $crud->callback_column('ingresos_adicionales',[$this,'ingresos_adicionales_column']);
                $crud->callback_column('deducciones_prestamos',[$this,'deducciones_column']);
                $crud->callback_column('pago_empleado',[$this,'pago_empleado_column']);
                $crud->callback_column('permisos_notificados',[$this,'permisos_notificados']);
                $crud->unset_searchs('permisos_notificados');
	        	$crud = $crud->render();
	        	$crud->output = $this->load->view('calculos/liquidar_nomina',['empresa'=>$this->empresa,'output'=>$crud->output,'simulacion'=>true],TRUE);
	        	if($return == 1){
	        		return $crud;
	        	}	        	
        	}
        }

        function pensionados($return = 0){
        	$this->as['liquidar_nomina'] = 'view_nomina_empleados';
        	$crud = $this->crud_function('','');
        	$crud->set_primary_key('id');
        	$crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
        	$crud->where('companias_id',$this->empresa->id);
        	$crud->where('view_nomina_empleados.reporte',2);
            $crud->where('fecha_contrato <= ',date("Y-m-d",$this->empresa->periodo[1]));
            $crud->where('(fecha_terminacion IS NULL OR \''.date("Y-m-d",$this->empresa->periodo[0]).'\' BETWEEN \''.date("Y-m-d",$this->empresa->periodo[0]).'\' AND fecha_terminacion)','ESCAPE',TRUE);
        	$crud->display_as('horas_extras_recargos','Horas extras y recargos');
        	$crud->display_as('vacaciones_incapacidad_licencia','Vac. Incap. y Lic.');
        	$crud->display_as('deducciones_prestamos','Deducciones y Prestamos');        	
        	$crud->columns('empleado','salario','horas_extras_recargos','vacaciones_incapacidad_licencia','ingresos_adicionales','deducciones_prestamos','pago_empleado');
        	$crud->set_url('nomina/liquidar_nomina/'.$this->empresa->id.'/pensionados/');
            $crud->callback_column('empleado',[$this,'empleados_column']);
        	$crud->callback_column('horas_extras_recargos',[$this,'horas_extras_column']);
            $crud->callback_column('vacaciones_incapacidad_licencia',[$this,'vac_inc_lic_column']);
            $crud->callback_column('ingresos_adicionales',[$this,'ingresos_adicionales_column']);
            $crud->callback_column('deducciones_prestamos',[$this,'deducciones_column']);
            $crud->callback_column('pago_empleado',[$this,'pago_empleado_column']);
        	$crud = $crud->render();        	
        	if($return == 1){
        		return $crud->output;
        	}	
        }

        function contratistas($return = 0){
        	$this->as['liquidar_nomina'] = 'view_nomina_empleados';
        	$crud = $this->crud_function('','');
        	$crud->set_primary_key('id');
        	$crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
        	$crud->where('companias_id',$this->empresa->id);
        	$crud->where('view_nomina_empleados.reporte',4);
            $crud->where('fecha_contrato <= ',date("Y-m-d",$this->empresa->periodo[1]));
            $crud->where('(fecha_terminacion IS NULL OR \''.date("Y-m-d",$this->empresa->periodo[0]).'\' BETWEEN \''.date("Y-m-d",$this->empresa->periodo[0]).'\' AND fecha_terminacion)','ESCAPE',TRUE);
        	$crud->display_as('salario','Honorarios Base');
        	$crud->display_as('horas_extras_recargos','Horas trabajadas');        	
        	$crud->display_as('ingresos_adicionales','Ingresos');        	
        	$crud->display_as('deducciones_prestamos','Deducciones y Prestamos');        	
        	$crud->display_as('pago_empleado','Pago Contratista');        	
        	$crud->columns('empleado','salario','horas_extras_recargos','ingresos_adicionales','deducciones_prestamos','pago_empleado');
        	$crud->set_url('nomina/liquidar_nomina/'.$this->empresa->id.'/contratistas/');        	
            $crud->callback_column('empleado',[$this,'empleados_column']);
        	$crud->callback_column('horas_extras_recargos',[$this,'horas_trabajadas_column']);
            $crud->callback_column('vacaciones_incapacidad_licencia',[$this,'vac_inc_lic_column']);
            $crud->callback_column('ingresos_adicionales',[$this,'ingresos_adicionales_column']);
            $crud->callback_column('deducciones_prestamos',[$this,'deducciones_column']);
            $crud->callback_column('pago_empleado',[$this,'pago_empleado_column']);
        	$crud = $crud->render();        	
        	if($return == 1){
        		return $crud->output;
        	}	
        }

        function aprendices($return = 0){
        	$this->as['liquidar_nomina'] = 'view_nomina_empleados';
        	$crud = $this->crud_function('','');
        	$crud->set_primary_key('id');
        	$crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
        	$crud->where('companias_id',$this->empresa->id);
        	$crud->where('view_nomina_empleados.reporte',3);        	
            $crud->where('fecha_contrato <= ',date("Y-m-d",$this->empresa->periodo[1]));
            $crud->where('(fecha_terminacion IS NULL OR \''.date("Y-m-d",$this->empresa->periodo[0]).'\' BETWEEN \''.date("Y-m-d",$this->empresa->periodo[0]).'\' AND fecha_terminacion)','ESCAPE',TRUE);
        	$crud->display_as('salario','Cuota de apoyo');
        	$crud->display_as('horas_extras_recargos','Horas extras y recargos');
        	$crud->display_as('vacaciones_incapacidad_licencia','Incapacidades y Licencias');
        	$crud->display_as('deducciones_prestamos','Deducciones y Prestamos');        	
        	$crud->display_as('pago_empleado','Pago Aprendiz');        	
        	$crud->columns('empleado','salario','vacaciones_incapacidad_licencia','ingresos_adicionales','deducciones_prestamos','pago_empleado');
        	$crud->set_url('nomina/liquidar_nomina/'.$this->empresa->id.'/aprendices/');        	
            $crud->callback_column('empleado',[$this,'empleados_column']);
            $crud->callback_column('vacaciones_incapacidad_licencia',[$this,'vac_inc_lic_column']);
            $crud->callback_column('ingresos_adicionales',[$this,'ingresos_adicionales_column']);
            $crud->callback_column('deducciones_prestamos',[$this,'deducciones_column']);
        	$crud->callback_column('pago_empleado',[$this,'pago_empleado_column']);
            $crud = $crud->render();        	
        	if($return == 1){
        		return $crud->output;
        	}	
        }

        function nomina_exclusion($return = 0){ 

            if(is_numeric($return) || (is_array($return) && !in_array('delete',$return))){
                $this->as['liquidar_nomina'] = 'view_nomina_excluidos';
            }else{
                $this->as['liquidar_nomina'] = 'nomina_exclusion';
            }
            $crud = $this->crud_function('','');
            $crud->set_primary_key('id');
            $crud->unset_add()->unset_edit()->unset_print()->unset_export()->unset_read();
            $crud->where('view_nomina_excluidos.companias_id',$this->empresa->id);            
            $crud->where('view_nomina_excluidos.periodo_desde',date("Y-m-d",$this->empresa->periodo[0]));
            $crud->where('view_nomina_excluidos.periodo_hasta',date("Y-m-d",$this->empresa->periodo[1]));
            $crud->columns('nombre_completo','documento','fecha_contrato');
            $crud->set_url('nomina/liquidar_nomina/'.$this->empresa->id.'/nomina_exclusion/');                        
            $crud = $crud->render();            
            if($return == 1){
                return $crud->output;
            }   
        }

        function excluir_empleado(){
            $response = ['success'=>false,'msj'=>'Empleado no encontrado'];
            $this->form_validation->set_rules('empleado','Empleado','required');
            $this->form_validation->set_rules('empresa','Empleado','required');
            if($this->form_validation->run()){
                $empleado = $this->db->get_where('empleados',['id'=>$_POST['empleado']]);
                if($empleado->num_rows()>0){
                    $empleado = $this->CalculosModel->get($empleado->row()->id);
                    if(!$empleado->excluido){
                        $this->db->insert('nomina_exclusion',[
                            'empleados_id'=>$empleado->id,
                            'periodo_desde'=>$empleado->companias->periodo_actual_desde,
                            'periodo_hasta'=>$empleado->companias->periodo_actual_hasta
                        ]);
                    }
                    $response['success'] = true;
                    $response['msj'] = '';
                }
            }else{
                $response['msj'] = $this->form_validation->error_string();
            }
            echo json_encode($response);
        }

        //Show Modal
        function modals(){
        	if(!empty($_POST['modal'])){
        		$this->load->view('calculos/modals/'.$_POST['modal']);
        	}
        	else{
        		echo 'Contenido no encontrado';
        	}
        }

        function empleados_modals($empleado_id = ''){
            $empleado_id = !is_numeric($empleado_id)?$this->user->empleados_id:$empleado_id;
            $empleado = $this->CalculosModel->get($empleado_id);            
            $this->empresa = $this->elements->companias($empleado->companias_id);
            if(!empty($_POST['modal'])){
                $this->load->view('calculos/modals/'.$_POST['modal']);
            }
            else{
                echo 'Contenido no encontrado';
            }
        }


        //Callback_columns
        function empleados_column($val,$row){
            return '<a href="'.base_url('nomina/empleados/empleados/detalle/'.$row->id).'">'.$val.'</a>';
        }
        function horas_extras_column($val,$row){
        	if(empty($val)){
        		return '<a href="javascript:;" onclick="horas_extras_recargos('.$row->id.')"><i class="fa fa-edit"></i> Añadir</a>';
        	}else{
				return '<a href="javascript:;" onclick="horas_extras_recargos('.$row->id.')"><i class="fa fa-edit"></i> '.$val.'</a>';
        	}
        }   
        //Solo contratistas
        function horas_trabajadas_column($val,$row){
            if(empty($val)){
                return '<a href="javascript:;" onclick="horas_trabajadas('.$row->id.')"><i class="fa fa-edit"></i> Añadir</a>';
            }else{
                return '<a href="javascript:;" onclick="horas_trabajadas('.$row->id.')"><i class="fa fa-edit"></i> '.$val.'</a>';
            }
        }  
             
        function vac_inc_lic_column($val,$row){
            if(empty($val)){
                return '<a href="javascript:;" onclick="vac_inc_lic('.$row->id.')"><i class="fa fa-edit"></i> Añadir</a>';
            }else{
                return '<a href="javascript:;" onclick="vac_inc_lic('.$row->id.')"><i class="fa fa-edit"></i> '.$val.'</a>';
            }
        }

        function ingresos_adicionales_column($val,$row){
            if(empty($val)){
                return '<a href="javascript:;" onclick="ingresos_adicionales('.$row->id.')"><i class="fa fa-edit"></i> Añadir</a>';
            }else{
                return '<a href="javascript:;" onclick="ingresos_adicionales('.$row->id.')"><i class="fa fa-edit"></i> '.$val.'</a>';
            }
        }

        function deducciones_column($val,$row){
            if(empty($val)){
                return '<a href="javascript:;" onclick="deducciones('.$row->id.')"><i class="fa fa-edit"></i> Añadir</a>';
            }else{
                return '<a href="javascript:;" onclick="deducciones('.$row->id.')"><i class="fa fa-edit"></i> '.$val.'</a>';
            }
        }

        function pago_empleado_column($val,$row){
            $empleado = $this->CalculosModel->get($row->id);
            $str = $empleado->total_pagarf.' <a href="'.base_url('nomina/calculos/simular_calculo/'.$row->id).'" target="_new" class="fa fa-calculator" title="Ver calculos"></i></a>';
            $str.= ' <a href="javascript:;" onclick="ajustesEmpleados('.$row->id.')" class="fa fa-wrench" title="Ajustar parametros"></i></a>';
            return $str;
        }

        function permisos_notificados($val,$row){
            $this->db->where("((desde BETWEEN '".date("Y-m-d H:i:s",$this->empresa->periodo[0])."' AND '".date("Y-m-d H:i:s",$this->empresa->periodo[1])."') OR (hasta BETWEEN '".date("Y-m-d H:i:s",$this->empresa->periodo[0])."' AND '".date("Y-m-d H:i:s",$this->empresa->periodo[1])."'))",NULL,TRUE);        
            $this->db->where('empleados_id',$row->id);
            $empleado = $this->db->get_where('empleados_notificacion_permisos');
            $str = $empleado->num_rows()>0?'<a href="javascript:;" onclick="permisos('.$row->id.')"><i class="fa fa-search"></i> '.$empleado->num_rows().'</a>':'';

            $this->db->where("((desde BETWEEN '".date("Y-m-d H:i:s",$this->empresa->periodo[0])."' AND '".date("Y-m-d H:i:s",$this->empresa->periodo[1])."') OR (hasta BETWEEN '".date("Y-m-d H:i:s",$this->empresa->periodo[0])."' AND '".date("Y-m-d H:i:s",$this->empresa->periodo[1])."'))",NULL,TRUE);        
            $this->db->where('empleados_id',$row->id);
            $empleado = $this->db->get_where('empleados_notificacion_horas');            
            $str.= $empleado->num_rows()>0?' / <a href="javascript:;" onclick="horas_notificadas('.$row->id.')"><i class="fa fa-search"></i> '.$empleado->num_rows().'</a>':'';

            return $str;
        }

        function empleados_notificacion_permisos(){
            $this->as['liquidar_nomina'] = 'empleados_notificacion_permisos';         
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Notificación de Permisos');
            $crud->where('empleados_id',$_POST['empleado'])
                 ->field_type('empleados_id','hidden',$_POST['empleado'])
                 ->unset_columns('empleados_id')
                 ->unset_delete()
                 ->unset_edit()
                 ->unset_read()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export();    
            $crud->where("(empleados_notificacion_permisos.desde BETWEEN '".date("Y-m-d H:i:s",$_POST['periodo'][0])."' AND '".date("Y-m-d H:i:s",$_POST['periodo'][1])."') OR (empleados_notificacion_permisos.hasta BETWEEN '".date("Y-m-d H:i:s",$_POST['periodo'][0])."' AND '".date("Y-m-d H:i:s",$_POST['periodo'][1])."')");        
            $crud = $crud->render();             
            return $crud->output;
        }

        function empleados_notificacion_horas(){
            $this->as['liquidar_nomina'] = 'empleados_notificacion_horas';         
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Notificación de Horas Extras');
            $crud->where('empleados_id',$_POST['empleado'])
                 ->field_type('empleados_id','hidden',$_POST['empleado'])
                 ->unset_columns('empleados_id')
                 ->unset_delete()
                 ->unset_edit()
                 ->unset_read()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export();    
            $crud->where("(empleados_notificacion_horas.desde BETWEEN '".date("Y-m-d H:i:s",$_POST['periodo'][0])."' AND '".date("Y-m-d H:i:s",$_POST['periodo'][1])."') OR (empleados_notificacion_horas.hasta BETWEEN '".date("Y-m-d H:i:s",$_POST['periodo'][0])."' AND '".date("Y-m-d H:i:s",$_POST['periodo'][1])."')");        
            $crud = $crud->render();             
            return $crud->output;
        }

        function nomina_ajustes_empleados(){
            $this->as['liquidar_nomina'] = 'nomina_ajustes_empleados';         
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Notificación de Horas Extras');
            $crud->where('empleados_id',$_POST['empleado'])
                 ->field_type('empleados_id','hidden',$_POST['empleado'])
                 ->unset_columns('empleados_id')
                 ->unset_delete()
                 ->unset_edit()
                 ->unset_read()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export();    
            $crud->where("(nomina_ajustes_empleados.desde BETWEEN '".date("Y-m-d H:i:s",$_POST['periodo'][0])."' AND '".date("Y-m-d H:i:s",$_POST['periodo'][1])."') OR (nomina_ajustes_empleados.hasta BETWEEN '".date("Y-m-d H:i:s",$_POST['periodo'][0])."' AND '".date("Y-m-d H:i:s",$_POST['periodo'][1])."')");        
            $crud = $crud->render();             
            return $crud->output;
        }

        
    }
?>
