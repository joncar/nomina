<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Aportes extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
        } 

        function showView($vista,$data = [],$title = ''){
            $this->loadView([
                'view'=>'panel',
                'crud'=>'user',
                'output'=>$this->load->view($vista,$data,TRUE,'nomina'),
                'title'=>$title
            ]);
        }

        function horas_extras_recargos(){
            $response = ['success'=>false,'msj'=>'Faltan datos para poder realizar su solicitud'];
            if(!empty($_POST) && !empty($_POST['periodo']) && !empty($_POST['empleados_id']) && !empty($_POST['charge'])){
                //Guardamos las horas
                $whereb = [
                    'empleados_id'=>$_POST['empleados_id'],
                    'periodo_desde'=>date("Y-m-d",$_POST['periodo'][0]),
                    'periodo_hasta'=>date("Y-m-d",$_POST['periodo'][1]),
                    'nomina_id'=>NULL
                ];
                $this->db->delete('nomina_aportes',$whereb);
                foreach($_POST['charge'] as $n=>$a){                    
                    if($n!='otros'){
                        $data = $whereb;
                        if(!empty($a)){
                            $data['nombre'] = $n;
                            $data['cantidad'] = $a;
                            $data['valor'] = get_parameter($n);
                            $data['extra'] = 0;
                            $this->db->insert('nomina_aportes',$data);
                        }
                    }else{
                        if(!empty($a['nombre'])){
                            foreach($a['nombre'] as $nn=>$aa){
                                $data = $whereb;
                                if(!empty($a['nombre'][$nn])){
                                    $data['nombre'] = $a['nombre'][$nn];
                                    $data['cantidad'] = empty($a['cantidad'][$nn])?0:$a['cantidad'][$nn];
                                    $data['valor'] = empty($a['valor'][$nn])?0:$a['valor'][$nn];
                                    $data['extra'] = 1;
                                    $this->db->insert('nomina_aportes',$data);
                                }
                            }
                        }
                    }

                    
                }
                $response['success'] = true;

            }
            echo json_encode($response);
        }

        function vac_inc_lic(){
            $response = ['success'=>false,'msj'=>'Faltan datos para poder realizar su solicitud'];
            if(!empty($_POST) && !empty($_POST['periodo']) && !empty($_POST['empleados_id'])){                
                $response['msj'] = '';
                $empleado = $this->elements->empleados($_POST['empleados_id']);
                $table = 'nomina_vac_inc_lic';
                $whereb = [
                    'empleados_id'=>$empleado->id,
                    'periodo_desde'=>date("Y-m-d",$_POST['periodo'][0]),
                    'periodo_hasta'=>date("Y-m-d",$_POST['periodo'][1]),
                    'nomina_id'=>NULL
                ];
                //Vacaciones                
                $this->db->delete($table,$whereb);
                if(!empty($_POST['vacaciones']) && !empty($_POST['vacaciones']['desde']) && !empty($_POST['vacaciones']['hasta'])){
                    $data = $whereb;
                    $data['tipo'] = 1;
                    $data['slug'] = 'vacaciones';
                    $data['nombre'] = 'Vacaciones';
                    $data['desde'] = $_POST['vacaciones']['desde'];
                    $data['hasta'] = $_POST['vacaciones']['hasta'];
                    $data['dias_compensados'] = empty($_POST['vacaciones']['dias_compensados'])?0:$_POST['vacaciones']['dias_compensados'];
                    $data['pago_anticipado'] = empty($_POST['vacaciones']['pago_anticipado'])?0:$_POST['vacaciones']['pago_anticipado'];
                    $data['vacaciones_acumuladas'] = $empleado->vacaciones;                    
                    if(strtotime($_POST['vacaciones']['desde'])>strtotime($empleado->fecha_contrato)){
                        $this->db->insert($table,$data);
                    }else{
                        $response['msj'].= '<p>La fecha de inicio de las vacaciones debe estar dentro del rango del contrato.</p>';                        
                    }
                }
                //Incapacidad
                if(!empty($_POST['incapacidad'])){
                    foreach($_POST['incapacidad'] as $n=>$i){
                        if(!empty($i['desde']) && !empty($i['hasta'])){
                            $data = $whereb;
                            $data['tipo'] = 2;
                            $data['slug'] = $n;
                            $data['nombre'] = $i['nombre'];
                            $data['desde'] = $i['desde'];
                            $data['hasta'] = $i['hasta'];
                            $data['adjuntos'] = empty($i['adjuntos'])?'':$i['adjuntos'];
                            $data['dias_compensados'] = 0;
                            $data['pago_anticipado'] = 0;
                            $data['vacaciones_acumuladas'] = 0;
                            if(strtotime($i['desde'])>strtotime($empleado->fecha_contrato)){
                                $this->db->insert($table,$data);
                            }else{
                                $response['msj'].= '<p>La fecha de inicio de '.$i['nombre'].' debe estar dentro del rango del contrato.</p>';                        
                            }                            
                        }
                    }
                }

                //Licencia
                //Incapacidad
                if(!empty($_POST['licencia'])){
                    foreach($_POST['licencia'] as $n=>$i){
                        if(!empty($i['desde']) && !empty($i['hasta'])){
                            $data = $whereb;
                            $data['tipo'] = 3;
                            $data['slug'] = $n;
                            $data['nombre'] = $i['nombre'];
                            $data['desde'] = $i['desde'];
                            $data['hasta'] = $i['hasta'];
                            $data['dias_compensados'] = 0;
                            $data['pago_anticipado'] = 0;
                            $data['vacaciones_acumuladas'] = 0;
                            $data['adjuntos'] = !isset($i['adjuntos']) || empty($i['adjuntos'])?NULL:$i['adjuntos'];
                            if(strtotime($i['desde'])>strtotime($empleado->fecha_contrato)){
                                $this->db->insert($table,$data);
                            }else{
                                $response['msj'].= '<p>La fecha de inicio de '.$i['nombre'].' debe estar dentro del rango del contrato.</p>';                        
                            }  
                        }
                    }
                }
                if(empty($response['msj'])){
                    $response['success'] = true;
                }
            }
            echo json_encode($response);
        }

        function ingresos_adicionales(){
            $response = ['success'=>false,'msj'=>'Faltan datos para poder realizar su solicitud'];
            if(!empty($_POST) && !empty($_POST['periodo']) && !empty($_POST['empleados_id'])){                
                $response['msj'] = '';
                $empleado = $this->elements->empleados($_POST['empleados_id']);
                $table = 'nomina_ingresos';
                $whereb = [
                    'empleados_id'=>$empleado->id,
                    'periodo_desde'=>date("Y-m-d",$_POST['periodo'][0]),
                    'periodo_hasta'=>date("Y-m-d",$_POST['periodo'][1]),
                    'nomina_id'=>NULL
                ];
                //constitutivo                
                $this->db->delete($table,$whereb);
                if(!empty($_POST['constitutivo']) && !empty($_POST['constitutivo']['id']) && !empty($_POST['constitutivo']['valor'])){
                    foreach($_POST['constitutivo']['id'] as $n=>$c){
                        if(!empty($_POST['constitutivo']['id'][$n])){
                            $data = $whereb;
                            $nombre = $this->elements->recurrente_ingresos_conceptos($_POST['constitutivo']['id'][$n]);
                            if($nombre){
                                $data['tipo'] = $nombre->tipo;
                                $data['recurrente_ingresos_conceptos_id'] = $nombre->id;
                                $data['nombre'] = $nombre->nombre;                        
                                $data['valor'] = $_POST['constitutivo']['valor'][$n];
                                $data['mas_detalles'] = $_POST['constitutivo']['mas_detalles'][$n];
                                $this->db->insert($table,$data);
                            }
                        }
                    }
                }
                //noconstitutivo                                
                if(!empty($_POST['noconstitutivo']) && !empty($_POST['noconstitutivo']['id']) && !empty($_POST['noconstitutivo']['valor'])){
                    foreach($_POST['noconstitutivo']['id'] as $n=>$c){
                        if(!empty($_POST['noconstitutivo']['id'][$n])){
                            $data = $whereb;
                            $nombre = $this->elements->recurrente_ingresos_conceptos($_POST['noconstitutivo']['id'][$n]);
                            if($nombre){
                                $data['tipo'] = $nombre->tipo;
                                $data['recurrente_ingresos_conceptos_id'] = $nombre->id;
                                $data['nombre'] = $nombre->nombre;                        
                                $data['valor'] = $_POST['noconstitutivo']['valor'][$n];
                                $data['mas_detalles'] = $_POST['noconstitutivo']['mas_detalles'][$n];
                                $this->db->insert($table,$data);
                            }
                        }
                    }
                }
                //otros                                
                if(!empty($_POST['otros']) && !empty($_POST['otros']['nombre']) && !empty($_POST['otros']['valor'])){
                    foreach($_POST['otros']['nombre'] as $n=>$c){
                        if(!empty($_POST['otros']['nombre'][$n])){
                            $data = $whereb;
                            $data['tipo'] = 3;
                            $data['recurrente_ingresos_conceptos_id'] = 0;
                            $data['nombre'] = $_POST['otros']['nombre'][$n];                        
                            $data['valor'] = $_POST['otros']['valor'][$n];
                            $this->db->insert($table,$data);
                        }
                    }
                }
                if(empty($response['msj'])){
                    $response['success'] = true;
                }
            }
            echo json_encode($response);
        }

        function deducciones(){
            $response = ['success'=>false,'msj'=>'Faltan datos para poder realizar su solicitud'];
            if(!empty($_POST) && !empty($_POST['periodo']) && !empty($_POST['empleados_id'])){                
                $response['msj'] = '';
                $empleado = $this->elements->empleados($_POST['empleados_id']);
                $table = 'nomina_deducciones';
                $whereb = [
                    'empleados_id'=>$empleado->id,
                    'periodo_desde'=>date("Y-m-d",$_POST['periodo'][0]),
                    'periodo_hasta'=>date("Y-m-d",$_POST['periodo'][1]),
                    'nomina_id'=>NULL
                ];
                //constitutivo                
                $this->db->delete($table,$whereb);
                if(!empty($_POST['deducciones']) && !empty($_POST['deducciones']['id']) && !empty($_POST['deducciones']['valor'])){
                    foreach($_POST['deducciones']['id'] as $n=>$c){
                        if(!empty($_POST['deducciones']['id'][$n])){
                            $data = $whereb;
                            $nombre = $this->elements->recurrente_deducciones_conceptos($_POST['deducciones']['id'][$n]);
                            if($nombre){
                                $data['tipo'] = 1;
                                $data['recurrente_deducciones_conceptos_id'] = $nombre->id;
                                $data['nombre'] = $nombre->nombre;                        
                                $data['valor'] = $_POST['deducciones']['valor'][$n];
                                $data['mas_detalles'] = $_POST['deducciones']['mas_detalles'][$n];
                                $this->db->insert($table,$data);
                            }
                        }
                    }
                }
                //Prestamos                                
                if(!empty($_POST['prestamos']) && !empty($_POST['prestamos']['nombre']) && !empty($_POST['prestamos']['valor'])){
                    foreach($_POST['prestamos']['nombre'] as $n=>$c){
                        if(!empty($_POST['prestamos']['nombre'][$n])){
                            $data = $whereb;
                            $data['tipo'] = 2;
                            $data['recurrente_deducciones_conceptos_id'] = 0;
                            $data['nombre'] = $_POST['prestamos']['nombre'][$n];                        
                            $data['valor'] = $_POST['prestamos']['valor'][$n];                            
                            $this->db->insert($table,$data);
                        }
                    }
                }
                //otros                                
                if(!empty($_POST['otros']) && !empty($_POST['otros']['nombre']) && !empty($_POST['otros']['valor'])){
                    foreach($_POST['otros']['nombre'] as $n=>$c){
                        if(!empty($_POST['otros']['nombre'][$n])){
                            $data = $whereb;
                            $data['tipo'] = 3;
                            $data['recurrente_deducciones_conceptos_id'] = 0;
                            $data['nombre'] = $_POST['otros']['nombre'][$n];                        
                            $data['valor'] = $_POST['otros']['valor'][$n];
                            $this->db->insert($table,$data);
                        }
                    }
                }
                if(empty($response['msj'])){
                    $response['success'] = true;
                }
            }
            echo json_encode($response);
        }

        function horas_trabajadas(){
            $response = ['success'=>false,'msj'=>'Faltan datos para poder realizar su solicitud'];
            if(!empty($_POST) && !empty($_POST['periodo']) && !empty($_POST['empleados_id']) && !empty($_POST['horas_trabajadas'])){
                //Guardamos las horas
                $whereb = [
                    'empleados_id'=>$_POST['empleados_id'],
                    'periodo_desde'=>date("Y-m-d",$_POST['periodo'][0]),
                    'periodo_hasta'=>date("Y-m-d",$_POST['periodo'][1]),
                    'nomina_id'=>NULL,
                    'nombre'=>'horas_trabajadas'
                ];
                $this->db->delete('nomina_aportes',$whereb);
                $data = $whereb;
                $data['cantidad'] = $_POST['horas_trabajadas'];
                $data['valor'] = 0;
                $data['extra'] = 0;
                $this->db->insert('nomina_aportes',$data);
                $response['success'] = true;

            }
            echo json_encode($response);
        }

        function inc_adjuntos(){
            $this->as['inc_adjuntos'] = 'nomina_vac_inc_lic';
            $crud = $this->crud_function('','');
            $crud->set_field_upload('adjuntos','adjuntos');
            $crud->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read()->unset_list();
            $crud = $crud->render(); 
            $this->loadView($crud);           
        }

        function ajustes($ajuste){
            if(is_numeric($ajuste)){
                $ajuste = $this->elements->nomina_ajustes($ajuste);
                if($ajuste){
                    $compania = $this->elements->companias($ajuste->companias_id);
                    if($this->user->admin == 1 || $compania->user_id == $this->user->id){ // Si es el dueño de la compania permite la modificacion
                        $this->db->update('nomina_ajustes',[
                            'liquidar_prima'=>$_POST['liquidar_prima'],
                            'liquidar_cesantia'=>$_POST['liquidar_cesantia'],
                            'liquidar_interes_cesantia'=>$_POST['liquidar_interes_cesantia'],
                            'liquidar_vacaciones'=>$_POST['liquidar_vacaciones'],
                            'prima_desde'=>$_POST['prima_desde'],
                            'prima_hasta'=>$_POST['prima_hasta'],
                            'cesantia_desde'=>$_POST['cesantia_desde'],
                            'cesantia_hasta'=>$_POST['cesantia_hasta'],
                            'cesantia_interes_desde'=>$_POST['cesantia_interes_desde'],
                            'cesantia_interes_hasta'=>$_POST['cesantia_interes_hasta'], 
                            'cesantia_valor'=>$_POST['cesantia_valor'],
                            'cesantias_exlusion'=>!empty($_POST['cesantias_exlusion'])?implode(',',$_POST['cesantias_exlusion']):''
                        ],[
                            'id'=>$ajuste->id
                        ]);
                        echo $this->success('Ajuste almacenado con éxito');
                    }else{
                        echo $this->error('Acción no permitida para el usuario actual');
                    }
                }else{
                    echo $this->error('Ajuste no encontrado');
                }
            }else{
                echo $this->error('Acción no permitida');
            }
        }

        function empleados_ajustes($ajuste){
            $response = ['success'=>false,'msj'=>'Faltan datos para poder realizar su solicitud'];
            if(is_numeric($ajuste)){
                $ajuste = $this->elements->nomina_ajustes_empleados($ajuste);
                if($ajuste){
                    $empleado = $this->elements->empleados($ajuste->empleados_id);                    
                    if($this->user->admin == 1 || $empleado->companias->user_id == $this->user->id){ // Si es el dueño de la compania permite la modificacion
                        $this->db->update('nomina_ajustes_empleados',[
                            'liquidar_prima'=>$_POST['liquidar_prima'],
                            'liquidar_cesantia'=>$_POST['liquidar_cesantia'],
                            'liquidar_interes_cesantia'=>$_POST['liquidar_interes_cesantia'],
                            'liquidar_vacaciones'=>$_POST['liquidar_vacaciones'],
                            'prima_desde'=>$_POST['prima_desde'],
                            'prima_hasta'=>$_POST['prima_hasta'],
                            'cesantia_desde'=>$_POST['cesantia_desde'],
                            'cesantia_hasta'=>$_POST['cesantia_hasta'],
                            'cesantia_interes_desde'=>$_POST['cesantia_interes_desde'],
                            'cesantia_interes_hasta'=>$_POST['cesantia_interes_hasta'], 
                            'cesantia_valor'=>$_POST['cesantia_valor']                            
                        ],[
                            'id'=>$ajuste->id
                        ]);
                        $response['msj'] = $this->success('Ajuste almacenado con éxito');
                        $response['success'] = true;
                    }else{
                        $response['msj'] =  $this->error('Acción no permitida para el usuario actual');
                    }
                }else{
                    $response['msj'] =  $this->error('Ajuste no encontrado');
                }
            }else{
                $response['msj'] =  $this->error('Acción no permitida');
            }
            echo json_encode($response);
        }


        
        
    }
?>

