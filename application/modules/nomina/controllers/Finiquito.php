<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Finiquito extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
            $this->load->model('HistoricoModel');
        } 

        function showView($vista,$data = [],$title = ''){
            $this->loadView([
                'view'=>'panel',
                'crud'=>'user',
                'output'=>$this->load->view($vista,$data,TRUE,'nomina'),
                'title'=>$title
            ]);
        }

        function generar_nomina($empresa = ''){
        	if(!is_numeric($empresa)){
        		redirect('nomina/liquidar_nomina');
        		die();
        	}
        	$empresa = $this->elements->companias($empresa);
        	$empleados = $this->CalculosModel->getEmpleadosEnPeriodo($empresa);
        	if($empleados->num_rows()>0){
        		//Verificamos que ya no se haya generado la nomina anteriormente
        		$nomina = $this->db->get_where('historico_nomina',[
        			'companias_id'=>$empresa->id,
        			'periodo_desde'=>$empresa->periodo_actual_desde,
        			'periodo_hasta'=>$empresa->periodo_actual_hasta
        		]);
        		if($nomina->num_rows()==0){
        			$this->db->insert('historico_nomina',[
        				'companias_id'=>$empresa->id,
	        			'periodo_desde'=>$empresa->periodo_actual_desde,
	        			'periodo_hasta'=>$empresa->periodo_actual_hasta,
	        			'user_id'=>$this->user->id,
	        			'fecha_generacion'=>date("Y-m-d H:i:s"),
						'cant_empleados'=>0,
						'total_personas'=>0,
						'total_empresa'=>0,
                        'liquidar_prima'=>$empresa->ajustes->liquidar_prima,
                        'liquidar_cesantia'=>$empresa->ajustes->liquidar_cesantia,
                        'liquidar_interes_cesantia'=>$empresa->ajustes->liquidar_interes_cesantia,
                        'liquidar_vacaciones'=>$empresa->ajustes->liquidar_vacaciones,
                        'prima_desde'=>$empresa->ajustes->prima_desde,
                        'prima_hasta'=>$empresa->ajustes->prima_hasta,
                        'cesantia_desde'=>$empresa->ajustes->cesantia_desde,
                        'cesantia_hasta'=>$empresa->ajustes->cesantia_hasta,
                        'cesantia_interes_desde'=>$empresa->ajustes->cesantia_interes_desde,
                        'cesantia_interes_hasta'=>$empresa->ajustes->cesantia_interes_hasta
        			]);
        			$nomina = $this->db->insert_id();
                    $total = 0;
	        		foreach($empleados->result() as $e){
                        $f = $e;                        
		        		$e = $this->CalculosModel->get($e->id);
                        $e->nomina_id = $nomina;
                        $e->salario2 = $f->salario;
                        $e->horas_extras_recargos2 = $f->horas_extras_recargos;
                        $e->vacaciones_incapacidad_licencia2 = $f->vacaciones_incapacidad_licencia;
                        $e->ingresos_adicionales2 = $f->ingresos_adicionales;
                        $e->deducciones_prestamos2 = $f->deducciones_prestamos;
                        $e->pago_empleado2 = $f->pago_empleado;
                        $e->empleado2 = $f->empleado;
                        $total+= $e->total_pagar;
		        		$this->HistoricoModel->insert($e);
		        	}		

                    $this->db->update('historico_nomina',[
                        'cant_empleados'=>$empleados->num_rows(),
                        'total_personas'=>round($total,2),
                        'total_empresa'=>round($total,2),
                    ],[
                        'id'=>$nomina
                    ]);      

                    //Añadir excluidos 
                    $this->db->select('nomina_exclusion.*');
                    $this->db->join('empleados','empleados.id = nomina_exclusion.empleados_id');
                    $excluidos = $this->db->get_where('nomina_exclusion',[
                        'companias_id'=>$empresa->id,
                        'periodo_desde'=>$empresa->periodo_actual_desde,
                        'periodo_hasta'=>$empresa->periodo_actual_hasta
                    ]);
                    foreach($excluidos->result() as $e){
                        $this->db->insert('historico_nomina_exclusion',[                            
                            'historico_nomina_id'=>$nomina,
                            'empleados_id'=>$e->empleados_id,
                            'periodo_desde'=>$e->periodo_desde,
                            'periodo_hasta'=>$e->periodo_hasta
                        ]);
                    } 	
	        	}
                
                redirect('nomina/finiquito/historico_nomina/'.$empresa->id.'/success');
        	}

            echo 'Error al generar la nomina, la empresa no se encuentra';
        }

        function empresa(){
            $this->as['historico_nomina'] = 'companias';
            $crud = $this->crud_function('',''); 
            $crud->columns('razon_social','nit','telefono');                       
            if($this->user->admin!=1){
                $crud->where('user_id',$this->user->id);
            }
            $crud->field_type('user_id','hidden',$this->user->id)
                 ->unset_columns('user_id')
                 ->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read()
                 ->callback_column('razon_social',function($val,$row){
                    return '<a href="'.base_url('nomina/finiquito/historico_nomina/'.$row->id).'"><u>'.$val.'</u></a>';
                 });
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function historico_nomina($compania = ''){
            if(!is_numeric($compania)){
                $this->empresa();
            }else{
                $cmp = $this->elements->companias($compania);
                $crud = $this->crud_function('','');
                $crud->columns('periodo_desde','periodo_hasta','user_id','fecha_generacion','cant_empleados','total_personas');
                $crud->display_as('periodo_desde','Fecha Inicio');
                $crud->display_as('periodo_hasta','Fecha Final');
                $crud->display_as('user_id','Creador');
                $crud->display_as('fecha_generacion','Fecha Realización');
                $crud->display_as('cant_empleados','Nro. empleados');
                $crud->display_as('total_personas','Total Nomina');
                $crud->unset_add()->unset_edit()->unset_print()->unset_export()->unset_read();
                $crud->callback_column('periodo_desde',function($val,$row){
                    return '<a href="'.base_url('nomina/finiquito/verNomina/'.$row->id).'">'.date("d/m/Y",strtotime($val)).'</a>';
                });
                $crud->callback_column('total_personas',function($val,$row){
                    return '$'.number_format($val,0,',','.');
                });
                //if($this->user->admin!=1){
                    $crud->where('companias_id',$compania); 
                //}  
                $crud->order_by('periodo_desde','DESC');
                $crud->add_action('Ver Nomina','',base_url('nomina/finiquito/verNomina/').'/');                       
                $crud = $crud->render();
                $this->loadView($crud);
            }   
        }

        function verNomina($id){
            if(is_numeric($id)){
                $nomina = $this->db->get_where('historico_nomina',['id'=>$id]); 
                if($nomina->num_rows()>0){
                    $nomina = $nomina->row();
                    $nomina->empresa = $this->elements->companias($nomina->companias_id);
                    $nomina->periodo = [strtotime($nomina->periodo_desde),strtotime($nomina->periodo_hasta)];
                    $nomina->periodof = date("d",$nomina->periodo[0]).'/'.meses_short(date("m",$nomina->periodo[0])).' al '.date("d",$nomina->periodo[1]).' de '.meses_short(date("m",$nomina->periodo[1])).'/'.date("Y",$nomina->periodo[1]);
                    $nomina->total_personasf = $this->elements->fm($nomina->total_personas);
                    $nomina = $this->HistoricoModel->get($nomina);
                    $this->showView('historico/detalle',['nomina'=>$nomina,'title'=>'Visualización de nomina']);
                }else{
                    redirect('nomina/finiquito/historico_nomina');
                }
            }
        }

        function resumen_nomina_xls($nomina = ''){
            $this->as['resumen_nomina_xls'] = 'view_exportable_banco';
            $crud = $this->crud_function('','');                        
            $crud->set_primary_key('id');
            $crud->unset_add()->unset_edit()->unset_print()->unset_read()->unset_delete();
            $crud->where('nomina_id',$nomina);  
            $crud->unset_columns('id','nomina_id');           
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function enviarColillas($id){
            if(is_numeric($id)){
                $str = '<p>Enviando correos</p>';
                $colillas = $this->db->get_where('historico_nomina_empleados',['historico_nomina_id'=>$id]);
                foreach($colillas->result() as $c){
                    $empleado = $this->db->get_where('empleados',['empleados.id'=>$c->empleados_id]);
                    if($empleado->num_rows()>0){
                        $empleado = $empleado->row();
                        $c->logo = $this->db->get_where('companias',['id'=>$empleado->companias_id]);
                        if($c->logo->num_rows()>0){
                            $c->logo = $c->logo->row()->logo;
                            if(!empty($c->logo)){
                                $c->logo = '<img src="'.base_url('img/companias_logos/'.$c->logo).'">';
                            }else{
                                $c->logo = '<img src="'.base_url('img/logos/'.$this->ajustes->logo).'">';
                            }
                        }else{
                            $c->logo = '<img src="'.base_url('img/logos/'.$this->ajustes->logo).'">';
                        }
                        $c->nombre = $empleado->nombre;
                        $c->apellidos = $empleado->apellidos;
                        $c->email = $empleado->email;                        
                        $_POST['adjunto'] = base_url().'reportes/rep/verReportes/1/pdf/empleado/'.$c->id.'.pdf';
                        $correo = get_instance()->enviarCorreo($c,1);                        
                        $str.= '<p>Enviado la colilla a '.$c->empleado.'['.$c->email.']['.$_POST['adjunto'].']</p>';
                    }
                }
                $str.= $this->success('Colillas de pago enviadas a los trabajadores <a href="'.base_url('nomina/finiquito/verNomina/'.$id).'" class="btn btn-info">Volver</a>'); 
                $this->loadView([
                    'view'=>'panel',
                    'crud'=>'user',
                    'output'=>$str,
                    'title'=>'Enviar colillas de pago'
                ]);
            }
        }
    }
?>
