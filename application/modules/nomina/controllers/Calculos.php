<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Calculos extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
        }

        function simular_calculo($empleado){
        	$empleado = $this->CalculosModel->get($empleado);
        	$compania = $this->elements->companias($empleado->companias_id);
        	$this->showSimpleView('calculos/simular_calculos',[
        		'empresa'=>$compania,
        		'empleado'=>$empleado
        	]);
        }
    }
?>
