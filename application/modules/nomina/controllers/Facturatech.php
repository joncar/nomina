<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class FacturaTech extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
            $this->load->model('HistoricoModel');
        }
        

        function enviar($nomina){
            $id = $nomina;
        	if(is_numeric($nomina)){
        		$nomina = $this->db->get_where('historico_nomina',['id'=>$nomina]);
        		if($nomina->num_rows()>0){
                    $nomina = $nomina->row();
                    $nomina->empresa = $this->elements->companias($nomina->companias_id);
                    $nomina->periodo = [strtotime($nomina->periodo_desde),strtotime($nomina->periodo_hasta)];
                    $nomina->periodof = date("d",$nomina->periodo[0]).'/'.meses_short(date("m",$nomina->periodo[0])).' al '.date("d",$nomina->periodo[1]).' de '.meses_short(date("m",$nomina->periodo[1])).'/'.date("Y",$nomina->periodo[1]);
                    $nomina->total_personasf = $this->elements->fm($nomina->total_personas);
                    $nomina = $this->HistoricoModel->get($nomina);                    
                    require_once APPPATH.'libraries/facturatech/FacturaTech.php';
                    $facturatech = new FacturaTechSoap();
                    $facturatech->enviarNomina($nomina);
                    redirect('nomina/finiquito/verNomina/'.$nomina->id.'?msj='.urlencode('Nomina enviada correctamente a FacturaTech'));
                    die();
        		}else{
                    redirect('nomina/finiquito/verNomina/'.$id.'?msj='.urlencode('Error la nomina no se encuentra o ha sido eliminada, intente de nuevo'));
                    die();
                }
        	}
        }

        function download($id = ''){
            if(!empty($id)){
                $nomina = $this->db->get_where('historico_nomina_empleados',['id'=>$id]);
                if($nomina->num_rows()>0){
                    $nomina = $nomina->row();
                    if(!empty($nomina->facturatech)){
                        $factura = json_decode($nomina->facturatech);
                        if(!empty($factura->file)){
                            redirect('files/facturatech/nomina/'.$factura->file);
                        }
                        if(!empty($factura->transactionID)){
                            require_once APPPATH.'libraries/facturatech/FacturaTech.php';
                            $facturatech = new FacturaTechSoap();
                            $facturatech->get($nomina,$factura);
                        }                        
                    }                    
                }
            }
            redirect('nomina/finiquito/verNomina/'.$nominaid.'?msj='.urlencode('Error la nomina no se encuentra o ha sido eliminada, intente de nuevo'));
            die();
        }
        
    }
?>

