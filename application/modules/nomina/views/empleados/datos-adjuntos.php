<div class="card">
	<div class="card-header">
		Datos adjuntos
		<a href="<?= base_url('nomina/empleados/empleados/adjuntos/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div class="card-body">	
		<div class="row" style="margin-left:0;margin-right:0">
			<?php if($empleado->empleados_adjuntos->num_rows()==0): ?>
				Sin datos adjuntos
			<?php endif ?>
			<?php foreach($empleado->empleados_adjuntos->result() as $a): ?>
				<div class="col text-center">
					<a href="<?= $a->fichero_src ?>" class="btn btn-info" target="_new">
						<i class="fa fa-download fa-2x"></i> <br/>
						<?= $a->nombre ?>
					</a>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>