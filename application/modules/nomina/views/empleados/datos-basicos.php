<div class="card">
	<div class="card-header">
		Datos Básicos
		<a href="<?= base_url('nomina/empleados/empleados/empresa/'.$empleado->companias_id.'/edit/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div style="overflow-x:auto">	
		<table class="table mb-0">
			<tr>
				<th>Nombre completo</th>
				<td><?= $empleado->nombre_completo ?></td>
			</tr>
			<tr>
				<th><?= $empleado->tipo_documento->nombre ?></th>
				<td><?= $empleado->documento ?></td>
			</tr>
			<tr>
				<th>Correo electrónico</th>
				<td><?= $empleado->email ?></td>
			</tr>
			<tr>
				<th>Tipo de contrato</th>
				<td><?= $empleado->contrato->nombre ?></td>
			</tr>
			<tr>
				<th>Término del contrato</th>
				<td>
					<?= $empleado->termino_contrato->nombre ?>
				</td>
			</tr>
			<tr>
				<th>Fecha de contratación</th>
				<td><?= $empleado->fecha_contratof ?></td>
			</tr>
			<tr>
				<th>Clase de riesgo ARL</th>
				<td><?= $empleado->riesgo_arl->nombre ?></td>
			</tr>
			<tr>
				<th>EPS</th>
				<td><?= $empleado->eps->nombre ?></td>
			</tr>
			<tr>
				<th>Fondo de pensiones</th>
				<td><?= $empleado->fondo_pensiones->nombre ?></td>
			</tr>
			<tr>
				<th>Fondo de cesantia</th>
				<td><?= $empleado->fondo_cesantias->nombre ?></td>
			</tr>
			<tr>
				<th>Días de descanso</th>
				<td><?= $empleado->descansof ?></td>
			</tr>
			<tr>
				<th>Vacaciones Acumuladas</th>
				<td><?= $empleado->vacacionesf ?></td>
			</tr>
			<tr>
				<th>¿Subsidio de transporte?</th>
				<td><?= $empleado->subsidio_transportef ?></td>
			</tr>
			<tr>
				<th>Compañia</th>
				<td><?= $empleado->companias->razon_social ?></td>
			</tr>
		</table>
	</div>
</div>