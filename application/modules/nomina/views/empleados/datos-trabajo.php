<div class="card">
	<div class="card-header">
		Datos Puesto de Trabajo
		<a href="<?= base_url('nomina/empleados/empleados/empresa/'.$empleado->companias_id.'/edit/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div>	
		<table class="table mb-0">
			<tr>
				<th>Sede de Trabajo</th>
				<td><?= $empleado->sedes->nombre ?></td>
			</tr>
			<tr>
				<th>Área</th>
				<td><?= $empleado->areas->nombre ?></td>
			</tr>
			<tr>
				<th>Cargo</th>
				<td><?= $empleado->cargos->nombre ?></td>
			</tr>
			<tr>
				<th>Centro de Costos</th>
				<td><?= $empleado->centro_costos->nombre ?></td>
			</tr>
			<tr>
				<th>Días de descanso</th>
				<td><?= $empleado->dias_descansof ?></td>
			</tr>
		</table>
	</div>
</div>