<?= $output ?>
<script>
	window.afterLoad.push(function(){
		window.elements = $("#field-concepto").html();		
		$("#field-saldo").attr('readonly',true);
		$("#field-meses").attr('readonly',true);
		$("#field-tipo").on('change',function(){
			if($(this).val()!=''){
				var val = $(this).val();
				$("#field_concepto_chzn").remove();
				switch(val){
					case '1':
						$("#field-concepto").replaceWith('<select id="field-concepto" name="concepto" class="chosen-select"></select>');
						$("#field-concepto").html(window.elements);
						$("#field-concepto").chosen();
						$("#field-valor").val('0');
						$("#field-saldo").val('0');
						$("#field-meses").val('0');
						$("#valor_field_box,#saldo_field_box,#meses_field_box").hide();
					break;
					case '2':
						$("#field-valor").val('');
						$("#field-saldo").val('');
						$("#field-meses").val('');
						$("#field-concepto").replaceWith('<input type="text" id="field-concepto" name="concepto" class="form-control">');						
						$("#valor_field_box,#saldo_field_box,#meses_field_box").show();
					break;
				}
			}else{
				$("#field-concepto").replaceWith('<select id="field-concepto" name="concepto" class="chosen-select"></select>');
				$("#field-concepto").html(window.elements);
				$("#field-concepto").chosen();
			}
		});

		$("#field-valor,#field-cuota_mensual").on('change',function(){
			var valor = parseFloat($("#field-valor").val());
			var cuotas = parseFloat($("#field-cuota_mensual").val());
			if(!isNaN(valor) && !isNaN(cuotas)){
				var meses = valor/cuotas;
				meses = Math.ceil(meses);
				$("#field-saldo").val(valor);
				$("#field-meses").val(meses);
			}
		});
	});
</script>