<div class="card">
	<div class="card-header">
		Conceptos recurrentes
		<a href="<?= base_url('nomina/empleados/empleados/recurrencias/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div>	
		<table class="table mb-0">
			<tr class="text-right">
				<th colspan="2">Deducciones</th>
			</tr>
			<?php if($empleado->recurrente_deducciones->num_rows()==0): ?>
				<tr>
					<td colspan="2">Sin deducciones registradas</td>
				</tr>
			<?php endif ?>
			<?php foreach($empleado->recurrente_deducciones->result() as $d): ?>
			<tr>
				<th><?= $d->concepto ?></th>
				<td><?= $d->valorf ?></td>
			</tr>
			<?php endforeach ?>
			<tr class="text-right">
				<th colspan="2">Ingresos salariales</th>
			</tr>	
			<?php if($empleado->recurrente_ingresos->num_rows()==0): ?>
				<tr>
					<td colspan="2">Sin ingresos registrados</td>
				</tr>
			<?php endif ?>
			<?php foreach($empleado->recurrente_ingresos->result() as $d): ?>		
			<tr>
				<th><?= $d->concepto->nombre ?></th>
				<td><?= $d->valorf ?></td>
			</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>