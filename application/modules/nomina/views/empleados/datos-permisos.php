<div class="card">
	<div class="card-header">
		Notificación de Permisos
		<a href="<?= base_url('nomina/empleados/empleados/empleados_notificacion_permisos/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div>	
		<table class="table mb-0">
			<tr>
				<th>Motivo</th>
				<th>Desde</th>
				<th>Hasta</th>
			</tr>
			<?php if($empleado->permisos->num_rows()==0): ?>
				<tr>
					<td colspan="2">Sin permisos registrados</td>
				</tr>
			<?php endif ?>
			<?php foreach($empleado->permisos->result() as $d): ?>
				<tr>
					<td><?= $d->motivo ?></td>
					<td><?= $d->desdef ?></td>
					<td><?= $d->hastaf ?></td>
				</tr>
			<?php endforeach ?>		
		</table>
	</div>
</div>