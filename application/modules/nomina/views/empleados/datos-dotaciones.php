<div class="card">
	<div class="card-header">
		Dotaciones
		<a href="<?= base_url('nomina/empleados/empleados/dotaciones/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div>	
		<table class="table mb-0">
			<tr>
				<th>Descripción</th>
				<th>Cantidad</th>
				<th>Fecha</th>
			</tr>
			<?php if($empleado->dotaciones->num_rows()==0): ?>
				<tr>
					<td colspan="3">Sin dotaciones asignadas</td>
				</tr>
			<?php endif ?>
			<?php foreach($empleado->dotaciones->result() as $d): ?>
				<tr>
					<td><?= @$d->dotacion->nombre ?></td>
					<td><?= $d->cantidad ?></td>
					<td><?= $d->fechaf ?></td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>