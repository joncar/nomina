<?php echo $output ?>

<script>
	window.ajustes = <?php echo json_encode(get_instance()->ajustes); ?>;
	window.salario_base = <?php echo get_parameter('salario_base'); ?>;
	window.afterLoad.push(function(){
		$("#field-salario").on('change',function(){
			var val = parseFloat($(this).val());
			if(isNaN(val)){
				$(this).val(window.salario_base);
			}else{
				if(val<window.salario_base){
					if(!confirm('¿Estás seguro de que esta persona tendrá un salario inferior al salario mínimo?')){
						$(this).val(window.salario_base);
					}
				}
			}
		});

		$("input[name='retencion_fuente_automatica']").on('click',function(){
			var act = $("input[name='retencion_fuente_automatica']:checked").val();
			var chlds = $('#retencion_fuente_vivienda_field_box,#retencion_fuente_medicina_field_box,#retencion_fuente_dependientes_field_box');
			if(act=='1'){
				chlds.show();
			}else{
				chlds.hide();
			}
		});
		$("input[name='retencion_fuente_automatica']:checked").trigger('click');
	});
</script>