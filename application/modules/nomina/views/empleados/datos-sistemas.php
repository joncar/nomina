<div class="card">
	<div class="card-header">
		Acceso a sistemas
		<a href="<?= base_url('nomina/empleados/empleados/sistemas/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div>	
		<table class="table mb-0">
			<tr>
				<th>Sistema</th>
				<th>Estado</th>
			</tr>
			<?php if($empleado->sistemas->num_rows()==0): ?>
				<tr>
					<td colspan="2">Sin sistemas asignados</td>
				</tr>
			<?php endif ?>
			<?php foreach($empleado->sistemas->result() as $d): ?>
				<tr>
					<td><?= @$d->sistemas->nombre ?></td>
					<td><?= $d->estadof ?></td>
				</tr>
			<?php endforeach ?>		
		</table>
	</div>
</div>