<div class="card">
	<div class="card-header">
		Datos Personales
		<a href="<?= base_url('nomina/empleados/empleados/empresa/'.$empleado->companias_id.'/edit/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div style="overflow-x:auto">	
		<table class="table mb-0">
			<tr>
				<th>Fecha nacimiento</th>
				<td><?= $empleado->fecha_nacimientof ?></td>
			</tr>
			<tr>
				<th>Dirección hogar</th>
				<td><?= $empleado->direccion_hogar ?></td>
			</tr>
			<tr>
				<th>Número de télefono</th>
				<td><?= $empleado->celular ?></td>
			</tr>
		</table>
	</div>
</div>