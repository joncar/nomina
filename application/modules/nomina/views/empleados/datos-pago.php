<div class="card">
	<div class="card-header">
		Datos de Pago
		<a href="<?= base_url('nomina/empleados/empleados/empresa/'.$empleado->companias_id.'/edit/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div>	
		<table class="table mb-0">
			<tr>
				<th>Medio de Pago</th>
				<td><?= $empleado->medios_pago->nombre ?></td>
			</tr>
			<tr>
				<th>Banco</th>
				<td><?= $empleado->bancos->nombre ?></td>
			</tr>
			<tr>
				<th>Tipo de cuenta</th>
				<td><?= $empleado->tipos_cuenta->nombre ?></td>
			</tr>
			<tr>
				<th>Número de cuenta</th>
				<td><?= $empleado->cuenta ?></td>
			</tr>
		</table>
	</div>
</div>