<?php 
	get_instance()->hcss[] = '
		<style>
			.detalleFoto img{
				max-width: 140px;
			}
		</style>
	';
?>
<div class="row">
	<div class="col-12">
		<div class="card">			
			<div class="card-body">
				<div class="row">
					<div class="col-12 col-md-6 text-center detalleFoto">
						<div>
							<?= $empleado->foto_img ?>
						</div>
						<div>
							<?= $empleado->nombre_completo ?> <br/>
						    <?= $empleado->contrato->nombre ?>
						</div>
						<div>							
							<div class="btn-group">
							  <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    Formatos
							  </a>
							  <div class="dropdown-menu">
							    <a class="dropdown-item" href="<?= $empleado->permisos_link ?>" target="_new">Permisos</a>
							    <a class="dropdown-item" href="<?= $empleado->examen_ocupacional_link ?>" target="_new">Examen Médico Ocupacional</a>
							    <a class="dropdown-item" href="<?= $empleado->apertura_cuenta ?>" target="_new">Apertura de cuenta</a>
							    <a class="dropdown-item" href="<?= $empleado->contratos_url ?>" target="_new">Contratos</a>
							    <a class="dropdown-item" href="<?= $empleado->sanciones_url ?>" target="_new">Llamados de atención</a>
							    <a class="dropdown-item" href="<?= $empleado->sanciones_funciones_url ?>" target="_new">Recordatorios de Funciones</a>
							    <a class="dropdown-item" href="<?= $empleado->fin_contrato_url ?>" target="_new">Comunicado Finalización de Contrato</a>
							    <a class="dropdown-item" href="<?= base_url('reportes/rep/verReportes/13/jpg/valor/'.$empleado->id) ?>">Imprimir Carnet Frontal</a>
							    <a class="dropdown-item" href="<?= base_url('img/carnet-trasero.jpeg') ?>" download>Imprimir Carnet Trasera</a>
							    
							  </div>
							</div> |
							<div class="btn-group">
							  <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    Certificados
							  </a>
							  <div class="dropdown-menu">
							    <?php foreach($this->db->get('certificados')->result() as $c): ?>
							    	<a class="dropdown-item" href="<?= base_url() ?><?= $c->url ?><?= $empleado->id ?>" target="_new"><?= $c->nombre ?></a>
								<?php endforeach ?>
							  </div>
							</div> |
							<a href="#colillasModal" data-toggle="modal">Historiales</a>
							<!--<a href="#">Conceptos recurrentes</a>-->
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="row">
							<div class="col-12 col-md-4 card bg-info text-center">
								<div class="card-body">
								   <i class="fa fa-money fa-3x"></i><br/>
								   Salario Base<br/>
								   <h3><?= $empleado->salariof ?></h3>
								</div>
							</div>
							<div class="col-12 col-md-4 card bg-info text-center">
								<div class="card-body">
								   <i class="fa fa-umbrella fa-3x"></i><br/>
								   Vacaciones acumuladas<br/>
								   <h3><?= $empleado->vacacionesf ?></h3>
								</div>
							</div>
							<div class="col-12 col-md-4 card bg-info text-center">
								<div class="card-body">
								   <i class="fa fa-address-book fa-3x"></i><br/>
								   Colilla de pago<br/>
								   <a href="#">Última</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-12 col-md-7">
		<?php $this->load->view('empleados/datos-basicos',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-trabajo',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-personales',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-pago',[],FALSE,'nomina'); ?>
	</div>
	<div class="col-12 col-md-5">
		<?php $this->load->view('empleados/datos-salario',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/conceptos-recurrentes',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-dotaciones',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-licencias',['empleado'=>$empleado,'empresa'=>$empresa],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-sistemas',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-permisos',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-horas',[],FALSE,'nomina'); ?>
		<?php $this->load->view('empleados/datos-adjuntos',[],FALSE,'nomina'); ?>		
	</div>
</div>		

<?php if(!empty($output)): ?>
	<?php $this->load->view('empleados/modals/colillasModal',[],FALSE,'nomina'); ?>
<?php endif ?>			