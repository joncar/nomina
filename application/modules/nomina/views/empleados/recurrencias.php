<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Ingresos</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Deducciones</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
  	<div class="alert alert-info">
  		<p>Acá podrás configurar todos los Ingresos recurrentes de este empleado, tanto Salariales (Bonos, Comisiones, etc) como No Salariales (Auxilios, Bonificaciones, etc).</p>
		<p><b>Recuerda:</b> Si agregas ingresos recurrentes no salariales y estos superan el 40% de los ingresos salariales en algún periodo, Nominapp calculará la base para la liquidación de aportes a seguridad social siguiendo lo estipulado en la Ley 1393 de 2010. Para conocer más al respecto haz <a href="https://ayuda.nominapp.com/help/nominapp-calcula-los-pagos-no-salariales-que-exceden-el-40-del-total-de-la-remuneracion" target="_blank">click aquí</a>.</p>
  	</div>	
  	<?php echo get_instance()->recurrente_ingresos($empleado->id)->output; ?>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
  	<div class="alert alert-info">
  		Acá podrás configurar todas las Deducciones recurrentes de este empleado, como Aportes voluntarios a pensión, Préstamos o los conceptos personalizados de tu empresa.
  	</div>
  	<?php echo get_instance()->recurrente_deducciones($empleado->id)->output; ?>
  </div>
</div>