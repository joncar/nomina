<div class="card">
	<div class="card-header">
		Notificación de Horas Extras
		<a href="<?= base_url('nomina/empleados/empleados/empleados_notificacion_horas/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div>	
		<table class="table mb-0">
			<tr>
				<th>Motivo</th>
				<th>Desde</th>
				<th>Hasta</th>
				<th>Horas</th>
			</tr>
			<?php if($empleado->horas_notificadas->num_rows()==0): ?>
				<tr>
					<td colspan="2">Sin Horas registradas</td>
				</tr>
			<?php endif ?>
			<?php foreach($empleado->horas_notificadas->result() as $d): ?>
				<tr>
					<td><?= $d->motivo ?></td>
					<td><?= $d->desdef ?></td>
					<td><?= $d->hastaf ?></td>
					<td><?= $d->horas ?></td>
				</tr>
			<?php endforeach ?>		
		</table>
	</div>
</div>