<?php 
	$horas = $this->db->get_where('view_nomina_empleados',[
		'companias_id'=>get_instance()->empresa->id,
		'id'=>$empleado->id
	]); 							
?>
<div class="card filtering_form">
	<div class="card-header">
		Licencias / Incapacidades		
	</div>
	<div style="overflow-x:auto">
		<table class="table mb-0">
			<tr>
				<th>Descripción</th>
				<th>Desde</th>
				<th>Hasta</th>
			</tr>
			<?php if($empleado->licencias->num_rows()==0): ?>
				<tr>
					<td colspan="3">Sin licencias asignadas</td>
				</tr>
			<?php endif ?>
			<?php foreach($empleado->licencias->result() as $d): ?>
				<tr>
					<td><?= $d->nombre ?></td>
					<td><?= $d->desdef ?></td>
					<td><?= $d->hastaf ?></td>
				</tr>
			<?php endforeach ?>
			<?php if($this->user->admin==1 && $empleado->licencias->num_rows()>0): ?>
				<tr>
					<td colspan="3">
						<a href="<?= base_url('nomina/empleados/empleados/licencias/'.$empleado->id) ?>" target="_new">Ver todas</a>
					</td>
				</tr>
			<?php endif ?>
			<?php 			
	            if(!empty(get_instance()->empresa->periodo_actual_desde) && $this->CalculosModel->validateSimulation(false,get_instance()->empresa->periodo_actual_desde,get_instance()->empresa->periodo_actual_hasta)):
			?>
				<tr>
					<td colspan="2">
						<a href="javascript:;" onclick="vac_inc_lic('<?= $empleado->id ?>')">
							<i class="fa fa-plus"></i> Vac / Lic / Inc <?= $horas->num_rows()==0?'':'('.$horas->row()->vacaciones_incapacidad_licencia.')' ?>
						</a>
					</td>
					<td>						
						<a href="javascript:;" onclick="horas_extras_recargos('<?= $empleado->id ?>')">
							<i class="fa fa-plus"></i> Hras. Extras <?= $horas->num_rows()==0?'':'('.$horas->row()->horas_extras_recargos.')' ?>
						</a>
					</td>
				</tr>				
			<?php endif ?>
		</table>
	</div>
</div>
<?php $this->load->view('calculos/modals/base',[],FALSE,'nomina'); ?>
<script>
	function spinner(){
		$("#modal .modal-body").html('<div class="text-center"><div class="spinner-border text-primary" role="status"><span class="sr-only">Cargando...</span></div></div>');
	}
	function vac_inc_lic($emp){		
		$("#modal .modal-title").html('Vacaciones, Incapacidades y Licencias');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/empleados_modals/'+$emp,{
			modal:'vacaciones_incapacidad_licencias',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	function horas_extras_recargos($emp){		
		$("#modal .modal-title").html('Extras y Recargos');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/empleados_modals/'+$emp,{
			modal:'horas_extras_recargos',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	window.afterLoad.push(function(){
		$(".filtering_form").on('submit',function(){
			document.location.reload();
		});
	});
</script>