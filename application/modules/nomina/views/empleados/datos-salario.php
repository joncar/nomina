<div class="card">
	<div class="card-header">
		Salario
		<a href="<?= base_url('nomina/empleados/empleados/empresa/'.$empleado->companias_id.'/edit/'.$empleado->id) ?>"  target="_blank">
			<i class="fa fa-edit"></i> Editar
		</a>
	</div>
	<div>	
		<table class="table mb-0">
			<tr>
				<th>Cantidad</th>
				<td><?= $empleado->salariof ?></td>
			</tr>
			<tr>
				<th>Período de pago</th>
				<td><?= $empleado->termino_contrato->nombre ?></td>
			</tr>
		</table>
	</div>
</div>