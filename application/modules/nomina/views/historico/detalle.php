<?php 
  get_instance()->hcss[] = "
    <style>
      .tab-content .card-body{
        overflow:auto;
      }
    </style>
  ";
?>
<div class="row ml-0 mr-0 justify-content-end">
  <div class="col-5 text-center">
    <h4 class="bg-info p-10">
      <i class="fa fa-calendar"></i>      
      Nómina del periodo: <?= $nomina->periodof ?>
    </h4>
  </div>
</div>
<div class="card">
  <div class="card-header">
    <?= $nomina->empresa->razon_social ?>
  </div>
  <div class="card-body">
    <div class="row ml-0 mr-0 justify-content-center">
      <div class="col-12 col-md-3">
        <div class="row">
          <div class="col-5 col-md-3 text-center color-secondary">
            <i class="fa fa-user fa-4x"></i>
          </div>
          <div class="col-7 col-md-9 color-black">
             <span>Total Pago a personas</span>
             <h3 class="m-0"><?= $nomina->total_personasf ?></h3>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-3">
        <div class="row">
          <div class="col-5 col-md-3 text-center color-secondary">
            <i class="fa fa-building fa-4x"></i>
          </div>
          <div class="col-7 col-md-9 color-black">
             <span>Costo total empresa</span>
             <h3 class="m-0"><?= $nomina->total_personasf ?></h3>
          </div>
        </div>
      </div>
      
        <div class="col-12"></div>
        <div class="col-12 col-md-7 text-center">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link color-secondary" href="<?= base_url('nomina/finiquito/resumen_nomina_xls/'.$nomina->id.'/export') ?>">
                <i class="fa fa-book"></i>
                Resumen de nomina
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link color-secondary" href="<?= base_url('nomina/finiquito/enviarColillas/'.$nomina->id) ?>">
                <i class="fa fa-envelope"></i>
                Enviar colillas
              </a>
            </li>
          </ul>
        </div>
      
    </div>
  </div>
</div>   

<?php if(!empty($_GET['msj'])): ?>
  <div class="alert alert-danger"><?= $_GET['msj'] ?></div>
<?php endif ?>
<?php if(!empty($_GET['msj2'])): ?>
  <div class="alert alert-success"><?= $_GET['msj2'] ?></div>
<?php endif ?>

<ul class="nav nav-pills mb-0" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="pills-empleados-tab" data-toggle="pill" href="#pills-empleados" role="tab" aria-controls="pills-empleados" aria-selected="true">Empleados <?= isset($nomina->cuerpo[1])?'<span class="badge badge-info">'.count($nomina->cuerpo[1]).'</span>':'' ?></span></a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-pensionados-tab" data-toggle="pill" href="#pills-pensionados" role="tab" aria-controls="pills-pensionados" aria-selected="false">Pensionados <?= isset($nomina->cuerpo[2])?'<span class="badge badge-info">'.count($nomina->cuerpo[2]).'</span>':'' ?></span></a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-aprendices-tab" data-toggle="pill" href="#pills-aprendices" role="tab" aria-controls="pills-aprendices" aria-selected="false">Aprendices <?= isset($nomina->cuerpo[3])?'<span class="badge badge-info">'.count($nomina->cuerpo[3]).'</span>':'' ?></span></a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-contratistas-tab" data-toggle="pill" href="#pills-contratistas" role="tab" aria-controls="pills-contratistas" aria-selected="false">Contratistas <?= isset($nomina->cuerpo[4])?'<span class="badge badge-info">'.count($nomina->cuerpo[4]).'</span>':'' ?></span></a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-ajustes-tab" data-toggle="pill" href="#pills-ajustes" role="tab" aria-controls="pills-ajustes" aria-selected="false">Ajustes adicionales</span></a>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Acciones</a>
    <div class="dropdown-menu">
      <!------------------ Enviar a aportes en linea ------------------------>
      <?php 
        $vinc = $this->db->get_where('companias_aportesenlinea',['companias_id'=>$nomina->companias_id]);
        if($nomina->liquidado_aportesenlinea==0 && $vinc->num_rows()>0 && !empty($vinc->row()->usuario_aportesnelinea) && !empty($vinc->row()->clave_aportesenlinea)): 
      ?>
      <a class="dropdown-item" href="<?= base_url('nomina/aportesEnLineaController/liquidarAportesEnLinea/'.$nomina->id) ?>"> <i class="fa fa-exclamation-triangle"></i> Liquidar Aportes en Linea</a>      
      <?php endif ?>  
      <!------------------- END ----------------------------->
      <!------------------- CONSULTAR ENVIO ----------------------------->
      <?php     
        if($vinc->num_rows()>0 && !empty($nomina->id_transaccion_aportesenlinea)): 
      ?>      
      <a class="dropdown-item" href="<?= base_url('nomina/aportesEnLineaController/consultarAportesEnLinea/'.$nomina->id) ?>"> <i class="fa fa-search"></i> Consultar Estado Aportes en Linea</a>      
      <?php endif ?>
      <a class="dropdown-item" href="<?= base_url('nomina/facturatech/enviar/'.$nomina->id) ?>"><i class="fa fa-envelope"></i> Enviar a FacturaTech</a>    
    </div>
  </li>
  
  <!---------------------- END -------------------------->
  <!--<li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-exclusion-tab" data-toggle="pill" href="#pills-exclusion" role="tab" aria-controls="pills-exclusion" aria-selected="false">Exclusión</a>
  </li>-->
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-empleados" role="tabpanel" aria-labelledby="pills-empleados-tab">
    <div class="card">
      <div class="card-body">
        <?php if(!isset($nomina->cuerpo[1])): ?>
          Sin Empleados asignados para esta nomina
        <?php else: ?>
        <?php echo sqlToHtml($nomina->cuerpo[1],[
          'empleado',
          'salario',
          'horas_extras_recargos',
          'vacaciones_incapacidad_licencia',
          'ingresos_adicionales',
          'deducciones_prestamos',
          'total_pagar',
          'pago_empleado'
        ],[],[
          'total_pagar'=>function($val,$row){
            return get_instance()->elements->fm($val);
          },
          'pago_empleado'=>function($val,$row){
            $acciones = '';
            $acciones.= '<a href="'.base_url('reportes/rep/verReportes/1/pdf/empleado/'.$row->id).'" target="_blank"><i class="fa fa-download"></i> Colilla</a>';
            if(!empty($row->facturatech)){
              $factura = json_decode($row->facturatech);
              if(!empty($factura->transactionID)){
                $acciones.= '<br/><a href="'.base_url('nomina/facturatech/download/'.$row->id).'" target="_blank"><i class="fa fa-download"></i> FacturaTech</a>';
              }
            }
            return $acciones;
          }
        ]); ?>
        <?php endif ?>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="pills-pensionados" role="tabpanel" aria-labelledby="pills-pensionados-tab">
    <div class="card">
      <div class="card-body">
        <?php if(!isset($nomina->cuerpo[2])): ?>
          Sin Empleados asignados para esta nomina
        <?php else: ?>
          <?php echo sqlToHtml($nomina->cuerpo[2],[
            'empleado',
            'salario',
            'horas_extras_recargos',
            'vacaciones_incapacidad_licencia',
            'ingresos_adicionales',
            'deducciones_prestamos',
            'total_pagar',
            'pago_empleado'
          ],[],[
          'total_pagar'=>function($val,$row){
            return get_instance()->elements->fm($val);
          },
          'pago_empleado'=>function($val,$row){
            $acciones = '';
            $acciones.= '<a href="'.base_url('reportes/rep/verReportes/1/pdf/empleado/'.$row->id).'" target="_blank"><i class="fa fa-download"></i> Colilla</a>';
            return $acciones;
          }
        ]); ?>
        <?php endif ?>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="pills-aprendices" role="tabpanel" aria-labelledby="pills-aprendices-tab">
    <div class="card">
      <div class="card-body">
        <?php if(!isset($nomina->cuerpo[3])): ?>
          Sin Empleados asignados para esta nomina
        <?php else: ?>
        <?php echo sqlToHtml($nomina->cuerpo[3],[
          'empleado',
          'salario',
          'horas_extras_recargos',
          'vacaciones_incapacidad_licencia',
          'ingresos_adicionales',
          'deducciones_prestamos',
          'total_pagar',
          'pago_empleado'
        ],[],[
          'total_pagar'=>function($val,$row){
            return get_instance()->elements->fm($val);
          },
          'pago_empleado'=>function($val,$row){
            $acciones = '';
            $acciones.= '<a href="'.base_url('reportes/rep/verReportes/1/pdf/empleado/'.$row->id).'" target="_blank"><i class="fa fa-download"></i> Colilla</a>';
            return $acciones;
          }
        ]); ?>
        <?php endif ?>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="pills-contratistas" role="tabpanel" aria-labelledby="pills-contratistas-tab">
    <div class="card">
      <div class="card-body">
        <?php if(!isset($nomina->cuerpo[4])): ?>
          Sin Empleados asignados para esta nomina
        <?php else: ?>
        <?php echo sqlToHtml($nomina->cuerpo[4],[
          'empleado',
          'salario',
          'horas_extras_recargos',
          'vacaciones_incapacidad_licencia',
          'ingresos_adicionales',
          'deducciones_prestamos',
          'total_pagar',
          'pago_empleado'
        ],[],[
          'total_pagar'=>function($val,$row){
            return get_instance()->elements->fm($val);
          },
          'pago_empleado'=>function($val,$row){
            $acciones = '';
            $acciones.= '<a href="'.base_url('reportes/rep/verReportes/1/pdf/empleado/'.$row->id).'" target="_blank"><i class="fa fa-download"></i> Colilla</a>';
            return $acciones;
          }
        ]); ?>
        <?php endif ?>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="pills-exclusion" role="tabpanel" aria-labelledby="pills-exclusion-tab">
    <div class="card">
      <div class="card-body">
        
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="pills-ajustes" role="tabpanel" aria-labelledby="pills-ajustes-tab">
    <div class="card">
      <div class="card-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th colspan="2">Ajustes adicionales de nomina</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Liquidar Prima</th>
              <td><?= empty($nomina->liquidar_prima)?'NO':'SI' ?></td>
            </tr>
            <tr>
              <th>Liquidar Cesantia</th>
              <td><?= empty($nomina->liquidar_cesantia)?'NO':'SI' ?></td>
            </tr>
            <tr>
              <th>Liquidar Intereses Cesantias</th>
              <td><?= empty($nomina->liquidar_interes_cesantia)?'NO':'SI' ?></td>
            </tr>
            <tr>
              <th>Liquidar Vacaciones</th>
              <td><?= empty($nomina->liquidar_vacaciones)?'NO':'SI' ?></td>
            </tr>
          </tbody>
        </table>        
      </div>
    </div>
  </div>
</div>