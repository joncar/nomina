<div class="card">
	<div class="card-header">
		Elegir Periodo de liquidación
	</div>
	<div class="card-body">
		<form action="" method="post">
			<?php echo @$msj ?>
			<div class="row ml-0 mr-0 justify-content-center">
				<div class="col-12 col-md-4">
					<label for="">Desde: *</label>
					<div class="form-group">
						<input type="date" class="form-control" name="desde" id="field-desde" value="<?= @$_POST['desde'] ?>">
					</div>
				</div>
				<div class="col-12 col-md-4">
					<label for="">Hasta: *</label>
					<div class="form-group">
						<input type="date" class="form-control" name="hasta" id="field-hasta" value="<?= @$_POST['hasta'] ?>">
					</div>
				</div>
				<div class="col-12 col-md-8 text-right">
					<button type="submit" class="btn btn-success">Seleccionar</button>
				</div>
			</div>
		</form>
	</div>
</div>