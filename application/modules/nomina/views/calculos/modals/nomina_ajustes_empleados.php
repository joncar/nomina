<?php if(empty($_POST['empleado']) || empty($_POST['periodo'])): ?>
	Trabajador no encontrado
<?php else: ?>
<?php 
	$emp = $_POST['empleado'];
	$ajustes = $this->elements->nomina_ajustes_empleados($emp,$_POST['periodo'][0],$_POST['periodo'][1]);
?>
<div>
	<form id="nomina_ajustes_empleados_form" onsubmit="return false;">	
		<table class="table table-bordered">
			<thead>
				<tr>
					<th colspan="2">Ajustes adicionales para el empleado</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>					
						<div class="row">
							<div class="col-12 col-md-4">
								Liquidar Prima 
							</div>
							<div class="col-12 col-md-4">
								Desde 
								<input type="date" value="<?= @$ajustes->prima_desde ?>" name="prima_desde" placeholder="Desde" class="form-control" style="display: inline-block;width: 80%;">
							</div>
							<div class="col-12 col-md-4">
								Hasta 
								<input type="date" value="<?= @$ajustes->prima_hasta ?>" name="prima_hasta" placeholder="Hasta" class="form-control" style="display: inline-block;width: 80%;">
							</div>
						</div>
					</th>
					<td>
						SI <input type="radio" name="liquidar_prima" value="1" style="position:initial;opacity:1" <?= $ajustes->liquidar_prima==1?'checked':'' ?>> <br/>
						NO <input type="radio" name="liquidar_prima" value="0" style="position:initial;opacity:1" <?= $ajustes->liquidar_prima==0?'checked':'' ?>>
					</td>
				</tr>
				<tr>
					<th>					
						<div class="row">
							<div class="col-12 col-md-4">
								Liquidar Cesantía
							</div>
							<div class="col-12 col-md-4">
								Desde 
								<input type="date" value="<?= @$ajustes->cesantia_desde ?>" name="cesantia_desde" placeholder="Desde" class="form-control" style="display: inline-block;width: 80%;">
							</div>
							<div class="col-12 col-md-4">
								Hasta 
								<input type="date" value="<?= @$ajustes->cesantia_hasta ?>" name="cesantia_hasta" placeholder="Hasta" class="form-control" style="display: inline-block;width: 80%;">
							</div>
						</div>
					</th>
					<td>
						SI <input type="radio" name="liquidar_cesantia" value="1" style="position:initial;opacity:1" <?= $ajustes->liquidar_cesantia==1?'checked':'' ?>> <br/>
						NO <input type="radio" name="liquidar_cesantia" value="0" style="position:initial;opacity:1" <?= $ajustes->liquidar_cesantia==0?'checked':'' ?>>
					</td>
				</tr>
				<tr>
					<th>
						
						<div class="row">
							<div class="col-12 col-md-4">
								Liquidar Intereses Cesantías
							</div>
							<div class="col-12 col-md-4">
								Desde 
								<input type="date" value="<?= @$ajustes->cesantia_interes_desde ?>" name="cesantia_interes_desde" placeholder="Desde" class="form-control" style="display: inline-block;width: 80%;">
							</div>
							<div class="col-12 col-md-4">
								Hasta 
								<input type="date" value="<?= @$ajustes->cesantia_interes_hasta ?>" name="cesantia_interes_hasta" placeholder="Hasta" class="form-control" style="display: inline-block;width: 80%;">
							</div>
						</div>
					</th>
					<td>
						SI <input type="radio" name="liquidar_interes_cesantia" value="1" style="position:initial;opacity:1" <?= $ajustes->liquidar_interes_cesantia==1?'checked':'' ?>> <br/>
						NO <input type="radio" name="liquidar_interes_cesantia" value="0" style="position:initial;opacity:1" <?= $ajustes->liquidar_interes_cesantia==0?'checked':'' ?>>
					</td>
				</tr>
				<tr>
					<th>
						
						<div class="row">
							<div class="col-12 col-md-4">
								Valor de Cesantías
							</div>
							<div class="col-12 col-md-4">
								Valor 
								<input type="text" value="<?= @$ajustes->cesantia_valor ?>" name="cesantia_valor" placeholder="0.00" class="form-control" style="display: inline-block;width: 80%;">
							</div>
						</div>
					</th>
					<td></td>
				</tr>
				<tr>
					<th>Liquidar Vacaciones</th>
					<td>
						SI <input type="radio" name="liquidar_vacaciones" value="1" style="position:initial;opacity:1" <?= $ajustes->liquidar_vacaciones==1?'checked':'' ?>> <br/>
						NO <input type="radio" name="liquidar_vacaciones" value="0" style="position:initial;opacity:1" <?= $ajustes->liquidar_vacaciones==0?'checked':'' ?>>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="response"></div>
	</form>
	<script>
		function saveModal(){
			var form = document.getElementById('nomina_ajustes_empleados_form');
			form = new FormData(form);
			info('#nominaAjustesEmpleados','Guardando su información por favor espere');
			remoteConnection('nomina/aportes/empleados_ajustes/<?= $ajustes->id ?>',form,function(data){
		        data = JSON.parse(data);
				if(data.success){
					$("#modal").modal('toggle');
					$(".filtering_form").trigger('submit');
				}else{
					error('#nomina_ajustes_empleados_form',data.msj);
				}
		    });
		}
	</script>
</div>
<?php endif ?>