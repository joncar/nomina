<div id="generarNomina" class="modal" tabindex="-1" id="modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Generar Nomina de empleados</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Esta seguro que desea generar la nomina?, esta acción no puede reversarse. Para cambios futuros a la misma deberá descartar estos datos y comenzar su generación nuevamente.
      </div>
      <div class="modal-footer text-right">     
      	<a class="btn btn-primary" href="<?= base_url('nomina/finiquito/generar_nomina/'.$this->empresa->id) ?>">Aceptar</a>      	   
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>