
<div class="card">
	<div class="alert alert-info">
		Ingresa aquí los días de incapacidad por enfermedad general o accidente de trabajo de la persona. Si tienes inquietudes ingresa a este <a href="<?= base_url() ?>calculo-incapacidades.html" target="_blank">artículo de nuestro centro de ayuda.</a>
	</div>
	<div style="overflow-x:auto">
		<table class="table">
			<tr>
				<tbody>								
					<tr>
						<th>Incapacidad por enfermedad general</th>
						<td>
							<div class="row">
								<div class="col-6">
									<label for="">Desde:</label>
									<div class="input-group">
										<input type="date" name="incapacidad[enfermedad_general][desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['enfermedad_general']->desde ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
								</div>
								<div class="col-6">
									<label for="">Hasta:</label>
									<div class="input-group">
										<input type="date" name="incapacidad[enfermedad_general][hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['enfermedad_general']->hasta ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
								    </div>
								</div>
								<input type="hidden" name="incapacidad[enfermedad_general][nombre]" value="Incapacidad por enfermedad general">
							</div>
							<div class="row d-none adjuntos">
								<div class="col-12">
									<?php $this->load->view('predesign/dropzone',[
										'alias'=>'enfermedadGeneral',
										'fieldNameOnCrud'=>'adjuntos',
										'name'=>"incapacidad[enfermedad_general][adjuntos]",
										"path"=>'adjuntos/',
										'handle'=>'nomina/aportes/inc_adjuntos',
										'files'=>!empty($aportes['enfermedad_general'])?explode(',',$aportes['enfermedad_general']->adjuntos):null
									]); ?>
								</div>							
							</div>													
						</td>
					</tr>
					<tr>
						<th>Incapacidad por enfermedad profesional o accidente de trabajo</th>
						<td>
							<div class="row">
								<div class="col-6">
									<label for="">Desde:</label>
									<div class="input-group">
										<input type="date" name="incapacidad[enfermedad_profesional_trabajo][desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['enfermedad_profesional_trabajo']->desde ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
								</div>
								<div class="col-6">
									<label for="">Hasta:</label>
									<div class="input-group">
										<input type="date" name="incapacidad[enfermedad_profesional_trabajo][hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['enfermedad_profesional_trabajo']->hasta ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
								</div>
								<input type="hidden" name="incapacidad[enfermedad_profesional_trabajo][nombre]" value="Incapacidad por enfermedad profesional o accidente de trabajo">
							</div>	
							<div class="row d-none adjuntos">
								<div class="col-12">
									<?php $this->load->view('predesign/dropzone',[
										'alias'=>'enfermedadProfesionalTrabajo',
										'fieldNameOnCrud'=>'adjuntos',
										'name'=>"incapacidad[enfermedad_profesional_trabajo][adjuntos]",
										"path"=>'adjuntos/',
										'handle'=>'nomina/aportes/inc_adjuntos',
										'files'=>!empty($aportes['enfermedad_profesional_trabajo']->adjuntos)?explode(',',$aportes['enfermedad_profesional_trabajo']->adjuntos):null
									]); ?>
								</div>							
							</div>											
						</td>
					</tr>
				</tbody>
			</tr>
		</table>
	</div>
</div>
