<div class="card">
	<div style="overflow-x:auto">
		<table class="table">
			<tr>
				<th>&nbsp;</th>
				<th class="text-center">Valor sobre hora ordinaria</th>
				<th class="text-center"># de horas</th>
			</tr>
			<tr>
				<td>Recargo Nocturno</td>
				<td class="text-center"><?= get_parameter('recargo_nocturno') ?></td>
				<td class="text-center"><input type="text" class="form-control text-right" placeholder="0" value="<?= @$aportes['recargo_nocturno']->cantidad ?>" name="charge[recargo_nocturno]"></td>
			</tr>
			<tr>
				<td>Recargo Dominical</td>
				<td class="text-center"><?= get_parameter('recargo_dominical') ?></td>
				<td class="text-center"><input type="text" class="form-control text-right" placeholder="0" value="<?= @$aportes['recargo_dominical']->cantidad ?>" name="charge[recargo_dominical]"></td>
			</tr>
			<tr>
				<td>Recargo Nocturno Dominical</td>
				<td class="text-center"><?= get_parameter('recargo_nocturno_dominical') ?></td>
				<td class="text-center"><input type="text" class="form-control text-right" placeholder="0" value="<?= @$aportes['recargo_nocturno_dominical']->cantidad ?>" name="charge[recargo_nocturno_dominical]"></td>
			</tr>	  			
		</table>
	</div>
</div>