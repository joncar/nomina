<div class="card">
	<div class="alert alert-info">
		Agrega aquí el valor a deducir por concepto de préstamos sobre el pago de la persona en el periodo. Si tienes dudas ingresa a <a href="<?= base_url() ?>calculo-prestamos.html" target="_blank">este artículo de nuestro centro de ayuda.</a>
	</div>
	<div style="overflow-x:auto">
		<table class="table">
			<thead>
					<tr>
						<th>Concepto</th>
						<th>$ Valor</th>
					</tr>
				</thead>
				<tbody>								
					<tr>
						<td>
							Prestamos
							<input type="hidden" name="prestamos[nombre][]" value="Prestamos">
						</td>
						<td>
							<input type="text" name="prestamos[valor][]" value="<?= @$aportes[2][0]->valor ?>" class="form-control text-right" placeholder="0">
						</td>
					</tr>
				</tbody>
		</table>
	</div>
</div>