<div class="card">
	<div style="overflow-x:auto">
		<table class="table">
			<tr>
				<th>&nbsp;</th>
				<th>Valor sobre hora ordinaria</th>
				<th># de horas</th>
				<th></th>
			</tr>
			<?php if(isset($aportes['extras'])): foreach($aportes['extras'] as $a): ?>
				<tr>
					<td><input type="text" class="form-control" name="charge[otros][nombre][]" value="<?= $a->nombre ?>" placeholder="Nueva hora extra"></td>
					<td><input type="text" class="form-control text-right" name="charge[otros][valor][]" value="<?= $a->valor ?>" placeholder="0"></td>
					<td><input type="text" class="form-control text-right" name="charge[otros][cantidad][]" value="<?= $a->cantidad ?>" placeholder="0"></td>
					<td>					
						<a href="javascript:;" onclick="window.origin.removeRow(this)" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
			<?php endforeach; endif; ?>
			<tr class="origin">
				<td><input type="text" class="form-control" name="charge[otros][nombre][]" value="" placeholder="Nueva hora extra"></td>
				<td><input type="text" class="form-control text-right" name="charge[otros][valor][]" value="" placeholder="0"></td>
				<td><input type="text" class="form-control text-right" name="charge[otros][cantidad][]" value="" placeholder="0"></td>
				<td>
					<a href="javascript:;" onclick="window.origin.addRow();" class="btn btn-success btn-xs"><i class="fa fa-plus"></i></a>
					<a href="javascript:;" onclick="window.origin.removeRow(this)" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
				</td>
			</tr>	  			
		</table>
	</div>
</div>

<script>
	var Origin = function(){
		this.origin = '';
		this.table = $("#otros-recargos-pill table");
		this.addRow = function(){
			this.table.append(this.origin.clone());
		}
		this.removeRow = function(el){
			$(el).parents('tr').remove();
		}
		this.init = function(){
			this.origin = $("#otros-recargos-pill .origin").clone();		
			this.origin = this.origin.removeClass('origin');
		}
	}
	window.origin = new Origin();
	window.origin.init();
</script>	