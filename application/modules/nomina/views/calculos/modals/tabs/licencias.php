<?php $tipo = $empleado->contrato->reporte; ?>
<div class="card">
	<div class="alert alert-info">
		Ingresa aquí los diferentes días de licencias que tuvo la persona en el periodo. Si tienes inquietudes ingresa a este <a href="<?= base_url() ?>calculo-licencias.html" target="_blank">artículo de nuestro centro de ayuda.</a>
	</div>
	<div  style="overflow-x:auto">
		<table class="table">
			<tr>
				<tbody>								
					<tr>
						<th>Licencia de maternidad</th>
						<td>
							<div class="row">
								<div class="col-6">
									<label for="">Desde:</label>
									<div class="input-group">									
										<input type="date" name="licencia[maternidad][desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['maternidad']->desde ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
								</div>
								<div class="col-6">
									<label for="">Hasta:</label>
									<div class="input-group">									
										<input type="date" name="licencia[maternidad][hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['maternidad']->hasta ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
									<input type="hidden" name="licencia[maternidad][nombre]" value="Licencia de maternidad">
								</div>							
							</div>
							<div class="row d-none adjuntos">
								<div class="col-12">
									<?php $this->load->view('predesign/dropzone',[
										'alias'=>'maternidad',
										'fieldNameOnCrud'=>'adjuntos',
										'name'=>"licencia[maternidad][adjuntos]",
										"path"=>'adjuntos/',
										'handle'=>'nomina/aportes/inc_adjuntos'
									]); ?>
								</div>							
							</div>
						</td>
					</tr>
					<tr>
						<th>Licencia de paternidad</th>
						<td>
							<div class="row">
								<div class="col-6">
									<label for="">Desde:</label>
									<div class="input-group">									
										<input type="date" name="licencia[paternidad][desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['paternidad']->desde ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
								</div>
								<div class="col-6">
									<label for="">Hasta:</label>
									<div class="input-group">									
										<input type="date" name="licencia[paternidad][hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['paternidad']->hasta ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
									<input type="hidden" name="licencia[paternidad][nombre]" value="Licencia de paternidad">
								</div>							
							</div>
							<div class="row d-none adjuntos">
								<div class="col-12">
									<?php $this->load->view('predesign/dropzone',[
										'alias'=>'paternidad',
										'fieldNameOnCrud'=>'adjuntos',
										'name'=>"incapacidad[paternidad][adjuntos]",
										"path"=>'adjuntos/',
										'handle'=>'nomina/aportes/inc_adjuntos'
									]); ?>
								</div>							
							</div>
						</td>
					</tr>
					<?php if($tipo==1 || $tipo==2): ?>
						<tr>
							<th>Licencia remunerada</th>
							<td>
								<div class="row">
									<div class="col-6">
										<label for="">Desde:</label>
										<div class="input-group">										
											<input type="date" name="licencia[remunerada][desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['remunerada']->desde ?>">
											<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
										</div>
									</div>
									<div class="col-6">
										<label for="">Hasta:</label>
										<div class="input-group">										
											<input type="date" name="licencia[remunerada][hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['remunerada']->hasta ?>">
											<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
										</div>
										<input type="hidden" name="licencia[remunerada][nombre]" value="Licencia remunerada">
									</div>							
								</div>
								<div class="row d-none adjuntos">
								<div class="col-12">
									<?php $this->load->view('predesign/dropzone',[
										'alias'=>'remunerada',
										'fieldNameOnCrud'=>'adjuntos',
										'name'=>"incapacidad[remunerada][adjuntos]",
										"path"=>'adjuntos/',
										'handle'=>'nomina/aportes/inc_adjuntos'
									]); ?>
								</div>							
							</div>
							</td>
						</tr>
						<tr>
							<th>Licencia no remunerada</th>
							<td>
								<div class="row">
									<div class="col-6">
										<label for="">Desde:</label>
										<div class="input-group">										
											<input type="date" name="licencia[no_remunerada][desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['no_remunerada']->desde ?>">
											<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
										</div>
									</div>
									<div class="col-6">
										<label for="">Hasta:</label>
										<div class="input-group">										
											<input type="date" name="licencia[no_remunerada][hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['no_remunerada']->hasta ?>">
											<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
										</div>
										<input type="hidden" name="licencia[no_remunerada][nombre]" value="Licencia no remunerada">
									</div>							
								</div>
								<div class="row d-none adjuntos">
								<div class="col-12">
									<?php $this->load->view('predesign/dropzone',[
										'alias'=>'no_remunerada',
										'fieldNameOnCrud'=>'adjuntos',
										'name'=>"incapacidad[no_remunerada][adjuntos]",
										"path"=>'adjuntos/',
										'handle'=>'nomina/aportes/inc_adjuntos'
									]); ?>
								</div>							
							</div>
							</td>
						</tr>
					<?php endif ?>
					<tr>
						<th>Suspensión</th>
						<td>
							<div class="row">
								<div class="col-6">
									<label for="">Desde:</label>
									<div class="input-group">									
										<input type="date" name="licencia[suspension][desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['suspension']->desde ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
								</div>
								<div class="col-6">
									<label for="">Hasta:</label>
									<div class="input-group">									
										<input type="date" name="licencia[suspension][hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['suspension']->hasta ?>">
										<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
									</div>
									<input type="hidden" name="licencia[suspension][nombre]" value="Suspensión">
								</div>							
							</div>
							<div class="row d-none adjuntos">
								<div class="col-12">
									<?php $this->load->view('predesign/dropzone',[
										'alias'=>'suspension',
										'fieldNameOnCrud'=>'adjuntos',
										'name'=>"incapacidad[suspension][adjuntos]",
										"path"=>'adjuntos/',
										'handle'=>'nomina/aportes/inc_adjuntos'
									]); ?>
								</div>							
							</div>
						</td>
					</tr>
					<?php if($tipo==1 || $tipo==2): ?>
						<tr>
							<th>Ausencia injustificada</th>
							<td>
								<div class="row">
									<div class="col-6">
										<label for="">Desde:</label>
										<div class="input-group">										
											<input type="date" name="licencia[ausencia_injustificada][desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['ausencia_injustificada']->desde ?>">
											<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
										</div>
									</div>
									<div class="col-6">
										<label for="">Hasta:</label>
										<div class="input-group">										
											<input type="date" name="licencia[ausencia_injustificada][hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['ausencia_injustificada']->hasta ?>">
											<div class="input-group-append">
									       <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
									    </div>
										</div>
										<input type="hidden" name="licencia[ausencia_injustificada][nombre]" value="Ausencia injustificada">
									</div>							
								</div>
								<div class="row d-none adjuntos">
								<div class="col-12">
									<?php $this->load->view('predesign/dropzone',[
										'alias'=>'ausencia_injustificada',
										'fieldNameOnCrud'=>'adjuntos',
										'name'=>"incapacidad[ausencia_injustificada][adjuntos]",
										"path"=>'adjuntos/',
										'handle'=>'nomina/aportes/inc_adjuntos'
									]); ?>
								</div>							
							</div>
							</td>
						</tr>
					<?php endif?>
				</tbody>
			</tr>
		</table>
	</div>
</div>