<div class="card">
	<div class="alert alert-info">
		Ingresa aquí los días de vacaciones disfrutados o compensados en dinero durante el periodo. Si tienes inquietudes <a href="<?= base_url() ?>calculo-vacaciones.html" target="_new">ingresa a este artículo de nuestro centro de ayuda.</a>
	</div>
	<div  style="overflow-x:auto">
		<table class="table">
			<tr>
				<tbody>				
					<tr>
						<th>Vacaciones acumuladas</th>
						<td class="text-center"><?= $empleado->vacaciones ?></td>
					</tr>
					<tr>
						<th>Días disfrutados en el período</th>
						<td>
							<div class="row">
								<div class="col-6">
									<label for="">Desde:</label>
									<input type="date" name="vacaciones[desde]" class="form-control" placeholder="Desde" value="<?= @$aportes['vacaciones']->desde ?>">
								</div>
								<div class="col-6">
									<label for="">Hasta:</label>
									<input type="date" name="vacaciones[hasta]" class="form-control" placeholder="Hasta" value="<?= @$aportes['vacaciones']->hasta ?>">
								</div>
							</div>												
						</td>
					</tr>
					<tr>
						<th>Días compensados en dinero y NO disfrutados</th>
						<td>
							<input type="text" name="vacaciones[dias_compensados]" class="form-control text-right" placeholder="0" value="<?= @$aportes['vacaciones']->dias_compensados ?>">
						</td>
					</tr>
				</tbody>
			</tr>
		</table>
	</div>
</div>