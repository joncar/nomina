<div class="card">
	<div class="alert alert-info">
		Agrega aquí el valor a deducir por concepto de retención en la fuente por salarios sobre el pago de la persona en el periodo. Si deseas que Nominapp calcule automáticamente este valor o tienes dudas ingresa a  <a href="<?= base_url() ?>calculo-prestamos.html" target="_blank">este artículo de nuestro centro de ayuda.</a>
	</div>
	<div style="overflow-x:auto">
		<table class="table">
			<thead>
					<tr>
						<th>Concepto</th>
						<th>$ Valor</th>
					</tr>
				</thead>
				<tbody>								
					<tr>
						<td>
							Retención en la fuente
							<input type="hidden" name="otros[nombre][]" value="Retención en la fuente">
						</td>
						<td>
							<input type="text" name="otros[valor][]" value="<?= @$aportes[3][0]->valor ?>" class="form-control text-right" placeholder="0">
						</td>
					</tr>
				</tbody>
		</table>
	</div>
</div>