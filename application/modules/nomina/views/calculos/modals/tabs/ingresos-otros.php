<div class="card" style="overflow-x:auto">
	<div class="alert alert-info">
		Agrega aquí el número de días que la persona laboró desde su domicilio en el periodo para que le sean reconocidos como Auxilio de conectividad. Esta es una medida de contingencia decretada a causa del COVID-19. Si tienes dudas ingresa a <a href="<?= base_url() ?>calculo-ingresos-auxiliar.html" target="_blank">este artículo de nuestro centro de ayuda.</a>
	</div>
	<div  style="overflow-x:auto">
		<table class="table">
			<thead>
					<tr>
						<th>Concepto</th>
						<th>Días</th>
					</tr>
				</thead>
				<tbody>								
					<tr>
						<td>
							Auxilio de conectividad
							<input type="hidden" name="otros[nombre][]" value="Auxilio de conectividad">
						</td>
						<td>
							<input type="text" name="otros[valor][]" value="<?= @$aportes[3][0]->valor ?>" class="form-control text-right" placeholder="0">
						</td>
					</tr>
				</tbody>
		</table>
	</div>
</div>