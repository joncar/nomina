<div class="card">
	<div style="overflow-x:auto">
		<table class="table">
			<tr>
				<th>&nbsp;</th>
				<th class="text-center">Valor sobre hora ordinaria</th>
				<th class="text-center"># de horas</th>
			</tr>
			<tr>
				<td>Hora Extra Ordinaria</td>
				<td class="text-center"><?= get_parameter('hora_extra_ordinaria') ?></td>
				<td class="text-center"><input type="text" class="form-control text-right" placeholder="0" value="<?= @$aportes['hora_extra_ordinaria']->cantidad ?>" name="charge[hora_extra_ordinaria]"></td>
			</tr>
			<tr>
				<td>Hora Extra Nocturna</td>
				<td class="text-center"><?= get_parameter('hora_extra_nocturna') ?></td>
				<td class="text-center"><input type="text" class="form-control text-right" placeholder="0" value="<?= @$aportes['hora_extra_nocturna']->cantidad ?>" name="charge[hora_extra_nocturna]"></td>
			</tr>
			<tr>
				<td>Hora Extra Ordinaria Dominical</td>
				<td class="text-center"><?= get_parameter('hora_extra_ordinaria_dominical') ?></td>
				<td class="text-center"><input type="text" class="form-control text-right" placeholder="0" value="<?= @$aportes['hora_extra_ordinaria_dominical']->cantidad ?>" name="charge[hora_extra_ordinaria_dominical]"></td>
			</tr>
			<tr>
				<td>Hora Extra Nocturna Dominical</td>
				<td class="text-center"><?= get_parameter('hora_extra_nocturna_dominical') ?></td>
				<td class="text-center"><input type="text" class="form-control text-right" placeholder="0" value="<?= @$aportes['hora_extra_nocturna_dominical']->cantidad ?>" name="charge[hora_extra_nocturna_dominical]"></td>
			</tr>	  			
		</table>
	</div>
</div>