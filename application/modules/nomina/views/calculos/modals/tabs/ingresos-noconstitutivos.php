<div class="card">
	<div class="alert alert-info">
		Agrega aquí todos los ingresos que NO sumarán para la base de seguridad social y prestaciones. Si tienes dudas ingresa a <a href="<?= base_url() ?>calculo-ingresos-no-constitutivos.html" target="_blank">este artículo de nuestro centro de ayuda.</a>
	</div>
	<div  style="overflow-x:auto">
		<table class="table">
			<thead>
					<tr>
						<th>Concepto</th>
						<th>$ Valor</th>
						<th></th>
					</tr>
				</thead>
				<tbody>		
					<?php if(isset($aportes[2])): foreach($aportes[2] as $a): ?>
						<tr>
							<td>
								<?php 
									$this->db->where('tipo',2);
									echo form_dropdown_from_query('noconstitutivo[id][]','recurrente_ingresos_conceptos','id','nombre',$a->recurrente_ingresos_conceptos_id); 
								?>
								<?php 
									$dnone = 'd-none';
									$req = $this->db->get_where('recurrente_ingresos_conceptos',['id'=>$a->recurrente_ingresos_conceptos_id]);
									if($req->num_rows()>0 && $req->row()->requiere_mas_detalles == 1){
										$dnone = '';
									}
								?>
								<input type="text" name="noconstitutivo[mas_detalles][]" class="form-control mas_detalles2 <?= $dnone ?>" placeholder="Especifique" value="<?= $a->mas_detalles ?>">
							</td>
							<td>
								<input type="text" name="noconstitutivo[valor][]" class="form-control text-right" placeholder="0" value="<?= $a->valor ?>">
							</td>
							<td>							
								<a href="javascript:;" onclick="window.origin.removeRow(this)" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
					<?php endforeach; endif; ?>							
					<tr class="origin">
						<td>
							<?php 
								$this->db->where('tipo',2);
								echo form_dropdown_from_query('noconstitutivo[id][]','recurrente_ingresos_conceptos','id','nombre',''); 
							?>
							<input type="text" name="noconstitutivo[mas_detalles][]" class="form-control mas_detalles2 d-none" placeholder="Especifique" value="">
						</td>
						<td>
							<input type="text" name="noconstitutivo[valor][]" value="" class="form-control text-right" placeholder="0">
						</td>
						<td>
							<a href="javascript:;" onclick="window.origin2.addRow();" class="btn btn-success btn-xs"><i class="fa fa-plus"></i></a>
							<a href="javascript:;" onclick="window.origin2.removeRow(this)" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				</tbody>
		</table>
	</div>
</div>

<script>	
	window.origin2 = new Origin('#noconstitutivos-pill');
	window.origin2.init();
	$(document).on('change',"select[name='noconstitutivo[id][]']",function(){
		var sel = $($(this).find('option:selected'));
		var mas_detalles = sel.data('requiere_mas_detalles');
		if(mas_detalles==1){
			$(this).parents('tr').find('.mas_detalles2').removeClass('d-none');
		}else{
			$(this).parents('tr').find('.mas_detalles2').addClass('d-none');
			$(this).parents('tr').find('.mas_detalles2').val('');
		}
	});
</script>	