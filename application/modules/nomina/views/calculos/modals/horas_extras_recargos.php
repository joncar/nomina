<?php if(empty($_POST['empleado']) || empty($_POST['periodo'])): ?>
	Trabajador no encontrado
<?php else: ?>
<?php $aportes = $this->elements->nomina_aportes($_POST['empleado'],$_POST['periodo']) ?>
<p class="alert alert-info">
	Ingresa aquí el número de horas extras o recargos. Si tienes inquietudes <a href="<?= base_url() ?>horas-recargos.html" target="_blank">ingresa a este artículo de nuestro centro de ayuda.</a>
</p>
<form id="horas_extras_recargos_form" action="" onsubmit="return false">
	<ul class="nav nav-pills mb-3" id="horas_extras_recargosPill" role="tablist">
	  <li class="nav-item" role="presentation">
	    <a class="nav-link active" id="horas_extras-pill-tab" data-toggle="pill" href="#horas_extras-pill" role="tab" aria-controls="horas_extras-pill" aria-selected="true">Horas Extras</a>
	  </li>
	  <li class="nav-item" role="presentation">
	    <a class="nav-link" id="recargos-pill-tab" data-toggle="pill" href="#recargos-pill" role="tab" aria-controls="recargos-pill" aria-selected="false">Recargos</a>
	  </li>
	  <li class="nav-item" role="presentation">
	    <a class="nav-link" id="otros-recargos-pill-tab" data-toggle="pill" href="#otros-recargos-pill" role="tab" aria-controls="otros-recargos-pill" aria-selected="false">Otros conceptos</a>
	  </li>
	</ul>
	<div class="tab-content" id="horas_extras_recargosPillContent">  
	  <div class="tab-pane fade show active" id="horas_extras-pill" role="tabpanel" aria-labelledby="horas_extras-pill-tab">
	  	<?php $this->load->view('calculos/modals/tabs/horas_extras_recargos_horas',['aportes'=>$aportes]); ?>
	  </div>
	  <div class="tab-pane fade" id="recargos-pill" role="tabpanel" aria-labelledby="recargos-pill-tab">
	  	<?php $this->load->view('calculos/modals/tabs/horas_extras_recargos_recargos',['aportes'=>$aportes]); ?>	  	
	  </div>
	  <div class="tab-pane fade" id="otros-recargos-pill" role="tabpanel" aria-labelledby="otros-recargos-pill-tab">
	  	<?php $this->load->view('calculos/modals/tabs/horas_extras_recargos_otros',['aportes'=>$aportes]); ?>	  	
	  </div>
	  <input type="hidden" name="empleados_id" value="<?= $_POST['empleado'] ?>">
	  <input type="hidden" name="companias_id" value="<?= get_instance()->empresa->id ?>">  
	  <input type="hidden" name="periodo[]" value="<?= $_POST['periodo'][0] ?>">  
	  <input type="hidden" name="periodo[]" value="<?= $_POST['periodo'][1] ?>">  
	</div>
	<div id="horasExtrasRecargoResponse"></div>
</form>
<script>
	function saveModal(){
		var form = document.getElementById('horas_extras_recargos_form');
		form = new FormData(form);
		info('#horasExtrasRecargoResponse','Guardando su información por favor espere');
		remoteConnection('nomina/aportes/horas_extras_recargos',form,function(data){
	        data = JSON.parse(data);
			if(data.success){
				$("#modal").modal('toggle');
				$(".filtering_form").trigger('submit');
			}else{
				error('#horasExtrasRecargoResponse',data.msj);
			}
	    });
	}
</script>
<?php endif ?>