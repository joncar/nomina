<?php if(empty($_POST['empleado']) || empty($_POST['periodo'])): ?>
	Trabajador no encontrado
<?php else: ?>
<?php 
	$empleado = $this->CalculosModel->get($_POST['empleado']);
	$aportes = $this->elements->deducciones($_POST['empleado'],$_POST['periodo']) ?>
<?php if(!$empleado){echo 'Empleado no encontrado'; die(); } ?>
<?php $tipo = $empleado->contrato->reporte; ?>
<form id="deducciones_form" action="" onsubmit="return false">
	<ul class="nav nav-pills mb-0" id="deduccionesPill" role="tablist">
	  <li class="nav-item" role="presentation">
	    <a class="nav-link active" id="deducciones-pill-tab" data-toggle="pill" href="#deducciones-pill" role="tab" aria-controls="deducciones-pill" aria-selected="true">Deducciones</a>
	  </li>
	  <li class="nav-item" role="presentation">
	    <a class="nav-link" id="prestamos-pill-tab" data-toggle="pill" href="#prestamos-pill" role="tab" aria-controls="prestamos-pill" aria-selected="false">Prestamos</a>
	  </li>
	  <?php if($tipo==1 || $tipo==2): ?>
		  <li class="nav-item" role="presentation">
		    <a class="nav-link" id="otros-pill-tab" data-toggle="pill" href="#otros-pill" role="tab" aria-controls="otros-pill" aria-selected="false">Retefuente</a>
		  </li>
	  <?php endif ?>
	</ul>
	<div class="tab-content" id="deduccionesPillContent">  
	  <div class="tab-pane fade show active" id="deducciones-pill" role="tabpanel" aria-labelledby="constitutivos-pill-tab">
	  	<?php $this->load->view('calculos/modals/tabs/deducciones-deducciones',['aportes'=>$aportes,'empleado'=>$empleado]); ?>
	  </div>
	  <div class="tab-pane fade" id="prestamos-pill" role="tabpanel" aria-labelledby="prestamos-pill-tab">
	  	<?php $this->load->view('calculos/modals/tabs/deducciones-prestamos',['aportes'=>$aportes,'empleado'=>$empleado]); ?>	  	
	  </div>
	  <?php if($tipo==1 || $tipo==2): ?>
		  <div class="tab-pane fade" id="otros-pill" role="tabpanel" aria-labelledby="otros-pill-tab">
		  	<?php $this->load->view('calculos/modals/tabs/deducciones-otros',['aportes'=>$aportes,'empleado'=>$empleado]); ?>	  	
		  </div>
	  <?php endif ?>
	  <input type="hidden" name="empleados_id" value="<?= $_POST['empleado'] ?>">
	  <input type="hidden" name="companias_id" value="<?= get_instance()->empresa->id ?>">  
	  <input type="hidden" name="periodo[]" value="<?= $_POST['periodo'][0] ?>">  
	  <input type="hidden" name="periodo[]" value="<?= $_POST['periodo'][1] ?>">  
	</div>
	<div id="horasExtrasRecargoResponse"></div>
</form>
<script>
	function saveModal(){
		var form = document.getElementById('deducciones_form');
		form = new FormData(form);
		info('#horasExtrasRecargoResponse','Guardando su información por favor espere');
		remoteConnection('nomina/aportes/deducciones',form,function(data){
	        data = JSON.parse(data);
			if(data.success){
				$("#modal").modal('toggle');
				$(".filtering_form").trigger('submit');
			}else{
				error('#horasExtrasRecargoResponse',data.msj);
			}
	    });
	}

	var Origin = function(content){
		this.origin = '';
		this.table = $(content+" table tbody");
		this.addRow = function(){
			this.table.append(this.origin.clone());
		}
		this.removeRow = function(el){
			$(el).parents('tr').remove();
		}
		this.init = function(){
			this.origin = $(content+" .origin").clone();		
			this.origin = this.origin.removeClass('origin');
		}
	}
</script>
<?php endif ?>