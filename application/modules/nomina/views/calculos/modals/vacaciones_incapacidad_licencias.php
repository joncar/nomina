<?php if(empty($_POST['empleado']) || empty($_POST['periodo'])): ?>
	Trabajador no encontrado
<?php else: ?>
<?php 
	$empleado = $this->CalculosModel->get($_POST['empleado']);
	$aportes = $this->elements->nomina_vac_inc_lic($_POST['empleado'],$_POST['periodo']) ?>	
<?php if(!$empleado){echo 'Empleado no encontrado'; die(); } ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" type="text/css" rel="stylesheet" />
<style>
	.dropzone{
		padding: 0;
		border: 1px solid #e2e5ed;
	}
	.dropzone .dz-preview .dz-image{
		border-radius:0;
	}
	.dz-error-message{
		top: 60px !important;
		opacity: 1 !important;
	}

	.dropzone.dz-started .dz-message {
	    display: block;
	    position: absolute;
	    width: 100%;
	}
	.dropzone .dz-preview .dz-image img {
	    max-width:100%;
	}

	.dropzone .dz-message {
	   font-size: 30px;
	}

	.dropzone.dz-started .dz-message {		    
	    width: 80%;
	}

	.dz-default.dz-message{
		font-size:16px;
	}

	@media screen and (max-width:480px){
		.dropzone .dz-message {
		    font-size: 17px;
		    padding: 0 20px;
		}

		.dropzone.dz-started .dz-message {			    
		    width: 90%;
		}
	}
</style>
<?php $tipo = $empleado->contrato->reporte; ?>
<script>
	window.afterLoad = [];
</script>
<div class="row ml-0 mr-0 justify-content-center">
	<?php if($tipo==1 || $tipo==2): ?>
		<div class="col-12 col-md-6 text-center">
			<span>Salario base vacaciones</span><br/>
		    <h3 class="m-0"><?= $empleado->salariof ?></h3>
		</div>
	<?php endif ?>
	<div class="col-12 col-md-6 text-center">
		<span>Salario base incapacidades y licencias</span><br/>
		<h3 class="m-0"><?= $empleado->salariof ?></h3>
	</div>
</div>
<div class="mb-30"></div>
<form id="vac_inc_lic_form" action="" onsubmit="return false">
	<ul class="nav nav-pills mb-0" id="vac_inc_licPill" role="tablist">
	  <?php if($tipo==1 || $tipo==2): ?>
		  <li class="nav-item" role="presentation">
		    <a class="nav-link active" id="vacaciones-pill-tab" data-toggle="pill" href="#vacaciones-pill" role="tab" aria-controls="vacaciones-pill" aria-selected="true">Vacaciones</a>
		  </li>
	  <?php endif ?>
	  <li class="nav-item" role="presentation">
	    <a class="nav-link <?= $tipo>2?' active':'' ?>" id="incapacidad-pill-tab" data-toggle="pill" href="#incapacidad-pill" role="tab" aria-controls="incapacidad-pill" aria-selected="false">Incapacidades</a>
	  </li>
	  <li class="nav-item" role="presentation">
	    <a class="nav-link" id="licencias-pill-tab" data-toggle="pill" href="#licencias-pill" role="tab" aria-controls="licencias-pill" aria-selected="false">Licencias</a>
	  </li>
	</ul>
	<div class="tab-content" id="vac_inc_licPillContent">  
	  <?php if($tipo==1 || $tipo==2): ?>
		  <div class="tab-pane fade show active" id="vacaciones-pill" role="tabpanel" aria-labelledby="vacaciones-pill-tab">
		  	<?php $this->load->view('calculos/modals/tabs/vacaciones',['aportes'=>$aportes,'empleado'=>$empleado]); ?>
		  </div>
	  <?php endif ?>
	  <div class="tab-pane fade <?= $tipo>2?' show active':'' ?>" id="incapacidad-pill" role="tabpanel" aria-labelledby="incapacidad-pill-tab">
	  	<?php $this->load->view('calculos/modals/tabs/incapacidades',['aportes'=>$aportes,'empleado'=>$empleado]); ?>	  	
	  </div>
	  <div class="tab-pane fade" id="licencias-pill" role="tabpanel" aria-labelledby="licencias-pill-tab">
	  	<?php $this->load->view('calculos/modals/tabs/licencias',['aportes'=>$aportes,'empleado'=>$empleado]); ?>	  	
	  </div>
	  <input type="hidden" name="empleados_id" value="<?= $_POST['empleado'] ?>">
	  <input type="hidden" name="companias_id" value="<?= get_instance()->empresa->id ?>">  
	  <input type="hidden" name="periodo[]" value="<?= $_POST['periodo'][0] ?>">  
	  <input type="hidden" name="periodo[]" value="<?= $_POST['periodo'][1] ?>">  
	</div>
	<div id="horasExtrasRecargoResponse"></div>
</form>
<script>
	function saveModal(){
		var form = document.getElementById('vac_inc_lic_form');
		form = new FormData(form);
		info('#horasExtrasRecargoResponse','Guardando su información por favor espere');
		remoteConnection('nomina/aportes/vac_inc_lic',form,function(data){
	        data = JSON.parse(data);
			if(data.success){
				$("#modal").modal('toggle');
				$(".filtering_form").trigger('submit');
			}else{
				error('#horasExtrasRecargoResponse',data.msj);
			}
	    });
	}
</script>
<script>
	$(".input-group-append button").on("click",function(){
		var adjRow = $($(this).parents('tr').find('.adjuntos'));
		if(adjRow.hasClass('d-none')){
			adjRow.removeClass('d-none');
		}else{
			adjRow.addClass('d-none')
		}
	});	
	if(typeof(Dropzone)=='undefined'){
		var o = document.createElement('script');
		o.src = '//cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js';
		o.addEventListener('load',function(){cargarZones();});
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(o,s);		
	}else{
		cargarZones();
	}

	function cargarZones(){
		$(enfermedadGeneral).dropzone();
		$(enfermedadProfesionalTrabajo).dropzone();
		$(maternidad).dropzone();
		$(paternidad).dropzone();
		$(remunerada).dropzone();
		$(no_remunerada).dropzone();
		$(suspension).dropzone();
		$(ausencia_injustificada).dropzone();
		window.afterLoad[0]();
		window.afterLoad[1]();
		window.afterLoad[2]();
		window.afterLoad[3]();
		window.afterLoad[4]();
		window.afterLoad[5]();
		window.afterLoad[6]();
		window.afterLoad[7]();
		window.afterLoad = [];
	}
</script>
<?php endif ?>