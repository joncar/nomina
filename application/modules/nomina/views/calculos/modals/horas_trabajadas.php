<?php if(empty($_POST['empleado']) || empty($_POST['periodo'])): ?>
	Trabajador no encontrado
<?php else: ?>
<?php $aportes = $this->elements->nomina_aportes($_POST['empleado'],$_POST['periodo']) ?>
<p class="alert alert-info">
	Ingresa aquí el número de horas extras o recargos. Si tienes inquietudes <a href="<?= base_url() ?>horas-recargos.html" target="_blank">ingresa a este artículo de nuestro centro de ayuda.</a>
</p>
<form id="horas_extras_recargos_form" action="" onsubmit="return false">
	<div class="row ml-0 mr-0">
		<div class="col-12">
			<label for="">Horas Trabajadas</label>
			<input type="text" name="horas_trabajadas" class="form-control" value="<?= @$aportes['horas_trabajadas']->cantidad ?>">
		</div>
	</div>
	<input type="hidden" name="empleados_id" value="<?= $_POST['empleado'] ?>">
    <input type="hidden" name="companias_id" value="<?= get_instance()->empresa->id ?>">  
    <input type="hidden" name="periodo[]" value="<?= $_POST['periodo'][0] ?>">  
    <input type="hidden" name="periodo[]" value="<?= $_POST['periodo'][1] ?>">  
	<div id="horasExtrasRecargoResponse"></div>
</form>
<script>
	function saveModal(){
		var form = document.getElementById('horas_extras_recargos_form');
		form = new FormData(form);
		info('#horasExtrasRecargoResponse','Guardando su información por favor espere');
		remoteConnection('nomina/aportes/horas_trabajadas',form,function(data){
	        data = JSON.parse(data);
			if(data.success){
				$("#modal").modal('toggle');
				$(".filtering_form").trigger('submit');
			}else{
				error('#horasExtrasRecargoResponse',data.msj);
			}
	    });
	}
</script>
<?php endif ?>