<div class="row ml-0 mr-0 justify-content-end">
	<div class="col-12 col-md-5 text-center">
		<h4 class="bg-info p-10">
			<i class="fa fa-calendar"></i>			
			Nómina del periodo: <?= $empresa->periodof ?> <a href="<?= base_url() ?>nomina/elegir_periodo/<?= $empresa->id ?>">Cambiar</a>
		</h4>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<?= $empresa->razon_social ?>
	</div>
	<div class="card-body">
		<div class="row ml-0 mr-0 justify-content-center">
			<div class="col-12 col-md-3">
				<div class="row">
					<div class="col-5 col-md-3 text-center color-secondary">
						<i class="fa fa-user fa-4x"></i>
					</div>
					<div class="col-7 col-md-9 color-black">
					   <span>Total Pago a personas</span>
					   <h3 class="m-0"><?= $this->CalculosModel->getTotalNomina($empresa)[1] ?> <a href="<?= base_url('nomina/liquidar_nomina/'.$empresa->id.'/refreshContadores') ?>"><i class="fa fa-refresh"></i></a></h3>					   
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3">
				<div class="row">
					<div class="col-5 col-md-3 text-center color-secondary">
						<i class="fa fa-building fa-4x"></i>
					</div>
					<div class="col-7 col-md-9 color-black">
					   <span>Costo total empresa</span>
					   <h3 class="m-0"><?= $this->CalculosModel->getTotalNomina($empresa)[1] ?></h3>
					</div>
				</div>
			</div>
			<?php if(!$simulacion): ?>
				<div class="col-12"></div>
				<div class="col-12 col-md-7">
					<ul class="nav">
					  <li class="nav-item">
					    <a class="nav-link color-secondary" href="#">
					    	<i class="fa fa-book"></i>
					    	Resumen de nomina
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link color-secondary" href="#">
					    	<i class="fa fa-calendar"></i>
					    	Reporte novedades
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link color-secondary" href="#">
					    	<i class="fa fa-upload"></i>
					    	Cargar novedades
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link color-secondary" href="#">
					    	<i class="fa fa-handshake-o"></i>
					    	Prestaciones sociales
					    </a>
					  </li>
					</ul>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>		

<ul class="nav nav-pills mb-0 flex-wrap" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="pills-empleados-tab" data-toggle="pill" href="#pills-empleados" role="tab" aria-controls="pills-empleados" aria-selected="true">Empleados</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-pensionados-tab" data-toggle="pill" href="#pills-pensionados" role="tab" aria-controls="pills-pensionados" aria-selected="false">Pensionados</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-aprendices-tab" data-toggle="pill" href="#pills-aprendices" role="tab" aria-controls="pills-aprendices" aria-selected="false">Aprendices</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-contratistas-tab" data-toggle="pill" href="#pills-contratistas" role="tab" aria-controls="pills-contratistas" aria-selected="false">Contratistas</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-exclusion-tab" data-toggle="pill" href="#pills-exclusion" role="tab" aria-controls="pills-exclusion" aria-selected="false">Exclusión</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-ajustes-tab" data-toggle="pill" href="#pills-ajustes" role="tab" aria-controls="pills-ajustes" aria-selected="false">Ajustes adicionales</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-empleados" role="tabpanel" aria-labelledby="pills-empleados-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php echo $output; ?>
  		</div>
  	</div>
  </div>
  <div class="tab-pane fade" id="pills-pensionados" role="tabpanel" aria-labelledby="pills-pensionados-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php $this->load->view('calculos/tablas/pensionados'); ?>
  		</div>
  	</div>
  </div>
  <div class="tab-pane fade" id="pills-aprendices" role="tabpanel" aria-labelledby="pills-aprendices-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php $this->load->view('calculos/tablas/aprendices'); ?>
  		</div>
  	</div>
  </div>
  <div class="tab-pane fade" id="pills-contratistas" role="tabpanel" aria-labelledby="pills-contratistas-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php $this->load->view('calculos/tablas/contratistas'); ?>
  		</div>
  	</div>
  </div>
  <div class="tab-pane fade" id="pills-exclusion" role="tabpanel" aria-labelledby="pills-exclusion-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php $this->load->view('calculos/tablas/exclusion_empleados'); ?>
  		</div>
  	</div>
  </div>
  <div class="tab-pane fade" id="pills-ajustes" role="tabpanel" aria-labelledby="pills-ajustes-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php $this->load->view('calculos/tablas/ajustes'); ?>
  		</div>
  	</div>
  </div>
</div>

<div class="row">	
	<div class="col-12">
		<div class="alert alert-info">
			<i class="fa fa-help"></i>
			Una vez generada la liquidación no se puede modificar, si desea realizar algún cambio posterior debe generarla nuevamente y anular la anterior.
		</div>
		<div class="text-right">
			<a href="#generarNomina" data-toggle="modal" class="btn btn-info">Generar liquidación</a>
		</div>
	</div>
</div>
<?php $this->load->view('modals/generarNominaAviso'); ?>
<?php $this->load->view('calculos/modals/base'); ?>

<script>	
	function spinner(){
		$("#modal .modal-body").html('<div class="text-center"><div class="spinner-border text-primary" role="status"><span class="sr-only">Cargando...</span></div></div>');
	}
	function horas_extras_recargos($emp){		
		$("#modal .modal-title").html('Extras y Recargos');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/modals',{
			modal:'horas_extras_recargos',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	function horas_trabajadas($emp){		
		$("#modal .modal-title").html('Horas trabajadas');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/modals',{
			modal:'horas_trabajadas',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	function vac_inc_lic($emp){		
		$("#modal .modal-title").html('Vacaciones, Incapacidades y Licencias');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/modals',{
			modal:'vacaciones_incapacidad_licencias',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	function ingresos_adicionales($emp){
		$("#modal .modal-title").html('Ingresos');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/modals',{
			modal:'ingresos_adicionales',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	function deducciones($emp){
		$("#modal .modal-title").html('Ingresos');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/modals',{
			modal:'deducciones_prestamos',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	function permisos($emp){
		$("#modal .modal-title").html('Permisos Notificados');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/modals',{
			modal:'empleados_notificacion_permisos',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	function horas_notificadas($emp){
		$("#modal .modal-title").html('Horas Extras Notificadas');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/modals',{
			modal:'empleados_notificacion_horas',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	function ajustesEmpleados($emp){
		$("#modal .modal-title").html('Ajustes por empleado');
		spinner();
		$("#modal").modal('toggle');
		$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/modals',{
			modal:'nomina_ajustes_empleados',
			empleado:$emp,
			'periodo[]':[<?= $empresa->periodo[0] ?>,<?= $empresa->periodo[1] ?>]		
		},function(data){
			$("#modal .modal-body").html(data);			
		});
	}

	

	function generar(){
		$("#generarNomina").modal('toggle');
	}

	
</script>		