<form action="nomina/aportes/ajustes/<?= $empresa->ajustes->id ?>" onsubmit="sendForm(this,'.response'); return false;">	
	<table class="table table-bordered">
		<thead>
			<tr>
				<th colspan="2">Ajustes generales para la nomina</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>					
					<div class="row">
						<div class="col-4">
							Liquidar Prima 
						</div>
						<div class="col-4">
							Desde 
							<input type="date" value="<?= @$empresa->ajustes->prima_desde ?>" name="prima_desde" placeholder="Desde" class="form-control" style="display: inline-block;width: 80%;">
						</div>
						<div class="col-4">
							Hasta 
							<input type="date" value="<?= @$empresa->ajustes->prima_hasta ?>" name="prima_hasta" placeholder="Hasta" class="form-control" style="display: inline-block;width: 80%;">
						</div>
					</div>
				</th>
				<td>
					SI <input type="radio" name="liquidar_prima" value="1" style="position:initial;opacity:1" <?= $empresa->ajustes->liquidar_prima==1?'checked':'' ?>> <br/>
					NO <input type="radio" name="liquidar_prima" value="0" style="position:initial;opacity:1" <?= $empresa->ajustes->liquidar_prima==0?'checked':'' ?>>
				</td>
			</tr>
			<tr>
				<th>					
					<div class="row">
						<div class="col-4">
							Liquidar Cesantía
						</div>
						<div class="col-4">
							Desde 
							<input type="date" value="<?= @$empresa->ajustes->cesantia_desde ?>" name="cesantia_desde" placeholder="Desde" class="form-control" style="display: inline-block;width: 80%;">
						</div>
						<div class="col-4">
							Hasta 
							<input type="date" value="<?= @$empresa->ajustes->cesantia_hasta ?>" name="cesantia_hasta" placeholder="Hasta" class="form-control" style="display: inline-block;width: 80%;">
						</div>
					</div>
				</th>
				<td>
					SI <input type="radio" name="liquidar_cesantia" value="1" style="position:initial;opacity:1" <?= $empresa->ajustes->liquidar_cesantia==1?'checked':'' ?>> <br/>
					NO <input type="radio" name="liquidar_cesantia" value="0" style="position:initial;opacity:1" <?= $empresa->ajustes->liquidar_cesantia==0?'checked':'' ?>>
				</td>
			</tr>
			<tr>
				<th>
					
					<div class="row">
						<div class="col-4">
							Liquidar Intereses Cesantías
						</div>
						<div class="col-4">
							Desde 
							<input type="date" value="<?= @$empresa->ajustes->cesantia_interes_desde ?>" name="cesantia_interes_desde" placeholder="Desde" class="form-control" style="display: inline-block;width: 80%;">
						</div>
						<div class="col-4">
							Hasta 
							<input type="date" value="<?= @$empresa->ajustes->cesantia_interes_hasta ?>" name="cesantia_interes_hasta" placeholder="Hasta" class="form-control" style="display: inline-block;width: 80%;">
						</div>
					</div>
				</th>
				<td>
					SI <input type="radio" name="liquidar_interes_cesantia" value="1" style="position:initial;opacity:1" <?= $empresa->ajustes->liquidar_interes_cesantia==1?'checked':'' ?>> <br/>
					NO <input type="radio" name="liquidar_interes_cesantia" value="0" style="position:initial;opacity:1" <?= $empresa->ajustes->liquidar_interes_cesantia==0?'checked':'' ?>>
				</td>
			</tr>
			<tr>
				<th>
					
					<div class="row">
						<div class="col-4">
							Valor de Cesantías
						</div>
						<div class="col-4">
							Valor 
							<input type="text" value="<?= @$empresa->ajustes->cesantia_valor ?>" name="cesantia_valor" placeholder="0.00" class="form-control" style="display: inline-block;width: 80%;">
						</div>
					</div>
				</th>
				<td></td>
			</tr>
			<tr>
				<th>
					
					<div class="row">
						<div class="col-4">
							Excluir Empleados
						</div>
						<div class="col-4">						
							<?php 
								$this->db->order_by('empleado','ASC');
								$todos = $this->CalculosModel->getEmpleadosEnPeriodo($empresa);
								echo form_dropdown_from_query('cesantias_exlusion[]',$todos,'id','empleado',explode(',',@$empresa->ajustes->cesantias_exlusion),'multiple="multple"',TRUE,'');
							?>
						</div>
					</div>
				</th>
				<td></td>
			</tr>
			<tr>
				<th>Liquidar Vacaciones</th>
				<td>
					SI <input type="radio" name="liquidar_vacaciones" value="1" style="position:initial;opacity:1" <?= $empresa->ajustes->liquidar_vacaciones==1?'checked':'' ?>> <br/>
					NO <input type="radio" name="liquidar_vacaciones" value="0" style="position:initial;opacity:1" <?= $empresa->ajustes->liquidar_vacaciones==0?'checked':'' ?>>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<div class="response"></div>
					<button type="submit" class="btn btn-success">Guardar</button>
				</td>
			</tr>
		</tfoot>
	</table>
</form>