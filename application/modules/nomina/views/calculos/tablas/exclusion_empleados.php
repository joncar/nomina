<div class="row">
	<div class="col-12 col-md-4 ">
		<div class="form-group">
			<?php 
				$this->db->order_by('empleado','ASC');
				$todos = $this->CalculosModel->getEmpleadosEnPeriodo($empresa);
				echo form_dropdown_from_query('excluir_empleado',$todos,'id','empleado','');
			?>
		</div>
	</div>
</div>

<?php 
	echo get_instance()->nomina_exclusion(1);
?>
<?php $this->load->view('predesign/chosen'); ?>
<script>
	window.afterLoad.push(function(){
		$("select[name='excluir_empleado']").chosen({"search_contains": true, allow_single_deselect:true});
		$("select[name='excluir_empleado']").on('change',function(){
			$.post('<?= base_url() ?>nomina/liquidar_nomina/<?= $empresa->id ?>/excluir_empleado',{
				empleado:$(this).val(),
				empresa:<?= $empresa->id ?>
			},function(data){
				data = JSON.parse(data);
				if(data.success){
					$(".filtering_form").trigger('submit');
				}else{
					alert(data.msj);
				}
			});	
			$(this).val('');
			$(this).chosen().trigger('liszt:updated');		
		});	
	});
</script>