<?php if($empleado->liquidar_prima['total']>0): ?>
<h1>Prima de servicios</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th>Días trabajados</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>  
  	<?php foreach($empleado->liquidar_prima as $a): if(is_object($a)):?>  
	    <tr>
	      <td><?= $a->nombre ?></td>
	      <td><?= (int)$a->dias_trabajados ?></td>
	      <td class="text-right"><?= $a->valorf ?></td>
	    </tr>
	<?php endif; endforeach; ?>
	<tr>
      <th>Total días remunerados</th> 
      <th><?= $empleado->liquidar_prima['total_diasf'] ?></th>           
      <th class="text-right"><?= $empleado->liquidar_prima['totalf'] ?></th>
    </tr>
  </tbody>
</table>
<?php endif ?>