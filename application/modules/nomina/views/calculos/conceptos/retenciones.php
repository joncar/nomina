<?php if($empleado->retenciones['total'][0]>0): ?>
<h1>Retenciones</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">%</th>
      <th class="text-right">IBC Seguridad Social</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Salud</td>
      <td class="text-right"><?= get_parameter('retenciones_salud') ?></td>
      <td class="text-right"><?= $empleado->ibc_seguridad_horasf ?></td>
      <td class="text-right"><?= $empleado->retenciones['retenciones_salud'][1] ?></td>
    </tr>
    <tr>
      <td>Pensión</td>
      <td class="text-right"><?= get_parameter('retenciones_pensión') ?></td>
      <td class="text-right"><?= $empleado->ibc_seguridad_horasf ?></td>
      <td class="text-right"><?= $empleado->retenciones['retenciones_pensión'][1] ?></td>
    </tr>
    <?php if($empleado->retenciones['fondo_solidaridad_pensional'][0]>0): ?>
      <tr>
        <td>Fondo de Solidaridad Pensional</td>
        <td class="text-right"><?= $this->CalculosModel->getPorcentajeFondoSolidaridadPensional($empleado) ?></td>
        <td class="text-right"><?= $empleado->ibc_seguridad_horasf ?></td>
        <td class="text-right"><?= $empleado->retenciones['fondo_solidaridad_pensional'][1] ?></td>
      </tr>    
    <?php endif ?>
    <tr>
      <th colspan="3">Total Retenciones</th>
      <th class="text-right"><?= $empleado->retenciones['total'][1] ?></th>
    </tr>
  </tbody>
</table>
<?php endif ?>