<?php 
  $ajste = $empleado->companias->ajustes;
?>
<div class="card-header">Detalle de la colilla de pago</div>
<h1>Resumen del pago</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>
    <?php 
      $termino = ''; 
      if($empleado->fecha_terminacion && strtotime($empleado->fecha_terminacion) < $empleado->companias->periodo[1]){
        $termino = ' hasta '.date("d/m/Y",strtotime($empleado->fecha_terminacion)).' por finalizar contrato';
      }
    ?>
    <tr>
      <td>Salario<?= $termino ?></td>
      <td class="text-right"><?= $empleado->salario_realf ?></td>
    </tr>
    <?php if($empleado->subsidio_transporte_valor>0): ?>
    <tr>
      <td>Subsidio de transporte</td>
      <td class="text-right"><?= $empleado->subsidio_transporte_valorf ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->horas_extras_recargos['total']['valor']>0): ?>
    <tr>
      <td>Horas extras, ordinarias y recargos</td>
      <td class="text-right"><?= $empleado->horas_extras_recargos['total']['valorf'] ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->nomina_vac_inc_lic['total']>0): ?>
    <tr>
      <td>Vacaciones, Licencias e Incapacidades</td>
      <td class="text-right"><?= $empleado->nomina_vac_inc_lic['totalf'] ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->ingresos_adicionales['total']>0): ?>
    <tr>
      <td>Ingresos adicionales</td>
      <td class="text-right"><?= $empleado->ingresos_adicionales['totalf'] ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->liquidar_prima['total']>0): ?>
    <tr>
      <td>Prima de servicios periodo <?= date("d/m/Y",strtotime($ajste->prima_desde)).'-'.date("d/m/Y",strtotime($ajste->prima_hasta)) ?></td>
      <td class="text-right"><?= $empleado->liquidar_prima['totalf'] ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->liquidar_cesantias['total']>0): ?>
    <tr>
      <td>Cesantias <?= date("d/m/Y",strtotime($ajste->cesantia_desde)).'-'.date("d/m/Y",strtotime($ajste->cesantia_hasta)) ?></td>
      <td class="text-right"><?= $empleado->liquidar_cesantias['totalf'] ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->liquidar_interes_cesantias['total']>0): ?>
    <tr>
      <td>Interes de Cesantias <?= date("d/m/Y",strtotime($ajste->cesantia_desde)).'-'.date("d/m/Y",strtotime($ajste->cesantia_hasta)) ?></td>
      <td class="text-right"><?= $empleado->liquidar_interes_cesantias['totalf'] ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->retenciones['totalRetenciones'][0]>0): ?>
    <tr>
      <td>Retenciones y deducciones</td>
      <td class="text-right" style="color:red"> - <?= $empleado->retenciones['totalRetenciones'][1] ?></td>
    </tr>
    <?php endif ?>
    <tr>
      <th>Total neto a pagar al empleado</th>
      <th class="text-right"><?= $empleado->total_pagarf ?></th>
    </tr>
  </tbody>
</table>