<div class="card-header">Seguridad social y parafiscales</div>
<h1>Cálculos seguridad social</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">IBC</th>
      <th class="text-right">% Empresa</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Pensión</td>
      <td class="text-right"><?= $empleado->ibc_pensionf ?></td>
      <td class="text-right"><?= get_parameter('calculos_seguridad_social_pension') ?> %</td>
      <td class="text-right"><?= $empleado->retenciones['calculos_seguridad_social_pension'][1] ?></td>
    </tr>
    <tr>
      <td><?= $empleado->riesgo_arl->nombre ?></td>
      <td class="text-right"><?= $empleado->ibc_riesgof ?></td>
      <td class="text-right"><?= $empleado->riesgo_arl->porcentaje ?> %</td>
      <td class="text-right"><?= $empleado->riesgo_arl->valorf ?></td>
    </tr>
    <tr>
      <th>Seguridad Social</th>      
      <th class="text-right" colspan="2"><?= $empleado->companias->aporte_seguridad_social ?> %</th>
      <th class="text-right"><?= $empleado->aporte_seguridad_socialf ?></th>
    </tr>    
  </tbody>
</table>