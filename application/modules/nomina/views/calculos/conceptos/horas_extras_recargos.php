<?php if($empleado->horas_extras_recargos['total']['valor']>0): ?>
<h1>Horas extras, ordinarias y recargos</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">Valor sobre hora ordinaria</th>
      <th class="text-right"># de horas</th>
      <th class="text-right">Valor</th>      
    </tr>
  </thead>
  <tbody>
    <?php foreach($empleado->horas_extras_recargos as $n=>$h): ?>
    	<?php if(!empty($h['nombre'])): ?>
	    	<tr>
		      <td><?= $h['nombre'] ?></td>
		      <td class="text-right"><?= $h['porcentaje'] ?></td>
		      <td class="text-right"><?= $h['cantidad'] ?></td>
		      <td class="text-right"><?= $h['valorf'] ?></td>      
		    </tr>
		<?php endif ?>
    <?php endforeach ?>
    <tr>
    	<th colspan="3">Total pago por horas</th>
    	<th class="text-right">
    		<?= $empleado->horas_extras_recargos['total']['valorf'] ?>
    	</th>
    </tr>
  </tbody>
</table>
<?php endif ?>