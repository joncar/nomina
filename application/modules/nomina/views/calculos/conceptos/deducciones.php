<?php if($empleado->retenciones['deducciones']['total'][0]>0): ?>
<h1>Deducciones</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>
    
    <?php foreach($empleado->retenciones['deducciones'] as $r): if(is_object($r)): ?>
	    <tr>
	      <td><?= $r->nombre ?> <?= !empty($r->mas_detalles)?$r->mas_detalles:'' ?></td>
	      <td class="text-right"><?= $r->valorf ?></td>
	    </tr>
    <?php endif; endforeach ?>
    <tr>
      <th>Total Deducciones</th>
      <th class="text-right"><?= $empleado->retenciones['deducciones']['total'][1] ?></th>
    </tr>
  </tbody>
</table>
<?php endif ?>