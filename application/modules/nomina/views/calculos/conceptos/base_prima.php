<h1>Cálculo base prima</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Salario</td>
      <td class="text-right"><?= $empleado->salario_realf ?></td>
    </tr>
    <?php if($empleado->subsidio_transporte_valor>0): ?>
    <tr>
      <td>Subsidio de transporte</td>
      <td class="text-right"><?= $empleado->subsidio_transporte_valorf ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->horas_extras_recargos['total']['valor']>0): ?>
    <tr>
      <td>Horas extras, ordinarias y recargos</td>
      <td class="text-right"><?= $empleado->horas_extras_recargos['total']['valorf'] ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->nomina_vac_inc_lic['total']>0): ?>
    <tr>
      <td>Vacaciones, Licencias e Incapacidades</td>
      <td class="text-right"><?= $empleado->nomina_vac_inc_lic['totalf'] ?></td>
    </tr>
    <?php endif ?>
    <?php if($empleado->ingresos_prestacionales['total']>0): ?>
    <tr>
      <td>Ingresos salariales</td>
      <td class="text-right"><?= $empleado->ingresos_prestacionales['totalf'] ?></td>
    </tr>
    <?php endif ?>
    <tr>
      <th>Base Prima</th>
      <th class="text-right"><?= $empleado->base_primaf ?></th>
    </tr>
  </tbody>
</table>