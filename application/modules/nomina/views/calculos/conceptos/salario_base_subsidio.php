<h1>Salario base y subsidio</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">Valor/Mensual</th>
      <th class="text-right">Días trabajados</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>    
    <tr>
      <td>Salario</td>
      <td class="text-right"><?= $empleado->salariof ?></td>
      <td class="text-right"><?= $empleado->dias_trabajados ?></td>
      <td class="text-right"><?= $empleado->salario_realf ?></td>
    </tr>
    <?php if($empleado->subsidio_transporte_valor>0): ?>
    <tr>
      <td>Subsidio de transporte</td>
      <td class="text-right"><?= $empleado->def_subsidio_transportef ?></td>
      <td class="text-right"><?= $empleado->dias_transporte ?></td>
      <td class="text-right"><?= $empleado->subsidio_transporte_valorf ?></td>
    </tr>
    <?php endif ?>
    <tr>
      <th colspan="3">Total</th>
      <th class="text-right"><?= $empleado->salario_transportef ?></th>
    </tr>
  </tbody>
</table>