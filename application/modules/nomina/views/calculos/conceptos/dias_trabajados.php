<h1>Días Trabajados</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Días del período</td>
      <td class="text-right"><?= $empleado->companias->dias_periodo ?></td>
    </tr>
    <?php foreach($empleado->nomina_vac_inc_lic as $n=>$i): if(is_object($i)): ?>
    <tr>
      <td><?= $i->nombre ?></td>
      <td class="text-right">- <?= $i->dias ?></td>
    </tr>
    <?php endif; endforeach ?>
    <tr>
      <th>Total días trabajados</th>
      <th class="text-right"><?= $empleado->dias_trabajados ?></th>
    </tr>
  </tbody>
</table>