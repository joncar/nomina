<h1>Cálculos parafiscales</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">IBC</th>
      <th class="text-right">% Empresa</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Caja de Compensación</td>
      <td class="text-right"><?= $empleado->ibc_seguridad_horasf ?></td>
      <td class="text-right"><?= get_parameter('calculos_parafiscales') ?></td>
      <td class="text-right"><?= $empleado->retenciones['calculos_parafiscales'][1] ?></td>
    </tr>    
    <tr>
      <th>Parafiscales</th>      
      <th class="text-right" colspan="2"><?= $empleado->companias->aporte_parafiscales ?> %</th>
      <th class="text-right"><?= $empleado->aporte_parafiscalesf ?></th>
    </tr>    
  </tbody>
</table>