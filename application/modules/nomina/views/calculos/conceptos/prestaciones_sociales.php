<div class="card-header">Provisión prestaciones sociales</div>
<h1>Resumen cálculos provisión prestaciones sociales</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>
      <th class="text-right">Base</th>
      <th class="text-right">%</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Cesantias</td>
      <td class="text-right"><?= $empleado->base_cesantiaf ?></td>
      <td class="text-right"><?= $empleado->fondo_cesantias->porcentaje ?> %</td>
      <td class="text-right"><?= $empleado->fondo_cesantias_valorf ?></td>
    </tr>
    <tr>
      <td>Intereses a las Cesantias</td>
      <td class="text-right"><?= $empleado->fondo_cesantias_valorf ?></td>
      <td class="text-right"><?= $empleado->fondo_cesantias->porcentaje_interes ?> %</td>
      <td class="text-right"><?= $empleado->fondo_cesantias_interesf ?></td>
    </tr>
    <tr>
      <td>Prima de Servicios</td>
      <td class="text-right"><?= $empleado->base_primaf ?></td>
      <td class="text-right"><?= get_parameter('prima_servicios') ?> %</td>
      <td class="text-right"><?= $empleado->prima_serviciosf ?></td>
    </tr>
    <tr>
      <td>Vacaciones</td>
      <td class="text-right"><?= $empleado->base_vacacionesf ?></td>
      <td class="text-right"><?= get_parameter('vacaciones') ?> %</td>
      <td class="text-right"><?= $empleado->valor_vacacionesf ?></td>
    </tr>
    <tr>
      <th colspan="3">Total provisiones</th>
      <th class="text-right"><?= $empleado->total_provisionesf ?></th>
    </tr>
  </tbody>
</table>
