<?php if($empleado->ingresos_adicionales['total']>0): ?>
  <h1>Ingresos adicionales</h1>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Concepto</th>      
        <th class="text-right">Valor</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($empleado->ingresos_adicionales as $i): if(is_object($i)): ?>
      <tr>
        <td><?= $i->nombre ?></td>      
        <td class="text-right"><?= $i->valorf ?></td>
      </tr>
      <?php endif; endforeach ?>
      <tr>
        <th>Total ingresos adicionales</th>      
        <th class="text-right"><?= $empleado->ingresos_adicionales['totalf'] ?></th>
      </tr>
    </tbody>
  </table>
<?php endif ?>