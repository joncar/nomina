<?php if($empleado->inc_licencias['total']>0): ?>
<h1>Licencias</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Concepto</th>      
      <th class="text-right">Días</th>
      <th class="text-right">Salario Promedio</th>
      <th class="text-right">Valor</th>
    </tr>
  </thead>
  <tbody>  
  	<?php foreach($empleado->inc_licencias as $a): if(is_object($a)):?>  
	    <tr>
	      <td><?= $a->nombre ?></td> 
	      <td class="text-right"><?= $a->dias ?></td>     
	      <td class="text-right"><?= $empleado->salariof ?></td>
	      <td class="text-right"><?= $a->valorf ?></td>
	    </tr>
	<?php endif; endforeach; ?>
	<tr>
      <th colspan="3">Total días remunerados</th>            
      <td class="text-right"><?= $empleado->inc_licencias['totalf'] ?></td>
    </tr>
  </tbody>
</table>
<?php endif ?>