<?php 
  get_instance()->hcss[] = "
    <style>
      .tab-content .card-body{
        overflow:auto;
      }
    </style>
  ";
?>
<h5>Cálculos</h5>
<p>A continuación, podrás encontrar los cálculos que se realizan, para que puedas revisar de dónde proviene cada valor de la liquidación.</p>

<div class="row">
	<div class="col-12 col-md-4 card">
		<div class="card-body text-center color-black">
		   <h3>Compañia</h3>
		   <p class="m-0"><span><?= $empresa->razon_social ?></span></p>
		   <p><span><?= $empresa->nit ?></span></p>
		</div>
	</div>
	<div class="col-12 col-md-4 card">
		<div class="card-body text-center color-black">
		   <h3>Persona</h3>
		   <p class="m-0"><span><?= $empleado->nombre_completo ?></span></p>
		   <p class="m-0"><span><?= $empleado->tipo_documento->nombre.': '.number_format($empleado->documento,0,',','.') ?></span></p>
		   <p><span><?= $empleado->cargos->nombre ?></span></p>
		</div>
	</div>
	<div class="col-12 col-md-4 card">
		<div class="card-body text-center color-black">
		   <h3>Periodo de pago</h3>
		   <p class="m-0"><span><?= $empresa->periodof ?></span></p>		   
		   <p><span>Días Trabajados: <?= $empleado->dias_trabajados ?></span></p>
		</div>
	</div>
</div>

<ul class="nav nav-pills mb-0" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="pills-colilla-tab" data-toggle="pill" href="#pills-colilla" role="tab" aria-controls="pills-colilla" aria-selected="true">Detalle de la colilla</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-seguridadsocial-tab" data-toggle="pill" href="#pills-seguridadsocial" role="tab" aria-controls="pills-seguridadsocial" aria-selected="false">Seguridad Social y parafiscales</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-prestaciones-tab" data-toggle="pill" href="#pills-prestaciones" role="tab" aria-controls="pills-prestaciones" aria-selected="false">Previsión prestaciones sociales</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-colilla" role="tabpanel" aria-labelledby="pills-colilla-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php $this->load->view('calculos/conceptos/colilla',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/dias_trabajados',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/salario_base_subsidio',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/horas_extras_recargos',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/liquidar_prima',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/liquidar_cesantia',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/liquidar_interes_cesantia',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/incapacidades',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/licencias',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/vacaciones',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/ingresos_adicionales',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/ibc_seguridad_social',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/retenciones',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/deducciones',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
  		</div>
  	</div>
  </div>
  <div class="tab-pane fade" id="pills-seguridadsocial" role="tabpanel" aria-labelledby="pills-seguridadsocial-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php $this->load->view('calculos/conceptos/seguridad_social_parafiscales',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/calculos_parafiscales',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/calculo_ibc_pension',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/calculo_ibc_riesgo',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/calculo_ibc_parafiscales',['empleado'=>$empleado,'empresa'=>$empresa]); ?>        
  		</div>
  	</div>
  </div>
  <div class="tab-pane fade" id="pills-prestaciones" role="tabpanel" aria-labelledby="pills-prestaciones-tab">
  	<div class="card">
  		<div class="card-body">
  			<?php $this->load->view('calculos/conceptos/prestaciones_sociales',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/cesantias_intereses',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/base_prima',['empleado'=>$empleado,'empresa'=>$empresa]); ?>
        <?php $this->load->view('calculos/conceptos/base_vacaciones',['empleado'=>$empleado,'empresa'=>$empresa]); ?>        
  		</div>
  	</div>
  </div>
</div>