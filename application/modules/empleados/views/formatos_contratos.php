<?= $output ?>
<script>
	var fijo = '#inicio_contrato_field_box,#cargo_field_box,#funciones_field_box,#periodo_prueba_field_box,#renovacion_meses_field_box,#renovacion_maxima_field_box,#tiempo_anhos_field_box';
	var obra = '#funciones_field_box,#cargo_field_box,#tiempo_ejecucion_field_box,#pagos_mensuales_field_box,#plazo_ejecucion_field_box,#valor_multa_field_box';	
	var indef = '#inicio_contrato_field_box,#cargo_field_box,#funciones_field_box,#periodo_prueba_field_box';
	window.afterLoad.push(function(){
		$(document).on('ready',function(){
			fijo = $(fijo);
			obra = $(obra);			
			indef = $(indef);	
			render();
		});
		$(document).on('change','#field-tipo_id',function(){
			render();
		});
	});

	function render(){
		var t = $("#field-tipo_id").val();
		fijo.hide();
		obra.hide();		
		indef.hide();
		if(t!=''){
			switch(t){
				case '7':				
					fijo.show();
				break;
				case '8':
					obra.show();
				break;
				case '9':
					indef.show();	
				break;
			}
		}
	}
</script>