<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Empleados extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
            $this->load->model('HistoricoModel');
        } 

        function showView($vista,$data = [],$title = ''){
            $this->loadView([
                'view'=>'panel',
                'crud'=>'user',
                'output'=>$this->load->view($vista,$data,TRUE,'nomina'),
                'title'=>$title
            ]);
        }       

        function empleados_colillas($act = '',$y = ''){
        	switch($act){
        		case 'historico':
        			$crud = $this->colillas();
        			if(is_numeric($y) && $this->user->admin==1){
        				$crud->where('empleados_id',$y);
        			}else{
        				$crud->where('empleados_id',$this->user->empleados_id);
        			}
        			$crud = $crud->render();
        		break;
        		default:
                    $empleado = $this->CalculosModel->get($this->user->empleados_id);
                    if(!empty($empleado->companias_id)){
                        $this->empresa = $this->elements->companias($empleado->companias_id);
                    }else{
                        $this->empresa = (object)[];
                    }
        			$crud = $this->colillas(); 
        			$crud->where('empleados_id','-1');
        			$crud = $crud->render();
		            $crud->title = 'Mis colillas';
		            $crud->output = $this->load->view('mis_colillas',['empleado'=>$empleado,'empresa'=>$this->empresa,'output'=>$crud->output],TRUE);
		            $this->loadView($crud);
        		break;
        	}
        }

        function colillas(){
        	return $this->elements->crudColillas();
        }
    }
?>
