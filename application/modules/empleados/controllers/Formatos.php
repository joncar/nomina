<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Formatos extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('CalculosModel');
            $this->load->model('HistoricoModel');
        } 

        function showView($vista,$data = [],$title = ''){
            $this->loadView([
                'view'=>'panel',
                'crud'=>'user',
                'output'=>$this->load->view($vista,$data,TRUE,'nomina'),
                'title'=>$title
            ]);
        }       

        function empleados_ocupacional($x = '',$y = ''){             
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Examenes Médicos');                 
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');
            if($this->user->admin!=1 && !in_array(3,$this->user->getGroupsArray())){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete();
            }         
            $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/5/pdf/permiso/').'/');
            $crud = $crud->render(); 
            $crud->title = 'Examenes Médicos Ocupacional';
            $this->loadView($crud);
        }

        function empleados_cartas_cuentas($x = '',$y = ''){             
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Aperturas de cuentas');                 
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');
            if($this->user->admin!=1 && !in_array(3,$this->user->getGroupsArray())){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete();
            }         
            $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/6/pdf/reporte/').'/');
            $crud = $crud->render(); 
            $crud->title = 'Aperturas de Cuentas';
            $this->loadView($crud);
        }

        function empleados_contratos($x = '',$y = ''){             
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Contratos');                 
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');
            if($this->user->admin!=1 && !in_array(3,$this->user->getGroupsArray())){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete();
            }    
            $crud->field_type('tipo_id','dropdown',[
                '7'=>'Termino Fijo',
                '8'=>'Obra Labor',
                '9'=>'Termino Indefinido'
            ]);     
            $crud->columns('tipo_id','fecha','cargo');
            $crud->display_as('periodo_prueba','Días del periodo de prueba')
                 ->display_as('renovacion_meses','Meses para autorenovación')
                 ->display_as('tiempo_anhos','Años de duración')
                 ->display_as('tipo_id','Tipo');
            $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
            $crud->field_type('user_id','hidden',$this->user->id);
            //$crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/6/pdf/reporte/').'/');
            $crud->callback_column('tipo_id',function($val,$row){
                $str = [
                    '7'=>'Termino Fijo',
                    '8'=>'Obra Labor',
                    '9'=>'Termino Indefinido'
                ];
                return '<a target="_blank" href="'.base_url('reportes/rep/verReportes/'.$row->tipo_id.'/pdf/valor/'.$row->id).'">'.$str[$row->tipo_id].'</a>';
            });
            $crud->callback_before_insert(function($post){$post['funciones'] = strip_tags($post['funciones']);return $post;});
            $crud->callback_before_update(function($post){$post['funciones'] = strip_tags($post['funciones']);return $post;});
            $crud = $crud->render(); 
            $crud->output = $this->load->view('formatos_contratos',['output'=>$crud->output],TRUE);
            $crud->title = 'Contratos';
            $this->loadView($crud);
        }

        function empleados_sanciones($x = '',$y = ''){             
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Llamados de atención');                 
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');
            if($this->user->admin!=1 && !in_array(3,$this->user->getGroupsArray())){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete();
            }         
            $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/10/pdf/reporte/').'/');
            $crud = $crud->render(); 
            $crud->title = 'Llamados de atención';
            $this->loadView($crud);
        }

        function empleados_recordatorios_funciones($x = '',$y = ''){             
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Recordatorios de funciones');                 
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');
            if($this->user->admin!=1 && !in_array(3,$this->user->getGroupsArray())){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete();
            }         
            $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/11/pdf/reporte/').'/');
            $crud = $crud->render(); 
            $crud->title = 'Recordatorios de funciones';
            $this->loadView($crud);
        }

        function empleados_fin_contrato($x = '',$y = ''){             
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Comunicado Finalización de Contrato');                 
            $crud->where('empleados_id',$x)
                 ->field_type('empleados_id','hidden',$x)
                 ->unset_columns('empleados_id');
            if($this->user->admin!=1 && !in_array(3,$this->user->getGroupsArray())){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete();
            }         
            $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud->field_type('notificar','hidden');
            $crud->add_action('<i class="fa fa-print"></i> Imprimir','',base_url('reportes/rep/verReportes/12/pdf/reporte/').'/');
            $crud = $crud->render(); 
            $crud->title = 'Comunicado Finalización de Contrato';
            $this->loadView($crud);
        }

        
    }
?>
