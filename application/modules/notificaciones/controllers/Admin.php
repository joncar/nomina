<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');  
            $crud->set_clone();
            $crud->field_type('emails_extras','tags')
                 ->add_action('Enviar','',base_url('notificaciones/admin/sendMail').'/');
            $this->loadView($crud->render());
        }

         
    }
?>
