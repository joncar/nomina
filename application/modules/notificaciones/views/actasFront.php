<?php
$this->db->reconnect();
if(empty($_SESSION['actas']) && (in_array(1,$this->user->getGroupsArray()) || in_array(4,$this->user->getGroupsArray()))){
	$this->db->where('DATE(NOW()) BETWEEN desde AND hasta',NULL,TRUE);
	$actas = $this->db->get('actas');
	$leidos = 0;
	if($actas->num_rows()>0){
		echo '<script src="'.base_url('js/md5.js').'"></script>';
		echo '<script> var tiempo = [];</script>';
	?>
	<script>
		function update(){
			for(var i in tiempo){
				if(tiempo[i]>0){
					tiempo[i]-=1;
					$("#acta"+i+" .closeModal").html('Espera '+tiempo[i]+' Segundos');					
				}else{
					$("#acta"+i+" .closeModal").html('Cerrar');
				}
			}
			setTimeout(function(){update();},1000);
		}
		update();
	</script>
	<?php 
		foreach($actas->result() as $a): 
			if(empty($_SESSION['acta'][$a->id])): 
				$yaaceptado = $this->db->get_where('actas_user',['user_id'=>$this->user->id,'actas_id'=>$a->id]);
				if($yaaceptado->num_rows()>0){
					$leidos++;
					$_SESSION['acta'][$a->id] = 1;
				}else{
	?>
			
			<div id="acta<?= $a->id ?>" class="modal fade" tabindex="-1" role="dialog">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title"><?= $a->titulo ?></h4>
			      </div>
			      <div class="modal-body">
			      	<div style="width:100%; height:50vh; overflow: auto">
			        	<?php echo $a->cuerpo ?>
			        </div>
			        <div class="form-inline">
			          <div class="form-group">
					    <label for="exampleInputName2">Usuario</label>
					    <input type="text" class="form-control" name="usuario" value="<?php echo $this->user->usuario ?>">
					  </div>
					  <div class="form-group">
					    <label for="exampleInputEmail2">Contraseña</label>
					    <input type="password" class="form-control" name="password" placeholder="Contraseña" value="">
					  </div>
			        </div>
			      </div>
			      <div class="modal-footer">			      	
			        <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Cerrar</button>        
			      </div>
			    </div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<script>
				tiempo[<?= $a->id ?>] = 30;			
				
				$(document).on('ready',function(){
					$("#acta<?= $a->id ?>").modal('show');
					$("#acta<?= $a->id ?>").on('hide.bs.modal',function(e){
						var user = $("#acta<?= $a->id ?> input[name='usuario']").val();
						var pwd = $("#acta<?= $a->id ?> input[name='password']").val();
						pwd = pwd!=''?md5(pwd):'';
						if(tiempo[<?= $a->id ?>] > 0){
							e.preventDefault();
							return false;
						}						
						if(user!='<?php echo $this->user->usuario ?>' || pwd != '<?php echo $this->user->password ?>'){
							e.preventDefault();
							if(pwd == ''){
								alert('Coloca tu contraseña para indicar que has leído el documento');
							}else if(pwd != '<?php echo $this->user->password ?>'){
								alert('Tu contraseña no es correcta');
							}
							return false;
						}
					});
					$("#acta<?= $a->id ?>").on('hidden.bs.modal',function(){
						for(var i in tiempo){
							tiempo[i] = 30;													
						}						
						$.get('<?php echo base_url() ?>notificaciones/admin/actas/disable/<?= $a->id ?>',{},function(){});
					});					
				});
			</script>
		<?php } ?>
		<?php  else: $leidos++;  endif; endforeach; 

		if($leidos==$actas->num_rows()){
			$_SESSION['actas'] = 1;
		}
	}
}