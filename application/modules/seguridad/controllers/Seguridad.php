<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }

        function log($x = ''){
            $crud = $this->crud_function('','');  
            $crud->where('user_id',$x);                 
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export();
            $crud->field_type('efectivo','true_false',['1'=>'SI','0'=>'NO']);
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function log_sessions(){
            $crud = $this->crud_function('','');  
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export();
            $crud->field_type('efectivo','true_false',['1'=>'SI','0'=>'NO']);
            $output = $crud->render();
            $this->loadView($output);
        }

        function menus(){
            $crud = $this->crud_function('','');  
            $crud->set_relation('menus_id','menus','nombre',array('menus_id'=>NULL));       
            $crud->columns('nombre','menus.url','tag','icon','menus_id','orden');          
            $crud->display_as('menus.url','URL')
                 ->callback_column('menus.url',function($val,$row){return $row->url;});
            $output = $crud->render();
            $this->loadView($output);
        }

        function ajustes(){
            $crud = $this->crud_function('','');            
            $crud->set_field_upload('logo','img/logos');
            $crud->set_field_upload('fondo','img/logos');
            $crud->set_field_upload('logo_login','img/logos');
            $crud->set_field_upload('favicon','img/logos');
            $crud->field_type('analytics','string')
                 ->field_type('description','string')
                 ->field_type('cookies','hidden')
                 ->field_type('aviso_legal','hidden')
                 ->field_type('politicas','hidden')
                 ->field_type('keywords','tags');
            $crud->columns('id','titulo_sistema','salario_minimo');
            $crud->unset_add()                 
                 ->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');            
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{nombre} {apellido} {email}');
            $dashs = array();
            foreach(scandir(APPPATH.'modules/dashboards/views') as $d){
                if($d!='.' && $d!='..' && strpos($d,'.php')){
                    $dashs[] = str_replace('.php','',$d);
                }
            }
            $crud->columns('nombre','pagina_principal');
            $crud->field_type('pagina_principal','enum',$dashs);
            $output = $crud->render();
            $this->loadView($output);
        }                
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }

        function usar($id){
            session_destroy();
            session_start();
            //unset($_SESSION['lastRecords']);
            $this->user->login_short($id);
            $_SESSION['auth2factor'] = 1;
            redirect('panel');
        }
        
        function user($x = '',$y = ''){            
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));            
            $crud->field_type('password','password');
            $crud->field_type('fecha_registro','invisible');
            $crud->callback_field('last_connect',function($val){
                return '<input type="text" name="last_connect" id="field-last_connect" class="form-control" value="'.$val.'">';
            });
            $crud->unset_columns('password','');
            
            $crud->set_field_upload('foto','img/fotos');
            
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                $post['reset_password'] = 1;
                return $post;
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            if($crud->getParameters()=='add'){
                $crud->set_rules('usuario','Usuario','required|is_unique[user.usuario]');
            }
            $crud->columns('foto','usuario','nombre','apellido','email','tipo_usuario','status');
            $crud->set_relation('empleados_id','empleados','{nombre} {apellidos}');
            $crud->add_action('Usar usuario','',base_url('seguridad/usar/').'/');
            $crud->add_action('Log','',base_url('seguridad/log/').'/');            
            //relation n n
            $crud->set_relation_n_n('grupos','user_group','grupos','user','grupo','nombre');
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function acciones($x = '',$y = ''){            
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';
            $crud = $this->crud_function($x,$y);    
            if($crud->getParameters()=='list'){
                redirect('panel');
                die();
            }
            $crud->where('id',$this->user->id);            
            $crud->field_type('password','password')
                 ->field_type('admin','hidden')
                 ->field_type('status','hidden')
                 ->field_type('codigo_ips','hidden')
                 ->field_type('fecha_registro','hidden')
                 ->callback_field('fecha_nacimiento',function($val){return '<input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control" value="'.$val.'">';})
                 ->fields('nombre','apellido','email','password','telefono','foto')
                 ->unset_back_to_list();
            $crud->set_field_upload('foto','img/fotos');
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
                $_SESSION['auth2factor'] = 1;
            });
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
