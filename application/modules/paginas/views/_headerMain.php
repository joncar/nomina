<?php get_instance()->hcss[] = '
<style>
  body {
      height: auto;
  }
  .main-header .navbar, .content-wrapper{
    margin-left: 0;
  }
  .card-img-top{
    height:40px;
    width:auto;
  }
  @media screen and (max-width:670px){
    .card-img-top{
      height:40px;
      width:auto;
    }
  }
</style>
';
?>
<header class="main-header">
     

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top pl-10">
      <!-- Sidebar toggle button-->
      <div class="app-menu">
        <ul class="header-megamenu nav">
            <li class="btn-group nav-item d-md-none">
                <a href="#" class="waves-effect waves-light nav-link rounded" data-toggle="push-menu" role="button">
                    <i class="nav-link-icon mdi mdi-menu text-dark"></i>
                </a>
            </li>
            <li class="btn-group nav-item">
                <img class="card-img-top" src="<?= base_url() ?>img/logos/<?= $this->ajustes->logo_login ?>" alt="Card image cap">
            </li>
            <li class="btn-group nav-item d-xl-inline-flex d-none">
                <a href="<?= base_url() ?>" class="waves-effect waves-light nav-link rounded">
                    <i class="nav-link-icon mdi mdi-view-dashboard text-dark mx-5 mx-lg-0"> </i>
                    <span class="d-xl-inline-block d-none"><?= $this->ajustes->titulo_sistema ?>
                </a>
            </li>
        </ul> 
      </div>
    </nav>
    
  </header>