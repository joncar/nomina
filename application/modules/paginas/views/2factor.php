<?php if(empty($_SESSION['user'])){redirect('main/unlog');}$this->load->view('_headerMain',[],FALSE,'paginas'); ?>

  
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container-full">

        <!-- Main content -->
        <section class="content">

          
		  <?php /*echo $_SESSION['authcode']; */ ?>
          <div class="row justify-content-md-center">            
            <div class="col-md-12 col-lg-4">
                <div class="card">                  
                  <div class="card-body">
                    <h4 class="card-title">Confirmación de identidad</h4>
                    
                    <form class="form" action="registro/auth2factor" onsubmit="sendForm(this,'.loginResponse'); return false;">
                        <div class="loginResponse"></div>
                        <div class="box-body">
                          <div class="form-group">
                            <label>Código</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="ti-user"></i>
                                </span>
                              </div>
                              <input type="te1xt" class="form-control" placeholder="Coloca el código que hemos enviado a tu teléfono móvil" name="codigo">
                              <div class="d-block d-md-none">
                                Coloca el código que hemos enviado a tu teléfono móvil
                              </div>
                            </div>
                          </div>
                          <!--<div class="form-group">
                            <div class="checkbox checkbox-success">
                              <input id="checkbox1" type="checkbox" name="recordar" value="1">
                              <label for="checkbox1"> Recordar este dispositivo </label>
                            </div>
                          </div>-->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">                          
                          <button type="submit" class="btn btn-rounded btn-primary btn-outline">
                            <i class="ti-unlock"></i> Entrar
                          </button>
                        </div>  
                      </form>

                  </div>
                  <div class="card-footer justify-content-between d-flex">                    
                  	<a href="javascript:;" onclick="resendCode()"><span class="text-muted" id="resendcode">Volver a enviar el código</span></a>
                  	<a href="<?php echo base_url('main/unlog') ?>"><span class="text-muted">Cerrar sesión</span></a>
                  </div>
                </div>
            </div>

          </div>
          <!-- /.row -->
          <!-- END Card with image -->
        </section>
        <!-- /.content -->
      </div>
  </div>
  <!-- /.content-wrapper -->
  
   <?php $this->load->view('includes/footer'); ?>
  
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

  <script>
  	var resendit = false;
  	var tiempo = 0;
  	var ht;
  	var htt;
  	function resendCode(){
  		if(!resendit){
	  		ht = $("#resendcode");
	  		htt = ht.html();
	  		ht.html('Enviando código');
	  		$.post('registro/tokenizar',{},function(){	  				  			
	  			tiempo = 10;
	  			updateTime();
	  		});
  		}
  	}

  	function updateTime(){  		
  		tiempo=tiempo-1;
  		ht.html('Espere '+tiempo+' seg para reintentar')
  		if(tiempo>0){
  			setTimeout(function(){updateTime();},1000);
  		}else{
  			resendit = false;
  			ht.html(htt)
  		}
  	}
  </script>