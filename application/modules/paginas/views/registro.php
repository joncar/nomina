<?php get_instance()->js[] = '<script src="https://www.google.com/recaptcha/api.js?render=6LcReqMZAAAAAAZgp0O63pY07I6SH5l-whD7Fieh"></script>'; ?>

  <div class="container-full">

        <!-- Main content -->
        <section class="content">

          

          <div class="row justify-content-md-center">

            <div class="col-md-12 col-lg-4">
                <div class="logo">
                  <img class="card-img-top" src="<?= base_url() ?>img/logos/<?= $this->ajustes->logo_login ?>" alt="Card image cap">
                </div>
                <div class="login card">                  
                  <div class="card-body">
                    <h4 class="card-title">¿Aún no tienes cuenta?</h4>
                    
                    <form class="form" method="post" onsubmit="insertar('registro/index/insert',this,'.registroResponse'); return false;">
                        <div class="registroResponse"></div>
                        <div class="box-body">                          
                          <div class="form-group">
                            <label>Nombre</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-id-badge"></i></span>
                              </div>
                              <input type="text" class="form-control" placeholder="Nombre" name="nombre">
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Apellidos</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-id-badge"></i></span>
                              </div>
                              <input type="text" class="form-control" placeholder="Apellidos" name="apellido">
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Tipo de documento</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-id-badge"></i></span>
                              </div>
                              <?php echo form_dropdown_from_query('tipo_documento','tipos_documento','iniciales','nombre','','id="field-tipo_documento"') ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Documento</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-id-badge"></i></span>
                              </div>
                              <input type="text" class="form-control" placeholder="Numero de documento de identidad" name="cedula">
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Teléfono móvil</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-mobile"></i></span>
                              </div>
                              <input type="text" class="form-control" placeholder="Teléfono Móvil" name="telefono">
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Correo electrónico</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-email"></i></span>
                              </div>
                              <input type="email" class="form-control" placeholder="E-mail" name="email">
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Contraseña</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-lock"></i></span>
                              </div>
                              <input type="password" class="form-control" placeholder="Password" name="password">
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">                          
                          <button type="submit" class="btn btn-rounded btn-primary btn-outline">
                            <i class="ti-unlock"></i> Crear usuario
                          </button>
                        </div>  
                      </form>

                  </div>
                  <div class="card-footer justify-content-between d-flex">                    
                    <a href="<?= base_url('login.html') ?>"><span class="text-muted">¿Ya tengo cuenta?</span></a>
                    <a href="<?= base_url('recover.html') ?>"><span class="text-muted">¿Olvidaste tu contraseña?</span></a>
                  </div>

                </div>
            </div>

          </div>
          <!-- /.row -->
          <!-- END Card with image -->
        </section>
        <!-- /.content -->
      </div>
  
   <?php $this->load->view('footerMain'); ?>
  
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  <?php 
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.mask.js"></script>';
  ?>
  <script>
    window.afterLoad.push(function(){
      $("input[name='telefono']").mask('0000000000');
    });
  </script>
