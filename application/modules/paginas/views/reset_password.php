<?php if(empty($_SESSION['user'])){redirect('main/unlog');}$this->load->view('_headerMain',[],FALSE,'paginas'); ?>

  
  
  <div class="container-full">

        <!-- Main content -->
        <section class="content">

          
      <?php /*echo $_SESSION['authcode']; */ ?>
          <div class="row justify-content-md-center">            
            <div class="col-md-12 col-lg-4">
                <div class="logo">
                  <img class="card-img-top" src="<?= base_url() ?>img/logos/<?= $this->ajustes->logo_login ?>" alt="Card image cap">
                </div>
                <div class="login card">                  
                  <div class="card-body">
                    <h4 class="card-title">Es necesario cambiar su contraseña para poder iniciar</h4>
                    
                    <form class="form" method="post" action="registro/resetPassword" onsubmit="sendForm(this,'.loginResponse'); return false;">
                        <div class="loginResponse"></div>
                        <div class="box-body">
                          <div class="form-group">
                            <label>Contraseña</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-user"></i></span>
                              </div>
                              <input type="password" class="form-control" name="pass">
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Repite la Contraseña</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-user"></i></span>
                              </div>
                              <input type="password" class="form-control" name="pass2">
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">                          
                          <button type="submit" class="btn btn-rounded btn-primary btn-outline">
                            <i class="ti-unlock"></i> Cambiar contraseña
                          </button>
                        </div>  
                      </form>

                  </div>
                  <div class="card-footer justify-content-between d-flex">                    
                    <a href="javascript:;" onclick="resendCode()"><span class="text-muted" id="resendcode">Volver a enviar el código</span></a>
                    <a href="<?php echo base_url('main/unlog') ?>"><span class="text-muted">Cerrar sesión</span></a>
                  </div>
                </div>
            </div>

          </div>
          <!-- /.row -->
          <!-- END Card with image -->
        </section>
        <!-- /.content -->
      </div>
  
   <?php $this->load->view('footerMain'); ?>
  
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>