<?php get_instance()->js[] = '<script src="https://www.google.com/recaptcha/api.js?render=6LcReqMZAAAAAAZgp0O63pY07I6SH5l-whD7Fieh"></script>'; ?>
<?php get_instance()->hcss[] = "
  <style>
    .fixed .wrapper{
      background:url(".base_url().'img/logos/'.$this->ajustes->fondo.")
    }
  </style>
"; ?>
  <div class="container-full">

        <!-- Main content -->
        <section class="content">

          
  <?php     
    //print_r($_COOKIE['auth2factor']);
  ?>
          <div class="row justify-content-md-center">            
            <div class="col-md-12 col-md-6 col-lg-4">
                <div class="logo">
                <img class="card-img-top" src="<?= base_url() ?>img/logos/<?= $this->ajustes->logo_login ?>" alt="Card image cap">
                </div>
                <div class="login card">                  
                  <div class="card-body">
                    <h4 class="card-title">Iniciar sesión</h4>                    
                    <form class="form" action="registro/login" method="post" onsubmit="sendForm(this,'.loginResponse'); return false;">
                        <div class="loginResponse"><?php echo @$_SESSION['msj']; unset($_SESSION['msj']); ?></div>
                        <div class="box-body">
                          <div class="form-group">
                            <label>Usuario</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-user"></i></span>
                              </div>
                              <input type="text" class="form-control" placeholder="usuario" name="usuario">
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Contraseña</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-lock"></i></span>
                              </div>
                              <input type="password" class="form-control" placeholder="Password" name="pass">
                            </div>
                          </div>
                        </div>                        
                        <div class="box-footer">                          
                          <button type="submit" class="btn btn-rounded btn-primary btn-outline">
                            <i class="ti-unlock"></i> Iniciar sesión
                          </button>
                        </div>  
                      </form>
                  </div>
                  <div class="card-footer justify-content-between d-flex">                    
                    <a href="<?= base_url('registro.html') ?>"><span class="text-muted">¿Aún no tienes cuenta?</span></a>
                    <a href="<?= base_url('recover.html') ?>"><span class="text-muted">¿Olvidaste tu contraseña?</span></a>
                  </div>
                </div>
            </div>

          </div>
          <!-- /.row -->
          <!-- END Card with image -->
        </section>
        <!-- /.content -->
      </div>
  
   <?php $this->load->view('footerMain'); ?>
  
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>