<?php  $this->load->view('_headerMain',[],FALSE,'paginas'); ?>

  
  
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container-full">

        <!-- Main content -->
        <section class="content">

          
  <?php     
    //print_r($_COOKIE['auth2factor']);
  ?>
          <div class="row justify-content-md-center">            
            <div class="col-md-12 col-lg-4">
                <div class="card">                  
                  <div class="card-body">
                    <h4 class="card-title">Recuperar contraseña</h4>
                    
                    <form class="form" action="<?= base_url('registro/forget') ?>" method="post">
                        <div class="loginResponse"></div>
                        <div class="box-body">
                          <div class="form-group">
                            <label>Usuario</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-user"></i></span>
                              </div>
                              <input type="text" class="form-control" placeholder="Usuario" name="usuario">
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Contraseña</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-lock"></i></span>
                              </div>
                              <input type="password" class="form-control" placeholder="Password" name="pass">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="checkbox checkbox-success">
                              <input id="checkbox1" type="checkbox">
                              <label for="checkbox1"> Recuperar contraseña </label>
                            </div>
                          </div>
                        </div>
                      </form>

                  </div>
                </div>
            </div>

          </div>
          <!-- /.row -->
          <!-- END Card with image -->
        </section>
        <!-- /.content -->
      </div>
  </div>
  <!-- /.content-wrapper -->
  
   <?php $this->load->view('includes/footer'); ?>
  
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>