<?php 

function get_parameter($option,$default_val = false){
	//Tipos de contrato	
	$option = get_instance()->db->get_where('parametros',['slug'=>$option]);
	if($option->num_rows()==0){
		return $default_val;
	}
	return $option->row()->valor;
}