/****************************************
	Traer dias trabajados del empleado al mes
****************************************/
DROP FUNCTION IF EXISTS get_dias_trabajados;
DELIMITER //
CREATE FUNCTION get_dias_trabajados(varEmp INT) RETURNS INT
BEGIN
	DECLARE diasTrabajados INT;
    
    SELECT 
    IF(dias_hasta_contrato<dias_desde_contrato,dias_hasta_contrato,dias_desde_contrato)  INTO diasTrabajados
    FROM (
        SELECT 
        IF(dias_mes>dias_desde_contrato,dias_desde_contrato,dias_mes) AS dias_desde_contrato,
        IF(dias_hasta_contrato<0,dias_hasta_contrato*-1,dias_hasta_contrato) as dias_hasta_contrato
        FROM(
            SELECT 
                DATEDIFF(periodo_actual_hasta,periodo_actual_desde)+1 AS dias_mes,
                DATEDIFF(periodo_actual_hasta,empleados.fecha_contrato)+1 AS dias_desde_contrato,
                IF(empleados.fecha_terminacion IS NULL,999,DATEDIFF(empleados.fecha_terminacion,periodo_actual_desde)+2) AS dias_hasta_contrato,
                empleados.fecha_contrato
            FROM empleados
            INNER JOIN companias ON companias.id = empleados.companias_id
        	WHERE empleados.id = varEmp
        ) as transcurso
    ) as transcurso2;

    RETURN diasTrabajados;
END