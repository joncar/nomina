/****************************************
	Traer dias del periodo actual de la empresa
****************************************/
DROP FUNCTION IF EXISTS get_dias_periodo;
DELIMITER //
CREATE FUNCTION get_dias_periodo(varEmp INT) RETURNS INT
BEGIN
    DECLARE dias INT;
    SELECT 
        DATEDIFF(periodo_actual_hasta,periodo_actual_desde)+1 INTO dias
    FROM companias    
    WHERE companias.id = varEmp;
    RETURN dias;
END