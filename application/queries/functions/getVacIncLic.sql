DROP FUNCTION IF EXISTS getVacIncLic;
DELIMITER //
CREATE FUNCTION getVacIncLic(varEmp INT) RETURNS INT
BEGIN
	DECLARE varCant INT;
	SELECT 		
		IFNULL(SUM(dias),0) INTO varCant
	FROM nomina_vac_inc_lic 
	INNER JOIN empleados ON empleados.id = nomina_vac_inc_lic.empleados_id
	INNER JOIN companias ON companias.id = empleados.companias_id 
	WHERE 
		nomina_id IS NULL AND 
		empleados_id = varEmp AND
		nomina_vac_inc_lic.periodo_desde = companias.periodo_actual_desde AND 
		nomina_vac_inc_lic.periodo_hasta = companias.periodo_actual_hasta;
	RETURN varCant;
END