/****************************************
	Traer etiqueta img del logo para los reportes
****************************************/
DROP FUNCTION IF EXISTS get_logo;
DELIMITER //
CREATE FUNCTION get_logo(varLogo VARCHAR(255)) RETURNS VARCHAR(255)
BEGIN
	DECLARE varHTML VARCHAR(255);
    SELECT 
    CONCAT('<img src="',
    	IF(varLogo IS NULL OR varLogo = '',
    		CONCAT('https://nomina.ipsnelsonmandela.com/img/logos/',aj.logo),
    		CONCAT('https://nomina.ipsnelsonmandela.com/img/companias_logos/',varLogo)
    	  ),'" width="118" height="59" />'
    ) INTO varHTML
    FROM ajustes aj;
    RETURN varHTML;
END