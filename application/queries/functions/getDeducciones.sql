DROP FUNCTION IF EXISTS getDeducciones;
DELIMITER //
CREATE FUNCTION getDeducciones(varEmp INT) RETURNS FLOAT
BEGIN
	DECLARE varCant FLOAT;
	SELECT 		
		IFNULL(SUM(valor),0) INTO varCant
	FROM nomina_deducciones
	INNER JOIN empleados ON empleados.id = nomina_deducciones.empleados_id
	INNER JOIN companias ON companias.id = empleados.companias_id 
	WHERE 
		nomina_id IS NULL AND 
		empleados_id = varEmp AND
		nomina_deducciones.periodo_desde = companias.periodo_actual_desde AND 
		nomina_deducciones.periodo_hasta = companias.periodo_actual_hasta;
	RETURN varCant;
END