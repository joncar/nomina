/****************************************
	Traer el valor real para el subsidio de transporte
****************************************/
DROP FUNCTION IF EXISTS get_subsidio_transporte;
DELIMITER //
CREATE FUNCTION get_subsidio_transporte(varEmp INT) RETURNS DOUBLE(11,2)
BEGIN
	DECLARE max_salario INT;
	DECLARE subsidio_def INT;
	DECLARE subsidio DOUBLE(11,2);
	DECLARE varDias_trabajados INT;
	DECLARE varDias_mes INT;
	DECLARE varSalario DOUBLE(11,2);
	DECLARE varActivo INT;
	SELECT valor INTO max_salario FROM parametros WHERE slug = 'subsidio_transporte_maximo';
	SELECT valor INTO subsidio_def FROM parametros WHERE slug = 'subsidio_transporte';
    SELECT  
    salario, dias_mes, dias_trabajados,subsidio_transporte INTO varSalario,varDias_mes,varDias_trabajados, varActivo
    FROM (
        SELECT 
        salario,        
        DATEDIFF(periodo_actual_hasta,periodo_actual_desde)+1 as dias_mes,
        get_dias_trabajados(empleados.id) as dias_trabajados,
        subsidio_transporte
        FROM `empleados` 
        INNER JOIN companias ON companias.id = empleados.companias_id
        WHERE empleados.id = varEmp
    ) as progreso;

    IF varActivo = 1 AND varSalario<=max_salario THEN
    	SET subsidio:= ((subsidio_def/varDias_mes)*varDias_trabajados);
    ELSE
    	SET subsidio:= 0;
    END IF;
    RETURN subsidio;
END