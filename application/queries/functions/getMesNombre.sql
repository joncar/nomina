DROP FUNCTION IF EXISTS getMesNombre;
DELIMITER &&
CREATE FUNCTION getMesNombre(varMes INT) RETURNS VARCHAR(255)
BEGIN
	DECLARE varNom VARCHAR(255);
    SELECT 
    CASE WHEN varMes = 12 THEN 'Diciembre'
         WHEN varMes = 11 THEN 'Noviembre'
         WHEN varMes = 10 THEN 'Octubre'
         WHEN varMes = 09 THEN 'Septiembre'
         WHEN varMes = 08 THEN 'Agosto'
         WHEN varMes = 07 THEN 'Julio'
         WHEN varMes = 06 THEN 'Junio'
         WHEN varMes = 05 THEN 'Mayo'
         WHEN varMes = 04 THEN 'Abril'
         WHEN varMes = 03 THEN 'Marzo'
         WHEN varMes = 02 THEN 'Febrero'
         WHEN varMes = 01 THEN 'Enero'
	END INTO varNom;
RETURN varNom;
END &&