DROP FUNCTION IF EXISTS getHorasExtrasRecargos;
DELIMITER //
CREATE FUNCTION getHorasExtrasRecargos(varEmp INT) RETURNS INT
BEGIN
	DECLARE varCant INT;
	SELECT 		
		IFNULL(SUM(cantidad),0) INTO varCant
	FROM nomina_aportes 
	INNER JOIN empleados ON empleados.id = nomina_aportes.empleados_id
	INNER JOIN companias ON companias.id = empleados.companias_id 
	WHERE 
		nomina_id IS NULL AND 
		empleados_id = varEmp AND
		nomina_aportes.periodo_desde = companias.periodo_actual_desde AND 
		nomina_aportes.periodo_hasta = companias.periodo_actual_hasta;
	RETURN varCant;
END