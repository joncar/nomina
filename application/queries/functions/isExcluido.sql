DROP FUNCTION IF EXISTS isExcluido;
DELIMITER //
CREATE FUNCTION isExcluido(varEmp INT) RETURNS INT
BEGIN
	DECLARE varExiste INT;
	SELECT
	IF(COUNT(nomina_exclusion.id)>0,1,0) INTO varExiste
	FROM nomina_exclusion
	INNER JOIN empleados ON empleados.id = nomina_exclusion.empleados_id
	INNER JOIN companias ON companias.id = empleados.companias_id AND companias.periodo_actual_desde = nomina_exclusion.periodo_desde AND companias.periodo_actual_hasta = nomina_exclusion.periodo_hasta
	WHERE nomina_exclusion.empleados_id = varEmp;

	RETURN varExiste;
END //