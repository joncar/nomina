/****************************************
	Traer salario real del empleado en base a dias trabajados
****************************************/
DROP FUNCTION IF EXISTS get_salario_real;
DELIMITER //
CREATE FUNCTION get_salario_real(varEmp INT) RETURNS DOUBLE(11,2)
BEGIN
	DECLARE salario_real DOUBLE(11,2);
    SELECT  
    ((salario/dias_mes)*dias_trabajados) INTO salario_real
    FROM (
        SELECT 
        salario,
        DATEDIFF(LAST_DAY(DATE_FORMAT(periodo_actual_desde,'%Y-%m-%d')),CONCAT(DATE_FORMAT(periodo_actual_desde,'%Y-%m'),'-01'))+1 as dias_mes,
        get_dias_trabajados(empleados.id) as dias_trabajados
        FROM `empleados` 
        INNER JOIN companias ON companias.id = empleados.companias_id
        WHERE empleados.id = varEmp
    ) as progreso;
    RETURN salario_real;
END