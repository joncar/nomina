DROP PROCEDURE IF EXISTS ajustar_diferencia_dias;
DELIMITER //
CREATE PROCEDURE ajustar_diferencia_dias(varID INT)
BEGIN
	DECLARE varDias INT;
	DECLARE varDesde DATE;
	DECLARE varHasta DATE;
	DECLARE varAux DATE;
	SELECT 
	nomina_vac_inc_lic.desde,
	nomina_vac_inc_lic.hasta,
	DATEDIFF(nomina_vac_inc_lic.hasta,nomina_vac_inc_lic.desde)
	INTO
	varDesde,varHasta,varDias
	FROM nomina_vac_inc_lic WHERE id = varID;
	IF varDias < 0 THEN
		SET varAux:= varDesde;
		SET varDesde:= varHasta;
		SET varHasta:= varAux;
		SET varDias:= varDias*-1;
	END IF;
	UPDATE nomina_vac_inc_lic 
	SET desde = varDesde,hasta = varHasta,dias = varDias
	WHERE id = varID;
END