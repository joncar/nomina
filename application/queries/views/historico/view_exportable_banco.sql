DROP VIEW IF EXISTS view_exportable_banco;
CREATE VIEW view_exportable_banco AS SELECT
historico_nomina_empleados.id,
historico_nomina_empleados.historico_nomina_id as nomina_id,
tipos_documento.id_banco as tipo_de_identificacion,
empleados.documento as numero_de_identificacion,
empleados.nombre,
empleados.apellidos as apellido,
bancos.codigo_banco as codigo_del_banco,
tipos_cuenta.iniciales as tipo_de_producto,
empleados.cuenta as numero_del_producto,
TRUNCATE(historico_nomina_empleados.total_pagar,0) as valor_del_pago
FROM
historico_nomina_empleados
INNER JOIN empleados ON empleados.id = historico_nomina_empleados.empleados_id
INNER JOIN tipos_documento ON tipos_documento.id = empleados.tipos_documento_id
INNER JOIN bancos ON bancos.id = empleados.bancos_id
INNER JOIN tipos_cuenta ON tipos_cuenta.id = empleados.tipos_cuenta_id