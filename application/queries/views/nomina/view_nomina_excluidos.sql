DROP VIEW IF EXISTS view_nomina_excluidos;
CREATE VIEW view_nomina_excluidos AS 
SElECT 
nomina_exclusion.*,
CONCAT(empleados.nombre,' ',empleados.apellidos) as nombre_completo,
empleados.documento,
empleados.companias_id,
companias.periodo_actual_desde,
companias.periodo_actual_hasta,
empleados.fecha_contrato,
empleados.fecha_terminacion
FROM nomina_exclusion
INNER JOIN empleados ON empleados.id = nomina_exclusion.empleados_id
INNER JOIN companias ON companias.id = empleados.companias_id