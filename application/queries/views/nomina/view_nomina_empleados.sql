DROP VIEW IF EXISTS view_nomina_empleados;
CREATE VIEW view_nomina_empleados AS SELECT 
empleados.id,
CONCAT(empleados.nombre,' ',empleados.apellidos) as empleado,
CONCAT('$ ',FORMAT(empleados.salario,0,'de_DE')) as salario,
getHorasExtrasRecargos(empleados.id) as horas_extras_recargos,
getVacIncLic(empleados.id) as vacaciones_incapacidad_licencia,
CONCAT('$ ',FORMAT(getIngresosAdicionales(empleados.id),0,'de_DE')) as ingresos_adicionales,
CONCAT('$ ',FORMAT(getDeducciones(empleados.id),0,'de_DE')) as deducciones_prestamos,
'' as pago_empleado,
empleados.companias_id,
empleados.tipos_contrato_id,
tipos_contrato.reporte,
empleados.fecha_contrato,
empleados.fecha_terminacion
FROM empleados
INNER JOIN tipos_contrato ON tipos_contrato.id = empleados.tipos_contrato_id
WHERE isExcluido(empleados.id) = 0