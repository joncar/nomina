DROP VIEW IF EXISTS view_postulaciones;
CREATE VIEW view_postulaciones AS 
SELECT 
solicitudes_empleo.*,
postulaciones.convocatorias_id
FROM postulaciones
INNER JOIN solicitudes_empleo ON solicitudes_empleo.id = postulaciones.solicitudes_empleo_id