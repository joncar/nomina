DROP TRIGGER IF EXISTS EmpleadosFinContrato_binsert;
DELIMITER //
CREATE TRIGGER EmpleadosFinContrato_binsert 
BEFORE INSERT ON `empleados_fin_contrato` FOR EACH ROW 
	BEGIN SET NEW.notificar:= DATE_ADD(NEW.fecha_fin, INTERVAL -50 DAY); 
END //