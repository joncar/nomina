DROP TRIGGER IF EXISTS nomina_vac_inc_lic_binsert;
DELIMITER //
CREATE TRIGGER nomina_vac_inc_lic_binsert BEFORE INSERT ON nomina_vac_inc_lic FOR EACH ROW 
BEGIN
	DECLARE varDias INT;	
	DECLARE varAux DATE;
	SELECT 	
	DATEDIFF(NEW.hasta,NEW.desde)+1
	INTO
	varDias;
	IF varDias < 0 THEN
		SET varAux:= NEW.desde;
		SET NEW.desde:= NEW.hasta;
		SET NEW.hasta:= varAux;
		SET varDias:= varDias*-1;
	END IF;	
	SET NEW.dias := varDias;	
END