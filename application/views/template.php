<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">    
    <title><?= empty($title)?'Nomina':$title.'' ?></title>
    <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/logos/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/logos/'.$favicon ?>" type="image/x-icon"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/vendors_css.css?v=1">
    <link rel="stylesheet" href="<?= base_url() ?>css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/skin_color.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/custom.css?v=1.2">        
    <script>window.URI = '<?= base_url() ?>'; window.wURL = window.URL; window.afterLoad = [];</script>
    <?php foreach($hcss as $cs): ?>
        <?php echo $cs ?>
    <?php endforeach ?>
    <?php foreach($hjs as $j): ?>
        <?php echo $j ?>
    <?php endforeach ?>
     
  </head>

    <body class="hold-transition light-skin sidebar-mini theme-primary fixed ">
        <div class="wrapper">
            <?php echo $view; ?>  
        </div>        
        <script src="<?= base_url() ?>js/vendors.min.js"></script>        
        <script src="<?= base_url() ?>js/frame.js?v=1.2"></script>
        <script src="<?= base_url().'js/jquery-migrate.min.js' ?>"></script>
        <?php foreach($css as $cs): ?>
            <?php echo $cs ?>
        <?php endforeach ?>
        <?php foreach($js as $j): ?>
            <?php echo $j ?>
        <?php endforeach ?>

        <script>
            $(document).ready(function(){
                doAfterLoad();
            });

            function doAfterLoad(){
                for(var i in window.afterLoad){
                    window.afterLoad[i]();
                }
            }
        </script>
        
        <script>
            var sec = 0;
            var time = <?php echo time()*1000; ?>;
            var fecha;
            var meses = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
            function updateReloj(){
                time = time+1000;
                fecha = new Date();
                fecha.setTime(time);
                var min = fecha.getMinutes();
                min = min<10?('0'+min):min;
                $('#fechaSistema').html(fecha.getDate()+' '+meses[fecha.getMonth()]+' '+fecha.getFullYear()+' '+fecha.getHours()+':'+min+':'+fecha.getSeconds());
                setTimeout(updateReloj,1000);
            }
            updateReloj();
            <?php if(!empty($_SESSION['user']) && $_SESSION['admin']==0): ?>
                setTimeout(function(){
                    document.location.href="<?= base_url() ?>main/unlog";
                },900000);
            <?php endif ?>
        </script> 
    </body>
</html>
