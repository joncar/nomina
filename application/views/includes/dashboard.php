<?php 
    $dash = $this->db->query("
        SELECT 
        '0' AS total
        #IFNULL(COUNT(id),0) as total
        #FROM tickets
        #WHERE (user_id = {$this->user->id} OR operador_id = {$this->user->id}) AND estado = 1
        UNION ALL
        SELECT 
        '0' AS total
        #IFNULL(COUNT(id),0) as total
        #FROM tickets
        #WHERE (user_id = {$this->user->id} OR operador_id = {$this->user->id}) AND estado = 2
        UNION ALL
        SELECT 
        '0' AS total
        #IFNULL(COUNT(id),0) as total
        #FROM tickets
        #WHERE (user_id = {$this->user->id} OR operador_id = {$this->user->id}) AND estado = 3
        UNION ALL
        SELECT 
        '0' AS total
        #IFNULL(COUNT(id),0) as total
        #FROM tickets
        #WHERE (user_id = {$this->user->id} OR operador_id = {$this->user->id})

        UNION ALL
        SELECT 
        '0' AS total
        #IFNULL(COUNT(id),0) as total
        #FROM tickets
        #WHERE operador_id IS NULL

    ");
    $total = $dash->row(3)->total;
?>
<div class="row dashboardMetricas">
    <div class="col-lg-3 col-md-6">
        <a href="<?= base_url('tickets/frontend/tickets') ?>">
            <div class="box box-primary">
                <div class="box-header no-border">
                    <h4 class="box-title">Solicitudes Abiertas</h4>
                </div>
                <div class="box-body">
                    <?php 
                        $perc = $total>0?($dash->row(0)->total*100)/$total:0;
                    ?>
                    <div class="text-right"> 

                        <h2 class="font-weight-300 mt-0">Total: <?= $dash->row()->total ?></h2>
                    </div>               
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="<?= base_url('tickets/frontend/tickets') ?>">
            <div class="box box-secondary">
                <div class="box-header no-border"
                <?php 
                        $perc = $total>0?($dash->row(1)->total*100)/$total:0;
                    ?>>
                    <h4 class="box-title">Solicitudes en Proceso</h4>
                </div>
                <div class="box-body">
                    <div class="text-right">
                        <h2 class="font-weight-300 mt-0">Total: <?= $dash->row(1)->total ?></h2>
                    </div>              
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="<?= base_url('tickets/frontend/tickets') ?>">
            <div class="box box-tertiary">
                <div class="box-header no-border">
                    <?php 
                        $perc = $total>0?($dash->row(2)->total*100)/$total:0;
                    ?>
                    <h4 class="box-title">Solicitudes Cerradas</h4>
                </div>
                <div class="box-body">
                    <div class="text-right">
                        <h2 class="font-weight-300 mt-0">Total: <?= $dash->row(2)->total ?></h2>
                    </div>              
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-3 col-md-6">
        <a href="<?= base_url('tickets/frontend/tickets') ?>">
            <div class="box box-fourth">
                <div class="box-header no-border">
                    <h4 class="box-title">Total Solicitudes</h4>
                </div>
                <div class="box-body">
                    <div class="text-right">
                        <h2 class="font-weight-300 mt-0">Total: <?= $dash->row(3)->total ?></h2>
                    </div>        
                </div>
            </div>
        </a>
    </div>

    <?php if($this->user->admin==1): ?>

        <div class="col-lg-3 col-md-6">
            <a href="<?= base_url('tickets/frontend/tickets') ?>">
                <div class="box box-primary">
                    <div class="box-header no-border">
                        <h4 class="box-title">Solicitudes sin Asignar</h4>
                    </div>
                    <div class="box-body">
                        <div class="text-right">
                            <h2 class="font-weight-300 mt-0">Total: <?= $dash->row(4)->total ?></h2>
                        </div>               
                    </div>
                </div>
            </a>
        </div>
    <?php endif ?>
    
</div>
<?php get_instance()->js[] = '<script src="'.base_url().'js/pages/dashboard.js"></script>';?>