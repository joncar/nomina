<?php if($this->user->log): ?>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">   
      <div class="user-profile px-20 py-5">
          <div class="">         
              <div class="image" style="background:url(<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>)"></div>
              <div class="info">
                  <a class="px-20" href="#"><?php echo $this->user->nombre.' '.$this->user->apellido ?></a>
              </div>
          </div>
      </div>  
        
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">        
        <li>
          <a href="<?= base_url('panel') ?>">
            <i data-feather="home"></i>
            <span>Escritorio</span>
          </a>
        </li>
        <li>
          <a href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>">
            <i data-feather="user"></i>
            <span>Perfil</span>
          </a>
        </li>
        <?php 
            $menu = $this->user->filtrarMenu(); 
            echo getMenu($menu); 
        ?>
      </ul>
    </section>
  </aside>
<?php endif ?>