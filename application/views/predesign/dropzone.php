<?php 
	$stillFile = !isset($stillFile)?false:$stillFile;
	get_instance()->js[] = '<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>';
	$accept = empty($accept)?'image/*,.pdf,.docx,.xlsx,.xls,.doc,.odt,.ods,.jfif,.eml':$accept;
	$alias = empty($alias)?$name:$alias; //Si el nombre es un array se crea un alias para los identificadores de variables
	$fieldNameOnCrud = empty($fieldNameOnCrud)?$name:$fieldNameOnCrud;//Si el nombre es diferente al que esta registrado en el crud se crea otro alias
?>
<?php get_instance()->hcss[] = '
	<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" type="text/css" rel="stylesheet" />
	<style>
		.dropzone{
			padding: 0;
			border: 1px solid #e2e5ed;
		}
		.dropzone .dz-preview .dz-image{
			border-radius:0;
		}
		.dz-error-message{
			top: 60px !important;
			opacity: 1 !important;
		}

		.dropzone.dz-started .dz-message {
		    display: block;
		    position: absolute;
		    width: 100%;
		}
		.dropzone .dz-preview .dz-image img {
		    max-width:100%;
		}

		.dropzone .dz-message {
		   font-size: 30px;
		}

		.dropzone.dz-started .dz-message {		    
		    width: 80%;
		}

		@media screen and (max-width:480px){
			.dropzone .dz-message {
			    font-size: 17px;
			    padding: 0 20px;
			}

			.dropzone.dz-started .dz-message {			    
			    width: 90%;
			}
		}
	</style>
'; ?>
<input type="hidden" id="field-<?= $alias ?>" name="<?= $name ?>" value="<?= @implode(',',$files) ?>">
<div id="<?= $alias ?>" action="<?= base_url() ?><?= $handle ?>/upload_file/<?= $fieldNameOnCrud ?>" class="dropzone"></div>
<script>
	window.afterLoad.push(function(){		
		<?= $alias ?>.dropzone.ficheros = [];
		<?= $alias ?>.dropzone.options.paramName = '<?= 's'.substr(md5($fieldNameOnCrud),0,8) ?>';
		<?= $alias ?>.dropzone.options.acceptedFiles = '<?= $accept ?>';
		<?= $alias ?>.dropzone.options.maxFilesize = 10; //10mb
		<?= $alias ?>.dropzone.options.addRemoveLinks = <?= empty($unset_delete)?'true':'false' ?>;
		<?= $alias ?>.dropzone.options.uploadMultiple = false;
		<?= $alias ?>.dropzone.options.dictRemoveFile = 'Borrar fichero';
		<?= $alias ?>.dropzone.options.dictCancelUploadConfirmation = '¿Estás seguro que deseas eliminar este fichero?, esta acción no tiene reversa';
		<?= $alias ?>.dropzone.options.dictDefaultMessage = 'Suelta aquí tus ficheros o pulsa aquí para adjuntar algún fichero';
		<?= $alias ?>.dropzone.options.dictInvalidFileType = "El tipo de documento que quieres subir no esta permitido";
		<?= $alias ?>.dropzone.options.dictFileTooBig =  "Tu documento es muy grande ({{filesize}}MiB). Máximo permitido: {{maxFilesize}}MiB.";
		/*<?= $alias ?>.dropzone.uploadFile = function(file){			
			if(confirm('¿Estás seguro que deseas subir este fichero?, luego no podrás removerlo.')){
				<?= $alias ?>.dropzone.uploadFiles([file]);
			}else{
				return false;
			}
		};*/
		
		<?= $alias ?>.dropzone.on('sending',function(file, response){			
			if(typeof(window.beforeUploadFile) !== 'undefined'){
				window.beforeUploadFile(file,response,'<?= $alias ?>');
			}
		});	
		<?= $alias ?>.dropzone.on('success',function(file, response){
			file.remoteName = response.files[0].name;
			file.remoteName.url = response.files[0].url;
			<?= $alias ?>.dropzone.ficheros.push(response.files[0].name);
			$("#field-<?= $alias ?>").val(<?= $alias ?>.dropzone.ficheros.join(','));
			$(file.previewElement.querySelectorAll(".dz-error-mark")).after('<a href="'+response.files[0].url+'" target="_new" style="font-size: 14px;text-align: center;display: block;cursor: pointer;">Abrir fichero</a>');
			if(typeof(window.afterUploadFile) !== 'undefined'){
				window.afterUploadFile(file,response,'<?= $alias ?>');
			}
		});
		<?= $alias ?>.dropzone.on('removedfile',function(file,response){
			if(typeof file.remoteName !== 'undefined'){
				var i = <?= $alias ?>.dropzone.ficheros.indexOf(file.remoteName);
				<?= $alias ?>.dropzone.ficheros.splice(i,1);
				<?php if(!$stillFile): ?>
					$.get('<?= base_url() ?><?= $handle ?>/delete_file/<?= $fieldNameOnCrud ?>/'+file.remoteName+'?_=1588886173343',{},function(data){

					});
				<?php endif ?>

				$("#field-<?= $alias ?>").val(<?= $alias ?>.dropzone.ficheros.join(','));	
				if(typeof(window.afterUploadFile) !== 'undefined'){
					window.afterRemoveFile(file,response,'<?= $alias ?>');
				}			
			}
		});	

		<?= $alias ?>.dropzone.on('canceled',function(file,response){
			if(typeof(window.afterUploadFile) !== 'undefined'){
				window.afterRemoveFile(file,response,'<?= $alias ?>');
			}
		});
			
		<?php if(!empty($files)): ?>
			//Importar ficheros por default
			var files<?= $alias ?> = $("#field-<?= $alias ?>").val();
			if(files<?= $alias ?>!=''){
				files<?= $alias ?> = files<?= $alias ?>.split(',');
				for(var i in files<?= $alias ?>){
					var mockFile = {name: files<?= $alias ?>[i],remoteName:files<?= $alias ?>[i] };
					<?= $alias ?>.dropzone.files.push(files<?= $alias ?>[i]);
					<?= $alias ?>.dropzone.options.addedfile.call(<?= $alias ?>.dropzone, mockFile);
					<?= $alias ?>.dropzone.options.thumbnail.call(<?= $alias ?>.dropzone, mockFile, '<?= base_url() ?><?= $path ?>'+files<?= $alias ?>[i]);
					mockFile.previewElement.classList.add('dz-success');
					mockFile.previewElement.classList.add('dz-complete');
					<?= $alias ?>.dropzone.ficheros.push(files<?= $alias ?>[i]);
					$($("#<?= $alias ?>").find('.dz-error-mark')[i]).after('<a href="<?= base_url() ?><?= $path ?>'+files<?= $alias ?>[i]+'" target="_new" style="font-size: 14px;text-align: center;display: block;cursor: pointer;">Abrir fichero</a>');
				}
			}
		<?php endif ?>
		$(".dz-default.dz-message span").html('Pulsa aquí, para adjuntar y/o añadir');
	});
</script>