<?php if(!empty($primary_key) && empty($rel)): echo 'Campo Rel no Definido'; return; endif; ?>
<?php if(!empty($primary_key)): ?>
	<?php 
		$editRows = get_instance()->db->get_where($table,[$rel=>$primary_key,'anulado'=>0]);		
	?>
<?php else: ?>		
	<?php $editRows = get_instance()->db->get_where($table,['id <'=>-1]); ?>
<?php endif ?>
<div class="kt-portlet tableBody <?= $table ?>_tableBody">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                <a href="javascript:;" class="btn btn-info btn-add" onclick="<?= $table ?>_tableBody.addRow()"><i class="fa fa-plus-circle"></i></a> <?php echo $subject?>
            </h3>
        </div>
    </div>

    <!--begin::Form-->        
    <div class="kt-portlet__body">
        <div class="kt-section kt-section--first">
            <div class="row">
            	<div class="col-12"  style="overflow:auto; min-height:50vh">
					<table class="table table-bordered">
						<thead>
							<tr>
								<?php foreach($fields as $field): ?>
									<th>
						                <?php echo $input_fields[$field->field_name]->display_as; ?>
					                </th>
					            <?php endforeach ?>
					            <th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<?php foreach($fields as $field): ?>
									<td class="td_<?= $field->field_name ?>">
						                <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">                    						                	
						                    <?php echo str_replace("name='".$field->field_name."'",'name="'.$table.'['.$field->field_name.'][]"',$input_fields[$field->field_name]->input) ?>
						                </div>
					                </td>
					            <?php endforeach ?>
					            <td class="td_actions">
					            	<a href="javascript:;" onclick="<?= $table ?>_tableBody.remRow(this)" class="btn btn-danger btn-xs btn-small btn-rm">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
							</tr>
							<?php foreach($editRows->result() as $e): ?>
								<tr>
									<?php foreach($fields as $field): ?>
										<td class="td_<?= $field->field_name ?>">
							                <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">                    						                	
							                    <?php echo str_replace("name='".$field->field_name."'",'name="'.$table.'['.$field->field_name.'][]"',$input_fields[$field->field_name]->input) ?>
							                </div>
						                </td>
						            <?php endforeach ?>
									<td class="td_actions">
										<input type="hidden" name="<?= $table ?>[id][]" value="<?= $e->id ?>">
						            	<a href="javascript:;" onclick="<?= $table ?>_tableBody.remRow(this)" class="btn btn-danger btn-xs btn-small btn-rm">
						            		<i class="fa fa-trash"></i>
						            	</a>
						            </td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>	
				</div>
    		</div>
		</div>

		<?php
            foreach($hidden_fields as $hidden_field){            	
                echo str_replace("name='".$hidden_field->name."'",'name="'.$table.'['.$hidden_field->name.'][]"',$hidden_field->input);
            }
        ?>
	</div>
</div>

<script>
	if(typeof TableBody == 'undefined'){
		function TableBody(containName){
			this.containName = containName;
			this.table = $(containName+' table');
			this.table.tbody = $(this.table.find('tbody'));
			this.table.tbody.find('.chosen-select').removeClass('chzn-done').removeAttr('id');
			this.table.tbody.find('.chzn-container').remove();
			this.trOrigin = $(this.table.find('tbody tr')[0]).clone();
			$(this.table.find('tbody tr')[0]).remove();

			this.addRow = function(e){
				var tr = this.trOrigin.clone();
				this.table.tbody.append(tr);
				if(typeof this.table.tbody.find('.chosen-select').chosen != 'undefined'){
					this.table.tbody.find('.chosen-select').chosen().trigger('liszt:updated');
				}
				$(this.containName).trigger('afterAddRow',[tr]);
				return tr;
			}

			this.remRow = function(e){
				$(e).parents('tr').remove();
				$(this.containName).trigger('afterRemRow');
			}	

			this.reset = function(){
				this.table.tbody.html('');
				$(this.containName).trigger('afterResetTable');
			}
			this.addRow();
		}
	}
	var <?= $table ?>_tableBody = undefined;
	window.afterLoad.push(function(){
		<?= $table ?>_tableBody = new TableBody('.<?= $table ?>_tableBody');
		$(".flexigrid > form").on('afterSend',function(){
			<?= $table ?>_tableBody.reset();
		});
		<?php if(!empty($primary_key)): ?>
			$(".<?= $table ?>_tableBody .chosen-select").chosen().trigger('liszt:updated');
		<?php endif ?>
		<?php foreach($editRows->result() as $n=>$e): ?>
			<?php foreach($e as $nn=>$ee): ?>
				$($(".<?= $table ?>_tableBody input[name='<?= $table ?>[<?= $nn ?>][]']")[<?= $n ?>]).val('<?= $ee ?>');
				$($(".<?= $table ?>_tableBody select[name='<?= $table ?>[<?= $nn ?>][]']")[<?= $n ?>]).val('<?= $ee ?>');
				$($(".<?= $table ?>_tableBody textarea[name='<?= $table ?>[<?= $nn ?>][]']")[<?= $n ?>]).val('<?= $ee ?>');
				$(".<?= $table ?>_tableBody .chosen-select").chosen().trigger('liszt:updated');
			<?php endforeach ?>
		<?php endforeach ?>
	});
</script>	