<?php $this->load->view('includes/header'); ?>
  
<?php $this->load->view('includes/sidebar'); ?>
<?php get_instance()->hcss[] = '
	<style>
		.morris-hover.morris-default-style{
			z-index:1;
		}
	</style>
'; ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container-full">
      	<?php $this->load->view('includes/breadcum'); ?>
      	<section class="content">	      	
	        <!-- Main content -->
				<?php
                    if(empty($crud)):
                        if(empty($_SESSION['dashboard']) || $this->ajustes->habilitar_cache==0){
                            $dashboard = isset($this->user->pagina_principal)?$this->user->pagina_principal:'dashboard';
                            $dashboard = empty($dashboard)?'dashboard':$dashboard;
                            $dashboard = $this->load->view($dashboard,array(),TRUE,'dashboards');
                            $_SESSION['dashboard'] = $dashboard;
                            $_SESSION['lastTime'] = time();
                        }                                    
                    endif;
                ?>
				<?= empty($crud) ? @$dashboard: $this->load->view('cruds/' . $crud) ?>
		</section>
	  </div>
</div>
<?php $this->load->view('includes/footer'); ?>
<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

<?php get_instance()->js[] = '		
        <script src="'.base_url().'css/icons/feather-icons/feather.min.js"></script>  
        <script src="'.base_url().'assets/vendor_components/apexcharts-bundle/irregular-data-series.js"></script>            
        <script src="'.base_url().'assets/vendor_components/jquery.peity/jquery.peity.js"></script>
        <script src="'.base_url().'assets/vendor_plugins/iCheck/icheck.min.js"></script>        
        <script src="'.base_url().'js/template.js"></script>        
        <script src="'.base_url().'js/demo.js"></script>'; 
?>