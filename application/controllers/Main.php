<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_start();

class Main extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    //Insert body css & js
    public $js = [];
    public $css = [];
    //Insert head css & js
    public $hjs = [];
    public $hcss = [];

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->helper('static_vars');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('querys');
        $this->load->model('elements');
        $this->load->model('elements_old');
        $this->load->library('grocery_crud');
        $this->load->library('ajax_grocery_crud');
        date_default_timezone_set('America/Bogota');
        if(!empty($_GET['pw'])){            
            $user = $this->db->get_where('user',array('email'=>base64_decode($_GET['pw'])));
            if($user->num_rows()>0){
                $this->user->login_short($user->row()->id);
            }   
            redirect('panel');
        }
        $this->ajustes = $this->db->get('ajustes')->row();        
    }

    public function index() {
        $log = $this->user;
        if ($log->log){
            redirect (base_url('panel'));
        }
        else{
            $this->loadView('main');
        }
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['usuario']) && !empty($_POST['pass'])) {
                $this->db->where('usuario', $this->input->post('usuario'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('usuario', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember'])){
                            $_SESSION['remember'] = 1;
                        }
                        if (empty($_POST['redirect'])){
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url() . '"</script>');
                        }
                        else{
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . base_url($_POST['redirect']) . '"</script>');
                        }
                    }
                    else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url());
        }
        header("Location:" . base_url('panel'));
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    function getHead($page){        
        return $page;
    }

    function getBody($page){                
        return $page;
    }

    public function loadView($param = array('view' => 'main')) {

        if (is_string($param)){
            $param = array('view' => $param);
        }
        if(is_object($param)){
            $param = (array)$param;
        }
        $ajustes = $this->db->get('ajustes')->row();
        if(empty($param['title'])){
            $param['title'] = $ajustes->titulo_sistema;
        }
        $param['favicon'] = $ajustes->favicon;         
        $param['view'] = $this->load->view($param['view'],$param,TRUE);
        $param = array_merge($param,['css'=>$this->css,'js'=>$this->js,'hcss'=>$this->hcss,'hjs'=>$this->hjs]);        
        if(!empty($param['css_files'])){
            foreach($param['css_files'] as $p){
                $param['hcss'][] = '<link rel="stylesheet" href="'.$p.'" type="text/css">';
            }
        }
        if(!empty($param['js_files'])){
            foreach($param['js_files'] as $p){
                $param['js'][] = '<script src="'.$p.'"></script>';
            }        
        }
        
        $page = $this->load->view('template', $param,true);        
        $page = $this->getHead($page);
        $page = $this->getBody($page); 
        echo $page;
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }
    
    function app()
    {            
        $this->loadView(array('view'=>'app'));
    }
    
    function error404(){
        echo 'Ha ocurrido un error';
    }

    function testMail(){
        correo('joncar.c@gmail.com','TEST','TEST');
    }

    function enviarcorreo($usuario,$idnotificacion,$destinatario = '',$return = false){
        if(is_numeric($idnotificacion)){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
        }else{
            $mensaje = (object)$idnotificacion;
        }
        $mensaje->texto = str_replace('usuarios.','',$mensaje->texto);
        $mensaje->titulo = str_replace('usuarios.','',$mensaje->titulo);
        foreach($usuario as $n=>$v){ 
            if(!is_array($v) && !is_object($v)){            
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }
        }   
        $mensaje->texto = str_replace('//ips.ipsnelsonmandela.com/',base_url(),$mensaje->texto);  
        if($return){
            return $mensaje;
        }      
        if(empty($destinatario)){
            correo($usuario->email,$mensaje->titulo,$mensaje->texto);
        }
        else{
            correo($destinatario,$mensaje->titulo,$mensaje->texto);
        }
    }

    function enviarsms($usuario,$idnotificacion,$destinatario = ''){
        $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
        foreach($usuario as $n=>$v){ 
            if(!is_array($v) && !is_object($v)){            
                $mensaje->texto = str_replace('{'.$n.'}',trim($v),$mensaje->texto);
            }
        }
        $destinatario = substr($destinatario,0,2) != '57'? '57'.$destinatario:$destinatario;        
        $post['to'] = array($destinatario);
        $post['text'] = strip_tags(strip_tags($mensaje->texto));
        $post['text'] = html_entity_decode($post['text']);
        $post['text'] = str_replace('  ','',trim(preg_replace('/\s+/',' ',$post['text'])));
        $post['from'] = "IPS";
        $post['parts'] = ceil(strlen($post['text'])/150);
        $user = $this->ajustes->usuario_sms_api;
        $password = $this->ajustes->pass_sms_api;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://dashboard.wausms.com/Api/rest/message");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        curl_setopt($ch, CURLOPT_HTTPHEADER,
        array(
        "Accept: application/json",
        "Authorization: Basic ".base64_encode($user.":".$password)));
        $result = curl_exec ($ch); 
        $result = json_decode($result);
        if(!empty($result->error)){
            correo('joncar.c@gmail.com','Error en enviar sms',json_encode($result));
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
