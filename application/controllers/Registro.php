<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Registro extends Main {
        const IDROLUSER = 3;
        public function __construct()
        {
            parent::__construct();            
        }

        public function index($url = 'main',$page = 0)
        {
            if($this->router->fetch_class()=='registro'){
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('bootstrap2');
                $crud->set_table('user');
                $crud->set_subject('<span style="font-size:27.9px">Si eres nuevo entra por aquí</span>');            
                //Fields
                if(empty($_POST)){
                    $_SESSION['captcha'] = '0';
                }
                //unsets
                $crud->unset_back_to_list()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_edit()
                     ->unset_list()
                     ->unset_export()
                     ->unset_print()
                     ->field_type('fecha_registro','invisible')
                     ->field_type('fecha_actualizacion','invisible')
                     ->field_type('status','invisible')
                     ->field_type('foto','invisible')
                     ->field_type('admin','invisible')
                     ->field_type('password','password')
                     ->field_type('cedula','invisible')
                     ->field_type('apellido_materno','invisible')
                     ->field_type('tiene_android','hidden',1);
                $crud->set_lang_string('insert_success_message','Sus datos han sido guardados con éxito <script>setTimeout(function(){document.location.href="'.base_url('panel').'"; },2000)</script>');
                $crud->display_as('password','Contraseña nuevo usuario')                 
                     ->display_as('email','Email de contacto')                 ;
                $crud->set_lang_string('form_add','');
                //$crud->required_fields('password','email','nombre','apellido');
                //Displays             
                $crud->set_lang_string('form_save','Registrarse');
                $crud->set_rules('cedula','Cédula','required|numeric|callback_verificarSiExiste');
                $crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_after_insert(array($this,'ainsertion'));
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
                $crud->set_rules('telefono','Teléfono','required');
                $crud->required_fields('tipo_documento','cedula','email','telefono','password','nombre','apellido');
                $output = $crud->render();
                $output->view = 'main';
                $output->crud = 'user';
                $output->title = 'Registrarse';
                $output->output = get_header_crud($output->css_files,$output->js_files,TRUE).$output->output;
                $this->loadView($output);   
            }
        }

        function verificarSiExiste(){
            $cedula = $_POST['cedula'];
            $tipo = $_POST['tipo_documento'];
            $tipo = $this->db->get_where('tipos_documento',['iniciales'=>$tipo])->row()->id;
            if(!empty($cedula) && !empty($tipo)){
                $empleados = $this->db->get_where('empleados',['tipos_documento_id'=>$tipo,'documento'=>$cedula]);
                if($empleados->num_rows()>0){
                    //Verifificar que ya no este registrado
                    $empleados = $empleados->row();
                    $user = $this->db->get_where('user',['empleados_id'=>$empleados->id]);
                    if($user->num_rows()==0){
                        return true;
                    }
                    $this->form_validation->set_message('verificarSiExiste','El trabajador ingresado ya se encuentra registrado');
                    return false;
                }else{
                    $this->form_validation->set_message('verificarSiExiste','El trabajador ingresado esta registrado en nuestra base de datos');
                    return false;
                }
            }
        }
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {       
            if(empty($_SESSION['captcha'])){
                //return false;
            }
            $tipo = $this->db->get_where('tipos_documento',['iniciales'=>$post['tipo_documento']])->row()->id;
            $post['empleados_id'] = $this->db->get_where('empleados',['tipos_documento_id'=>$tipo,'documento'=>$post['cedula']])->row()->id;
            $post['email'] = strtolower($post['email']);
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['password'] = md5($post['password']);
            $post['usuario'] = $post['tipo_documento'].$post['cedula'];                        
            return $post;
        }
        
        function ainsertion($post,$primary)
        {
            get_instance()->db->insert('user_group',['user'=>$primary,'grupo'=>2,'priority'=>0]);
            get_instance()->user->login_short($primary);
            get_instance()->tokenizar();
            return true;
        }
        
       
        function forget($key = '',$ajax = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadView(array('view'=>'forget'));
            }
            else
            {                
                if(empty($key)){
                if(empty($_SESSION['key'])){
                    $this->form_validation->set_rules('usuario','usuario','required');
                    if($this->form_validation->run())
                    {
                        $user = $this->db->get_where('user',array('usuario'=>$this->input->post('usuario')));
                        if($user->num_rows()>0){
                            $_SESSION['key'] = md5($user->row()->email);
                            $_SESSION['email'] = $user->row()->email;
                            correo($user->row()->email,'reestablecimiento de contraseña',$this->load->view('email/forget',array('user'=>$user->row()),TRUE));
                            echo $this->success('Los pasos para la restauracion han sido enviados a su correo electronico');                                
                        }
                        else{
                            if(empty($ajax)){
                                echo $this->error('El correo que desea restablecer no esta registrado.');
                            }else{
                                echo $this->traduccion->traducir($this->error('El correo que desea restablecer no esta registrado.'),$_SESSION['lang']);
                            }
                        }
                    }
                    else{
                        if(empty($ajax)){
                            echo $this->error($this->form_validation->error_string());
                        }else{
                            echo $this->error($this->form_validation->error_string());
                        }
                    }
                }
                else
                {
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                    $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');
                    //$this->form_validation->set_rules('key','Llave','required');
                    if($this->form_validation->run())
                    {
                        /*if($this->input->post('key') == $_SESSION['key'])
                        {*/
                            $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                            session_unset(); 
                            $_SESSION['msj'] = $this->success('Se ha restablecido su contraseña <a href="'.base_url().'">Volver al inicio</a>');
                            redirect('login.html');
                        /*}
                        else
                            $this->loadView(array('view'=>'recover','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));*/
                    }
                    else{
                        if(empty($_POST['key'])){
                            if(empty($ajax)){
                                echo $this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.');    
                            }else{
                                echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                            }
                            session_unset();
                        }
                        else{
                            $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                        }
                    }
                }
                }
                else
                {
                    /*if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {*/
                        if(!empty($key)){
                            foreach($this->db->get_where('user')->result() as $u){
                                if($key==md5($u->email)){
                                    $_SESSION['email'] = $u->email;                                    
                                }
                            }
                            if(!empty($_SESSION['email'])){
                                $this->loadView(array('view'=>'recover','key'=>$key));
                            }else{
                                redirect('login.html');
                            }
                        }                        
                    /*}
                    else{
                        if(empty($ajax)){
                            $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                        }else{
                            echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                        }
                    }*/
                }
            }
        }    

        public function login() {
            $maxIntentos = 10;
            $maxDiasInactivo = 90;
            if (!$this->user->log) {
                //Verificar si lo que introdujo fue el correo                
                $_SESSION['fallidos'] = empty($_SESSION['fallidos'])?0:$_SESSION['fallidos'];
                if (!empty($_POST['usuario']) && !empty($_POST['pass']) && !empty($_POST['token'])) {                    
                    $this->db->where('usuario', $this->input->post('usuario'));
                    $r = $this->db->get('user');
                    if ($this->user->login($this->input->post('usuario', TRUE), $this->input->post('pass', TRUE))) {
                        if($r->row()->status == 1){
                            $this->load->library('recaptcha');                
                            if(!$this->recaptcha->recaptcha_check_answer($_POST['token'])){
                                $_SESSION['msj'] = $this->error('Captcha es requerido');
                            }else{
                                //Bloquear por inactividad   
                                $lastConnect = empty($r->row()->last_connect)?date("Y-m-d H:i:s"):$r->row()->last_connect;                             
                                $datetime1 = new DateTime(date("Y-m-d",strtotime($lastConnect)));
                                $datetime2 = new DateTime(date("Y-m-d"));
                                $interval = $datetime1->diff($datetime2);
                                $dias = $interval->format('%a');
                                if($dias>$maxDiasInactivo){
                                    //Bloquear
                                    session_destroy();
                                    echo $this->error('Su usuario supero el limite de inactividad de '.$maxDiasInactivo.' días, por lo que por motivo de seguridad ha sido bloqueado, contacte con un administrador para volver a ingresar');
                                    $this->db->update('user',['status'=>0],['id'=>$r->row()->id]);
                                    //$this->db->insert('log_sessions',['user_id'=>$r->row()->id,'efectivo'=>0,'fecha'=>date("Y-m-d H:i:s")]);
                                    die();
                                }
                                if (!empty($_POST['remember'])){
                                    $_SESSION['remember'] = 1;
                                }
                                unset($_SESSION['fallidos']);
                                if($this->tokenizar()){
                                    echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . base_url('panel') . '"</script>');                                
                                }else{
                                    echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . base_url('2factor.html') . '"</script>');                                
                                }
                                //$this->db->insert('log_sessions',['user_id'=>$r->row()->id,'efectivo'=>1,'fecha'=>date("Y-m-d H:i:s")]);
                            }
                        }else{
                            echo $this->error('Su usuario se encuentra bloqueado por el administrador o por cantidad de intentos fallidos exedido');
                            die();
                        }
                    } 
                    else{
                        $user = $this->db->get_where('user',['usuario'=>$_POST['usuario']]);
                        if($user->num_rows()>0){
                            if($user->row()->status==1){
                                $_SESSION['fallidos']++;
                                if($_SESSION['fallidos']>=$maxIntentos){
                                    $this->db->update('user',['status'=>0],['id'=>$user->row()->id]);
                                    echo $this->error('Su usuario se ha bloqueado por motivos de seguridad, contacte con un administrador para proceder con su desbloqueo');
                                    unset($_SESSION['fallidos']);
                                }else{
                                    echo $this->error('Usuario o contrasena incorrecta, intente de nuevo. {Intentos restantes: '.($maxIntentos-$_SESSION['fallidos']).'}');
                                }
                                //$this->db->insert('log_sessions',['user_id'=>$r->row()->id,'efectivo'=>0,'fecha'=>date("Y-m-d H:i:s")]);
                            }else{
                                echo $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                            }
                        }else{
                            echo $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                        }
                    }
                }
                else{
                    echo $this->error('Debe completar todos los campos antes de continuar');
                }

                if (!empty($_SESSION['msj'])){                    
                    echo $_SESSION['msj'];
                }
            }else{                
                echo '<script>document.location.href="' . site_url() . '"</script>';
            }
        }    

        public function tokenizar(){
            if(!empty($_SESSION['user'])){
                $user = $this->db->get_where('user',['id'=>$_SESSION['user'],'status'=>1]);
                if($user->num_rows()>0){
                    if(!empty($_COOKIE['auth2factor']) || $user->row()->auth2factor==0){
                        $_SESSION['auth2factor'] = 1;                
                        return true;
                    } 
                    return true;//eliminar para habilitar autenticacion
                    $_SESSION['authcode'] = str_pad(rand(0,9999),4,"0");  
                    if($this->ajustes->habilitar_sms==1){
                        $this->enviarsms([],'El código para continuar con su requerimiento es '.$_SESSION['authcode'],$user->row()->telefono);
                    }
                    return false; //Validar si el cookie esta ya validado
                }else{
                    $_SESSION['msj'] = $this->error('Usuario bloqueado o no existe registrado'); 
                    $this->unlog();
                }
            }else{
                $this->unlog();
            }
        }

        public function auth2factor(){
            $this->form_validation->set_rules('codigo','Código','required|numeric');
            if($this->form_validation->run()){
                if($_SESSION['authcode']==$_POST['codigo']){
                    $_SESSION['auth2factor'] = 1;
                    if(!empty($_POST['recordar'])){
                        setcookie('auth2factor', 1, time() + (86400 * 30), "/"); // 86400 = 1 day
                    }
                    echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . base_url('panel') . '"</script>');
                }else{
                    echo $this->error('El código que has colocado no es correcto');
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }

        function resetPassword(){
            if(empty($_SESSION['user'])){
                echo $this->error('Usuario no registrado <script>document.location.href="'.base_url().'";</script>');
            }
            $this->form_validation->set_rules('pass','Contraseña','required')
                                  ->set_rules('pass2','Repetir Contraseña','required|matches[pass]');
            if($this->form_validation->run()){
                $validate = valid_password($_POST['pass']);
                if($validate=='success'){
                    $password = md5($_POST['pass']);
                    $this->db->update('user',['password'=>$password,'reset_password'=>0],['id'=>$this->user->id]);
                    $this->user->login_short($this->user->id);
                    if(!$this->user->auth2factor){
                        $_SESSION['auth2factor'] = 1;
                    }
                    echo $this->success('Contraseña cambiada correctamente, <script>document.location.href="'.base_url('panel').'";</script>');
                }else{
                    echo $this->error($validate);
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }

        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
