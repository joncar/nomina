<?php
class AportesEnLinea {

	protected $url = 'https://marketplacepruebas.aportesenlinea.com';
	protected $token = '';
	protected $ci = null;
    function __construct()
    {    
    	$this->ci = get_instance();
    	$this->db = $this->ci->db;
    	$this->elements = $this->ci->elements;    	
    }

    function sendToServer($url,$data = [],$tokenizar = false){
    	$headers = array(          
          'Content-Type:application/json',
          'Anon:Mareigua.Fanaia'          
        );  
        if($tokenizar){
            if(!empty($this->token)){
                $headers[] = 'Token:'.$this->token;
            }else{
                echo 'Se debe crear el token primero antes de tokenizar una consulta';
                return false;
            }
        }   

        echo 'Headers: '.print_r($headers,TRUE).'<br/><br/>';           
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_HEADER,false);
        curl_setopt( $ch, CURLOPT_URL,$url);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );  
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );      
        curl_setopt( $ch, CURLOPT_POST,true);              	
        curl_setopt( $ch, CURLOPT_POSTFIELDS,$data);
        $response = curl_exec($ch);        
        curl_close($ch);
        return $response;
    }

    function registrarEmpresa($empresa){
    	$endpoint = '/Fanaia.Servicios.Fachada/api/Aportantes/RegistrarAportantesEmpresa';
    	$url = $this->url.$endpoint;
    	$comp = $this->elements->companias($empresa);
    	if($comp){
    		$data = new stdClass();    		
    		$data->data = [
    			(object)[
    				"Aportante"=>[(object)[
    					 "TipoAportante"=>1,
    					 "TipoPersona"=>"N", 
    					 "TipoIdAportante"=>$_POST['tipo_documento_legal'], 
    					 "NumeroIdAportante"=>$_POST['documento_legal'], 
    					 "RazonSocial"=>$comp->razon_social, 
    					 "Naturaleza"=>2, 
    					 "Clase"=>"C", 
    					 "DepartamentoDomicilioPrincipal"=>5, 
    					 "CodigoCIIU"=>$_POST['codigo_ciiu'],
    					 "Presentacion"=>"S", 
    					 "ExoneradoSaludSenaICBF"=>true, 
    					 "CodigoARL"=>$comp->riesgo_arl->codigo_arl, 
    					 "CodigoCCF"=>$_POST['codigo_ccf'], 
    					 "CodigoCentroTrabajo"=>$comp->id, 
    					 "NombreCentroTrabajo"=>$_POST['nombre_centro_trabajo'], 
    					 "TarifaRiesgosLaborales"=>$comp->riesgo_arl->porcentaje, 
    					 "CodigoSucursal"=>$comp->id, 
    					 "NombreSucursal"=>$_POST['nombre_sucursal'], 
    					 "DireccionSucursal"=>$_POST['direccion_sucursal'], 
    					 "CodigoCiudad"=>40, 
    					 "DigitoVerificacion"=>"", 
    					 "CodigoDepartamento"=>5, 
    					 "Telefono"=>$comp->telefono, 
    					 "Celular"=>$_POST['celular_contacto'], 
    					 "CorreoElectronicoAportante"=>$comp->email_contacto, 
    					 "NombreContacto"=>$_POST['nombre_contacto'], 
    					 "FechaMatricula"=>date("Y-m-d",strtotime($_POST['fecha_matricula'])), 
    					 "DescuentoLey1429"=>($comp->ley_1429?true:false), 
    					 "AceptarTerminosYCondiciones"=>true     					
    				]],
    				"RepresentanteLegal"=>[(object)[
    					"TipoIdRepresentanteLegal"=>$_POST['tipo_documento_legal'], 
    					"NumeroIdRepresentanteLegal"=>$_POST['documento_legal'], 
    					"PrimerApellidoRepresentanteLegal"=>$_POST['primer_apellido_legal'], 
    					"SegundoApellidoRepresentanteLegal"=>$_POST['segundo_apellido_legal'], 
    					"PrimerNombreRepresentanteLegal"=>$_POST['primer_nombre_legal'], 
    					"SegundoNombreRepresentanteLegal"=>$_POST['segundo_nombre_legal']
    				]], 
    				"UsuarioTesoreria"=>[(object)[
    					"TipoIdUsuarioTesoreria"=>$_POST['tipo_documento_tesorero'], 
    					"NumeroIdUsuarioTesoreria"=>$_POST['documento_legal'], 
    					"PrimerApellidoUsuarioTesoreria"=>$_POST['primer_apellido_tesorero'], 
    					"SegundoApellidoUsuarioTesoreria"=>$_POST['segundo_apellido_tesorero'], 
    					"PrimerNombreUsuarioTesoreria"=>$_POST['primer_nombre_tesorero'], 
    					"SegundoNombreUsuarioTesoreria"=>$_POST['segundo_nombre_tesorero'], 
    					"UsuarioTesoreria"=>explode('@',$_POST['email_tesorero'])[0].'_aportesenlinea',
    					"CorreoElectronicoUsuarioTesoreria"=>$_POST['email_tesorero']
    				]],
    				"UsuarioNomina"=>[(object)[
    					"TipoIdUsuarioNomina"=>$_POST['tipo_documento_nomina'], 
    					"NumeroIdUsuarioNomina"=>$_POST['documento_nomina'], 
    					"PrimerApellidoUsuarioNomina"=>$_POST['primer_apellido_nomina'],
    					"SegundoApellidoUsuarioNomina"=>$_POST['segundo_apellido_nomina'],
    					"PrimerNombreUsuarioNomina"=>$_POST['primer_nombre_nomina'],
    					"SegundoNombreUsuarioNomina"=>$_POST['segundo_nombre_nomina'],
    					"UsuarioNomina"=>explode('@',$_POST['email_nomina'])[0].'_aportesenlinea',
    					"CorreoElectronicoUsuarioNomina"=>$_POST['email_nomina'],
    				]]
	    		]
    		];              
    		$response = $this->sendToServer($url,json_encode($data));
            if(!empty($response) && is_json($response)){
                $response = json_decode($response);
                if(!empty($response->mensajes) && $response->mensajes[0]->Tipo=="Error"){
                    return (object)['success'=>false,'msj'=>$response->mensajes[0]->Texto];
                }elseif(!empty($response->data)){
                    get_instance()->db->update('companias',['usuario_aportesenlinea'=>explode('@',$_POST['email_nomina'])[0].'_aportesenlinea'],['id'=>$empresa]);
                    return (object)['success'=>true,'msj'=>"Aportante registrado con éxito"];
                }
            }else{
                return (object)['success'=>false,'msj'=>'Respuesta obtenida érronea del servidor'];
            }
            //print_r($response);
    	}
    }

    function login($user,$pass){
    	$endpoint = '/Transversales.Servicios.Fachada/api/ControlAcceso/Autenticar';
    	$url = $this->url.$endpoint;
    	$data = ['data'=>[
                (object)[
                    "NombreUsuario"=> $user,
                    "Password"=> $pass,
                    "Aplicacion"=> "E2271FA7-0FCA-4293-BF6D-53414286FDB0"
                ]
            ]
        ];
    	$response = $this->sendToServer($url,json_encode($data));
        if(!empty($response) && is_json($response)){
            $response = json_decode($response);
            if(!empty($response->mensajes) && $response->mensajes[0]->Tipo=="Error"){
                return (object)['success'=>false,'msj'=>$response->mensajes[0]->Texto];
            }elseif(!empty($response->data)){
                $this->token = $response->data;
                return (object)['success'=>true,'token'=>$response->data];
            }
        }else{
            return (object)['success'=>false,'msj'=>'Respuesta obtenida érronea del servidor'];
        }
    	//print_r($resp);
    }

    function liquidar($vinc,$nomina){
        $endpoint = '/Fanaia.Servicios.Fachada/api/TransmisorPlanillaIntegrada/recepcionSolicitudPlanillaIntegrada';
        $url = $this->url.$endpoint;
        $data = ['data'=>[
                (object)[
                    "IndicadorLiquidacion"=> 1,
                    "TipoIDAportante"=> $vinc->tipo_documento_nomina,
                    "NumeroIDAportante"=> $vinc->documento_nomina,
                    "CodigoSucursalPrincipal"=> $vinc->codigo_sucursal,
                    "TipoRegistro"=> 1,
                    "TipoArchivo"=> 0,
                    "Archivo"=> $this->crearTXT($nomina,$vinc),
                    "EstructuraArchivo"=> 1,
                    "IndicadorNotificacion"=> 1
                ]
            ]
        ];
        $response = $this->sendToServer($url,json_encode($data),true);        
        if(!empty($response) && is_json($response)){
            $response = json_decode($response);
            if(!empty($response->mensajes) && $response->mensajes[0]->Tipo=="Error"){
                return (object)['success'=>false,'msj'=>$response->mensajes[0]->Texto];
            }elseif(!empty($response->data)){
                $this->token = $response->data;
                return (object)['success'=>true,'id'=>$response->data[0]->IDTransaccion];
            }

        }else{
            return (object)['success'=>false,'msj'=>'Respuesta obtenida érronea del servidor'];
        }
        //print_r($response);        
    }

    function consultarEstadoLiquidacion($id){
        $endpoint = '/Fanaia.Servicios.Fachada/api/TransmisorPlanillaIntegrada/consultarEstadoSolicitud';
        $url = $this->url.$endpoint;
        $data = ['data'=>[
                (object)[
                    "IDTransaccion"=>$id
                ]
            ]
        ];
        
        $response = $this->sendToServer($url,json_encode($data),true);        
        //print_r($response);
        /*die();*/
        if(!empty($response) && is_json($response)){
            $response = json_decode($response);
            if(!empty($response->mensajes) && $response->mensajes[0]->Tipo=="Error"){
                return (object)['success'=>false,'msj'=>$response->mensajes[0]->Texto];
            }elseif(!empty($response->data)){
                $this->token = $response->data;
                return (object)['success'=>true,'data'=>$response->data[0]];
            }

        }else{
            return (object)['success'=>false,'msj'=>'Respuesta obtenida érronea del servidor'];
        }        
    }

    function crearTXT($nomina,$vinc){  
        ob_end_clean();      
        $historico = $this->db->get_where('historico_nomina_empleados',['historico_nomina_id'=>$nomina->id]);
        $compania = $this->elements->companias($nomina->companias_id);
        $txt = '';
        $data = [];
        $data['tipo'] = '01';
        $data['modalidad'] = '1';
        $data['secuencia'] = str_pad(1,4,'0',STR_PAD_LEFT);
        $data['razon_social'] = str_pad(strip_tags($compania->razon_social),200,' ',STR_PAD_RIGHT);
        $data['tipo_documento'] = str_pad($vinc->tipo_documento_nomina,2,' ',STR_PAD_RIGHT);
        $data['documento'] = str_pad($vinc->documento_nomina,16,' ',STR_PAD_RIGHT);
        $data['dv'] = ' ';
        $data['tipo_planilla'] = 'E';
        $data['numero_planilla'] = str_pad(0,10,'0',STR_PAD_LEFT);
        $data['fecha_pago'] = str_pad($nomina->periodo_hasta,10,'0',STR_PAD_LEFT);
        $data['forma_presentacion'] = 'U';
        $data['codigo_sucursal'] = str_pad($vinc->codigo_sucursal,10,' ',STR_PAD_RIGHT);
        $data['nombre_sucursal'] = str_pad($vinc->nombre_sucursal,40,' ',STR_PAD_RIGHT);
        $data['ARL'] = str_pad($compania->riesgo_arl->codigo_arl,6,' ',STR_PAD_RIGHT);
        $data['periodo'] = str_pad(date("Y-m",strtotime($nomina->periodo_hasta)),7,' ',STR_PAD_RIGHT);
        $data['periodo_salud'] = str_pad(date("Y-m",strtotime($nomina->periodo_hasta)),7,' ',STR_PAD_RIGHT);
        $data['radicacion'] = str_pad($nomina->id,10,'0',STR_PAD_LEFT);
        $data['fecha_pago2'] = str_pad($nomina->periodo_hasta,10,'0',STR_PAD_LEFT);
        $data['empleados'] = str_pad($historico->num_rows(),5,'0',STR_PAD_LEFT);
        $data['total_nomina'] = str_pad(number_format($nomina->total_personas,2,'',''),12,'0',STR_PAD_LEFT);
        $data['tipo_aportante'] = 1;
        $data['codigo_operador'] = str_pad(1,2,'0',STR_PAD_LEFT);;        
        foreach($data as $d){
            $txt.= $d;
        }
        //echo strlen($txt);
        //die();
        //header("Content-Type:text/plain");
        //header('Content-Disposition: attachment; filename="default-filename.txt"');
        //echo $txt;
        //die();
        return base64_encode($txt);
        foreach($historico->result() as $n=>$h){            
        }
        die();
    }
}