<?php
class FacturaTechSoap {

	//Demo
	protected $url = 'https://ws-nomina.facturatech.co/v1/demo/index.php?wsdl';	
	//Production
	protected $url_pro = 'https://ws-nomina.facturatech.co/v1/pro/index.php?wsdl';
	protected $usuario = 'aacnm23022021';
	protected $password = 'dd9b1cc6fda3b97f2f2710f96cbe4c9506c285d95b9c747607cb9058a9b2aac2';
    function __construct()
    {    
    	$this->ci = get_instance();
    	$this->db = $this->ci->db;
    	$this->elements = $this->ci->elements;    	
    }

    function generarXML($nomina,$empleado,$prefijo,$correlativo){
        $secuencia = $prefijo.$correlativo;        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml.= '<!--Version #1.0-->';
        $xml.= '<NominaIndividual xmlns="dian:gov:co:facturaelectronica:NominaIndividual"
                  xmlns:xs="http://www.w3.org/2001/XMLSchema-instance"
                  xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                  xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"
                  xmlns:xades="http://uri.etsi.org/01903/v1.3.2#"
                  xmlns:xades141="http://uri.etsi.org/01903/v1.4.1#"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  SchemaLocation=""
                  xsi:schemaLocation="dian:gov:co:facturaelectronica:NominaIndividual NominaIndividualElectronicaXSD.xsd">';
        $xml.= '<ext:UBLExtensions></ext:UBLExtensions>';
        $xml.= '<Novedad CUNENov="A">false</Novedad>';
        $dias_trabajados = 0;
        foreach($empleado->historico_nomina_dias_trabajados->result() as $e){
            if($e->concepto=='Días del período'){
                $dias_trabajados = $e->valor;
            }
        }        
        $xml.= '<Periodo FechaIngreso="'.$empleado->datos->fecha_contrato.'"           
                 FechaLiquidacionInicio="'.date("Y-m-d",$nomina->periodo[0]).'"
                 FechaLiquidacionFin="'.date("Y-m-d",$nomina->periodo[1]).'"
                 TiempoLaborado="'.$dias_trabajados.'"
                 FechaGen="'.date("Y-m-d").'"/>';

        $xml.= '<NumeroSecuenciaXML CodigoTrabajador="'.$empleado->empleados_id.'"
                        Prefijo="'.$prefijo.'"
                        Consecutivo="'.$correlativo.'"
                        Numero="'.$secuencia.'"/>'; 

        $xml.= '<LugarGeneracionXML Pais="CO"
                        DepartamentoEstado="11"
                        MunicipioCiudad="11001"
                        Idioma="es"/>';

        $xml.= '<ProveedorXML RazonSocial="FACTURATECH SA. DE CV"
                  NIT="901143311"
                  DV="8"
                  SoftwareID="A"
                  SoftwareSC="A"/>';

        $xml.= '<CodigoQR>https://nomina.ipsnelsonmandela.com</CodigoQR>';

        $xml.= '<InformacionGeneral Version="V1.0: Documento Soporte de Pago de Nómina Electrónica"
                        Ambiente="2"
                        TipoXML="102"                                               
                        FechaGen="'.date("Y-m-d").'"
                        HoraGen="'.date("H:i:s").'-05:00"
                        PeriodoNomina="4"
                        TipoMoneda="COP"
                        TRM="1.00"/>';

        $xml.= '<Notas>Comprobante de Egreso #'.$empleado->id.'</Notas>';

        $xml.= '<Empleador RazonSocial="'.$nomina->empresa->razon_social.'"
               NIT="901143311"
               DV="8"
               PrimerApellido="'.$nomina->empresa->primer_apellido_legal.'"
               SegundoApellido="'.(!empty($nomina->empresa->segundo_apellido_legal)?$nomina->empresa->segundo_apellido_legal:'NINGUNO').'"
               PrimerNombre="'.$nomina->empresa->primer_nombre_legal.'"
               Pais="CO"
               DepartamentoEstado="11"
               MunicipioCiudad="11001"
               Direccion="'.$nomina->empresa->direccion.'"/>';

        $xml.= '<Trabajador TipoTrabajador="31"
                SubTipoTrabajador="00"
                AltoRiesgoPension="false"
                TipoDocumento="13"
                NumeroDocumento="'.$empleado->datos->documento.'"
                PrimerApellido="'.$empleado->datos->apellidos.'"
                SegundoApellido="Ninguno"
                PrimerNombre="'.$empleado->datos->nombre.'"
                OtrosNombres="Ninguno"
                LugarTrabajoPais="CO"
                LugarTrabajoDepartamentoEstado="11"
                LugarTrabajoMunicipioCiudad="11001"
                LugarTrabajoDireccion="'.$nomina->empresa->direccion.'"
                SalarioIntegral="true"
                TipoContrato="2"
                Sueldo="'.$empleado->datos->salario.'"
                CodigoTrabajador="'.$empleado->datos->id.'"/>';

        $xml.= '<Pago Forma="1" Metodo="47" Banco="'.$empleado->datos->banco->nombre.'" TipoCuenta="'.$empleado->datos->tipo_cuenta->nombre.'" NumeroCuenta="'.$empleado->datos->cuenta.'"/>';

        $xml.= '<FechasPagos>
                    <FechaPago>'.date("Y-m-d",$nomina->periodo[1]).'</FechaPago>
                </FechasPagos>';

        $xml.= '<Devengados>';
            $devengados = 0;
            //Salario Transporte
            foreach($empleado->historico_nomina_salario->result() as $h){
                switch($h->concepto){
                    case 'Salario':
                    case 'Honorarios':
                        $xml.= '<Basico DiasTrabajados="'.$h->dias_trabajados.'" SueldoTrabajado="'.$h->valor.'"/>';
                        $devengados+= $h->valor;
                    break;
                    case 'Subsidio de transporte':
                        $xml.= '<Transporte AuxilioTransporte="'.$h->valor.'" ViaticoManuAlojS="0.0" ViaticoManuAlojNS="0.0"/>';
                        $devengados+= $h->valor;
                    break;
                }                
            }
            
            //Primas
            foreach($empleado->historico_nomina_liquidar_prima->result() as $h){
                $xml.= '<Primas Cantidad="'.$h->dias.'" Pago="'.$h->valor.'" PagoNS="0.00" />';
                $devengados+= $h->valor;
            }

            //Cesantias
            $interes = 0;
            foreach($empleado->historico_nomina_liquidar_interes_cesantias->result() as $h){
                $interes+= $h->valor;
            }
            
            foreach($empleado->historico_nomina_liquidar_cesantias->result() as $h){
                $xml.= '<Cesantias Pago="'.$h->valor.'" Porcentaje="'.$empleado->fondo_cesantias->porcentaje.'" PagoIntereses="'.$interes.'"/>';
                $devengados+= $h->valor;
                $devengados+= $interes;
            }

            if($empleado->historico_nomina_ingresos->num_rows()==0){
                $otros = [];
                 //Vacaciones
                foreach($empleado->historico_nomina_vacaciones->result() as $h){
                    $otros[] = (object)['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$h->valor];                                        
                }   
                //Horas Extras
                if($empleado->historico_nomina_horas_extras->num_rows()>0){
                    $otros[] = (object)['concepto'=>'Horas Extras y Recargos','valor'=>$h->valor];                                        
                }    
                if(count($otros)>0){
                    $xml.= '<OtrosConceptos>';
                        foreach($otros as $b){
                            $xml.= '<OtroConcepto DescripcionConcepto="'.$b->concepto.'" ConceptoS="'.$b->valor.'" ConceptoNS="0.00"/>';
                            $devengados+= $b->valor;
                        }
                    $xml.= '</OtrosConceptos>';
                }
            }
            
            if($empleado->historico_nomina_ingresos->num_rows()>0){
                //Bonificaciones
                $bonos = [];
                $auxilios = [];
                $otros = [];

                foreach($empleado->historico_nomina_ingresos->result() as $b){
                    if(strpos($b->concepto,'Bonificación')>-1){
                        $bonos[] = $b;                        
                    }
                    if(strpos($b->concepto,'Auxilio')>-1){
                        $auxilios[] = $b;
                    }

                    if(strpos($b->concepto,'otros ingresos')>-1){
                        $otros[] = $b;
                    }
                }     
                //Vacaciones
                foreach($empleado->historico_nomina_vacaciones->result() as $h){
                    $otros[] = (object)['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$h->valor];                                        
                }   
                //Horas Extras
                if($empleado->historico_nomina_horas_extras->num_rows()>0){
                    $otros[] = (object)['concepto'=>'Horas Extras y Recargos','valor'=>$h->valor];                                        
                }        

                if(count($bonos)>0){
                    $xml.= '<Bonificaciones>';
                        foreach($bonos as $b){
                            $xml.= '<Bonificacion BonificacionS="'.$b->valor.'" BonificacionNS="0.00"/>';
                            $devengados+= $b->valor;
                        }
                    $xml.= '</Bonificaciones>';
                }

                if(count($auxilios)>0){
                    $xml.= '<Auxilios>';
                        foreach($auxilios as $b){
                            $xml.= '<Auxilio AuxilioS="'.$b->valor.'" AuxilioNS="0.00"/>';
                            $devengados+= $b->valor;
                        }
                    $xml.= '</Auxilios>';
                }

                if(count($otros)>0){
                    $xml.= '<OtrosConceptos>';
                        foreach($otros as $b){
                            $xml.= '<OtroConcepto DescripcionConcepto="'.$b->concepto.'" ConceptoS="'.$b->valor.'" ConceptoNS="0.00"/>';
                            $devengados+= $b->valor;
                        }
                    $xml.= '</OtrosConceptos>';
                }
            }
        
        $xml.= '</Devengados>';

        $xml.= '<Deducciones>';
            $deducciones = 0;
            foreach($empleado->historico_nomina_retenciones->result() as $h){
                switch($h->concepto){
                    case 'Salud':
                        $xml.= '<Salud Porcentaje="'.$h->porcentaje.'" Deduccion="'.$h->valor.'"/>';
                        $deducciones+= $h->valor;
                    break;
                    case 'Pensión':
                        $xml.= '<FondoPension Porcentaje="'.$h->porcentaje.'" Deduccion="'.$h->valor.'"/>';
                        $deducciones+= $h->valor;
                    break;
                    case 'Fondo de Solidaridad Pensional':
                        $xml.= '<FondoSP Porcentaje="'.$h->porcentaje.'"
                                 DeduccionSP="'.$h->valor.'"
                                 PorcentajeSub="0.00"
                                 DeduccionSub="0.00"/>';
                        $deducciones+= $h->valor;
                    break;                
                }
            }  
            $otras = []; 
            foreach($empleado->historico_nomina_deducciones->result() as $h){
                if(strpos($h->concepto,'Prestamos')>-1){
                    $xml.= '<Deuda>'.$h->valor.'</Deuda>';
                    $deducciones+= $h->valor;
                }
                if(strpos($h->concepto,'Otras deducciones')>-1){                    
                    $otras[] = $h;
                }
            }     

            if(count($otras)>0){
                $xml.= '<OtrasDeducciones>';
                    foreach($otras as $b){
                        $xml.= '<OtraDeduccion>'.$b->valor.'</OtraDeduccion>';
                        $deducciones+= $b->valor;
                    }
                $xml.= '</OtrasDeducciones>';
            } 


        $xml.= '</Deducciones>';
        $xml.= '<Redondeo>0.00</Redondeo>
                <DevengadosTotal>'.$devengados.'</DevengadosTotal>
                <DeduccionesTotal>'.$deducciones.'</DeduccionesTotal>
                <ComprobanteTotal>'.$empleado->total_pagar.'</ComprobanteTotal>';
        $xml.= '</NominaIndividual>';
        
        return $xml;
    }

    function enviarNomina($nomina){    	       
        if(!empty($nomina->cuerpo[1])){
            foreach($nomina->cuerpo[1] as $cuerpo){
                if(empty($cuerpo->facturatech) || empty(json_decode($cuerpo->facturatech)->transactionID)){                    
                    $prefijo = $nomina->empresa->prefijo_facturatech;
                    $correlativo = $this->db->get_where('companias',['id'=>$nomina->empresa->id])->row()->secuencia_facturatech+1;
                    $xml = $this->generarXML($nomina,$cuerpo,$prefijo,$correlativo);                
                    $client = new SoapClient($this->url, array("trace" => 1, "soap_version" => SOAP_1_1));
                    $params = array(
                      "userId" => $this->usuario,
                      "password" => $this->password,
                      "xml" => base64_encode($xml)
                    );  
                    $response = @$client->__soapCall("FtechAction.uploadDocument",$params);  
                    $response->prefijo = $prefijo;
                    $response->correlativo = $correlativo;
                    $response->secuencia = $prefijo.$correlativo;              
                    $this->db->update('historico_nomina_empleados',['facturatech'=>json_encode($response)],['id'=>$cuerpo->id]);
                    if(empty($response->transactionID)){
                        print_r($response);
                        die();
                    }else{
                        $this->db->update('companias',['secuencia_facturatech'=>$correlativo],['id'=>$nomina->empresa->id]);
                    }
                }
            }        	
        }else{
            echo "Nomina No encontrada, por favor retorne y vuelva a intentarlo";
            die();
        }
    }

    function get($nomina,$factura){
        if(!empty($nomina) && !empty($factura)){
            $client = new SoapClient($this->url, array("trace" => 1, "soap_version" => SOAP_1_1));              
            $params = array(
              "userId" => $this->usuario,
              "password" => $this->password,
              "transaccionID" => $factura->transactionID
            );  
            $response = @$client->__soapCall("FtechAction.documentStatus",$params);            
            if($response->code == 200){                
                $this->download($nomina,$factura);
            }
        }
    }

    function download($nomina,$factura){
        if(!empty($nomina) && !empty($factura)){
            $client = new SoapClient($this->url, array("trace" => 1, "soap_version" => SOAP_1_1));              
            $params = array(
              "userId" => $this->usuario,
              "password" => $this->password,
              "prefix" => 'NOM',  
              'number'=>$factura->correlativo
            );  
            $response = @$client->__soapCall("FtechAction.downloadPDF",$params);                                 
            if($response->code == 200){                
                $file = base64_decode($response->documentBase64);
                $path = 'files/facturatech/nomina/';
                $name = uniqid().'.pdf';                
                file_put_contents($path.$name,$file);
                $factura->file = $name;
                $this->db->update('historico_nomina_empleados',['facturatech'=>json_encode($factura)],['id'=>$nomina->id]);
                redirect(base_url($path.$name));
                die();
            }else{
                echo get_instance()->error('La factura aún no ha sido generada, espere unos minutos e intente nuevamente');
            }     
        }else{
            echo get_instance()->error('Documento no encontrado');
        }           
    }
}