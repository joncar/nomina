<?php 
class Calculosmodel extends Elements{	
	function __construct(){
		parent::__construct();
        $this->licencias_no_remuneradas = explode(',',get_parameter('licencias_no_remuneradas'));
        $this->totalNomina = 0;
	}

	

	function getPorcentajeFondoSolidaridadPensional($emp){            
        $salario_minimo = get_parameter('salario_base');
        $salarios = ceil($emp->salario/$salario_minimo);
        if($salarios<=0){
            return 0;
        }
        $this->db->order_by('id','DESC');
        $this->db->where("'$salarios' BETWEEN desde and hasta",NULL,TRUE);
        $rango = $this->db->get_where('fondo_solidaridad_pensional_rangos');
        if($rango->num_rows()==0){
            return 0;
        }
        return $rango->row()->porcentaje;
    }

    function getRetenciones($emp){
        $parametros = $this->db->get_where('parametros');
        $params = [];
        foreach($parametros->result() as $p){
            if(is_numeric($p->valor)){                
                if($p->valor>0){
                    $params[$p->slug] = [$emp->ibc_seguridad_horas*($p->valor/100),$this->fm($emp->ibc_seguridad_horas*($p->valor/100))];
                }else{
                    $params[$p->slug] = 0;
                }
            }
        }        
        $porcentaje = $this->getPorcentajeFondoSolidaridadPensional($emp);
        $params['fondo_solidaridad_pensional'] = $porcentaje>0?[$emp->ibc_seguridad_horas*($porcentaje/100),number_format($emp->ibc_seguridad_horas*($porcentaje/100),2,',','.')]:[0,0];
        $params['total'][0] = 0;
        if($emp->contrato->reporte!=4){
            $params['total'][0]+= $params['retenciones_salud'][0];
            $params['total'][0]+= $params['retenciones_pensión'][0];
            $params['total'][0]+= $params['fondo_solidaridad_pensional'][0];
        }
        //Deducciones
        $deducciones = [];
        $deducciones['total'] = 0;
        $ded = $this->deducciones($emp->id,$emp->companias->periodo);
        foreach($ded as $n=>$d){
            if(is_array($d)){
                foreach($d as $nn=>$dd){
                    if(is_object($dd)){                        
                        if($dd->valor>0){
                            $dd->valorf = $this->fm($dd->valor);
                            $deducciones[] = $dd;
                            $deducciones['total']+= $dd->valor;
                        }
                    }
                }
            }
        }        
        $deducciones['total'] = [$deducciones['total'],$this->fm($deducciones['total'])];
        $params['totalRetenciones'][0] = $params['total'][0]+$deducciones['total'][0];
        $params['totalRetenciones'][1] = $this->fm($params['totalRetenciones'][0]);
        $params['total'][1] = $this->fm($params['total'][0]);
        $params['deducciones'] = $deducciones;        
        return $params;
    }

    function getTotalPrevisiones($emp){
        $total = 0;        
        $total+=$emp->fondo_cesantias_valor;
        $total+=$emp->fondo_cesantias_interes;
        $total+=$emp->prima_servicios;
        $total+=$emp->valor_vacaciones;
        return $total;
    }

    function getHorasExtrasRecargos($emp,$desde = '',$hasta = ''){        
        $params = []; 
        if(empty($desde) || empty($hasta)){            
            $aportes = $this->nomina_aportes($emp->id,$emp->companias->periodo);
        }else{
            $aportes = $this->nomina_aportes($emp->id,[strtotime($desde),strtotime($hasta)],TRUE);
        }
        $total = 0;  
        //Subsidio completo
        $max_salario_para_subsidio = get_parameter('subsidio_transporte_maximo');
        $subsidio_transporte_legal = get_parameter('subsidio_transporte');
        $transporte = 0;
        if($emp->subsidio_transporte==1 && $emp->salario<=$max_salario_para_subsidio){
            $transporte = $subsidio_transporte_legal;
        }

        foreach($aportes as $n=>$a){
            if(get_parameter($n.'_caption')){
                $label = get_parameter($n.'_caption');
            }else{
                $label = '';
            }
            if(isset($a->cantidad) && is_numeric($a->cantidad) && $a->cantidad > 0){
                $porcentaje = get_parameter($n);
                //$valor = $emp->salario_hora*$porcentaje*$a->cantidad;
                $valor = (($emp->salario+$transporte)*$porcentaje/240)*$a->cantidad;
                $params[$n] = [
                    'nombre'=>$label,
                    'cantidad'=>$a->cantidad,
                    'porcentaje'=>$porcentaje,
                    'valor'=>$valor,
                    'valorf'=>$this->fm($valor)
                ];
                $total+= $valor;
            }            
            if($n=='extras' && is_array($a)){                
                foreach($a as $nn=>$aa){
                    if(isset($aa->cantidad) && is_numeric($aa->cantidad) && $aa->cantidad>0){
                        //$porcentaje = $aa->valor;
                        //$valor = $emp->salario_hora*$porcentaje*$aa->cantidad;
                        $valor = $aa->valor*$aa->cantidad;
                        $params[$nn] = [
                            'nombre'=>$aa->nombre,
                            'cantidad'=>$aa->cantidad,
                            'porcentaje'=>$aa->valor,
                            'valor'=>$valor,
                            'valorf'=>$this->fm($valor)
                        ];
                        $total+= $valor;
                    }
                }
            }
        }
        $params['total'] = [
            'valor'=>$total,
            'valorf'=>$this->fm($total)
        ];        
        return $params;
    }

    function get_nomina_vac_inc_lic($e,$desde = '',$hasta = ''){
    	if(empty($desde) || empty($hasta)){
            $aportes = $this->nomina_vac_inc_lic($e->id,$e->companias->periodo);
        }else{
            $aportes = $this->nomina_vac_inc_lic($e->id,[strtotime($desde),strtotime($hasta)],TRUE);
        }

        //Si no tiene aportes y este mes se le vencio el contrato se le aplica pago de vacaciones
        if(empty($aportes) && (!empty($e->fecha_terminacion) && strtotime($e->fecha_terminacion)<$e->companias->periodo[1] && $e->tipos_contrato->devenga_prestaciones == 1)){
           $fecha_contrato = new DateTime($e->fecha_contrato);
           $fecha_terminacion = new DateTime($e->fecha_terminacion);
           $dias = $fecha_terminacion->diff($fecha_contrato)->format('%a');           
           $aportes['vacaciones'] = (object)[
                'nombre'=>'Vacaciones acumuladas en periodo '.date("d/m/Y",strtotime($e->fecha_contrato)).' '.date("d/m/Y",strtotime($e->fecha_terminacion)),
                'tipo'=>'4',
                'dias'=>$dias,
                'desde'=>$e->fecha_contrato,
                'hasta'=>$e->fecha_terminacion,
                'desdef'=>date("d/m/Y",strtotime($e->fecha_contrato)),
                'hastaf'=>date("d/m/Y",strtotime($e->fecha_terminacion)),
           ];
        }



    	$aportes['total_dias'] = 0;
    	$aportes['total'] = 0;
    	$aportes['total_inc'] = 0;
    	$aportes['total_lic'] = 0;
        $incapacidades = [];
        $licencias = [];
        $vacaciones = [];
        $incapacidades['total'] = 0;
        $licencias['total'] = 0;
        $vacaciones['total'] = 0;
        $salario_minimo = get_parameter('salario_base');        
        if($e->salario<$salario_minimo){
            $salario_por_dia = $e->dias_periodo>0?($salario_minimo/30):0;
        }else{
            $salario_por_dia = $e->dias_periodo>0?($e->salario/30):0;
        }        
        
    	foreach($aportes as $n=>$a){
    		if(is_object($a)){                
	    		$aportes[$n]->desdef = date("d/m/Y",strtotime($a->desde));
	    		$aportes[$n]->hastaf = date("d/m/Y",strtotime($a->hasta));    		                
	    		if(!in_array($n,$this->licencias_no_remuneradas)){
	    			$a->valor = $a->dias*($salario_por_dia*(get_parameter($n.'_porcentaje_pago',100)/100));
                    $a->valorf = $this->fm($a->valor);	    				
		    		switch($a->tipo){
		    			case '1': //Vac
                            $vacaciones[] = $a;
                            $vacaciones['total']+= $a->valor;
                            $aportes['total_dias']+= $a->dias;
		    			break;
		    			case '2':			    				
		    				$incapacidades[] = $a;
		    				$incapacidades['total']+= $a->valor;
                            $aportes['total_dias']+= $a->dias;
		    			break;
		    			case '3':		    				
		    				$licencias[] = $a;
		    				$licencias['total']+= $a->valor;
                            $aportes['total_dias']+= $a->dias;
		    			break;
                        case '4':
                            $vacaciones[] = $a;             
                            $a->valor = $a->dias*$e->salario/720;
                            $a->valorf = $this->fm($a->valor);    
                            $vacaciones['total']+= $a->valor;
                        break;
		    		}
                    $aportes['total']+= $a->valor;
	    		}
    		}
    	}	  
          	
    	$aportes['totalf'] = $this->fm($aportes['total']);
    	$incapacidades['totalf'] = $this->fm($incapacidades['total']);
    	$licencias['totalf'] = $this->fm($licencias['total']);
        $vacaciones['totalf'] = $this->fm($vacaciones['total']);
    	$e->nomina_vac_inc_lic = $aportes;
        $e->incapacidades = $incapacidades;
        $e->inc_licencias = $licencias;
        $e->inc_vacaciones = $vacaciones;
    	$e->dias_trabajados-= $aportes['total_dias'];
    	$e = $this->get_salario($e);        
    	return $e;
    }

    function get_salario($e){                
    	$e->salario_real = $e->dias_periodo>0?($e->salario/30)*$e->dias_trabajados:0;        
    	$e->salario_dia = $e->dias_periodo>0?($e->salario/30):0;
    	$e->salario_hora = $e->salario/get_parameter('horas_mes');
    	//Variables con formato de dinero
        $e->salariof = $this->fm($e->salario);
    	$e->salario_realf = $this->fm($e->salario_real);
    	$e->salario_diaf = $this->fm($e->salario_dia);
    	$e->salario_horaf = $this->fm($e->salario_dia);    	
    	return $e;
    }

    function getSubsidioTransporte($e,$dias_trabajados = ''){
        if(empty($dias_trabajados)){
            $dias_trabajados = $e->dias_trabajados;
        }
        $e->subsidio_transporte_valor = 0;
        //$e->dias_transporte = $e->dias_trabajados-$e->nomina_vac_inc_lic['total_dias'];        
        $e->dias_transporte = $dias_trabajados;
        $max_salario_para_subsidio = get_parameter('subsidio_transporte_maximo');
        $subsidio_transporte_legal = get_parameter('subsidio_transporte');
        $def_subsidio_transporte = $subsidio_transporte_legal;
        if($e->subsidio_transporte==1 && $e->salario<=$max_salario_para_subsidio){
            $e->subsidio_transporte_valor = $e->dias_periodo>0?($subsidio_transporte_legal/30)*$dias_trabajados:0;
        }
        $e->def_subsidio_transporte = $def_subsidio_transporte;
        $e->def_subsidio_transportef = $this->fm($def_subsidio_transporte);
        $e->subsidio_transporte_valorf = $this->fm($e->subsidio_transporte_valor);
        return $e;
    }

    function getIngresosAdicionales($e,$desde = '',$hasta = ''){
        if(empty($desde) || empty($hasta)){
            $ingresos = $this->ingresos_adicionales($e->id,$e->companias->periodo);        
        }else{
            $ingresos = $this->ingresos_adicionales($e->id,[strtotime($desde),strtotime($hasta)],TRUE);
        }
        $ing = [];
        $ing['total'] = 0;
        $cons = [];
        $cons['total'] = 0;                
        foreach($ingresos as $i){
            if(is_array($i)){
                foreach($i as $nn=>$ii){
                    if(is_object($ii) && $ii->valor>0){
                        $ing['total']+= $ii->valor;
                        $ii->valorf = $this->fm($ii->valor);
                        if(!empty($ii->mas_detalles)){
                            $ii->nombre.= ': '.$ii->mas_detalles;
                        }
                        $ing[] = $ii;
                        if($ii->tipo==1){
                            $cons[] = $ii;
                            $cons['total']+= $ii->valor;
                        }
                    }
                }
            }
        }
        //Variables con formato de dinero        

        $ing['totalf'] = $this->fm($ing['total']);        
        $cons['totalf'] = $this->fm($cons['total']);
        $e->ingresos_adicionales = $ing;
        $e->ingresos_prestacionales = $cons;
        return $e;
    }

    function get_asignaciones($e){
    	$e = $this->getSubsidioTransporte($e);        
        $e = $this->getIngresosAdicionales($e);
        $e->horas_extras_recargos = $this->getHorasExtrasRecargos($e);
        $e->ibc_seguridad_horas = $e->salario_real;
        $e->ibc_seguridad_horas+= $e->horas_extras_recargos['total']['valor'];
        $e->ibc_seguridad_horas+= $e->nomina_vac_inc_lic['total'];
        $e->ibc_seguridad_horas+= $e->ingresos_prestacionales['total'];
        //Variables con formato de dinero
        $e->ibc_seguridad_horasf = $this->fm($e->ibc_seguridad_horas);        
        return $e;
    }

    function sumarTotales($e){
        $e->total_pagar = $e->salario_real;
        $e->total_pagar+= $e->subsidio_transporte_valor;
        $e->total_pagar+= $e->horas_extras_recargos['total']['valor'];
        $e->total_pagar+= $e->nomina_vac_inc_lic['total'];
        $e->total_pagar+= $e->ingresos_adicionales['total'];
        $e->total_pagar+= $e->liquidar_cesantias['total'];
        $e->total_pagar+= $e->liquidar_interes_cesantias['total'];
        $e->total_pagar+= $e->liquidar_prima['total'];
        $e->total_pagar-= $e->retenciones['totalRetenciones'][0];            
        $e->total_pagarf = '$ '.number_format($e->total_pagar,0,',','.');
        return $e;
    }

    function getLiquidacionCesantias($e){
        $emp = clone $e;
        $prima = ['total'=>0,'total_dias'=>0];
        $ajste = $emp->companias->ajustes;
        if(!$ajste->cesantia_hasta || empty($ajste->cesantia_hasta) || $ajste->cesantia_hasta=='0000-00-00'){
            $ajste->cesantia_desde = date("Y-m-d",strtotime($emp->fecha_contrato));
            $ajste->cesantia_hasta = date("Y-m-d",$emp->companias->periodo[1]);            
        }
        if(
            (!empty($emp->fecha_terminacion) && strtotime($emp->fecha_terminacion)<$emp->companias->periodo[1] && $emp->tipos_contrato->devenga_prestaciones == 1)
            || (!in_array($emp->id,explode(',',$ajste->cesantias_exlusion)) && date("Y",strtotime($emp->fecha_contrato)) < date("Y") && $emp->tipos_contrato->devenga_prestaciones == 1 && $ajste->liquidar_cesantia == 1 && !empty($ajste->cesantia_desde) && !empty($ajste->cesantia_hasta))
        )
        {
            $desde = strtotime($emp->fecha_contrato)>strtotime($ajste->cesantia_desde)?$emp->fecha_contrato:$ajste->cesantia_desde;
            $hasta = $emp->fecha_terminacion && strtotime($emp->fecha_terminacion)<strtotime($ajste->cesantia_hasta)?$emp->fecha_terminacion:$ajste->cesantia_hasta;            

            $dias = $this->getDiasTrabajadosEnPeriodo($emp,strtotime($desde),strtotime($hasta));
            $salario_integral = $emp->salario+get_parameter('subsidio_transporte');            
            $prima_servicios = $ajste->cesantia_valor && $ajste->cesantia_valor>0?($ajste->cesantia_valor*$dias)/360:(($salario_integral*$dias))/360;
            $prima_servicios = $emp->salario_cesantias && $emp->salario_cesantias>0?($emp->salario_cesantias*$dias)/360:$prima_servicios;
            /*$prima_servicios = $emp->salario_cesantias && $emp->salario_cesantias>0?($emp->salario_cesantias*$dias)/360:($salario_integral*$dias)/360;
            $prima_servicios = $ajste->cesantia_valor && $ajste->cesantia_valor>0?($ajste->cesantia_valor*$dias)/360:$prima_servicios;*/
            //echo $emp->salario.'+'.get_parameter('subsidio_transporte').'='.$salario_integral.'<br/>('.$salario_integral.'*'.$dias.')/360='.$prima_servicios;
            //die();
            $prima[] = (object)[
                'nombre'=>'Cesantías para el periodo '.date("d/m/Y",strtotime($desde)).'-'.date("d/m/Y",strtotime($hasta)),
                'valor'=>$prima_servicios,
                'valorf'=>$this->fm($prima_servicios),
                'dias_trabajados'=>$dias
            ];
            $prima['desde'] = $desde;
            $prima['hasta'] = $hasta;
            $prima['total']+= $prima_servicios;
            $prima['total_dias'] = $dias;
            $prima['total_diasf'] = $dias;
            $prima['totalf'] = $this->fm($prima['total']);

            $ajste->cesantia_desde = $desde;
            $ajste->cesantia_hasta = $hasta;          
            //die();          
        }        
        return $prima;
    }

    function getLiquidacionInteresCesantias($e){
        $emp = clone $e;
        $prima = ['total'=>0,'total_dias'=>0];
        $ajste = $emp->companias->ajustes;        
        if(!$ajste->cesantia_hasta || empty($ajste->cesantia_hasta) || $ajste->cesantia_hasta=='0000-00-00'){
            $ajste->cesantia_desde = date("Y-m-d",strtotime($emp->fecha_contrato));
            $ajste->cesantia_hasta = date("Y-m-d",$emp->companias->periodo[1]);
        }
        if(
            (!empty($emp->fecha_terminacion) && strtotime($emp->fecha_terminacion)<$emp->companias->periodo[1] && $emp->tipos_contrato->devenga_prestaciones == 1)
            || (!in_array($emp->id,explode(',',$ajste->cesantias_exlusion)) && date("Y",strtotime($emp->fecha_contrato)) < date("Y") && $emp->tipos_contrato->devenga_prestaciones == 1 && $ajste->liquidar_cesantia == 1 && !empty($ajste->cesantia_desde) && !empty($ajste->cesantia_hasta))
        ){
            $desde = strtotime($emp->fecha_contrato)>strtotime($ajste->cesantia_desde)?$emp->fecha_contrato:$ajste->cesantia_desde;
            $hasta = $emp->fecha_terminacion && strtotime($emp->fecha_terminacion)<strtotime($ajste->cesantia_hasta)?$emp->fecha_terminacion:$ajste->cesantia_hasta;            
            $dias = $this->getDiasTrabajadosEnPeriodo($emp,strtotime($desde),strtotime($hasta));            
            $salario_integral = $emp->salario+get_parameter('subsidio_transporte');            
            $prima_servicios = $ajste->cesantia_valor && $ajste->cesantia_valor>0?($ajste->cesantia_valor*$dias)/360:($salario_integral*$dias)/360;
            $prima_servicios = $emp->salario_cesantias && $emp->salario_cesantias>0?($emp->salario_cesantias*$dias)/360:$prima_servicios;
            $prima_servicios = $emp->fondo_cesantias->porcentaje>0?$prima_servicios*(($emp->fondo_cesantias->porcentaje_interes/100)*$dias)/360:0;

            $prima[] = (object)[
                'nombre'=>'Intereses de cesantias periodo '.date("d/m/Y",strtotime($desde)).'-'.date("d/m/Y",strtotime($hasta)),
                'valor'=>$prima_servicios,
                'valorf'=>$this->fm($prima_servicios),
                'dias_trabajados'=>$dias
            ];
            $prima['desde'] = $desde;
            $prima['hasta'] = $hasta;
            $prima['total']+= $prima_servicios;
            $prima['total_dias'] = $dias;
            $prima['total_diasf'] = $dias;
            $prima['totalf'] = $this->fm($prima['total']);
        }        
        return $prima;
    }

    function getLiquidarPrima($e){
        $emp = clone $e;
        $prima = ['total'=>0,'total_dias'=>0];
        $ajste = $emp->companias->ajustes;
        if(!$ajste->prima_hasta || empty($ajste->prima_hasta) || $ajste->prima_hasta=='0000-00-00'){
            $ajste->prima_desde = date("Y-m-d",strtotime($emp->fecha_contrato));
            $ajste->prima_hasta = date("Y-m-d",$emp->companias->periodo[1]);
        }
        if(
            (!empty($emp->fecha_terminacion) && strtotime($emp->fecha_terminacion)<$emp->companias->periodo[1] && $emp->tipos_contrato->devenga_prestaciones == 1)
            || ($emp->tipos_contrato->devenga_prestaciones == 1 && $ajste->liquidar_prima == 1 && !empty($ajste->prima_desde) && !empty($ajste->prima_hasta))
        ){
            $desde = strtotime($emp->fecha_contrato)>strtotime($ajste->prima_desde)?$emp->fecha_contrato:$ajste->prima_desde;
            $hasta = $emp->fecha_terminacion && strtotime($emp->fecha_terminacion)<strtotime($ajste->prima_hasta)?$emp->fecha_terminacion:$ajste->prima_hasta;            
            $dias = $this->getDiasTrabajadosEnPeriodo($emp,strtotime($desde),strtotime($hasta));            
            $salario_integral = $emp->salario+get_parameter('subsidio_transporte');            
            $prima_servicios = ($salario_integral*$dias)/360;
            $prima[] = (object)[
                'nombre'=>'Prima de servicio periodo '.date("d/m/Y",strtotime($desde)).'-'.date("d/m/Y",strtotime($hasta)),
                'valor'=>$prima_servicios,
                'valorf'=>$this->fm($prima_servicios),
                'dias_trabajados'=>$dias
            ];  
            $prima['desde'] = $desde;
            $prima['hasta'] = $hasta;          
            $prima['total']+= $prima_servicios;
            $prima['total_dias'] = $dias;
            $prima['total_diasf'] = $dias;
            $prima['totalf'] = $this->fm($prima['total']);  
            $ajste->prima_desde = $desde;
            $ajste->prima_hasta = $hasta;  
            $emp->companias->ajustes = $ajste;
        }        
        return $prima;
    }

    function get($id = ''){
    	if(is_numeric($id)){
            $e = $this->empleados($id);
        }else{
            $e = $id;
        }
        if(is_object($e) && !empty($e->id)){
            $e->contrato_terminado = 0;           
            if($e->fecha_terminacion && strtotime($e->fecha_terminacion) < $e->companias->periodo[1]){
                $e->contrato_terminado = 1;
            }
            $this->DiasTrabajadosEnPeriodo = 0;
	    	//$e->dias_trabajados = $this->db->query('SELECT get_dias_trabajados('.$e->id.') as dias')->row()->dias;
            $e->dias_trabajados = $this->getDiasTrabajadosEnPeriodo($e);
	    	$e->dias_periodo = $this->db->query('SELECT get_dias_periodo('.$e->companias_id.') as dias')->row()->dias;            	    	
            $e->tipos_contrato = $this->tipos_contrato($e->tipos_contrato_id);
	    	$e = $this->get_salario($e);
	    	$e = $this->get_nomina_vac_inc_lic($e);
	        $e = $this->get_asignaciones($e);
	        $e->retenciones = $this->getRetenciones($e);                        
	        $e->riesgo_arl = $this->db->get_where('riesgo_arl',['id'=>$e->riesgo_arl_id]);         
	        $e->riesgo_arl = $e->riesgo_arl->num_rows()>0?$e->riesgo_arl->row():(object)['id'=>'0','nombre'=>'Pendiente','porcentaje'=>0];            	        
	        $e->aporte_parafiscales = $e->retenciones['calculos_parafiscales'][0];
	        $e->aporte_parafiscalesf = $e->retenciones['calculos_parafiscales'][1];            
            
			//Optionals    		
	        $e->eps = $this->db->get_where('eps',['id'=>$e->eps_id]);         
	        $e->eps = $e->eps->num_rows()>0?$e->eps->row():(object)['id'=>'0','nombre'=>'Pendiente'];
	        $e->fondo_pensiones = $this->db->get_where('fondo_pensiones',['id'=>$e->fondo_pensiones_id]);         
	        $e->fondo_pensiones = $e->fondo_pensiones->num_rows()>0?$e->fondo_pensiones->row():(object)['id'=>'0','nombre'=>'Pendiente','porcentaje'=>0];
	        $e->fondo_cesantias = $this->db->get_where('fondo_cesantias',['id'=>$e->fondo_cesantias_id]);         
	        $e->fondo_cesantias = $e->fondo_cesantias->num_rows()>0?$e->fondo_cesantias->row():(object)['id'=>'0','nombre'=>'Pendiente','porcentaje'=>0];            
			$e->sedes = $this->db->get_where('sedes',['id'=>$e->sedes_id]);    		
			$e->sedes = $e->sedes->num_rows()>0?$e->sedes->row():(object)['id'=>'0','nombre'=>'Pendiente'];
			$e->areas = $this->db->get_where('areas',['id'=>$e->areas_id]);    		
			$e->areas = $e->areas->num_rows()>0?$e->areas->row():(object)['id'=>'0','nombre'=>'Pendiente'];
			$e->cargos = $this->db->get_where('cargos',['id'=>$e->cargos_id]);    		
			$e->cargos = $e->cargos->num_rows()>0?$e->cargos->row():(object)['id'=>'0','nombre'=>'Pendiente'];
			$e->centro_costos = $this->db->get_where('centro_costos',['id'=>$e->centro_costos_id]);    		
			$e->centro_costos = $e->centro_costos->num_rows()>0?$e->centro_costos->row():(object)['id'=>'0','nombre'=>'Pendiente'];    		
			$e->medios_pago = $this->db->get_where('medios_pago',['id'=>$e->medios_pago_id]);    		
			$e->medios_pago = $e->medios_pago->num_rows()>0?$e->medios_pago->row():(object)['id'=>'0','nombre'=>'Pendiente'];    		    		
			$e->bancos = $this->db->get_where('bancos',['id'=>$e->bancos_id]);    		
			$e->bancos = $e->bancos->num_rows()>0?$e->bancos->row():(object)['id'=>'0','nombre'=>'Pendiente'];    		
			$e->tipos_cuenta = $this->db->get_where('tipos_cuenta',['id'=>$e->tipos_cuenta_id]);    		
			$e->tipos_cuenta = $e->tipos_cuenta->num_rows()>0?$e->tipos_cuenta->row():(object)['id'=>'0','nombre'=>'Pendiente'];    		
	        $e->dotaciones = $this->empleados_dotaciones(NULL,$e->id);
	        $e->licencias = $this->empleados_licencias(NULL,$e->id);
	        $e->sistemas = $this->empleados_sistemas(NULL,$e->id);	        
            $e->permisos = $this->empleados_notificacion_permisos(NULL,$e->id);
            $e->horas_notificadas = $this->empleados_notificacion_horas(NULL,$e->id);          
			//Formats
			$e->nombre_completo = $e->nombre.' '.$e->apellidos;
			$e->salariof = '$ '.number_format($e->salario,0,',','.');
			$e->vacacionesf = $e->vacaciones.' Días';
			$e->descansof = $e->descanso?$e->descanso.' Días':'0 Días';
			$e->subsidio_transportef = $e->subsidio_transporte==1?'SI':'NO';
			$e->fecha_nacimientof = empty($e->fecha_nacimiento)?'Pendiente':date("d/m/Y",strtotime($e->fecha_nacimiento));
	        $e->fecha_contratof = empty($e->fecha_contrato)?'Pendiente':date("d/m/Y",strtotime($e->fecha_contrato));
	        $e->dias_descanso = explode(',',$e->dias_descanso);
	        $e->dias_descansof = implode(', ',$e->dias_descanso);            
			//Tablas
			$e->recurrente_ingresos = $this->recurrente_ingresos($e->id);
			$e->recurrente_deducciones = $this->recurrente_deducciones($e->id);
	        $e->empleados_adjuntos = $this->empleados_adjuntos($e->id);                        	        
	        
            $e->base_cesantia = $e->ibc_seguridad_horas;
            $e->base_cesantia+= $e->subsidio_transporte_valor;
	        $e->base_cesantiaf = $this->fm($e->base_cesantia);
	        $e->fondo_cesantias_valor = $e->fondo_cesantias->porcentaje>0?$e->base_cesantia*($e->fondo_cesantias->porcentaje/100):0;
	        $e->fondo_cesantias_valorf = $this->fm($e->fondo_cesantias_valor);
	        $e->fondo_cesantias_interes = $e->fondo_cesantias->porcentaje>0?$e->fondo_cesantias_valor*($e->fondo_cesantias->porcentaje_interes/100):0;
	        $e->fondo_cesantias_interesf = $this->fm($e->fondo_cesantias_interes);

	        $e->ibc_pension = $e->salario_real;
            $e->ibc_pension+= $e->horas_extras_recargos['total']['valor'];
            $e->ibc_pension+= $e->nomina_vac_inc_lic['total'];
            $e->ibc_pension+= $e->ingresos_prestacionales['total'];
	        $e->ibc_pensionf = $this->fm($e->ibc_pension);

	        $e->ibc_riesgo = $e->salario_real;
            $e->ibc_riesgo+= $e->horas_extras_recargos['total']['valor'];
            $e->ibc_riesgo+= $e->ingresos_prestacionales['total'];
	        $e->ibc_riesgof = $this->fm($e->ibc_riesgo);

            $e->riesgo_arl->valor = $e->riesgo_arl->porcentaje>0?$e->ibc_riesgo*($e->riesgo_arl->porcentaje/100):0;
            $e->riesgo_arl->valorf = $this->fm($e->riesgo_arl->valor);
            $ssp = get_parameter('calculos_seguridad_social_pension');
            $sep = $e->ibc_pension*($ssp/100);
            $e->retenciones['calculos_seguridad_social_pension'] = [$sep,$this->fm($sep)];

            $e->aporte_seguridad_social = $e->retenciones['calculos_seguridad_social_pension'][0]+$e->riesgo_arl->valor;
            $e->aporte_seguridad_socialf = $this->fm($e->aporte_seguridad_social);

	        $e->ibc_parafiscales = $e->salario_real;
            $e->ibc_parafiscales+= $e->horas_extras_recargos['total']['valor'];
            $e->ibc_parafiscales+= $e->nomina_vac_inc_lic['total'];
            $e->ibc_parafiscales+= $e->ingresos_prestacionales['total'];
	        $e->ibc_parafiscalesf = $this->fm($e->ibc_parafiscales);
	        
	        $e->base_prima = $e->ibc_seguridad_horas;
            $e->base_prima+= $e->subsidio_transporte_valor;
	        $e->base_primaf = $this->fm($e->base_prima);
	        $e->prima_servicios = $e->base_prima*(get_parameter('prima_servicios')/100);
	        $e->prima_serviciosf = $this->fm($e->prima_servicios);

            $e->base_vacaciones = $e->ibc_seguridad_horas-$e->horas_extras_recargos['total']['valor'];
	        $e->base_vacaciones = isset($e->horas_extras_recargos['recargo_nocturno'])?$e->base_vacaciones+$e->horas_extras_recargos['recargo_nocturno']['valor']:$e->base_vacaciones;
	        $e->base_vacacionesf = $this->fm($e->base_vacaciones);

	        $e->valor_vacaciones = $e->base_vacaciones*(get_parameter('vacaciones')/100);
	        $e->valor_vacacionesf = $this->fm($e->valor_vacaciones);

	        $e->salario_transporte = $e->salario_real+$e->subsidio_transporte_valor;
	        $e->salario_transportef = $this->fm($e->salario_transporte);

	        $e->total_provisiones = $this->getTotalPrevisiones($e);
	        $e->total_provisionesf = $this->fm($e->total_provisiones);

            $e->liquidar_cesantias = $this->getLiquidacionCesantias($e);
            $e->liquidar_interes_cesantias = $this->getLiquidacionInteresCesantias($e);
            $e->liquidar_prima = $this->getLiquidarPrima($e);
	        $e = $this->sumarTotales($e);
             
            $e->excluido = $this->db->get_where('nomina_exclusion',[
                'empleados_id'=>$e->id,
                'periodo_desde'=>$e->companias->periodo_actual_desde,
                'periodo_hasta'=>$e->companias->periodo_actual_hasta
            ])->num_rows()>0?true:false;

	        return $e;
    	}
    	return null;
    }

    function getEmpleadosEnPeriodo($compania){
        $this->db->where('companias_id',$compania->id);        
        $this->db->where('fecha_contrato <= ',date("Y-m-d",$compania->periodo[1]));
        $this->db->where('(fecha_terminacion IS NULL OR \''.date("Y-m-d",$compania->periodo[0]).'\' BETWEEN \''.date("Y-m-d",$compania->periodo[0]).'\' AND fecha_terminacion OR \''.date("Y-m",$compania->periodo[0]).'\' = DATE_FORMAT(fecha_terminacion,"%Y-%m"))',NULL,TRUE);
        $empleados = $this->db->get('view_nomina_empleados');
        return $empleados;
    }

    function getTotalNomina($compania,$force = true){
        if($this->totalNomina == 0){
            //Calculamos
            if(empty($_SESSION['totalNomina']) || $force){
                $total = 0;
                $empleados = $this->getEmpleadosEnPeriodo($compania);
                foreach($empleados->result() as $e){                
                    $t = $this->get($e->id)->total_pagar;
                    //echo '<p>'.$t.'</p>';
                    $total+=$t;
                }
                $_SESSION['totalNomina'] = $total;
            }        
            $this->totalNomina = $_SESSION['totalNomina'];
        }        
        return [$this->totalNomina,$this->fm($this->totalNomina)];
    }

    function validateSimulation($redirect = true,$desde = '',$hasta = ''){
        $isSimulate = true;
        $empresa = !isset(get_instance()->empresa)?false:get_instance()->empresa;        
        if($empresa){            
            //Verificamos que ya exista registrada una nomina en este periodo
            $desde = empty($desde)?$empresa->periodo_actual_desde:$desde;
            $hasta = empty($hasta)?$empresa->periodo_actual_hasta:$hasta;
            $nomina = $this->db->get_where('historico_nomina',
                [
                    'companias_id'=>$empresa->id,
                    'periodo_desde'=>$desde,
                    'periodo_hasta'=>$hasta
                ]
            );
            if($nomina->num_rows()>0){
                $isSimulate = false;
                if($redirect){
                    redirect('nomina/elegir_periodo/'.$empresa->id);
                }
            }
        }        
        return $isSimulate;
    }

}