<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Elements_old extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->ajustes = $this->db->get('ajustes')->row();
    }

    function addDatosFaltantes($fichero){
        $fichero->medicamentos = 1;
        $fichero->medicamentos_prescripcion = 1;
        $fichero->medicamentos_codigo_ips = 22;
        $fichero->medicamentos_observacion_admin = 3;
        $fichero->medicamentos_observacion_ips = 7;
        $fichero->nutricion = 2;
        $fichero->nutricion_prescripcion = 1;
        $fichero->nutricion_codigo_ips = 21;
        $fichero->nutricion_observacion_admin = 3;
        $fichero->nutricion_observacion_ips = 6;
        $fichero->procedimientos = 3;
        $fichero->procedimientos_prescripcion = 1;
        $fichero->procedimientos_codigo_ips = 21;
        $fichero->procedimientos_observacion_admin = 3;
        $fichero->procedimientos_observacion_ips = 6;
        $fichero->complementarios = 4;
        $fichero->complementarios_prescripcion = 1;
        $fichero->complementarios_codigo_ips = 19;
        $fichero->complementarios_observacion_admin = 3;
        $fichero->complementarios_observacion_ips = 6;
        $fichero->dispositivos = 5;
        $fichero->dispositivos_prescripcion = 1;
        $fichero->dispositivos_codigo_ips = 21;
        $fichero->dispositivos_observacion_admin = 3;
        $fichero->dispositivos_observacion_ips = 6;
        return $fichero;
    }

    function getDatosFromExcel($datosXLS,$fichero){
        $fichero = $this->addDatosFaltantes($fichero);
        $datos = array(
            'medicamentos'=>array(),
            'nutricion'=>array(),
            'procedimientos'=>array(),
            'complementarios'=>array(),
            'dispositivos'=>array()
        );

        $required = array('_prescripcion','_codigo_ips','_observacion_admin','_observacion_ips');        
        //Recorrer las tablas
        foreach($datos as $n=>$d){
            //Traer los datos de cada tabla
            $row = $datosXLS->rows(($fichero->{$n})-1);
            $ids = array();
            if(is_array($row)){
                //Recorrer las filas
                foreach($row as $nn=>$dd){
                    if($nn>0){ //Si no es la primera fila
                        $fila = array();
                        foreach($required as $r=>$rv){ //insertar requeridos
                            $fila[$rv] = $dd[($fichero->{$n.$rv})-1];
                            $ids[] = ($fichero->{$n.$rv})-1;
                        }
                        if(!empty($fila)){
                            $datos[$n][$nn] = $fila;
                        }
                    }   
                }

                //Añadir datos faltantes
                //Recorrer las filas                
                $labels = array();                
                foreach($row as $nn=>$dd){
                    if($nn>0){ //Si no es la primera fila
                        $fila = array();
                        foreach($dd as $r=>$rv){ //insertar datos
                            if(!in_array($r,$ids)){
                                $fila[$labels[$r]] = $rv;
                            }
                        }
                        if(!empty($fila)){
                            $datos[$n][$nn] = array_merge($datos[$n][$nn],$fila);
                        }
                    }else{
                        foreach($dd as $r=>$rv){ //insertar datos
                            $labels[] = $rv;
                        }
                    }
                }
            }

        }
        
        return $datos;
    }

    function insertarDatos($datos,$id){
        foreach($datos as $label=>$array){            
            foreach($array as $n=>$v){
                $dato = array();
                foreach($v as $nn=>$vv){
                    if(!empty($nn)){
                        if(strlen($nn)>30){
                            //Asignar un id genérico
                            $nn = substr($nn,0,30);
                        }
                        $tag = trim($nn);
                        $tag = substr($tag,0,1)=='_'?substr($tag,1):$tag;                        
                        $tag = toUrl($tag);
                        $tag = str_replace('-','_',$tag);
                        $tag = substr($tag,0,1)=='_'?substr($tag,1):$tag;
                        $tag = substr($tag,(strlen($tag)-2),1)=='_'?substr($tag,0,(strlen($tag)-2)):$tag;
                        $dato[$tag] = $vv;
                        $ex = $this->db->get_where('labels',array('table'=>$label,'field'=>$tag,'display_as'=>$nn));
                        if($ex->num_rows()==0){
                            $this->db->insert('labels',array('table'=>$label,'field'=>$tag,'display_as'=>$nn));
                        }
                    }
                }
                if(!empty($dato['codigo_ips'])){
                    $dato['importador_id'] = $id;
                    $this->db->delete($label,array('prescripcion'=>$dato['prescripcion']));
                    $existe = $this->db->get_where($label,array('prescripcion'=>$dato['prescripcion']));                    
                    if($existe->num_rows()==0){
                        $this->checkFields($label,$dato);
                        $this->db->insert($label,$dato);
                    }
                }
            }            
        }
        $this->notificarIPS($id);
    }

    function checkFields($table,$fields){
        foreach($fields as $n=>$v){
            $data = $this->db->field_data($table);
            $enc = false;
            foreach($data as $d){
                if($d->name==$n){
                    $enc = true;
                }
            }
            if(!$enc){
                $this->db->query('ALTER TABLE '.$table.' ADD '.$n.' TEXT');
            }
        }
    }

    function notificarIPS($importador_id){
        $query = "
            SELECT *
            FROM (SELECT 
            importador_id,
            codigo_ips
            FROM complementarios
            UNION 
            SELECT 
            importador_id,
            codigo_ips
            FROM dispositivos
            UNION
            SELECT 
            importador_id,
            codigo_ips
            FROM medicamentos
            UNION
            SELECT 
            importador_id,
            codigo_ips
            FROM nutricion
            UNION 
            SELECT 
            importador_id,
            codigo_ips
            FROM procedimientos) AS datos
            INNER JOIN user ON user.codigo_ips = datos.codigo_ips
            WHERE importador_id = '".$importador_id."'
            GROUP BY datos.codigo_ips
        ";
        $ips = $this->db->query($query);
        foreach($ips->result() as $i){
            $datos = array(
                'email'=>$i->email,
                'nombre'=>$i->nombre,
                'apellido'=>$i->apellido,
                'nombre_importador'=>$this->db->get_where('importador',array('id'=>$importador_id))->row()->nombre,
                'link'=>base_url().'?pw='.base64_encode($i->email)
            );
            get_instance()->enviarcorreo((object)$datos,1);
        }
        //die();
    }

    function notificarModIPS($table,$id){
        $ips = $this->db->get_where($table,array('id'=>$id));
        if($ips->num_rows()>0){
            $ips = $ips->row();
            $str = '<table border="1">';
            $str.= '<thead><tr>';
            foreach($ips as $n=>$v){
                $str.= '<th>'.$n.'</th>';
            }
            $str.= '</tr></thead>';
            $str.= '<tbody><tr>';
            foreach($ips as $n=>$v){
                $str.= '<td>'.$v.'</td>';
            }
            $str.= '</tr></tbody>';
            $str.= '</table>';
            $datos = $ips;
            $datos->data = $str;
            $datos->email = '';
            $datos->nombre = $this->user->nombre.' '.$this->user->apellido;
            get_instance()->enviarcorreo((object)$datos,11);
        }
    }
}
?>
