<?php 
class HistoricoModel extends Elements{	
	function __construct(){
		parent::__construct();       
	}

	/****************************** INSERT **********************/

	function ibd($data,$empleado = false){
		if(!$empleado){
			$data['historico_nomina_empleados_id'] = $this->empleado_nomina;
		}
		$this->db->insert($this->actual_table,$data);
	}

	function ini_empleado(){
		$this->actual_table = 'historico_nomina_empleados';
		$this->ibd([
			'tipos_contrato_id'=>$this->empleado->tipos_contrato_id,
			'reporte'=>$this->empleado->contrato->reporte,
			'historico_nomina_id'=>$this->empleado->nomina_id,
			'empleados_id'=>$this->empleado->id,
			'empleado'=>$this->empleado->empleado2,
			'total_pagar'=>$this->empleado->total_pagar,
			'salario'=>$this->empleado->salario2,
			'horas_extras_recargos'=>$this->empleado->horas_extras_recargos2,
			'vacaciones_incapacidad_licencia'=>$this->empleado->vacaciones_incapacidad_licencia2,
			'ingresos_adicionales'=>$this->empleado->ingresos_adicionales2,
			'deducciones_prestamos'=>$this->empleado->deducciones_prestamos2,
			'pago_empleado'=>$this->empleado->pago_empleado2	
		],true);
		$this->empleado_nomina = $this->db->insert_id();
	}

	function add_colilla(){
		$this->actual_table = 'historico_nomina_resumen_pago';
		$empleado = $this->empleado;
		$empleado->companias = $this->elements->companias($empleado->companias_id);
		$ajste = $empleado->companias->ajustes;	

		$this->ibd(['concepto'=>'Salario','valor'=>$empleado->salario_real]);
		if($empleado->subsidio_transporte_valor>0){
			$this->ibd(['concepto'=>'Subsidio de transporte','valor'=>$empleado->subsidio_transporte_valor]);
		}
		if($empleado->horas_extras_recargos['total']['valor']>0){
			$this->ibd(['concepto'=>'Horas extras, ordinarias y recargos','valor'=>$empleado->horas_extras_recargos['total']['valor']]);
		}
		if($empleado->nomina_vac_inc_lic['total']>0){
			$this->ibd(['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$empleado->nomina_vac_inc_lic['total']]);
		}
		if($empleado->ingresos_adicionales['total']>0){
			$this->ibd(['concepto'=>'Ingresos adicionales','valor'=>$empleado->ingresos_adicionales['total']]);
		}
		if($empleado->liquidar_prima['total']>0){
			$this->ibd(['concepto'=>'Prima de servicios periodo '.date('d/m/Y',strtotime($empleado->liquidar_prima['desde'])).' - '.date('d/m/Y',strtotime($empleado->liquidar_prima['hasta'])),'valor'=>$empleado->liquidar_prima['total']]);
		}
		if($empleado->liquidar_cesantias['total']>0){
			$this->ibd(['concepto'=>'Cesantias periodo '.date('d/m/Y',strtotime($empleado->liquidar_cesantias['desde'])).' - '.date('d/m/Y',strtotime($empleado->liquidar_cesantias['hasta'])),'valor'=>$empleado->liquidar_cesantias['total']]);
		}
		if($empleado->liquidar_interes_cesantias['total']>0){
			$this->ibd(['concepto'=>'Interes Cesantias periodo '.date('d/m/Y',strtotime($empleado->liquidar_interes_cesantias['desde'])).' - '.date('d/m/Y',strtotime($empleado->liquidar_interes_cesantias['hasta'])),'valor'=>$empleado->liquidar_interes_cesantias['total']]);
		}
		if($empleado->retenciones['totalRetenciones'][0]>0){
			$this->ibd(['concepto'=>'Retenciones y deducciones','valor'=>$empleado->retenciones['totalRetenciones'][0]]);
		}		
	}
	function add_dias_trabajados(){
		$this->actual_table = 'historico_nomina_dias_trabajados';
		$empleado = $this->empleado;
		$this->ibd(['concepto'=>'Días del período','valor'=>$empleado->dias_periodo]);
		foreach($empleado->nomina_vac_inc_lic as $n=>$i){
			if(is_object($i)){
				$this->ibd(['concepto'=>$i->nombre,'valor'=>-$i->dias]);
			}
		}
	}
	function add_salario_base_subsidio(){
		$this->actual_table = 'historico_nomina_salario';
		$empleado = $this->empleado;
		$str = 'Salario';		
		if($this->empleado->tipos_contrato_id == 2){
			$str = 'Honorarios';
		}
		$this->ibd(['concepto'=>$str,'valor_mensual'=>$empleado->salario,'dias_trabajados'=>$empleado->dias_trabajados,'valor'=>$empleado->salario_real]);
		if($empleado->subsidio_transporte_valor>0){
			$this->ibd(['concepto'=>'Subsidio de transporte','valor_mensual'=>$empleado->def_subsidio_transporte,'dias_trabajados'=>$empleado->dias_transporte,'valor'=>$empleado->subsidio_transporte_valor]);
		}
	}
	function add_horas_extras_recargos(){
		$this->actual_table = 'historico_nomina_horas_extras';
		$empleado = $this->empleado;
		foreach($empleado->horas_extras_recargos as $n=>$h){
			if(!empty($h['nombre'])){
				$this->ibd([
					'concepto'=>$h['nombre'],
					'valor_hora'=>$h['porcentaje'],
					'horas'=>$h['cantidad'],
					'valor'=>$h['valor'],
				]);
			}
		}

	}
	function add_incapacidades(){
		$this->actual_table = 'historico_nomina_incapacidades';
		$empleado = $this->empleado;
		foreach($empleado->incapacidades as $a){
			if(is_object($a)){
				$this->ibd([
					'desde'=>$a->desde,
					'hasta'=>$a->hasta,
					'concepto'=>$a->nombre,
					'dias'=>$a->dias,
					'salario'=>$empleado->salario,
					'valor'=>$a->valor
				]);
			}
		}
	}
	function add_licencias(){
		$this->actual_table = 'historico_nomina_licencias';
		$empleado = $this->empleado;
		foreach($empleado->inc_licencias as $a){
			if(is_object($a)){
				$this->ibd([
					'desde'=>$a->desde,
					'hasta'=>$a->hasta,
					'concepto'=>$a->nombre,
					'dias'=>$a->dias,
					'salario'=>$empleado->salario,
					'valor'=>$a->valor
				]);
			}
		}
	}
	function add_vacaciones(){
		$this->actual_table = 'historico_nomina_vacaciones';
		$empleado = $this->empleado;
		foreach($empleado->inc_vacaciones as $a){
			if(is_object($a)){
				$this->ibd([
					'desde'=>$a->desde,
					'hasta'=>$a->hasta,
					'concepto'=>$a->nombre,
					'dias'=>$a->dias,
					'salario'=>$empleado->salario,
					'valor'=>$a->valor
				]);
			}
		}
	}
	function add_ingresos_adicionales(){
		$this->actual_table = 'historico_nomina_ingresos';
		$empleado = $this->empleado;
		foreach($empleado->ingresos_adicionales as $i){
			if(is_object($i)){
				$this->ibd([
					'concepto'=>$i->nombre,
					'valor'=>$i->valor,
				]);
			}
		}		
	}
	function add_ibc_seguridad_social(){
		$this->actual_table = 'historico_nomina_ibc_seguridad';
		$empleado = $this->empleado;
		$this->ibd(['concepto'=>'Salario','valor'=>$empleado->salario_real]);
		if($empleado->horas_extras_recargos['total']['valor']>0){
			$this->ibd(['concepto'=>'Horas extras, ordinarias y recargos','valor'=>$empleado->horas_extras_recargos['total']['valor']>0]);
		}
		if($empleado->nomina_vac_inc_lic['total']>0){
			$this->ibd(['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$empleado->nomina_vac_inc_lic['total']>0]);
		}
		if($empleado->ingresos_prestacionales['total']>0){
			$this->ibd(['concepto'=>'Ingresos salariales','valor'=>$empleado->ingresos_prestacionales['total']>0]);
		}
	}

	function add_retenciones(){
		if($this->empleado->retenciones['total'][0]>0){
			$this->actual_table = 'historico_nomina_retenciones';
			$empleado = $this->empleado;
			$this->ibd([
				'concepto'=>'Salud',
				'porcentaje'=>get_parameter('retenciones_salud'),
				'ibc_seguridad_social'=>$empleado->ibc_seguridad_horas,
				'valor'=>$empleado->retenciones['retenciones_salud'][0]
			]);
			$this->ibd([
				'concepto'=>'Pensión',
				'porcentaje'=>get_parameter('retenciones_pensión'),
				'ibc_seguridad_social'=>$empleado->ibc_seguridad_horas,
				'valor'=>$empleado->retenciones['retenciones_pensión'][0]
			]);
			if($empleado->retenciones['fondo_solidaridad_pensional'][0]>0){
				$this->ibd([
					'concepto'=>'Fondo de Solidaridad Pensional',
					'porcentaje'=>$this->CalculosModel->getPorcentajeFondoSolidaridadPensional($empleado),
					'ibc_seguridad_social'=>$empleado->ibc_seguridad_horas,
					'valor'=>$empleado->retenciones['fondo_solidaridad_pensional'][0]
				]);
			}
		}
	}
	function add_deducciones(){
		if($this->empleado->retenciones['deducciones']['total'][0]>0){
			$this->actual_table = 'historico_nomina_deducciones';
			$empleado = $this->empleado;
			foreach($empleado->retenciones['deducciones'] as $r){
				if(is_object($r)){
					if(isset($r->mas_detalles)){
						$r->nombre.= ' '.$r->mas_detalles;
					}
					$this->ibd(['concepto'=>$r->nombre,'valor'=>$r->valor]);
				}
			}
		}
	}
	function add_seguridad_social_parafiscales(){
		$this->actual_table = 'historico_nomina_seguridad_social';
		$empleado = $this->empleado;
		$this->ibd([
			'concepto'=>'Pensión',
			'ibc'=>$empleado->ibc_pension,
			'porcentaje_empresa'=>get_parameter('calculos_seguridad_social_pension'),
			'valor'=>$empleado->retenciones['calculos_seguridad_social_pension'][0]
		]);
		$this->ibd([
			'concepto'=>$empleado->riesgo_arl->nombre,
			'ibc'=>$empleado->ibc_riesgo,
			'porcentaje_empresa'=>$empleado->riesgo_arl->porcentaje,
			'valor'=>$empleado->riesgo_arl->valor
		]);
	}
	function add_calculos_parafiscales(){
		$this->actual_table = 'historico_nomina_parafiscales';
		$empleado = $this->empleado;
		$this->ibd([
			'concepto'=>'Caja de Compensación',
			'ibc'=>$empleado->ibc_seguridad_horas,
			'porcentaje_empresa'=>get_parameter('calculos_parafiscales'),
			'valor'=>$empleado->retenciones['calculos_parafiscales'][0]			
		]);
	}
	function add_calculo_ibc_pension(){
		$this->actual_table = 'historico_nomina_ibc_pension';
		$empleado = $this->empleado;
		$this->ibd(['concepto'=>'Salario','valor'=>$empleado->salario_real]);
		
		if($empleado->horas_extras_recargos['total']['valor']>0){
			$this->ibd(['concepto'=>'Horas extras, ordinarias y recargos','valor'=>$empleado->horas_extras_recargos['total']['valor']]);
		}
		if($empleado->ingresos_prestacionales['total']>0){
			$this->ibd(['concepto'=>'Ingresos salariales','valor'=>$empleado->ingresos_prestacionales['total']]);
		}
		if($empleado->nomina_vac_inc_lic['total']>0){
			$this->ibd(['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$empleado->nomina_vac_inc_lic['total']]);
		}
	}
	function add_calculo_ibc_riesgo(){
		$this->actual_table = 'historico_nomina_ibc_riesgos';
		$empleado = $this->empleado;
		$this->ibd(['concepto'=>'Salario','valor'=>$empleado->salario_real]);
		if($empleado->horas_extras_recargos['total']['valor']>0){
			$this->ibd(['concepto'=>'Horas extras, ordinarias y recargos','valor'=>$empleado->horas_extras_recargos['total']['valor']]);
		}
		if($empleado->ingresos_prestacionales['total']>0){
			$this->ibd(['concepto'=>'Ingresos salariales','valor'=>$empleado->ingresos_prestacionales['total']]);
		}
	}
	function add_calculo_ibc_parafiscales(){
		$this->actual_table = 'historico_nomina_ibc_parafiscales';
		$empleado = $this->empleado;
		$this->ibd(['concepto'=>'Salario','valor'=>$empleado->salario_real]);
		if($empleado->horas_extras_recargos['total']['valor']>0){
			$this->ibd(['concepto'=>'Horas extras, ordinarias y recargos','valor'=>$empleado->horas_extras_recargos['total']['valor']]);
		}
		if($empleado->ingresos_prestacionales['total']>0){
			$this->ibd(['concepto'=>'Ingresos salariales','valor'=>$empleado->ingresos_prestacionales['total']]);
		}
		if($empleado->nomina_vac_inc_lic['total']>0){
			$this->ibd(['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$empleado->nomina_vac_inc_lic['total']]);
		}
	}
	function add_prestaciones_sociales(){
		$this->actual_table = 'historico_nomina_prestaciones_sociales';
		$empleado = $this->empleado;
		$this->ibd([
			'concepto'=>'Cesantias',
			'base'=>$empleado->base_cesantia,
			'porcentaje'=>$empleado->fondo_cesantias->porcentaje,
			'valor'=>$empleado->fondo_cesantias_valor,
		]);
		$this->ibd([
			'concepto'=>'Intereses a las Cesantias',
			'base'=>$empleado->fondo_cesantias_valor,
			'porcentaje'=>$empleado->fondo_cesantias->porcentaje_interes,
			'valor'=>$empleado->fondo_cesantias_interes,
		]);
		$this->ibd([
			'concepto'=>'Prima de Servicios',
			'base'=>$empleado->base_prima,
			'porcentaje'=>get_parameter('prima_servicios'),
			'valor'=>$empleado->prima_servicios,
		]);
		$this->ibd([
			'concepto'=>'Vacaciones',
			'base'=>$empleado->base_vacaciones,
			'porcentaje'=>get_parameter('vacaciones'),
			'valor'=>$empleado->valor_vacaciones,
		]);
	}
	function add_cesantias_intereses(){
		$this->actual_table = 'historico_nomina_base_cesantias';
		$empleado = $this->empleado;
		$this->ibd(['concepto'=>'Salario','valor'=>$empleado->salario_real]);
		if($empleado->subsidio_transporte_valor>0){
			$this->ibd(['concepto'=>'Subsidio de transporte','valor'=>$empleado->subsidio_transporte_valor]);
		}
		if($empleado->horas_extras_recargos['total']['valor']>0){
			$this->ibd(['concepto'=>'Horas extras, ordinarias y recargos','valor'=>$empleado->horas_extras_recargos['total']['valor']]);
		}
		if($empleado->nomina_vac_inc_lic['total']>0){
			$this->ibd(['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$empleado->nomina_vac_inc_lic['total']]);
		}
		if($empleado->ingresos_prestacionales['total']>0){
			$this->ibd(['concepto'=>'Ingresos salariales','valor'=>$empleado->ingresos_prestacionales['total']]);
		}
	}
	function add_base_prima(){
		$this->actual_table = 'historico_nomina_base_prima';
		$empleado = $this->empleado;
		$this->ibd(['concepto'=>'Salario','valor'=>$empleado->salario_real]);
		if($empleado->subsidio_transporte_valor>0){
			$this->ibd(['concepto'=>'Subsidio de transporte','valor'=>$empleado->subsidio_transporte_valor]);
		}
		if($empleado->horas_extras_recargos['total']['valor']>0){
			$this->ibd(['concepto'=>'Horas extras, ordinarias y recargos','valor'=>$empleado->horas_extras_recargos['total']['valor']]);
		}
		if($empleado->nomina_vac_inc_lic['total']>0){
			$this->ibd(['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$empleado->nomina_vac_inc_lic['total']]);
		}
		if($empleado->ingresos_prestacionales['total']>0){
			$this->ibd(['concepto'=>'Ingresos salariales','valor'=>$empleado->ingresos_prestacionales['total']]);
		}		
	}
	function add_base_vacaciones(){
		$this->actual_table = 'historico_nomina_base_vacaciones';
		$empleado = $this->empleado;
		$this->ibd(['concepto'=>'Salario','valor'=>$empleado->salario_real]);
		if(isset($empleado->horas_extras_recargos['recargo_nocturno'])){
			$this->ibd(['concepto'=>'Recargo nocturno','valor'=>$empleado->horas_extras_recargos['recargo_nocturno']]);
		}
		if($empleado->nomina_vac_inc_lic['total']>0){
			$this->ibd(['concepto'=>'Vacaciones, Licencias e Incapacidades','valor'=>$empleado->nomina_vac_inc_lic['total']]);
		}
		if($empleado->ingresos_prestacionales['total']>0){
			$this->ibd(['concepto'=>'Ingresos salariales','valor'=>$empleado->ingresos_prestacionales['total']]);
		}
	}

	function add_liquidar_cesantias(){
		$this->actual_table = 'historico_nomina_liquidar_cesantias';
		$empleado = $this->empleado;
		if($empleado->liquidar_cesantias['total']>0){
			foreach($empleado->liquidar_cesantias as $a){
				if(is_object($a)){
					$this->ibd([
						'concepto'=>$a->nombre,
						'dias'=>$a->dias_trabajados,
						'valor'=>$a->valor
					]);
				}
			}
		}
	}

	function add_liquidar_cesantias_interes(){
		$this->actual_table = 'historico_nomina_liquidar_interes_cesantias';
		$empleado = $this->empleado;
		if($empleado->liquidar_cesantias['total']>0){
			foreach($empleado->liquidar_interes_cesantias as $a){
				if(is_object($a)){
					$this->ibd([
						'concepto'=>$a->nombre,
						'dias'=>$a->dias_trabajados,
						'valor'=>$a->valor
					]);
				}
			}
		}
	}

	function add_liquidar_prima(){
		$this->actual_table = 'historico_nomina_liquidar_prima';
		$empleado = $this->empleado;
		if($empleado->liquidar_prima['total']>0){
			foreach($empleado->liquidar_prima as $a){
				if(is_object($a)){
					$this->ibd([
						'concepto'=>$a->nombre,
						'dias'=>$a->dias_trabajados,
						'valor'=>$a->valor
					]);
				}
			}
		}
	}

	function insert($empleado){
		$this->empleado = $empleado;
		$this->ini_empleado();
		$this->add_colilla();
        $this->add_dias_trabajados();
        $this->add_salario_base_subsidio();
        $this->add_horas_extras_recargos();
        $this->add_incapacidades();
        $this->add_licencias();
        $this->add_vacaciones();
        $this->add_ingresos_adicionales();
        $this->add_ibc_seguridad_social();
        $this->add_retenciones();
        $this->add_deducciones();
		$this->add_seguridad_social_parafiscales();
        $this->add_calculos_parafiscales();
        $this->add_calculo_ibc_pension();
        $this->add_calculo_ibc_riesgo();
        $this->add_calculo_ibc_parafiscales();
		$this->add_prestaciones_sociales();
        $this->add_cesantias_intereses();
        $this->add_base_prima();
        $this->add_base_vacaciones();
        $this->add_liquidar_prima();
        $this->add_liquidar_cesantias();
        $this->add_liquidar_cesantias_interes();
	}

	/************************** FIN INSERT *******************/
	/*														 */
	/*														 */
	/*														 */
	/*														 */
	/*														 */
	/*														 */
	/*														 */
	/*														 */
	/************************** GET **************************/

	function gdb($table,$emp){		
		$tabla = $this->db->get_where($table,['historico_nomina_empleados_id'=>$emp->id]);
		foreach($tabla->result() as $n=>$t){
			foreach($t as $nn=>$tt){
				if(is_numeric($tt)){
					$tabla->row($n)->{$nn.'f'} = $this->fm($tt);
				}
			}
		}
		return $tabla;
	}

	function get_empleados($id = ''){
		if(!is_numeric($id)){
			$nomina = $this->nomina;
			$this->db->order_by('historico_nomina_empleados.empleado','ASC');
			$empleados = $this->db->get_where('historico_nomina_empleados',['historico_nomina_id'=>$nomina->id]);
			$nomina->cuerpo = [];
		}else{			
			$empleados = $this->db->get_where('historico_nomina_empleados',['historico_nomina_empleados.id'=>$id]);
		}		
		foreach($empleados->result() as $n=>$e){			
			$e = $this->get_colilla($e);
	        $e = $this->get_dias_trabajados($e);
	        $e = $this->get_salario_base_subsidio($e);
	        $e = $this->get_horas_extras_recargos($e);
	        $e = $this->get_incapacidades($e);
	        $e = $this->get_licencias($e);
	        $e = $this->get_vacaciones($e);
	        $e = $this->get_ingresos_adicionales($e);
	        $e = $this->get_ibc_seguridad_social($e);
	        $e = $this->get_retenciones($e);
	        $e = $this->get_deducciones($e);
			$e = $this->get_seguridad_social_parafiscales($e);
	        $e = $this->get_calculos_parafiscales($e);
	        $e = $this->get_calculo_ibc_pension($e);
	        $e = $this->get_calculo_ibc_riesgo($e);
	        $e = $this->get_calculo_ibc_parafiscales($e);
			$e = $this->get_prestaciones_sociales($e);
	        $e = $this->get_cesantias_intereses($e);
	        $e = $this->get_base_prima($e);
	        $e = $this->get_base_vacaciones($e);
	        $e = $this->get_liquidar_cesantias($e);	
	        $e = $this->get_liquidar_prima($e);
	        $e = $this->get_liquidar_interes_cesantias($e);
	        $empleados->row($n)->datos = $this->empleados($e->empleados_id);
	        $empleados->row($n)->nomina = $e; 
	        if(!is_numeric($id)){
	        	$nomina->cuerpo[$empleados->row($n)->datos->contrato->reporte][] = $e;
	    	}
	    	$empleados->row($n)->fondo_pensiones = $this->db->get_where('fondo_pensiones',['id'=>$empleados->row($n)->datos->fondo_pensiones_id]);         
	        $empleados->row($n)->fondo_pensiones = $empleados->row($n)->fondo_pensiones->num_rows()>0?$empleados->row($n)->fondo_pensiones->row():(object)['id'=>'0','nombre'=>'Pendiente','porcentaje'=>0];
	        $empleados->row($n)->fondo_cesantias = $this->db->get_where('fondo_cesantias',['id'=>$empleados->row($n)->datos->fondo_cesantias_id]);         
	        $empleados->row($n)->fondo_cesantias = $empleados->row($n)->fondo_cesantias->num_rows()>0?$empleados->row($n)->fondo_cesantias->row():(object)['id'=>'0','nombre'=>'Pendiente','porcentaje'=>0];            
		}	
		if(is_numeric($id)){
			return $empleados->num_rows()>0?$empleados->row():false;
		}else{
			$nomina->empleados = $empleados;
			return $nomina;	
		}
	}

	function get_colilla($e){		
		$table = 'historico_nomina_resumen_pago';	
		$e->$table = $this->gdb($table,$e);
		return $e;																																					
	}	
    function get_dias_trabajados($e){
    	$table = 'historico_nomina_dias_trabajados';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_salario_base_subsidio($e){
    	$table = 'historico_nomina_salario';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_horas_extras_recargos($e){
    	$table = 'historico_nomina_horas_extras';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_incapacidades($e){
    	$table = 'historico_nomina_incapacidades';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_licencias($e){
    	$table = 'historico_nomina_licencias';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_vacaciones($e){
    	$table = 'historico_nomina_vacaciones';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_ingresos_adicionales($e){
    	$table = 'historico_nomina_ingresos';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_ibc_seguridad_social($e){
    	$table = 'historico_nomina_ibc_seguridad';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_retenciones($e){
    	$table = 'historico_nomina_retenciones';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_deducciones($e){
    	$table = 'historico_nomina_deducciones';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
	function get_seguridad_social_parafiscales($e){
		$table = 'historico_nomina_seguridad_social';
		$e->$table = $this->gdb($table,$e);
		return $e;
	}
    function get_calculos_parafiscales($e){
    	$table = 'historico_nomina_parafiscales';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_calculo_ibc_pension($e){
    	$table = 'historico_nomina_ibc_pension';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_calculo_ibc_riesgo($e){
    	$table = 'historico_nomina_ibc_riesgos';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_calculo_ibc_parafiscales($e){
    	$table = 'historico_nomina_ibc_parafiscales';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
	function get_prestaciones_sociales($e){
		$table = 'historico_nomina_prestaciones_sociales';
		$e->$table = $this->gdb($table,$e);
		return $e;
	}
    function get_cesantias_intereses($e){
    	$table = 'historico_nomina_base_cesantias';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }
    function get_base_prima($e){
    	$table = 'historico_nomina_base_prima';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }

    function get_base_vacaciones($e){
    	$table = 'historico_nomina_base_vacaciones';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }

    function get_liquidar_cesantias($e){
    	$table = 'historico_nomina_liquidar_cesantias';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }

    function get_liquidar_prima($e){
    	$table = 'historico_nomina_liquidar_prima';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }

    function get_liquidar_interes_cesantias($e){
		$table = 'historico_nomina_liquidar_interes_cesantias';
    	$e->$table = $this->gdb($table,$e);
		return $e;
    }

    function get_exclusion(){
    	$nomina = $this->nomina;
    	$nomina->exclusion = $this->db->get_where('historico_nomina_empleados',['historico_nomina_id'=>$nomina->id]);		
    	foreach($nomina->exclusion->result() as $n=>$ee){
    		$nomina->exclusion->row($n)->datos = $this->empleados($ee->empleados_id);
    	}
    	return $nomina;
    }	

	function get($nomina){
		$this->nomina = $nomina;
		$nomina = $this->get_empleados();  
		$nomina = $this->get_exclusion();
        return $nomina;
	}
}