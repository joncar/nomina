<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Elements extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->ajustes = $this->db->get('ajustes')->row();
    }

    //Dar formato a dinero
    function fm($number){
        return '$ '.number_format($number,0,',','.');
    }

    

    function recurrente_deducciones($empleado_id = '',$id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('recurrente_deducciones',['recurrente_deducciones.id'=>$id]);
        }else{
            $datos = $this->db->get_where('recurrente_deducciones',['empleados_id'=>$empleado_id]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations            
            $datos->row($n)->valorf = '$'.number_format($e->valor,0,',','.');
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function recurrente_ingresos($empleado_id = '',$id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('recurrente_ingresos',['recurrente_ingresos.id'=>$id]);
        }else{
            $datos = $this->db->get_where('recurrente_ingresos',['empleados_id'=>$empleado_id]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations
            $datos->row($n)->concepto = $this->db->get_where('recurrente_ingresos_conceptos',['id'=>$e->concepto])->row();
            $datos->row($n)->valorf = '$'.number_format($e->valor,0,',','.');
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function recurrente_deducciones_conceptos($id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('recurrente_deducciones_conceptos',['recurrente_deducciones_conceptos.id'=>$id]);
        }else{
            $datos = $this->db->get_where('recurrente_deducciones_conceptos',[]);
        }         
        foreach($datos->result() as $n=>$e){
            
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function recurrente_ingresos_conceptos($id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('recurrente_ingresos_conceptos',['recurrente_ingresos_conceptos.id'=>$id]);
        }else{
            $datos = $this->db->get_where('recurrente_ingresos_conceptos',[]);
        }         
        foreach($datos->result() as $n=>$e){
                    
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function empleados_adjuntos($empleado_id = '',$id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('empleados_adjuntos',['empleados_adjuntos.id'=>$id]);
        }else{
            $datos = $this->db->get_where('empleados_adjuntos',['empleados_id'=>$empleado_id]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations
            $datos->row($n)->fichero_src = base_url('adjuntos/'.$e->fichero);
            $datos->row($n)->fichero_link = '<a href="'.$datos->row($n)->fichero_src.'">'.$e->nombre.'</a>';
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function empleados_licencias($id = '',$empleado_id = '',$limit = 5){
        $this->db->order_by('desde','DESC');
        if($limit>0){
            $this->db->limit($limit);
        }
        if(!empty($id)){
            $datos = $this->db->get_where('nomina_vac_inc_lic',['nomina_vac_inc_lic.id'=>$id]);
        }else{
            $datos = $this->db->get_where('nomina_vac_inc_lic',['empleados_id'=>$empleado_id]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations            
            $datos->row($n)->desdef = date("d/m/Y",strtotime($e->desde));
            $datos->row($n)->hastaf = date("d/m/Y",strtotime($e->hasta));
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function empleados_dotaciones($id = '',$empleado_id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('empleados_dotaciones',['empleados_dotaciones.id'=>$id]);
        }else{
            $datos = $this->db->get_where('empleados_dotaciones',['empleados_id'=>$empleado_id]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations
            $datos->row($n)->dotacion = @$this->db->get_where('dotaciones',['id'=>$e->dotaciones_id])->row();
            $datos->row($n)->fecha = $e->fecha_dotacion;
            $datos->row($n)->fechaf = date("d/m/Y",strtotime($e->fecha));
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function empleados_sistemas($id = '',$empleado_id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('empleados_sistemas',['empleados_sistemas.id'=>$id]);
        }else{
            $datos = $this->db->get_where('empleados_sistemas',['empleados_id'=>$empleado_id]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations            
            $datos->row($n)->sistemas = @$this->db->get_where('sistemas',['id'=>$e->sistemas_id])->row();
            $datos->row($n)->estadof = $e->estado==1?'Activo':'Inactivo';
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    /************ Tabla informativa donde los empleados suben sus permisos ********************/
    function empleados_notificacion_permisos($id = '',$empleado_id = ''){
        $this->db->order_by('desde','DESC');
        if(!empty($id)){
            $datos = $this->db->get_where('empleados_notificacion_permisos',['empleados_notificacion_permisos.id'=>$id]);
        }else{
            $datos = $this->db->get_where('empleados_notificacion_permisos',['empleados_id'=>$empleado_id]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations            
            $datos->row($n)->desdef = date("d-m-Y H:i",strtotime($e->desde));
            $datos->row($n)->hastaf = date("d-m-Y H:i",strtotime($e->hasta));

        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function empleados_notificacion_horas($id = '',$empleado_id = ''){
        $this->db->order_by('desde','DESC');
        if(!empty($id)){
            $datos = $this->db->get_where('empleados_notificacion_horas',['empleados_notificacion_horas.id'=>$id]);
        }else{
            $datos = $this->db->get_where('empleados_notificacion_horas',['empleados_id'=>$empleado_id]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations            
            $datos->row($n)->desdef = date("d-m-Y H:i",strtotime($e->desde));
            $datos->row($n)->hastaf = date("d-m-Y H:i",strtotime($e->hasta));

        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function termino_contrato($id = '',$empleado_id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('termino_contrato',['termino_contrato.id'=>$id]);
        }else{
            $datos = $this->db->get_where('termino_contrato',[]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations                        
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function tipos_arl($id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('tipos_arl',['tipos_arl.id'=>$id]);
        }else{
            $datos = $this->db->get_where('tipos_arl',[]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations                        
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function riesgo_arl($id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('riesgo_arl',['riesgo_arl.id'=>$id]);
        }else{
            $datos = $this->db->get_where('riesgo_arl',[]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations                        
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function tipos_documento($id = ''){
        if(!empty($id)){
            $datos = $this->db->get_where('tipos_documento',['tipos_documento.id'=>$id]);
        }else{
            $datos = $this->db->get_where('tipos_documento',[]);
        }         
        foreach($datos->result() as $n=>$e){
            //Relations                        
        }

        if(!empty($id)){
            return $datos->num_rows()>0?$datos->row():null;
        }
        return $datos;
    }

    function companias($id = ''){        
        if(!empty($id)){
            //$this->db->select('companias.*, get_dias_periodo(companias.id) as dias_periodo');
            $this->db->select('companias.*');
            $companias = $this->db->get_where('companias',['companias.id'=>$id]);
        }else{
            if($this->user->admin==1){
                $companias = $this->db->get_where('companias');
            }else{
                if(!empty($this->user->empleados_id)){
                    $compania_id = $this->empleados($this->user->empleados_id)->companias_id;
                }else{
                    $compania_id = -1;
                }
                //$this->db->select('companias.*, get_dias_periodo(companias.id) as dias_periodo');
                $this->db->select('companias.*');
                $companias = $this->db->get_where('companias',['id'=>$compania_id]);                
            }
        }  
        if($this->user->admin!=1){            
            $emp = $this->db->get_where('empleados',['id'=>$this->user->empleados_id]);
            $emp = $emp->num_rows()>0?$emp->row()->companias_id:-1;
            if($companias->num_rows()>0 && $companias->row()->user_id!=$this->user->id && $companias->row()->id != $emp){
                redirect('panel');
                die();
            }
        }

        foreach($companias->result() as $n=>$e){
            $companias->row($n)->periodo = [strtotime($e->periodo_actual_desde),strtotime($e->periodo_actual_hasta)];
            $companias->row($n)->periodof = date("d",$companias->row($n)->periodo[0]).'/'.meses_short(date("m",$companias->row($n)->periodo[0])).' al '.date("d",$companias->row($n)->periodo[1]).' de '.meses_short(date("m",$companias->row($n)->periodo[1])).'/'.date("Y",$companias->row($n)->periodo[1]);
            $companias->row($n)->caja_compensacion_familiar = $this->caja_compensacion_familiar($e->caja_compensacion_familiar_id);
            $companias->row($n)->ajustes = $this->nomina_ajustes($e->id,$e->periodo_actual_desde,$e->periodo_actual_hasta);
            $companias->row($n)->dias_periodo = $this->getDiasEnPeriodo($companias->row($n)->periodo);
            $companias->row($n)->riesgo_arl = $this->riesgo_arl($companias->row($n)->riesgo_arl_id);
            $companias->row($n)->tipo_documento_legal = $this->tipos_documento($companias->row($n)->tipo_documento_legal);
        }

        if(!empty($id)){
            return $companias->num_rows()>0?$companias->row():null;
        }
        return $companias;

    }

    function mergeAjustes($origin,$replace){        
        if($replace->liquidar_prima==1){
            $origin->liquidar_prima = 1;
            $origin->prima_desde = !empty($replace->prima_desde) && $replace->prima_desde!='0000-00-00'?$replace->prima_desde:$origin->prima_desde;
            $origin->prima_hasta = !empty($replace->prima_hasta) && $replace->prima_hasta!='0000-00-00'?$replace->prima_hasta:$origin->prima_hasta;
        }
        if($replace->liquidar_cesantia==1){
            $origin->liquidar_cesantia = 1;
            $origin->cesantia_desde = !empty($replace->cesantia_desde) && $replace->cesantia_desde!='0000-00-00'?$replace->cesantia_desde:$origin->cesantia_desde;
            $origin->cesantia_hasta = !empty($replace->cesantia_hasta) && $replace->cesantia_hasta!='0000-00-00'?$replace->cesantia_hasta:$origin->cesantia_hasta;
        }
        if($replace->liquidar_interes_cesantia==1){
            $origin->liquidar_interes_cesantia = 1;
            $origin->cesantia_interes_desde = !empty($replace->cesantia_interes_desde) && $replace->cesantia_interes_desde!='0000-00-00'?$replace->cesantia_interes_desde:$origin->cesantia_interes_desde;
            $origin->cesantia_interes_hasta = !empty($replace->cesantia_interes_hasta) && $replace->cesantia_interes_hasta!='0000-00-00'?$replace->cesantia_interes_hasta:$origin->cesantia_interes_hasta;
        }
        if($replace->liquidar_vacaciones==1){
            $origin->liquidar_vacaciones = 1;            
        }
        if(!empty($replace->cesantia_valor)){
            $origin->cesantia_valor = $replace->cesantia_valor;            
        }        
        return $origin;
    }

    /************************** CALCULOS DE NOMINA *****************/

    function empleados($id = ''){
    	if(!empty($id)){
    		$empleados = $this->db->get_where('empleados',['empleados.id'=>$id]);
    	}else{
    		$empleados = $this->db->get_where('empleados');
    	}    	

    	foreach($empleados->result() as $n=>$e){    		
    		//Relations
    		$empleados->row($n)->contrato = $this->db->get_where('tipos_contrato',['id'=>$e->tipos_contrato_id])->row();
            $empleados->row($n)->banco = $this->db->get_where('bancos',['id'=>$e->bancos_id])->row();
            $empleados->row($n)->tipo_cuenta = $this->db->get_where('tipos_cuenta',['id'=>$e->tipos_cuenta_id])->row();
    		$empleados->row($n)->tipo_documento = $this->db->get_where('tipos_documento',['id'=>$e->tipos_documento_id])->row();    		
    		$empleados->row($n)->termino_contrato = $this->termino_contrato($e->termino_contrato_id);
            $empleados->row($n)->companias = $this->companias($e->companias_id);     
            $empleados->row($n)->ajustes = $this->nomina_ajustes_empleados($e->id,$empleados->row($n)->companias->periodo[0],$empleados->row($n)->companias->periodo[1]);            
            $empleados->row($n)->companias->ajustes = $this->mergeAjustes($empleados->row($n)->companias->ajustes,$empleados->row($n)->ajustes);            
            //Link
            $empleados->row($n)->permisos_link = base_url('nomina/empleados/empleados_permisos/'.$e->id.'/');
            $empleados->row($n)->examen_ocupacional_link = base_url('empleados/formatos/empleados_ocupacional/'.$e->id.'/');
            $empleados->row($n)->apertura_cuenta = base_url('empleados/formatos/empleados_cartas_cuentas/'.$e->id.'/');
            $empleados->row($n)->contratos_url =  base_url('empleados/formatos/empleados_contratos/'.$e->id.'/');
            $empleados->row($n)->sanciones_url =  base_url('empleados/formatos/empleados_sanciones/'.$e->id.'/');
            $empleados->row($n)->sanciones_funciones_url =  base_url('empleados/formatos/empleados_recordatorios_funciones/'.$e->id.'/');
            $empleados->row($n)->fin_contrato_url =  base_url('empleados/formatos/empleados_fin_contrato/'.$e->id.'/');
            
    		//Foto
    		if(!empty($e->foto)){
    			$foto = $e->foto;
    		}else{
    			$foto = 'vacio.png';
    		}
    		$empleados->row($n)->foto_src = base_url('img/empleados/'.$foto);    		
    		$empleados->row($n)->foto_img = '<img src="'.base_url('img/empleados/'.$foto).'">';
    		
    	}
    	if(!empty($id)){
    		return $empleados->num_rows()>0?$empleados->row():null;
    	}
    	return $empleados;
    }

    /****************** FIN CALCULOS **********************/

    function caja_compensacion_familiar($id = ''){        
        if(!empty($id)){
            $companias = $this->db->get_where('caja_compensacion_familiar',['caja_compensacion_familiar.id'=>$id]);
        }else{
            $companias = $this->db->get_where('caja_compensacion_familiar');
        }  

        foreach($companias->result() as $n=>$e){            

        }

        if(!empty($id)){
            return $companias->num_rows()>0?$companias->row():null;
        }
        return $companias;

    }

    function tipos_contrato($id = ''){        
        if(!empty($id)){
            $tipo = $this->db->get_where('tipos_contrato',['tipos_contrato.id'=>$id]);
        }else{
            $tipo = $this->db->get_where('tipos_contrato');
        }  

        foreach($tipo->result() as $n=>$e){            

        }

        if(!empty($id)){
            return $tipo->num_rows()>0?$tipo->row():null;
        }
        return $tipo;

    }

    function nomina_ajustes($companias_id,$desde = '',$hasta = ''){
        if(!empty($desde) && !empty($hasta)){
            $nomina = $this->db->get_where('nomina_ajustes',['companias_id'=>$companias_id,'periodo_desde'=>$desde,'periodo_hasta'=>$hasta]);
            if($nomina->num_rows()==0){
                $this->db->insert('nomina_ajustes',['companias_id'=>$companias_id,'periodo_desde'=>$desde,'periodo_hasta'=>$hasta]);
                $nomina = $this->db->get_where('nomina_ajustes',['companias_id'=>$companias_id,'periodo_desde'=>$desde,'periodo_hasta'=>$hasta]);
            }
            return $nomina->row();
        }else{
            $nomina = $this->db->get_where('nomina_ajustes',['nomina_ajustes.id'=>$companias_id]);
            if($nomina->num_rows()>0){
                return $nomina->row();
            }
        }
        return null;    
    }

    function nomina_ajustes_empleados($empleados_id,$desde = '',$hasta = ''){
        if(!empty($desde) && !empty($hasta)){
            $desde = date("Y-m-d",$desde);
            $hasta = date("Y-m-d",$hasta);
            $nomina = $this->db->get_where('nomina_ajustes_empleados',['empleados_id'=>$empleados_id,'periodo_desde'=>$desde,'periodo_hasta'=>$hasta]);
            if($nomina->num_rows()==0){
                $this->db->insert('nomina_ajustes_empleados',['empleados_id'=>$empleados_id,'periodo_desde'=>$desde,'periodo_hasta'=>$hasta]);
                $nomina = $this->db->get_where('nomina_ajustes_empleados',['empleados_id'=>$empleados_id,'periodo_desde'=>$desde,'periodo_hasta'=>$hasta]);
            }
            return $nomina->row();
        }else{
            $nomina = $this->db->get_where('nomina_ajustes_empleados',['nomina_ajustes_empleados.id'=>$empleados_id]);
            if($nomina->num_rows()>0){
                return $nomina->row();
            }
        }
        return null;    
    }

    

    function getDiasEnPeriodo($periodo){
        $date1 = new DateTime(date("Y-m-d H:i:s",$periodo[0]));
        $date2 = new DateTime(date("Y-m-d H:i:s",$periodo[1]));
        $dias = $date1->diff($date2)->format('%a')+1;
        if(date("d",$periodo[1])==31){
            $dias-= 1;
        }
        if(date("d",$periodo[1])==28){
            $dias+= 2;
        }
        if(date("d",$periodo[1])==29){
            $dias+= 1;
        }        
        return $dias;
    }

    function getDiasTrabajadosEnPeriodo($e,$desde = NULL,$hasta = NULL){
        if(isset($e->fecha_contrato)){
            $fecha_contrato = strtotime($e->fecha_contrato);
            $fecha_termino = $e->companias->periodo[1];
            if(!empty($e->fecha_terminacion) && strtotime($e->fecha_terminacion)>$fecha_contrato){
                $fecha_termino = strtotime($e->fecha_terminacion);
            }
            if(!$desde){
                $desde = $e->companias->periodo[0]<$fecha_contrato?$fecha_contrato:$e->companias->periodo[0];
            }
            if(!$hasta){
                $hasta = $e->companias->periodo[1]>$fecha_termino?$fecha_termino:$e->companias->periodo[1];
            }
            
            $desde = new DateTime(date("Y-m-d H:i:s",$desde));
            $hasta = new DateTime(date("Y-m-d H:i:s",$hasta));            
            $dias = $desde->diff($hasta)->format('%a')+1;                    
            if(date("d",$e->companias->periodo[1])==31){
                $dias-= 1;
            }
            if(date("d",$e->companias->periodo[1])==28){
                $dias+= 2;
            }
            if(date("d",$e->companias->periodo[1])==29){
                $dias+= 1;
            }
            return $dias;
        }        
    }

    function nomina_aportes($empleado,$periodo,$between = FALSE){
        if(!$between){
            $aportes = $this->db->get_where('nomina_aportes',[
                'empleados_id'=>$empleado,
                'periodo_desde'=>date('Y-m-d',$periodo[0]),
                'periodo_hasta'=>date('Y-m-d',$periodo[1])
            ]);
        }else{
            $this->db->where("periodo_desde BETWEEN '".date("Y-m-d",$periodo[0])."' AND '".date("Y-m-d",$periodo[1])."' AND periodo_hasta BETWEEN '".date("Y-m-d",$periodo[0])."' AND '".date("Y-m-d",$periodo[1])."'",NULL,TRUE);
            $aportes = $this->db->get_where('nomina_aportes');
        }
        $ap = [];
        $ap['extras'] = [];
        foreach($aportes->result() as $n=>$a){
            if($a->extra==0){
                $ap[$a->nombre] = $a;
            }else{
                $ap['extras'][] = $a;
            }
        }
        return $ap;
    }

    function nomina_vac_inc_lic($empleado,$periodo,$between = FALSE){
        if(!$between){
            $aportes = $this->db->get_where('nomina_vac_inc_lic',[
                'empleados_id'=>$empleado,
                'periodo_desde'=>date('Y-m-d',$periodo[0]),
                'periodo_hasta'=>date('Y-m-d',$periodo[1])
            ]);
        }else{
            $this->db->where("periodo_desde BETWEEN '".date("Y-m-d",$periodo[0])."' AND '".date("Y-m-d",$periodo[1])."' AND periodo_hasta BETWEEN '".date("Y-m-d",$periodo[0])."' AND '".date("Y-m-d",$periodo[1])."'",NULL,TRUE);
            $aportes = $this->db->get_where('nomina_vac_inc_lic');
        }
        $ap = [];        
        foreach($aportes->result() as $n=>$a){
            $ap[$a->slug] = $a;
        }
        return $ap;
    }

    function ingresos_adicionales($empleado,$periodo,$between = FALSE){
        if(!$between){
            $aportes = $this->db->get_where('nomina_ingresos',[
                'empleados_id'=>$empleado,
                'periodo_desde'=>date('Y-m-d',$periodo[0]),
                'periodo_hasta'=>date('Y-m-d',$periodo[1])
            ]);
        }else{
            $this->db->where("periodo_desde BETWEEN '".date("Y-m-d",$periodo[0])."' AND '".date("Y-m-d",$periodo[1])."' AND periodo_hasta BETWEEN '".date("Y-m-d",$periodo[0])."' AND '".date("Y-m-d",$periodo[1])."'",NULL,TRUE);
            $aportes = $this->db->get_where('nomina_ingresos');
        }
        $ap = [];        
        foreach($aportes->result() as $n=>$a){
            $ap[$a->tipo][] = $a;
        }
        return $ap;
    }
    function deducciones($empleado,$periodo){
        $aportes = $this->db->get_where('nomina_deducciones',[
            'empleados_id'=>$empleado,
            'periodo_desde'=>date('Y-m-d',$periodo[0]),
            'periodo_hasta'=>date('Y-m-d',$periodo[1])
        ]);
        $ap = [];        
        foreach($aportes->result() as $n=>$a){
            $ap[$a->tipo][] = $a;
        }
        return $ap;
    }

    function crudColillas(){
        get_instance()->as["empleados_colillas"] = "historico_nomina_empleados";
        get_instance()->as["empleados"] = "historico_nomina_empleados";
        $crud = get_instance()->crud_function('','');            
        $crud->display_as('tipos_contrato_id','Contrato');
        $crud->display_as('horas_extras_recargos','H.extras/recargos');
        $crud->display_as('vacaciones_incapacidad_licencia','Vac.Lic.Inc');
        $crud->display_as('ingresos_adicionales','Ing. Adicionales');
        $crud->display_as('deducciones_prestamos','Deducciones');
        $crud->display_as('total_personas','Total Nomina');
        $crud->display_as('historico_nomina_id','Periodo');
        $crud->set_relation('historico_nomina_id','historico_nomina','{periodo_desde} - {periodo_hasta}');
        $crud->callback_column('total_pagar',function($val,$row){
            return '$'.number_format($val,0,',','.').' <a href="'.base_url('reportes/rep/verReportes/1/pdf/empleado/'.$row->id).'" target="_blank"><i class="fa fa-download"></i></a>';
        });            
        $crud->unset_add()->unset_delete()->unset_edit()->unset_print()->unset_export()->unset_read();
        $crud->columns('historico_nomina_id','tipos_contrato_id','salario','horas_extras_recargos','vacaciones_incapacidad_licencia','ingresos_adicionales','deducciones_prestamos','total_pagar');
        $crud->set_url('empleados/empleados_colillas/historico/');
        return $crud;
    }

}
?>
