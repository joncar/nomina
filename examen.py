def getPromedio(ingenierias,programas):
	promedioTotal = 0
	#Se suman todos los promedios
	for i in range(len(programas)):
		promedioTotal+= programas[i]
	#se divide entre la cantidad de programas
	promedioTotal/=len(programas)
	return promedioTotal
def total(ingenierias,programas):
	promedioTotal = getPromedio(ingenierias,programas) #Se busca el promedio de todos
	print("Promedio Total: "+str(promedioTotal)+"\n")
	for i in range(len(programas)):
		if programas[i]>=promedioTotal: #Si el promedio del elemento es superior al total se imprime
			print(str(programas[i])+"\n")
def mayorPromedio(ingenierias,programas):
	ingenieria = ""
	mayorPromedio = 0
	for i in range(len(programas)):
		if programas[i]>mayorPromedio:
			mayorPromedio = i
	print("\n"+ingenierias[mayorPromedio]+"\n")
def buscarPromedio(ingenierias,programas):
	opt = str(input("\n"+"Introduce el nombre del programa a buscar: "+"\n"))
	promedio = "Nombre no encontrado"
	for i in range(len(ingenierias)):
		if ingenierias[i] == opt:
			promedio = ingenierias[i]+": "+str(programas[i])
	print("\n"+promedio+"\n")
def ordenarPromedios(ingenierias,programas):	
	#Ordenamos mediante el metodo de la burbuja	
	newArray = [programas[i] for i in range(len(programas))]
	for i in range(1,len(newArray)):
		for j in range(0,len(newArray)-i):
			if(newArray[j+1] < newArray[j]):
				aux=newArray[j]
				newArray[j]=newArray[j+1]
				newArray[j+1]=aux
	
	#Imprimimos el array
	for i in range(len(newArray)):
		#Se busca el id del valor para mostrarlo en ingenierias
		for k in range(len(programas)):
			if(programas[k]==newArray[i]):
				print("\n"+ingenierias[k]+"    "+str(programas[k]))	

def main(ingenierias,programas):
	print("1 Listar programas con promedio superior al total de ingenierias")
	print("2 nombre de la ingenieria que obtuvo el mayor promedio")
	print("3 Buscar promedio de alguna ingenieria")
	print("4 para listar")
	print("5 para salir")
	opt = int(input("Introduce una opción: "))
	if opt == 1:
		total(ingenierias,programas)
	if opt == 2:
		mayorPromedio(ingenierias,programas)
	if opt == 3:
		buscarPromedio(ingenierias,programas)
	if opt == 4:
		ordenarPromedios(ingenierias,programas)
	if opt != 5:
		main(ingenierias,programas)

from random import randrange
ingenierias = ["programa "+str(i+1) for i in range(10)]
programas = [randrange(50) for i in range(10)]
print(programas)
promedio = 0
main(ingenierias,programas)