<?php
$this->set_css($this->default_theme_path . '/bootstrap/css/flexigrid.css');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js($this->default_theme_path . '/bootstrap/js/cookies.js');
$this->set_js($this->default_theme_path . '/bootstrap/js/flexigrid.js?v=1.10');
$this->set_js($this->default_theme_path . '/bootstrap/js/jquery.form.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . '/bootstrap/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . '/bootstrap/js/pagination.js');
$this->set_css($this->default_css_path . '/jquery_plugins/fancybox/jquery.fancybox.css?v=1.2');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js?v=1.1');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');
/** Jquery UI */
$this->load_js_jqueryui();
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
    var print_url = '<?= $print_url ?>';
    var export_url = '<?= $export_url ?>';
</script>

<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>><?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?>
</div>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">    
    









    <div class="kt-portlet kt-portlet--mobile">
        
        <div style="text-align: right">
            <div class="btn-group">
              <?php if(!$unset_add): ?>
                <a href="<?php echo $add_url?>" title='<?php echo $this->l('list_add'); ?> <?php echo $subject ?>' class="waves-effect waves-light btn btn-primary"><i class="fa fa-plus"></i> <?php echo $this->l('list_add'); ?> <?php echo $subject?></a>
              <?php endif ?>
              <button type="button" class="waves-effect waves-light btn btn-info searchActionClick"><i class="fa fa-search"></i></button>
              <button type="button" class="waves-effect waves-light btn btn-light ajax_refresh_and_loading"><i class="ace-icon fa fa-refresh bigger-110 grey"></i></button>
              <?php if(!$unset_export || !$unset_print || !$unset_delete): ?>
                  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-align-justify"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php if(!$unset_export): ?>
                        <a class="dropdown-item" href="javascript:;" onclick="openNewTab(this,'export')"><i class="fa fa-file-excel-o"></i> <?php echo $this->l('list_export'); ?> </a>
                    <?php endif ?>
                    <?php if(!$unset_print): ?>
                        <a class="dropdown-item" href="javascript:;" onclick="openNewTab(this,'print')"><i class="fa fa-print"></i> <?php echo $this->l('list_print'); ?></a>
                    <?php endif ?>
                    <?php if(!$unset_delete_all && !$unset_delete): ?>
                        <a class="dropdown-item delete_all" href="javascript:;"><i class="fa fa-trash"></i> Eliminar</a>
                    <?php endif ?>
                    <?php if(!$unset_import): ?>
                        <a title="Importar desde excel" href="javascript:;" onclick="$($(this).data('target')).modal('show')" data-target="#<?php echo $unique_hash; ?>import" class="dropdown-item"><i class="fa fa-upload"></i> Importar</a>
                    <?php endif; ?>
                    <?php if(isset($actions_list) && !empty($actions_list)): ?>
                        <?php foreach($actions_list as $a): ?>
                            <a title="<?= $a[1] ?>" href="<?= $a[2] ?>" class="dropdown-item">
                                <i class="<?= $a[0] ?>"></i> <?= $a[1] ?>
                            </a>
                        <?php endforeach ?>
                    <?php endif; ?>
                  </div>
              <?php endif ?>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" style="width:100%;">
            <!--begin: Datatable -->
                <div style="overflow: auto;max-height: 60vh;">
                    <table class="table table-striped- table-bordered table-hover table-checkable">
                        <thead>
                            <tr>
                                <?php if(!$unset_delete_all && !$unset_delete): ?>
                                    <th data-field="RecordID" class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check">
                                        <div class="ichack-input ">
                                            <label class="kt-checkbox--all">
                                              <input type="checkbox" class="minimal">
                                            </label>
                                        </div>
                                    </th>
                                <?php endif ?>
                                <?php foreach ($columns as $column): ?>
                                    <th class="field-sorting sorting" rel='<?php echo $column->field_name ?>'>
                                        <?php echo $column->display_as ?>                        
                                        <span id="th_<?= $column->field_name ?>"></span>
                                        <?php if (isset($order_by[0]) && $column->field_name == $order_by[0]) { ?>
                                            <?php if ($order_by[1] == 'asc'): ?>
                                                <i class="fa fa-arrow-up"></i>                        
                                            <?php else: ?>
                                                <i class="fa fa-arrow-down"></i>
                                            <?php endif; ?> 
                                        <?php } ?>
                                    </th>
                                <?php endforeach ?>

                                <?php if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)): ?>
                                    <th align="right" abbr="tools" axis="col1" class="" width='20%'>
                                        <div class="text-right">
                                            <?php echo $this->l('list_actions'); ?>
                                        </div>
                                    </th>
                                <?php endif ?>
                            </tr>

                            <tr class="d-none searchRow">  
                                <?php if(!$unset_delete_all && !$unset_delete): ?>
                                    <th data-field="RecordID" class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check">
                                        &nbsp;
                                    </th>   
                                <?php endif ?>              
                                <?php foreach($columns as $column):?>
                                <th>
                                    <?php if(!in_array($column->field_name,$unset_searchs)): ?>
                                        <input type="hidden" name="search_field[]" value="<?= $column->field_name ?>">
                                        <?php
                                           $value = '';
                                           if(!empty($_POST['search_text']) && !empty($_POST['search_field'])){
                                               foreach($_POST['search_field'] as $n=>$v){
                                                   if($v==$column->field_name && !empty($_POST['search_text'][$n])){
                                                       $value = $_POST['search_text'][$n];
                                                   }
                                               }
                                           }                                       
                                           if(empty($search_types[$column->field_name])){
                                                echo form_input('search_text[]',$value,'style="width:100%" class="form-control" placeholder="Filtrar por '.$column->display_as.'"');
                                           }else{
                                                echo $search_types[$column->field_name];
                                           }
                                        ?>
                                    <?php endif ?>
                                </th>
                                <?php endforeach?>                
                            </tr>
                        </thead>
                        <tbody class="ajax_list">                        
                            <?php echo $list_view?>
                        </tbody>
                    </table>
                </div>

                <div class="kt-datatable__pager kt-datatable--paging-loaded">                    
                    <div class="row" style="margin-left: 0; margin-right: 0;">
                        <div class="col-12 col-md-6">
                            <div class="kt-datatable__pager-info">
                                <div class="dropdown bootstrap-select kt-datatable__pager-size" style="width: 100px;">
                                    <select name="per_page" id='per_page' class="per_page selectpicker kt-datatable__pager-size" title="Por Página" data-width="100px" data-container="body" data-selected="10" tabindex="-98">                                
                                         <?php foreach ($paging_options as
                                            $option) { ?>
                                                <option value="<?php echo $option; ?>" <?php if ($option == $default_per_page) { ?>selected="selected"<?php } ?>><?php echo $option; ?>&nbsp;&nbsp;</option>
                                            <?php } ?>
                                    </select>                            
                                    <div class="dropdown-menu ">
                                        <div class="inner show" role="listbox" id="bs-select-3" tabindex="-1">
                                            <ul class="dropdown-menu inner show" role="presentation"></ul>
                                        </div>
                                    </div>
                                </div>
                                <span class="kt-datatable__pager-detail">
                                    <?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                                    <?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>" . ($total_results < $default_per_page ? $total_results : $default_per_page) . "</span>"; ?>
                                    <?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>" ?>
                                    <?php echo str_replace(array('{start}', '{end}', '{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results), $this->l('list_displaying')); ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <ul class="pagination kt-datatable__pager-nav" style="float: right"></ul>
                        </div>
                    </div>

                </div>
            </div>
            <!--end: Datatable -->
        </div>
    </div>















</div>
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<?php if(!empty($_GET)): ?>
    <?php foreach($_GET as $n=>$v): ?>
        <input type="hidden" name="<?= $n ?>" value="<?= $v ?>">
    <?php endforeach ?>
<?php endif ?>
<?php echo form_close() ?>
<?php if(!$unset_import): ?>
    <?php include $this->default_theme_path . '/bootstrap/views/import.php'; ?>
<?php endif ?>  