<?php if(!empty($list)): ?>
    <div class="box">
        <div class=" box-primary box-header no-border">
            <h4 class="box-title">Datos de <?= $subject ?></h4>
            <a href="<?= str_replace('ajax_list','',$ajax_list_url) ?>" style="color:white; position:absolute; right: 28px; top: 10px;"><i class="fa fa-edit"></i> Cambiar</a>
        </div>
        <div class="box-body">
            <form class="">   
                <div class="row">
                    <?php foreach ($columns as $column): ?>
                        <div class="form-group col-xs-6 col-sm-2">
                          <label for="id"><?= $column->display_as ?>: </label>
                          <input type="text" class="form-control" id="<?= $column->field_name ?>" value="<?= strip_tags($list[0]->{$column->field_name}) ?>" readonly="">
                        </div>
                    <?php endforeach ?>
                </div>  
            </form>             
        </div>
    </div>
<?php else: ?>
Sin datos para mostrar
<?php endif ?>
